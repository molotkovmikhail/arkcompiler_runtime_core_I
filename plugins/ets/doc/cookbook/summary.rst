Recipes Summarized
==================

This documents provides an informal summary of |TS| features that |LANG| either
does not support or supports with limitations. For the full list, with more
detailed code examples and workaround suggestions, please see
:doc:`this section </recipes>`.

Static typing is enforced
-------------------------

|LANG| was designed with following goals in mind:

- It should be easy for developers to read and understand |LANG| programs,
  because the code is read more often than written;
- |LANG| should execute fast with as little power consumption as possible,
  which is critical for mobile devices which |LANG| targets.

Static typing is one of the most important features of |LANG|, which helps
achieve both goals. Indeed, if your program is statically typed, i.e. all
types are known at the compile time, it's much easier to understand which data
structures are used in the code. At the same time, since all types are known
before the program actually runs, code correctness can be verified by the
compiler, which eliminates many runtime type checks and improves the
performance.

Therefore, it was decided to prohibit usage of the type ``any``.

Example
~~~~~~~

.. code-block:: typescript

    //
    // Not supported:
    //

    let res : any = some_api_function("hello", "world")

    // What is `res`? A numeric error code? Some string? An object?
    // How should we work with it?

    //
    // Supported:
    //

    class CallResult {
        public succeeded() : boolean { ... }
        public errorMessage() : string { ... }
    }

    let res : CallResult = some_api_function("hello", "world")
    if (!res.succeeded()) {
        console.log("Call failed: " + res.errorMessage())
    }

Rationale and impact
~~~~~~~~~~~~~~~~~~~~

Our research and experiments let us conclude that this feature is already
not welcome in the modern |TS|. According to our measurements, it is used
in approximately 1% of |TS| code bases. Moreover, code linters (e.g., ESLint)
already include a set of rules that prohibit usage of ``any``. Thus, although
prohibiting ``any`` requires code refactoring, we consider it a low-effort
change with the high impact result.

Changing object layout in runtime is prohibited
-----------------------------------------------

To achieve maximum performance benefits, |LANG| requires that layout of objects
does not change during program execution. In other words, it is prohibited to:

- add new properties or methods to the objects;
- delete existing properties or methods from the objects;
- assign values of arbitrary types to object properties.

Example
~~~~~~~

.. code-block:: typescript

    class Point {
        public x : number
        public y : number

        constructor(x : number, y : number) {
            this.x = x
            this.y = y
        }
    }

    // It is impossible to delete a property from the object.
    // It is guaranteed that all Point objects have the property x:
    let p1 = new Point(1.0, 1.0)
    delete p1.x               // Compile-time error

    // Class Point does not define any property named `z`, and
    // it is impossible to add it while the program runs.
    let p2 = new Point(2.0, 2.0)
    p2.z = "Label"             // Compile-time error

    // It is guaranteed that all Point objects have only properties x and y,
    // it is impossible to generate some arbitrary identifier and use it as a
    // new property:
    let p3 = new Point(3.0, 3.0)
    let unique = new Symbol()  // Compile-time error
    p3[unique] = p3.x          // Compile-time error

    // It is guaranteed that all Point objects have properties x and y of type
    // number, so assigning a value of any other type is impossible:
    let p4 = new Point(4.0, 4.0)
    p4.x = "Hello!"            // Compile-time error

    // Usage of Point objects which is compliant with the class definition:
    function distance(p1 : Point, p2 : Point) : number {
        return Math.sqrt((p2.x - p1.x) * (p2.x - p1.x) + (p2.y - p1.y) * (p2.y - p1.y))
    }
    let p5 = new Point(5.0, 5.0)
    let p6 = new Point(6.0, 6.0)
    console.log("Distance between p5 and p6: " + distance(p5, p6))

Rationale and impact
~~~~~~~~~~~~~~~~~~~~

Unpredictable changing of object layout contradicts both good readability and
good performance of the code. Indeed, having class definition at one place and
modifying actual object layout elsewhere is confusing and error-prone from the
developer's point of view. Additionally, it requires extra runtime support,
which brings undesired execution overhead. Moreover, it contradicts the idea of
static typing. If we want to make typing as explicit as possible, why would we
need to add or remove additional properties?

At the same time, according to our observations and experiments, this feature
is already not welcome in the modern |TS|. It is used in the marginal number of
cases in real-world projects and state of the art linters have rules to
prohibit its usage.

Thus, we conclude that prohibiting runtime changes to objects' layout results
in low-effort refactoring with the high positive impact on performance.

Semantics of operators is restricted
------------------------------------

To achieve better performance and encourage developers write clearer code,
|LANG| restricts semantics of some operators. A couple of examples are given
below, and the full list of restrictions is outlined :doc:`here </recipes>`.

Example
~~~~~~~

.. code-block:: typescript

    // Operator `+` is defined for numbers and strings, but not for other types:
    class C {
        ...
    }
    let c1 : C = new C()
    let c2 : C = new C()
    console.log(c1 + c2) // Compile-time error

    // Implicit casting from string to boolean is not supported:
    let s : string = "I'm a string"
    if (s) { // Compile-time error
        ...
    }
    if (s.length > 0) { // OK
        ...
    }
    if (s != "") { // OK
        ...
    }

Rationale and impact
~~~~~~~~~~~~~~~~~~~~

Overloading language operators with extra semantics complicates language
specification and makes developers remember about all possible corner cases
and their handling rules. Besides, in certain cases it incurs some runtime
overhead, which is undesired.

At the same time, according to our observations and experiments, this feature
is not popular in the modern |TS|. It is used in less than 1% of real-world
code bases. Besides, such cases are very easy to refactor.

Thus, although restricting semantics of operators requires changing the code,
this is a low-effort change which results in a clearer and more performant code.

Structured typing is not supported (yet)
----------------------------------------

Let's consider a situation when two unrelated classes ``T`` and ``U`` have the
same public API:

.. code-block:: typescript

    class T {
        public name : string

        public greet() : void {
            console.log("Hello, " + this.name)
        }
    }

    class U {
        public name : string

        public greet() : void {
            console.log("Greetings, " + this.name)
        }
    }

Can we assign a value of ``T`` to a variable of ``U``?

.. code-block:: typescript

    let u : U = new T() // Is this allowed?

Can we pass a value of ``T`` to a function that accepts a parameter of ``U``?

.. code-block:: typescript

    function greeter(u : U) {
        console.log("To " + u.name)
        u.greet()
    }

    let t : T = new T()
    greeter(t) // Is this allowed?

Or, in other words, which approach will we take:

- ``T`` and ``U`` are not related by inheritance or any common interface, but
  since they have the same public API, they are "somewhat equivalent", so the
  answer to both questions above is "yes";
- ``T`` and ``U`` are not related by inheritance or any common interface, and
  should always be considered as totally different types, so the
  answer to both questions above is "no".

Languages that take the first approach are said to support structural typing,
while languages that take the second approach do not support it. Currently,
|TS| supports structural typing, and |LANG| does not.

It is debatable if structural typing helps produce clear and
understandable code, arguments can be found both for and against it.
Moreover, structural typing does not harm program performance (at least in some
cases). Why is it not supported then?

The answer is that support for structural typing is a major feature which needs
lots of consideration and careful implementation in language specification,
compiler and runtime. Safe and efficient implementation requires that other
aspects (static typing, restrictions on changing object layout) are also
taken into account. That's why support for this feature is postponed. The team
will be ready to reconsider based on real-world scenarios and feedback. More
cases and suggested workarounds can be found in :doc:`Recipes </recipes>`.

|

|
