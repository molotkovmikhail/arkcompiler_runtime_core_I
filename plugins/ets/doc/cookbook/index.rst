===============
|LANG| Cookbook
===============

.. toctree::
   :numbered:
   :maxdepth: 2

   /intro
   /guide
   /summary
   /recipes
