Functions
=========

|

Function Declarations
----------------------

A function declaration introduces a named function, specifying its name,
parameters, return type and body.

Below is a simple function with two string parameters and string return type:

.. code-block:: typescript

    function add(x: string, y: string): string {
        let z : string = x + " " + y
        return z
    }


|

Functions with No Return Type
------------------------------

The return type of a function that does not need to return a value may be
explicitly specified as ``void`` or omitted altogether. No return statement
is needed for such functions.

Both notations below are valid:

.. code-block:: typescript

    function hi1() { console.log("hi") }
    function hi2(): void { console.log("hi") }

|

Function Scope
---------------

Variables and other entities defined in a function are local to the function
and cannot be accessed outside.

If the name of a variable defined in the function is equal to the name of an
entity in the outer scope, then the local definition shadows outer entity.

|

Function Calls
---------------

Calling a function actually performs the actions along the given parameters.

If the function is defined as follows:

.. code-block:: typescript

    function join(x :string, y :string) :string {
        let z: string = x + " " + y
        return z
    }

then it is called with two arguments of the type ``string``:

.. code-block:: typescript

    let x = join("hello", "world")
    console.log(x)

|

Function Types
---------------

Function types are commonly used as follows to define callbacks:

.. code-block:: typescript

    type trigFunc = (x: double) => double

    function do_action(f: trigFunc) {
         f(3.141592653589)  // call the function
    }

    do_action(sin) // pass the function as the argument

|

Arrow Functions or Lambdas
---------------------------

A function can be defined as an arrow function, for example:

.. code-block:: typescript


    let sum = (x: number, y: number): number => {
        return x + y
    }

Currently |LANG| does not allow omitting parameter types in arrow functions. 
A return type can be omitted, in such case a return type is implied to be
``void``.


|

Closure
--------

An arrow function is usually defined inside another function. As an inner
function, it can access all variables and functions defined in the outer
functions. 

To capture the context, an inner function forms a closure of its environment.
The closure allows to access such inner function outside its own environment.

.. code-block:: typescript

    function f(): () => number {
        let count = 0
        return (): number => { count++; return count }
    }

    let z = f()
    console.log(z()) // output: 1
    console.log(z()) // output: 2

In the sample above, the arrow function closure captures the ``count`` variable. 


|

Function Overloading
---------------------

The function name is said to be overloaded if two functions declared in the
same declaration space have the same name but a different parameter list.

When calling an overloaded function name, |LANG| chooses the most appropriate
function to call:

.. code-block:: typescript

    function log(x: string) {}
    function log(x: number) {}

    log("abc") // 1st function is called
    log(1) // 2nd function is called

Parameter lists are different if they contain different number of parameters,
or at least one pair of parameters have distinguishable types.

It is an error if two overloaded functions have the same name and identical
parameter lists.

|
|


