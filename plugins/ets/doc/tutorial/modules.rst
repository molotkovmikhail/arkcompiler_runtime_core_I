Modules
=======

|

Programs are organized as sets of compilation units or modules.

Each module creates its own scope, i.e., any declarations (variables, functions,
classes, etc.) declared in the module are not visible outside that module unless
they are explicitly exported. 

Conversely, a variable, function, class, interface, etc. exported from another
module must first be imported to a module.

|

Export
------

A top-level declaration can be exported by using the keyword ``export``.

A declared name that is not exported is considered private and can be used
only in the module where it is declared.

.. code-block:: typescript

    export class Point {}
    export let Origin = new Point(0, 0)
    export function Distance(p1: Point, p2: Point): number { ... }

|

Import
-------

Import declarations are used to import entities exported from other modules and
provide them bindings in the current module. An import declaration consists of
two parts:

* Import path that determines the module to import from;
* Import bindings that define the set of usable entities in the imported module and the form of use. i.e., qualified or unqualified use.

Import bindings may have several forms.

Let's assume a module has the path './utils' and export entities 'X' and 'Y'.

An import binding of the form ``* as A`` binds the name 'A', and all entities
exported from the
module defined by the import path can be accessed by using the qualified name
``A.name``:

.. code-block:: typescript

    import * as Utils from "./utils"
    Utils.X // denotes X from Utils
    Utils.Y // denotes Y from Utils

An import binding of the form ``{ ident1, ..., identN }`` binds an exported
entity with a specified name, which can be used as a simple name:

.. code-block:: typescript

    import { X, Y } from "./utils"
    X // denotes X from Utils
    Y // denotes Y from Utils

If a list of identifiers contains aliasing of the form ``ident as alias``, then
entity ``ident`` is bound under the name ``alias``:

.. code-block:: typescript

    import { X as Z, Y } from "./utils"
    Z // denotes X from Utils
    Y // denotes Y from Utils
    X // Compile-time error: 'X' is not visible

|

Top-Level Statements
---------------------

All statements in the module level must be enclosed in curly braces to form a
block as seen below:

.. code-block:: typescript

    { statements }


A module may contain any number of such blocks that merge logically into a
single block in the order of writing:

.. code-block:: typescript

    { statements_1 }
    /* declarations */
    { statements_2 }

Is equal to:

.. code-block:: typescript

    /* declarations */
    { statements_1; statements_2 }

If a module contains a main function (program entry point), statements in this
block are executed immediately before the body of this function.
Otherwise, they are executed before execution of any other statement of the
module.

|

Program Entry Point
--------------------

An entry point of a program (application) is the top-level main function. The
main function should have either an empty parameter list or a single parameter
of string[] type.

.. code-block:: typescript

    function main() {
        console.log("this is the entry")
    }

|
|

