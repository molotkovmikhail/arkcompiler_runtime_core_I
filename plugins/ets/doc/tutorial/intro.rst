
============
Introduction
============

|

Welcome to the tutorial for |LANG|, a |TS|-based programming language
designed specifically to build high-performance mobile applications!

|LANG| is optimized to provide better performance and efficiency, while still
maintaining the familiar syntax of |TS|.

As mobile devices continue to become more prevalent in our daily lives,
there is a growing need for programming languages optimized for the
mobile environment. Many current programming languages were not designed with
mobile devices in mind, resulting in slow and inefficient applications that
drain battery life. |LANG| has been specifically designed to address such concerns
by prioritizing higher execution efficiency.

|LANG| is based on the popular programming language |TS| that extends
|JS| by adding type definitions. |TS| is well-loved by many developers as it 
provides a more structured approach to coding in |JS|. |LANG| aims to
keep the look and feel of |TS| to enable a seamless transition for the existing
|TS| developers, and to let mobile developers learn |LANG| quickly.

One of the key features of |LANG| is its focus on low runtime overhead.
|LANG| imposes stricter limitations on the |TS|'s dynamically typed features,
reducing runtime overhead and allowing faster execution. By eliminating
the dynamically typed features from the language, |LANG| code can be compiled 
ahead-of-time more efficiently, resulting in faster application startup and
lower power consumption.

Interoperability with |JS| was a critical consideration in the |LANG| language
design. Many mobile app developers already have |TS| and |JS| code and libraries
they would want to reuse. |LANG| has been designed for seamless |JS| 
interoperability, making it easy for the developers to integrate the |JS| code 
into their applications and vice versa. This will allow the developers to
use their existing codebases and libraries to leverage the power of our 
new language.

This tutorial will guide the developers through the core features, syntax, 
and best practices of |LANG|. After reading this tutorial through the end,
the developers will be able to build performant and efficient mobile 
applications in |LANG|.

|
|


