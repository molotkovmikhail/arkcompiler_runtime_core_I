Generic Types and Functions
===========================

|

Generic types and functions allow creating the code capable to work over a
variety of types rather than a single one.

|

Generic Classes and Interfaces
------------------------------

A class and an interface can be defined as generics, adding parameters to
the type definition, like the type parameter ``Element`` in the following example:

.. code-block:: typescript

    class Stack<Element> {
        public pop(): Element { ... }
        public push(e: Element) { ... }
    }

To use type Stack, the type argument must be specified for each type parameter:

.. code-block:: typescript

    let s = new Stack<string>
    s.push("hello")

Compiler ensure type safety while working with generic types and functions.

|

Generic Constraints
-------------------

Type parameters of generic types can be bounded. For example, the Key type
parameter in the ``HashMap<Key, Value>`` container must have a hash method, i.e.,
it should be hashable.

.. code-block:: typescript

    interface Hashable {
        hash(): long
    }
    class HasMap<Key extends Hashable, Value> {
        public set(k: Key, v: Value) { 
            let h = k.hash()
            ... other code ...
        }
    }

In the above example, the ``Key`` type extends ``Hashable``, and all methods
of ``Hashable`` interface can be called for keys.

|

Generic Functions
-----------------

Use a generic function to create a more universal code. Consider a function
that returns the last element of the array:

.. code-block:: typescript

    function last(x: number[]): number {
        return x[x.length -1]
    }
    console.log(last([1, 2, 3])) // output: 3

If the same function needs to be defined for any array, define it as generic
with a type parameter:

.. code-block:: typescript

    function last<T>(x: T[]): T {
        return x[x.length -1]
    }

Now, the function can be used with any array.

In a function call, type argument may be set explicitly of implicitly:

.. code-block:: typescript

    // Explicit type argument
    console.log(last<string>(["aa", "bb"]))
    console.log(last<number>([1, 2, 3]))
    // Implicit type argument
    console.log(last([1, 2, 3]))


|
|



