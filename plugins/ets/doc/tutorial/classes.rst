Classes
=======

|

A class declaration introduces a new type and defines its fields, methods and
constructors.

In the following example, a Person class is defined, which has fields 'name',
'surname', a constructor function and a method ``fullName``:

.. code-block:: typescript

    class Person {
        name: string = ""
        surname: string = ""
        constructor (n: string, sn: string) {
            this.name = n
            this.surname = sn
        }
        fullName(): string {
            return this.name + " " + this.surname
        }
    }

After the class is defined, its instances can be created using the keyword
``new``:

.. code-block:: typescript

    let p = new Person("John", "Smith")
    console.log(p.fullName())

|

Constructors
-------------

A class declaration may contain several constructors that are used to initialize
object state. 

Constructor is defined as follows:

.. code-block:: typescript

    constructor ([parameters]) {...}

If there is more than one constructor, all their respective parameter lists must
be different, for example:

.. code-block:: typescript

        constructor (n: string, sn: string) {
            this.name = n
            this.surname = sn
        }
        constructor (n: string) {
            this.name = n
            this.surname = ""
        }

If no constructor is defined, then a default constructor with an empty parameter
list is created automatically, for example:

.. code-block:: typescript

    class Point {
        x: double
        y: double
    }
    let p = new Point()

In this case the default constructor fills the instance fields with default
values for the field types.

|

Fields
-----------

A field is a variable of some type that is declared directly in a class. 
A classes may have instance fields, static fields or both.


|

Instance Fields
~~~~~~~~~~~~~~~

Instance fields exist on every instance of a class. Each instance has its own
set of instance fields.

.. code-block:: typescript

    class Person {
        name: string = ""
        age: int = 0
        constructor(n: string, a: int) {
            this.name = n
            this.age = a
        }
    }

    let p1 = new Person("Alice", 25)
    let p2 = new Person("Bob", 28)


To access an instance field, use an instance of the class:

.. code-block:: typescript

    p1.name 
    this.name


|

Static Fields
~~~~~~~~~~~~~

Use the keyword ``static`` to declare a field as static. Static fields belong
to the class itself, and all instances of the class share one static field.

To access a static field, use the class name:

.. code-block:: typescript

    class Person {
        static numberOfPersons = 0
        constructor() {
           ...
           Person.numberOfPersons++
           ...
        }
    }

    console.log(Person.numberOfPersons)


|

Getters and Setters 
~~~~~~~~~~~~~~~~~~~

Setters and getters can be used to provide controlled access to object
properties.

In the following example, a setter is used to forbid setting invalid values of
the 'age' property:

.. code-block:: typescript

    class Person {
        name: string = ""
        private _age: int = 0
        get age(): int { return _age }
        set age(x: int) {
            if (x < 0) { throw Error("Invalid age argument") }
			this._age = x
        }
    }

    let p = new Person ()
    console.log (p.age) // 0 will be printed out
    p.age = -666 // Error will be thrown as an attempt to set incorrect age

A class may define a getter, a setter or both.


|

Methods
--------

A method is a function that belongs to a class.
A class can define instance methods, static methods or both. 
A static method belongs to the class itself, and can have access to static
fields only.
While instance method has access to both static (class) fields and instance fields 
including private ones of its class.

|

Instance Methods
~~~~~~~~~~~~~~~~

Example below illustrates how instanced methods work. 
The ``calculateArea`` method calculates the area of a rectangle by multiplying
the height by the width:

.. code-block:: typescript

    class Rectangle {
        private height: number;
        private width: number;
        constructor(height: number, width: number) { ... }
        calculateArea(): number {
            return this.height * this.width;
        }
    }

To use an instance method, call it on an instance of the class:

.. code-block:: typescript

    let r = new Rectangle(10, 10);
    console.log(square.calculateArea()); // output: 100

|

Static Methods
~~~~~~~~~~~~~~

Use the keyword ``static`` to declare a method as static. Static methods belong
to the class itself and have access to static fields only.
A static method defines a common behaviour of the class as a whole.
All instances have access to static methods.

To call a static method, use the class name:

.. code-block:: typescript

    class Cl {
        static staticMethod(): string {
            return "this is a static method.";
        }
    }
    Cl.staticMethod();

|

Inheritance
~~~~~~~~~~~

A class can extend another class (called base class) and implement several
interfaces using the following syntax:

.. code-block:: typescript

    class [extends BaseClassName] [implements listOfInterfaces] { ... }

The extended class inherits fields and methods from the base class, but not
constructors, and can add its own fields and methods as well as override methods
defined by the base class.

The base class is also called 'parent class' or 'superclass'. The extended class
also called 'derived class' or 'subclass'.

Example:

.. code-block:: typescript

    class Person {
        name: string = ""
        get age(): int
    }
    class Employee extends Person {
        salary: number
        calculateTaxes(): number {}
    }

A class containing the ``implements`` clause must implement all methods defined
in all listed interfaces, except the methods defined with default implementation.

.. code-block:: typescript

    interface DateInterface {
        now(): string;
    }
    class Date implements DateInterface {
        now(): string {
            // implementation is here
        }
    }

|

Override Methods
~~~~~~~~~~~~~~~~

A subclass may override implementation of a method defined in its superclass.
An overriden method must be marked with the keyword ``override``. An overriden
method must have the same types of parameters and same or derived return type as the
original method.

.. code-block:: typescript

    class Rectangle {
        ...
        area(): number {
            // implementation
        }
    }
    class Square extends Rectangle {
        private side: number = 0
        override area(): number {
            return this.side*this.side
        }
    }

|

Access to Super
~~~~~~~~~~~~~~~

The keyword ``super`` can be used to access instance fields, instance methods
and constructors from the super class.

It is often used to extend basic functionality of subclass with the required 
behaviour taken fronm the super class:

.. code-block:: typescript

    class Rectangle {
        protected height: number = 0
        protected width: number = 0

        constructor (h: number, w: number) {
                     this.height = h
                     this.width = w
        }

        draw() {
                         /* draw bounds */

        }
    }
    class FilledRectangle extends Rectangle {
                color = ""
                constructor (h: number, w: number, c: string) {
                     super(h, w) // call of super constructor
                     this.color = c
                }

        override draw() {
             super.draw() // call of super methods
             // super.height - can be used here
                         /* fill rectangle */
        }
    }

|

Constructors in Derived Class
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

The first statement of a constructor body can use the keyword ``super`` to
explicitly call a constructor of the direct superclass.

.. code-block:: typescript

    class Rectangle {
        constructor(width: number, height: number) { ... }
    }
    class Square extends Rectangle {
        constructor(side: number) { 
            super(side, side)
        }
    }

If a constructor body does not begin with such an explicit call of a superclass
constructor, then the constructor body implicitly begins with a superclass
constructor call ``super()``.

Visibility Modifiers
----------------------

Both methods and properties of a class can have visibility modifiers.

There are several visibility modifiers: ``private``, ``protected``, 
``public``, and ``internal``. The default visibility is ``public``.
``internal`` allows to limit visibilty within the current package.

Public Visibility
~~~~~~~~~~~~~~~~~

The ``public`` members (fields, methods, constructors) of a class are visible
in any part of the program, where their class is visible.

Private Visibility
~~~~~~~~~~~~~~~~~~

A ``private`` member cannot be accessed outside the class in which it is
declared, for example:

.. code-block:: typescript

    class C {
        public x: string = ""
        private y: string = ""
        set_y (new_y: string) {
            y = new_y // ok, as y is accessible within the class itself
        }
    }
    let c = C()
    c.x = "a" // ok, the field is public
    c.y = "b" // compile-time error, 'y' is not visible


Protected Visibility
~~~~~~~~~~~~~~~~~~~~

The ``protected`` modifier acts much like the ``private`` modifier, but
``protected`` members are also accessible in derived classes,
for example:

.. code-block:: typescript

    class Base {
        protected x: string = ""
        private y: string = ""
    }
    class Derived extends Base {
        foo() {
            this.x = "a" // ok, access to protected member
            this.y = "b" //  compile-time error, 'y' is not visible, as it is private
        }
    }

|

|
