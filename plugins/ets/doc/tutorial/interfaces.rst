Interfaces
==========


|

An interface declaration introduces a new type. Interfaces are a common way
of defining contracts between various part of codes.

Interfaces are used to write polymorphic code, which can be applied to any
class instances that implement a particular interface.

An interface usually contains properties and method headers. Some interface
methods may have default implementation.

Examples:

.. code-block:: typescript

    interface Style {
        color: string // property
    }
    interface Area {
        calculateArea(): number // method header
		someMethod() { ... some actions ... } 
        // method with default implementation
    }

Examples of a class implementing interface:

.. code-block:: typescript

    class Rectangle implements Area {
        ...
        calculateArea(): number {
            someMethod() // calls another method and returns result 
            return this.width * this.height
        }
    }

|

Interface Properties
--------------------

An interface property looks like a field, getter, setter or both getter and
setter.

A property in the form of a field is just a shortcut notation of a getter/setter
pair, and the following notations are equal:

.. code-block:: typescript

    interface Style {
        color: string
    }
    interface Style {
        get color(): string
        set color(x: string)
    }

A class that implements an interface may also use a short or a long notation:

.. code-block:: typescript

    class StyledRectangle implements Style {
        color: string
    }

The short notation implicitly defines a private field and getter and setter:

.. code-block:: typescript

    class StyledRectangle implements Style {
        _color: string
        get color(): string { return this._color }
        set color(x: string) { this._color = x }
    }

|

Interface Methods
-----------------

An interface method is either a method header (no method body is present) or
a method with default implementation.

.. code-block:: typescript

    interface I {
        foo(): number // method header
        goo(): number { return 0 } // method with default implementation
    }

A method with default implementation does not require implementation in a class
that implements the interface but it can be provided if necessary.

|

Interface Inheritance
---------------------

An interface may extend other interfaces like in the example below:

.. code-block:: typescript

    interface Style {
        color: string;
    }
    interface ExtendedStyle extends Style {
        width: number
    }

An extended interface contains all properties and methods of the interface it
extends and may also add its own properties and methods.

|

Interface Visibility Modifiers
------------------------------

Properties and methods are ``public``.

Only methods with default implementation may be defined as ``private``.

|
|
