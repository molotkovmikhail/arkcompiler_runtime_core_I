The Basics
==========

|

Declarations
------------

Declarations in |LANG| introduce variables, constants, functions and types.

Variable Declaration
~~~~~~~~~~~~~~~~~~~~
A variable declaration introduces a reassignable variable. 

.. code-block:: typescript

    let hi: string = "hello"
    hi = "hello, world"


Constant Declaration
~~~~~~~~~~~~~~~~~~~~

A constant declaration introduces a read-only constant that can be assigned a
value only once.

.. code-block:: typescript

    const hello: string = "hello"


Assigning a new value to a constant is an error.

Automatic Type Inference
~~~~~~~~~~~~~~~~~~~~~~~~

As |LANG| is a statically typed language, the types of all entities, like
variables and constants have to be known at compile time.

However, developers do not need to explicitly specify the type of a declared
entity if a variable or a constant declaration contains an initial value.
All cases that allow the type to be inferred automatically are specified in
the |LANG| Specification.

Both variable declarations are valid, and both variables are of the ``string``
type:

.. code-block:: typescript

    let hi1: string = "hello"
    let hi2 = "hello, world"

|

Types
-----

``Class``, ``interface``, ``function``, ``nullable`` and ``enum`` types are
described in the corresponding sections.

Number and Other Numeric Types
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

|LANG| has both ``number`` type and a set of specific numeric types:

* signed integer types: ``Byte``, ``Short``, ``Int``, ``Long``
* unsigned integer types: ``UByte``, ``UShort``, ``UInt``, ``ULong``
* floating-point types: ``Float``, ``Double``
* a ``Char`` type, that is equal to ``UShort``

The ``number`` type equals to ``Double``, any integer and floating-point
value can be assigned to a variable of this type.

There is also a set of so-called *primitive types* (predefined value types):
``byte``, ``short``, ``int``, ``long``, ``ubyte``, ``ushort``, ``uint``,
``ulong``, ``float``, ``double``, ``char``, that can be used for better
application performance and for interoperability with low-level languages. 

The use of primitive types is restricted: such types cannot be used as
generic type arguments.

Numeric literals include integer literals and floating-point literals
with the decimal base.

Integer literals include the following:

* decimal integers that consist of a sequence of digits. For example: ``0``, ``117``, ``-345``;
* hexadecimal integers that start with 0x (or 0X) and can contain digits (0-9) and letters a~f or A~F. For example: ``0x1123``, ``0x00111``, ``-0xF1A7``;
* octal integers that start with 0o (or 0O) and can only contain digits (0-7). For example: ``0o777``;
* binary integers that start with 0b (or 0B) and can only contain the digits 0 and 1. For example: ``0b11``, ``0b0011``, ``-0b11``.

A floating-point literal includes the following:

* decimal integer, optionally signed (i.e., prefixed with "+" or "-");
* decimal point (".");
* fractional part (represented by a string of decimal digits);
* exponent part that starts with "e" or "E", followed by an optionally signed (i.e., prefixed with "+" or "-") integer.

For example:

.. code-block:: typescript

    3.14
    3.141592
    .5
    1e10

Using the ``number`` type:

.. code-block:: typescript

    function f(n: number) : number {
        if (n <= 1) return 1
        return n * f(n - 1)
    }

Using the ``long`` type for better performance:

.. code-block:: typescript

    function f(n: long) : long {
        if (n <= 1) return 1
        return n * f(n - 1)
    }

``Boolean``
~~~~~~~~~~~

The ``boolean`` type represents logical values that are either ``true``
or ``false``.

Usually variables of this type are used in conditional statements:

.. code-block:: typescript

    let isDone: boolean = false
    ...

    if (isDone) {
        console.log ("Done!")
    }


``String``
~~~~~~~~~~

A ``string`` is a sequence of characters; some characters can be set using
escape-sequences.

A ``string`` literal consists of zero or more characters enclosed in double
quotation marks ("). 

.. code-block:: typescript

    "Hello, world!\n"


``Void`` Type
~~~~~~~~~~~~~~

The ``void`` type is used to specify that a function does not return a value.
This type has the only one value,
which is also ``void``. As ``void`` is reference type, it can be used as type
argument for generic types.

``Object`` Type
~~~~~~~~~~~~~~~

An ``Object`` class type is a base type for all reference types. Any value, except
values of primitive types, can be directly assigned to variables of type ``Object``.
For details see `Standard Library Reference`.

``Array`` Type
~~~~~~~~~~~~~~~~

An ``array`` is an object comprised of elements of data types assignable to 
the element type specified in the array declaration.
A value of an ``array`` is set using *array composite literal*, that is a list
of zero or more expressions
enclosed in square brackets ([]). Each expression represents an element of the
``array``. The length of the ``array`` is set by the number of expressions.

The following example creates the ``array`` with three elements:

.. code-block:: typescript

    let names: string[] = ["Alice", "Bob", "Carol"]


``Enum`` Type
~~~~~~~~~~~~~~~

An ``enum`` type is a value type with a defined set of named values called
enum constants.
To be used, an ``enum`` constant must be prefixed with an enum ``type`` name.

.. code-block:: typescript

    enum Color { Red, Green, Blue }
    let c: Color = Color.Red


``Alias`` Type 
~~~~~~~~~~~~~~~

``Alias`` types provide names for anonymous types (array, function or nullable
types) or alternative names for existing types. 

.. code-block:: typescript

    type Matrix = double[][]
    type Handler = (s: string, no: int): string
    type Predicate <T> = (x: T): Boolean

|

Operators
-------------------

Assignment Operators
~~~~~~~~~~~~~~~~~~~~

Simple assignment operator '=' is used as in "x = y".

Compound assignment operators combine an assignment with an operator, where
``x op = y`` equals to ``x = x op y``.

Compound assignment operators are as follows: ``+=``, ``-=``, ``*=``, ``/=``,
``%=``, ``<<=``, ``>>=``, ``>>>=``, ``&=``, ``|=``, ``^=``.

Comparison Operators
~~~~~~~~~~~~~~~~~~~~

.. table::

    +--------------+-----------------------------------------------------------------------------+
    | Operator     | Description                                                                 |
    +==============+=============================================================================+
    | ``==``       |   returns true if both operands are equal                                   |
    +--------------+-----------------------------------------------------------------------------+
    | ``!=``       |   returns true if both operands are not equal                               |
    +--------------+-----------------------------------------------------------------------------+
    | ``>``        |   returns true if the left operand is greater than the right                |
    +--------------+-----------------------------------------------------------------------------+
    | ``>=``       |   returns true if the left operand is greater than or equal to the right    |
    +--------------+-----------------------------------------------------------------------------+
    | ``<``        |   returns true if the left operand is less then the right                   |
    +--------------+-----------------------------------------------------------------------------+
    | ``<=``       |   returns true if the left operand is less than or equal to the right       |
    +--------------+-----------------------------------------------------------------------------+


Arithmetic Operators
~~~~~~~~~~~~~~~~~~~~

Unary operators are ``-``, ``+``, ``--``, ``++``.

Binary operators are as follows:

.. table::

    +--------------+-------------------------------------+
    | Operator     | Description                         |
    +==============+=====================================+
    | ``+``        |   addition                          |
    +--------------+-------------------------------------+
    | ``-``        |   subtraction                       |
    +--------------+-------------------------------------+
    | ``*``        |   multiplication                    |
    +--------------+-------------------------------------+
    | ``/``        |   division                          |
    +--------------+-------------------------------------+
    | ``%``        |   remainder after division          |
    +--------------+-------------------------------------+


Bitwise Operators
~~~~~~~~~~~~~~~~~

.. csv-table::
   :header: "Operator", "Description"
   :widths: 5, 30

   "``a & b``", "Bitwise AND: sets each bit to 1 if the corresponding bits of both operands are 1, otherwise to 0."
   "``a | b``", "Bitwise OR: sets each bit to 1 if at least one of the corresponding bits of both operands is 1, otherwise to 0."
   "``a ^ b``", "Bitwise XOR: sets each bit to 1 if the corresponding bits of both operands are different, otherwise to 0."
   "``~ a``", "Bitwise NOT: inverts the bits of the operand."
   "``a << b``", "Shift left: shifts the binary representation of *a* to the left by *b* bits."
   "``a >> b``", "Arithmetic right shift: shifts the binary representation of *a* to the right by *b* bits with sign-extension."
   "``a >>> b``", "Arithmetic right shift: shifts the binary representation of *a* to the right by *b* bits with zero-extension."

Logical Operators
~~~~~~~~~~~~~~~~~

.. table::

    +--------------+---------------------+
    | Operator     | Description         |
    +==============+=====================+
    | ``a && b``   |   logical AND       |
    +--------------+---------------------+
    | ``a || b``   |   logical OR        |
    +--------------+---------------------+
    | ``! a``      |   logical NOT       |
    +--------------+---------------------+

|

Control Flow
-------------

``If`` Statements
~~~~~~~~~~~~~~~~~~

Use ``if`` statement to execute a sequence of statements when a logical
condition is true, or to choose one of two statement sequences to execute.

The ``if`` statement looks as follows:

.. code-block:: typescript

    if (condition1) {
        statements1
    } else if (condition2) {
        statements2
    } else {
        else_statements
    }

All conditional expressions must be of the type ``boolean``.


``Switch`` Statements
~~~~~~~~~~~~~~~~~~~~~~~

Use ``switch`` statement to execute a sequence of statements that match the
value of the switch expression.

The ``switch`` statement looks as follows:

.. code-block:: typescript

    switch (expression) {
    case label1:
        statements1
        [break;]
    case label2:
    case label3:
        statements23
        [break;]
    default:
        default_statements
    }

The ``switch`` expression type must be of ``integer``, ``enum`` or ``string``
type.

Each label must be either a constant expression or the name of an enum constant.

If the value of a ``switch`` expression equals the value of some label, then
the corresponding statements are executed. 

If there is no match, and the ``switch`` has the default clause, then the
default statements are executed.

An optional ``break`` statement allows to break out of the ``switch`` and
continue executing the statement that follows the ``switch``.

If there is no break, then the next statements in the ``switch`` is executed.


Conditional Expressions
~~~~~~~~~~~~~~~~~~~~~~~

The conditional expression ``?`` : uses the ``boolean`` value of the first
expression to decide which of two other expressions to evaluate.

The ``switch`` statement looks as follows:

.. code-block:: typescript

    condition ? expression1 : expression2

The condition must be a logical expression. If that logical expression is
``true``, then the first expression is used as the result of the ternary
expression; otherwise, the second expression is used.

Example:

.. code-block:: typescript

    let message = isValid ? 'Valid' : 'Failed'


``For`` Statements
~~~~~~~~~~~~~~~~~~~

A ``for`` statement is executed repeatedly until the specified loop condition
is ``false``. A for statement looks as follows:

.. code-block:: typescript

    for ([init]; [condition]; [update]) {
        statements
    }

When a ``for`` statement is executed, the following process takes place:

#. An ``init`` expression is executed, if any. This expression usually
initializes one or more loop counters.
#. The condition is evaluated. If the value of condition is ``true``, or if
the conditional expression is omitted, then the statements in the for body are
to be executed. If the value of condition is ``false``, the for loop terminates.
#. The statements of the ``for`` body are executed. 
#. If there is an ``update`` expression, the ``update`` expression is executed.
#. Go back to step 2.

Example:

.. code-block:: typescript

    for (let i = 0; i < 10; i+=2) {
        sum += i
    }

``For-of`` Statements
~~~~~~~~~~~~~~~~~~~~~~

Use ``for-of`` statements to iterate over an array or string. A ``for-of``
statement looks as follows:

.. code-block:: typescript

    for (forVar of expression) { 
        statements 
    }

Example:

.. code-block:: typescript

    for (let ch of "a string object") { /*process ch*/ }


``While`` Statements
~~~~~~~~~~~~~~~~~~~~~~

A ``while`` statement has its block executed as long as the specified condition
evaluates to ``true``. It looks as follows:

.. code-block:: typescript

    while (condition W) {
        statements
    }
    
The condition must be a logical expression.

Example:

.. code-block:: typescript

    let n = 0;
    let x = 0;
    while ( n < 3 ) {
        n++;
        x += n;
    }

``Do-while`` Statements
~~~~~~~~~~~~~~~~~~~~~~~~~

The ``do-while`` statement is executed repetitively until the specified
condition evaluates to false. It looks as follows:

.. code-block:: typescript

    do {
        statements
    } while (condition)

The condition must be a logical expression.

Example:

.. code-block:: typescript

    let i = 0;
    do {
        i += 1
    } while (i < 10)

``Break`` Statements
~~~~~~~~~~~~~~~~~~~~~

Use a ``break`` statement to terminate a ``loop`` statement or ``switch``.

Example:

.. code-block:: typescript

    let x = 0;
    while (true) {
        x++;
        if (x > 5) {
            break;
        }
    }

A ``break`` statement with a label identifier transfers control out of the
enclosing statement, which has the same label identifier. 

Example:

.. code-block:: typescript

    label: while (true) {

        switch (x) {

        case 1: 
            statements;
            break label // breaks the while
        }
    }

``Continue`` Statements
~~~~~~~~~~~~~~~~~~~~~~~~~~

A ``continue`` statement stops the execution of the current loop iteration and
passes control to the next iteration.

Example:

.. code-block:: typescript

    for (x = 0 ; x < 100 ; x ++) {
        if (x % 2 == 0) {
            continue;
        }
        sum += x
    }


``Throw`` and ``Try`` Statements
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

A ``throw`` statement is used to throw an exception or error:

.. code-block:: typescript

    throw new Error("this error")

A ``try`` statement is used to catch and handle an exception or error:

.. code-block:: typescript

    try {
        // try block
    } catch (e) {
        // handle the situation
    }

The example below shows the ``throw`` and ``try`` statements  used to handle
the zero division case:

.. code-block:: typescript

    class ZeroDivisor extends Error {}

    function divide (a: number, b: number): number{
        if (b == 0) throw new ZeroDivisor()
        return a / b
    }

    function process (a: number, b: number) {
        try {
            let res = divide(a, b)
            console.log (res)
        } catch (x) { 
            console.log ("some error")
        }
    }

|
|

