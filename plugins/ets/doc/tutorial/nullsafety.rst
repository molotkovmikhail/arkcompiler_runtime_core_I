Null Safety
===========

|

All types in |LANG| by default are non-nullable, so the value of a type cannot
be null.
It is similar to |TS| behaviour in strict null checking mode
(``strictNullChecks``), but the rules are stricter, and the ``undefined`` type
is not supported.

In the example below, all lines cause a compile-time error:

.. code-block:: typescript

    let x: number = null
    let y: string = null
    let z: number[] = null

A variable that can have a null value is defined with ``T | null`` type.

**Note**: Currently |LANG| supports only nullable types and does not support
general union type.

.. code-block:: typescript

    let x: number | null = null
    x = 1 // ok
    x = null // ok
    if x != null { /*do smth*/ }

|

Non-Null Assertion Operator
-----------------------------

A postfix operator ``!`` can be used to assert that its operand is non-null.

If applied to a null value, the operator throws an error. Otherwise, the type
of the value is changed from ``T | null`` to ``T``:

.. code-block:: typescript

    let x: number | null = 1
    let y: number
    y = x + 1 // compile time error: cannot add to a nullable value
    y = x! + 1 // ok

|

Null-Coalescing Operator
--------------------------

The null-coalescing binary operator ``??`` checks whether the evaluation of the
left-hand-side expression is equal to null. 
If it is, then the result of the expression is the right-hand-side expression;
otherwise, it is the left-hand-side expression.

In other words, ``a ?? b`` equals the ternary operator ``a != null ? a : b``.

In the following example, the method ``getNick`` returns a nickname if it is
set; otherwise, the string is empty:

.. code-block:: typescript

    class Person {
        ...
        nick: string | null = null
        getNick(): string {
            return this.nick ?? "" 
        }
    }

|

Optional Chaining
---------------------------------

Optional chaining operator ``?.`` allows writing code where the evaluation
stops of an expression that is partially evaluated to ``null``.

.. code-block:: typescript

    class Person {
        ...
        spouse: Person | null = null
        nick: string | null = null
        getSpouseNick(): string | null {
            return this.spouse?.nick
        }
    }

**Note**: that the return type of ``getSpouseNick`` must be ``string | null``,
as the method may return null.

An optional chain may be of any length and contain any number of ``?.``
operators.

In the following sample, the output is a person's spouse nickname if the person
has a spouse, and the spouse has a nickname.

Otherwise, the output is ``null``:

.. code-block:: typescript

    let p: Person = ...
    console.log(p?.spouse?.nick) 

|
|



