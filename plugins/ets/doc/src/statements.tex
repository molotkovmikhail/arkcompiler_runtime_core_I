\hypertarget{statements}{%
\section{Statements}\label{stm:statements}}

Statements are designed to control execution.

\begin{grammar}
statement
    : expressionStatement
    | block
    | localDeclaration
    | ifStatement
    | loopStatement
    | breakStatement
    | continueStatement
    | returnStatement
    | switchStatement
    | throwStatement
    | tryStatement
    | assertStatement
    ;
\end{grammar}

\hypertarget{normal-and-abrupt-execution-of-statement}{%
\subsection{Normal and Abrupt Statement Execution}\label{stm:normal-and-abrupt-execution-of-statement}}

Every statement in a normal mode of execution performs some action specific for the particular kind of statement.
The normal modes of evaluation for each kind of statement are described in the following sections.

A statement execution is considered to \emph{complete normally} if
desired action was performed without throwing an exception or error.

On the contrary, a statement is said to \emph{complete abruptly} if
its execution led to exception or error thrown.


\hypertarget{expression-statements}{%
\subsection{Expression Statements}\label{stm:expression-statements}}

Any expression may be used as statement.

\begin{grammar}
expressionStatement
    : expression
    ;
\end{grammar}

The execution of such statements leads to the execution of the expression,
and the result of its execution is discarded.

\hypertarget{block}{%
\subsection{Block}\label{stm:block}}

A sequence of statements enclosed in balanced braces forms a \emph{block}.

\begin{grammar}
block
    : '{' statement* '}'
    ;
\end{grammar}

The execution of the block means that all block statements 
except type declarations are executed one by one sequentially
in the textual order of their appearance 
within the block unless exception, error or return occurs.

If block is the body of a \emph{functionDeclaration} or a
\emph{classMethodDeclaration} which was declared with no return type
or with return type \emph{void} then the block may contain no return
statements at all and execution of such block is equivalent the block
execution plus \emph{return void} after the block is successfully executed.


\hypertarget{local-declarations}{%
\subsection{Local Declarations}\label{stm:local-declarations}}

\emph{Let}, \emph{const} and \emph{type} declarations can act as statements.
Such declarations define new mutable or immutable variable and type within
the enclosing context.

\emph{Let} and \emph{const} declarations have initialization part that
presumes execution, and actually act as statements.

\begin{grammar}
localDeclaration
    : variableDeclaration
    | constantDeclaration
    | typeDeclaration
    ;
\end{grammar}

The name of a local declaration visibility is defined by the function
(method) and block scopes rules (\See{nds:accessible}).


\hypertarget{if-statements}{%
\subsection{\code{If} Statements}\label{stm:if-statements}}

An \code{if} statement allows, based on a certain condition, executing one
statement or another if provided.

\begin{grammar}
ifStatement
    : 'if' '(' expression ')' statement1
      ('else' statement2)?
    ;
\end{grammar}

An expression represents the condition, and if it was successfully evaluated
as \emph{true}, then statement1 is executed; otherwise, statement2 is executed
if provided. A compile-time error occurs unless the type of the expression
is \emph{boolean}.

Any \code{else} matches the first \code{if} of an \code{if} statement.

\begin{source}
if (Cond1)
if (Cond2) statement1
else statement2 // Executes only if: Cond1 \&\& !Cond2
\end{source}

A list of statements in braces (block \See{stm:block}) is used to combine
the \code{else} part with the first \code{if}:

\begin{source}
if (Cond1) {
  if (Cond2) statement1
}
else statement2 // Executes if: !Cond1
\end{source}


\hypertarget{loop-statements}{%
\subsection{\code{Loop} Statements}\label{stm:loop-statements}}

\thelang{} has four kinds of loops. Each kind can have an optional label
which can be used only by \code{break} and \code{continue} statements
contained  in the body of the loop. A label is characterized by the
\emph{identifier} as shown below:

\begin{grammar}
loopStatement
    : (identifier ':')?
      whileStatement
    | doStatement
    | forStatement
    | forOfStatement
    ;
\end{grammar}


\hypertarget{while-statements-and-do-statements}{%
\subsection{\code{While} Statements and \code{Do} Statements}\label{stm:while-statements-and-do-statements}}

\code{While} statements and \code{do} statements execute an \emph{expression}
and a \emph{statement} repeatedly while the \emph{expression} value is
\emph{true}. The key difference is that a \emph{whileStatement} first evaluates
and checks the \emph{expression}, and a \emph{doStatement} starts by executing
the \emph{statement}:

\begin{grammar}
whileStatement
    : 'while' '(' expression ')' statement
    ;
\end{grammar}


\begin{grammar}
doStatement
    : 'do' statement 'while' '(' expression ')'
    ;
\end{grammar}


\hypertarget{for-statements}{%
\subsection{\code{For} Statements}\label{stm:for-statements}}

\begin{grammar}
forStatement
    : 'for' '(' forInit? ';' expression? ';' forUpdate? ')'
      statement
    ;

forInit
    : expressionSequence
    | variableDeclarations
    ;

forUpdate
    : expressionSequence
    ;
\end{grammar}

\begin{source}
// existing variable 
let i: number
for (i = 1; i < 10; i++) {
  console.log(i)
}

// new variable, explicit type:
for (let i: number = 1; i < 10; i++) {
  console.log(i)
}

// new variable, implicit type
// inferred from variable declaration
for (let i = 1; i < 10; i++) {
  console.log(i)
}
\end{source}

\hypertarget{for-of-statements}{%
\subsection{\code{For-Of} Statements}\label{stm:for-of-statements}}

The \code{for-of} loop iterates elements from an \emph{array} or \emph{string}

\begin{grammar}
forOfStatement
    : 'for' '(' forVar 'of' expression ')' statement
    ;

forVar
    : 'let'
    | 'const' identifier forVarType?
    ;
forVarType: ':' type
\end{grammar}

A compile-time error occures if
the type of an expression is not an \emph{array} or \emph{string}.

The existing variable can be used in \code{for-of} or the new local variable is declared, if the statement
contains \code{let} or \code{const} keyword.

The \emph{for variable} is set during the statement execution to
successive \textbf{elements} of the array or string that are produced
by the expression on each loop iteration.

For the new local variable:
the modifier \code{let} allows assigning a new value inside the loop body
statement; the modifier \code{const} does not allow new value assignment.

As experimental feature, an explicit type annotation is allowed for a \emph{for variable} (\See{xpm:for-of-statements}).

\begin{source}
// existing variable 'ch'
let ch : string
for (ch of "a string object") {
  console.log(ch)
}

// new variable 'ch', its type is
// inferred from expression
for (let ch of "a string object") {
  console.log(ch)
}
\end{source}


\hypertarget{break-statements}{%
\subsection{\code{Break} Statements}\label{stm:break-statements}}

A \code{break} statement transfers control out of an enclosing
\emph{loopStatement} or \emph{switchStatement}.

\begin{grammar}
breakStatement
    : 'break' identifier?
    ;
\end{grammar}

A \code{break} statement with a label \emph{identifier} transfers control
out of the enclosing statement which has the same label \emph{identifier}.
Such statement must be found within the body of the surrounding function
or method; a compile-time error occurs otherwise.

A \code{break} statement without a label transfers control out of the
innermost enclosing \code{switch}, \code{while}, \code{do}, \code{for} or
\code{for-of} statement.

A compile-time error occurs if a \emph{breakStatement} is not found within
the \emph{loopStatment} or \emph{switchStatement}.


\hypertarget{continue-statements}{%
\subsection{\code{Continue} Statements}\label{stm:continue-statements}}

A \code{continue} statement stops execution of the current loop iteration
and passes control to the next iteration with proper check of the loop exit
condition that depends on the kind of the loop.

\begin{grammar}
continueStatement
    : 'continue' identifier?
    ;
\end{grammar}

A \code{continue} statement with a label \emph{identifier} transfers control
out of the enclosing loop statement which has the same label \emph{identifier}.
Such a statement must be found within the body of the surrounding function or
method; a compile-time error occurs otherwise.

A compile-time error occurs if a \emph{continueStatement} is not found within
the \emph{loopStatment}.


\hypertarget{return-statements}{%
\subsection{\code{Return} Statements}\label{stm:return-statements}}

A \code{return} statement can have an expression or none.

\begin{grammar}
returnStatement
    : 'return' expression?
    ;
\end{grammar}

A `return expression' statement may only occur inside a function, method, or constructor body.
A `return' statement can occur in any place where statements are allowed. This
form is also valid for functions or methods with the \code{void} return type if
it is semantically equivalent to the \code{return void} statement.

A compile-time error occurs if a return statement
in class initializers (\See{cls:class-initializer}), top-level statements
(\See{pkg:top-level-statements}) and constructors (\See{cls:constructor-declaration})
has an expression.

A compile-time error occurs if a return statement in a function or a method 
with not \code{void} return type does not have an expression. 

The execution of a \emph{returnStatement} leads to the evaluation of the
\emph{expression} if it is provided; otherwise, its value or \code{void if}
\emph{expression} is used as a result of the surrounding function or method
execution.

In case of constructors, class initializers and top-level statements,
the control leaves the scope of the construction mentioned but no result
is required. Other statements of the surrounding function or method body,
class initializers or top-level statements are not executed.



\hypertarget{switch-statements}{%
\subsection{\code{Switch} Statements}\label{stm:switch-statements}}

\code{Switch} statements transfer control to a statement or block using
the result of a successful evaluation of the \code{switch}
expression's value.

\begin{grammar}
switchStatement
    : (identifier ':')? 'switch' '(' expression ')' switchBlock
    ;

switchBlock
    : '{' caseClause* defaultClause? caseClause* '}'
    ;

caseClause
    : 'case' expression ':' (statement+ | block)?
    ;

defaultClause
    : 'default' ':' (statement+ | block)?
    ;
\end{grammar}

The \emph{expression} type must be \emph{char, byte, short, int, long, 
Char, Byte, Short, Int, Long, string} or an
\emph{enum} type, and every case expression must be either a constant
expression or the name of an enum constant.

A compile-time error occurs unless all of the following is true:

\begin{itemize}
\item
  Every case expression associated with a \code{switch} statement is
  assignment-compatible (\See{cvt:assignment-compatible}) with the type of
  the \code{switch} statement's expression.
\item
  For a \code{switch} statement expression of the \emph{enum} type, every
  case constant associated with the \code{switch} statement is of the
  \emph{enum} type.
\item
  There are no two case expressions associated with the \code{switch} statement
  that have identical values.
\item
  None of the case constants associated with the \code{switch} statement is
  \emph{null}.
\end{itemize}

\begin{source}
let arg = prompt("Enter a value?");
switch (arg) {
  case '0':
  case '1':
    alert('One or zero')
    break
  case '2':
    alert('Two')
    break
  default:
    alert('An unknown value')
}
\end{source}

The execution of a \code{switch} statement starts from the \emph{expression}
evaluation. The unboxing conversion follows if the evaluation result is of
the type \emph{Char, Byte, Short}, or \emph{Int}.

Otherwise, the value of the Expression is compared repeatedly to each case
constant.

A case label \emph{matches} if a case constant equals the expression value
in terms of the `$==$' operator; however, the equality for strings determines
the equality if the expression value is a \emph{string}.

\hypertarget{throw-statements}{%
\subsection{\code{Throw} Statements}\label{stm:throw-statements}}

\code{Throw} statements cause an exception or error to be thrown (\See{exc:exceptions-and-errors}).
They immediately transfer control and can exit multiple statements, constructor,
function and method calls until a \code{try} statement (\See{stm:try-statements})
is found that catches the thrown value; the \emph{UncatchedExceptionError} is
thrown if no such try statement is found.

\begin{grammar}
throwStatement
    : 'throw' expression
    ;
\end{grammar}

The \emph{expression}'s  type must be assignable (see \See{cvt:assignment-compatible}
``Assignability'') to the type \emph{Exception} or \emph{Error}. A compile-time
error occurs otherwise.

This implies that thrown object is never \emph{null}.

It is necessary to check at compile-time that a \emph{throw} statement which
throws the exception is placed in a \code{try} block of a \code{try} statement
or in a \emph{throwing function} (\See{exc:throwing-functions}). Errors can be
thrown at any place in the code.


\hypertarget{try-statements}{%
\subsection{\code{Try} Statements}\label{stm:try-statements}}

\code{Try} statements run a block of code and provide a set of catch clauses
to handle different exceptions and errors (\See{exc:exceptions-and-errors}).

\begin{grammar}
tryStatement
    : 'try' block catchClauses finallyClause?
    ;

catchClauses
    : typedCatchClause* catchClause?
    ;

catchClause
    : 'catch' '(' identifier ')' block
    ;

typedCatchClause
    : 'catch' '(' identifier ':' typeReference ')' block
    ;

finallyClause
    : 'finally' block
    ;
\end{grammar}

The \thelang{} programming language supports \emph{mutltiple typed catch clauses}
as experimental feature (\See{xpm:try-multiple-catch}).

A \code{try} statement should contain either \code{finally} clause 
or at least one \code{catch} clause, otherwise a compile-time error occurs.

No action is taken, and no catch clause block is executed if the \code{try}
block completes normally.

The control is transferred to the catch clause 
if an error was thrown in the \code{try} block directly or indirectly.

\hypertarget{catch-clauses}{%
\subsubsection{\code{Catch} Clause}\label{stm:catch-clauses}}

A catch clause has two parts:
\begin{itemize}
\item
  a \emph{catch identifier} that provides access to the object associated with
  the error occurred, and
\item
  a block of code that is to handle the situation.
\end{itemize}

The type of a \emph{catch identifier} is \emph{Object}.

See (\See{xpm:try-multiple-catch}) for the experimental \emph{typed catch clause}.

\begin{source}
class ZeroDivisor extends Error {}

function divide(a: number, b: number): number {
  if (b == 0)
    throw new ZeroDivisor()
  return a / b
}

function process(a: number, b: number): number {
  try {
    let res = divide(a, b)

    // further processing ...
  }
  catch (e) {
    return e instanceof ZeroDivisor? -1 : 0
  }
}
\end{source}

The catch clause handles all errors in run-time. For 
the \code{ZeroDivisor} it returns \code{-1} and \code{0} for all other errors.


\hypertarget{finally-clause}{%
\subsubsection{\code{Finally} Clause}\label{stm:finally-clause}}

A \code{finally} clause defines a set of actions as a block that is executed
without regard to how (normally or abruptly) \code{try-catch} completed.

\begin{grammar}
finallyClause
    : 'finally' block
    ;
\end{grammar}

The \code{finally} block is executed without regard to how (by reaching
\emph{exception}, \emph{error}, \emph{return} or \emph{try-catch} end)
the program control is transferred out.
A \code{finally} block is particularly useful to ensure proper resource management:
any required actions (for example, flush buffers, close file descriptors) can
be performed  while leaving the try-catch.

\begin{source}
class SomeResource {
  // some API
  // ...
  close() : void {}
}

function ProcessFile(name: string) {
  let r = new SomeResource()
  try {
    // some processing
  }
  finally {
    // finally clause will be executed after try-catch is executed normally or abruptly
    r.close()
  }
}
\end{source}


\hypertarget{try-statement-execution}{%
\subsubsection{\code{Try} Statement Execution}\label{stm:try-statement-execution}}

\begin{enumerate}
\def\labelenumi{\arabic{enumi}.}
\item
The \code{try} block and the entire \code{try} statement completes normally
if no catch block is executed.

The execution of a \code{try} block completes abruptly if an \emph{exception}
or \emph{error} is thrown inside the try block.

Catch clauses are checked in the order of their placement in the source code.

\item
The execution of a \code{try} block completes abruptly if an \emph{exception}
or \emph{error} \emph{x} is thrown inside the try block.\\

If the runtime type of \emph{x} is assignment-compatible with an
\emph{exception} class of the \emph{exception} parameter (i.e., the
catch clause matches \emph{x}), and the execution of the body of the
catch clause completes normally, then the entire \code{try} statement
completes normally.
Otherwise, the \code{try} statement completes abruptly.

\item
If no catch clause can handle the \emph{exception} or \emph{error}, then
those propagate to the surrounding scope. If the surrounding scope is a
function, method or constructor, then the execution depends on whether
the surrounding scope is a \emph{throwing function} (\See {exc:throwing-functions}).
If so, then the exception propagates to the caller context.
Otherwise, the \emph{UncatchedExceptionError} is thrown.

\end{enumerate}

\hypertarget{assert-statements}{%
\subsection{\code{Assert} Statements}\label{stm:assert-statements}}

This statements are described in the experimental section (\See{xpm:assert-statements}).
