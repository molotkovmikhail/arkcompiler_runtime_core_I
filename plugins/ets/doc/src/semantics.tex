\hypertarget{semantics}{%
\section{Semantic Rules}\label{smt:semantics}}

This Chapter contains semantic rules to be used throughout
the Specification document.

Note that the description of the rules is more or less informal.

Some details are omitted to make the understanding easier.
See the formal language description for more information.

\hypertarget{subtyping}{%
\subsection{Subtyping}\label{smt:subtyping}}

The \emph{subtype} relationships are the binary relationships of types.

The subtyping relation of \emph{S} as a subtype of \emph{T} is recorded
as \emph{S}\textless:\emph{T} and means that any object of the type \emph{S} can be
safely used in any context in place of an object of the type \emph{T}.

By definition, the \emph{S}\textless:\emph{T} of the type \emph{T} belongs
to the set of \emph{supertypes} of the type \emph{S}.

Such set of \emph{supertypes} includes all \emph{direct supertypes}
(see below) and all their respective \emph{direct supertypes}.

More formally speaking, the set is obtained by reflexive and transitive
closure over the direct supertype relation.

The \emph{direct supertypes} of a non-generic class or interface type
\emph{C} are all of the following:

\begin{itemize}
\item
  The direct superclass of \emph{C} (as mentioned in its extension clause
  \See{cls:class-extension-clause}), or the type \emph{Object} if \emph{C}
  has no extension clause specified.
\item
  The direct superinterfaces of \emph{C} (as mentioned in \emph{C}'
  implementation clause \See{cls:class-implementation-clause}).
\item
  The type \emph{Object} if \emph{C} is an interface type with no direct
  superinterfaces (\See{inf:superinterfaces-and-subinterfaces}).
\end{itemize}

The \emph{direct supertypes} of the generic type \emph{C}\textless \emph{F\textsubscript{1},...,F\textsubscript{n}}\textgreater{}
(for a generic class or interface type declaration \emph{C}\textless \emph{F\textsubscript{1},...,F\textsubscript{n}}\textgreater{}
with \emph{n}\textgreater{}\emph{0}) are all of the following:

\begin{itemize}
\item
  The direct superclass of \emph{C}\textless \emph{F\textsubscript{1},...,F\textsubscript{n}}\textgreater{}.
\item
  The direct superinterfaces of \emph{C}\textless \emph{F\textsubscript{1},...,F\textsubscript{n}}\textgreater{}.
\item
  The type \emph{Object} if \emph{C}\textless \emph{F\textsubscript{1},...,F\textsubscript{n}}\textgreater{}
  is a generic interface type with no direct superinterfaces.
\end{itemize}

The direct supertype of a type parameter is the type specified as
the bound of that type parameter.


\hypertarget{least-upper-bound}{%
\subsection{Least Upper Bound}\label{smt:least-upper-bound}}

The notion of a \emph{least upper bound} (\emph{LUB}) is used
where finding a single type is necessary that is a common
supertype of a set of reference types.

The word \emph{least} means that the most specific supertype must be
found, and that no other shared supertype is a subtype of LUB.

A single type is the LUB for itself.

In a set \emph{(T\textsubscript{1}, ..., T\textsubscript{k})} that
contains at least two types, the LUB is determined as follows:

\begin{itemize}
\item
  The set of supertypes \emph{ST\textsubscript{i}} is determined for
  each type in the set;
\item
  The intersection of the \emph{ST\textsubscript{i}} sets is calculated.
  Note that the intersection always contains the \emph{Object} and thus
  cannot be empty.
\item
  The most specific type is selected from the intersection.
\end{itemize}

A compile-time error occurs of any types in the original set
\emph{(T\textsubscript{1}, ..., T\textsubscript{k})} are not
reference types.

\hypertarget{override-equivalent}{%
\subsection{Override-Equivalent Signatures}\label{nds:override-equivalent}}

Two functions, methods or constructors \emph{M} and \emph{N} have the
\emph{same signature} if their names and type parameters (if any) are
the same (see \See{gen:generics}\space ``Generic Declarations''), and their
formal parameter types are also the same (after the formal parameter types of
\emph{N} are adapted to the type parameters of \emph{M}).

Signatures \emph{s\textsubscript{1}} and \emph{s\textsubscript{2}}
are \emph{override-equivalent} only if \emph{s\textsubscript{1}}
and \emph{s\textsubscript{2}} are the same.

A compile-time error occurs if:
\begin{itemize}
\item
  A package declares two functions with override-equivalent signatures.
\item
  A class declares
  \begin{itemize}
  \item
    two methods with override-equivalent signatures.
  \item
    two constructors with override-equivalent signatures.
  \end{itemize}
\end{itemize}

\hypertarget{overload-signature-compatibility}{%
\subsection{Overload Signature Compatibility}\label{smt:overload-signature-compatibility}}

\notwritten{TBD: exact rules}

\hypertarget{TScompatibility}{%
\subsection{Compatibility Features}\label{smt:TScompatibility}}

To support smooth Typescript compatibility some features were added into \thelang{}.
In most cases it is not recommended to use such features while doing the \thelang{} programming.

\hypertarget{extended-conditional}{%
\subsubsection{Extended Conditional Expressions}\label{smt:extended-conditional}}

For better alignment with the \theTS semantics of conditional-and and
conditional-or expressions an extended semantics is introduced for \thelang{}.
It affects semantics of conditional expressions (\See{exp:conditional-expressions}),
and the \code{while} and \code{do} statements (\See{stm:while-statements-and-do-statements})
Approach is based on the concept of truthiness which extends the Boolean logic
to operands and the result of non-Boolean types.
The value of any valid expression of non-void type can be treated as \emph{Truthy} or \emph{Falsy}
depending on the kind of the value type. See the table below for details.

\begin{longtable}[]{@{}
  >{\raggedright\arraybackslash}p{(\columnwidth - 2\tabcolsep) * \real{0.2000}}
  >{\raggedright\arraybackslash}p{(\columnwidth - 2\tabcolsep) * \real{0.2000}}
  >{\raggedright\arraybackslash}p{(\columnwidth - 2\tabcolsep) * \real{0.2000}}
  >{\raggedright\arraybackslash}p{(\columnwidth - 2\tabcolsep) * \real{0.3000}}@{}}
\toprule()
\begin{minipage}[b]{\linewidth}\raggedright
Value type
\end{minipage} &
\begin{minipage}[b]{\linewidth}\raggedright
When Falsy
\end{minipage} &
\begin{minipage}[b]{\linewidth}\raggedright
When Truthy
\end{minipage} &
\begin{minipage}[b]{\linewidth}\raggedright
\thelang{} code
\end{minipage} \\
\midrule()
\endhead
string&
"" empty string&
non-empty string &
if (stringExpr.length() == 0) {} \\
number& 
0 or NaN& 
any other number &
if (integralExpr == 0) {} \\
nullableExpr&
== null&
!= null &
if (nullableExpr == null) {} \\
nonNullableExpr&
never&
always&
n/a\\
\bottomrule()
\end{longtable}

The actual extended semantics of the conditional-and and conditional-or
expressions is described by following truth tables (assuming `A' and `B' are
any valid expressions):

\begin{longtable}[]{@{}
  >{\raggedright\arraybackslash}p{(\columnwidth - 2\tabcolsep) * \real{0.2000}}
  >{\raggedright\arraybackslash}p{(\columnwidth - 2\tabcolsep) * \real{0.3000}}@{}}
\toprule()
\begin{minipage}[b]{\linewidth}\raggedright
A
\end{minipage} &
\begin{minipage}[b]{\linewidth}\raggedright
!A
\end{minipage} \\
\midrule()
\endhead
Falsy&
true\\
Truthy&
false\\
\bottomrule()
\end{longtable}

\begin{longtable}[]{@{}
  >{\raggedright\arraybackslash}p{(\columnwidth - 2\tabcolsep) * \real{0.2000}}
  >{\raggedright\arraybackslash}p{(\columnwidth - 2\tabcolsep) * \real{0.2000}}
  >{\raggedright\arraybackslash}p{(\columnwidth - 2\tabcolsep) * \real{0.2000}}
  >{\raggedright\arraybackslash}p{(\columnwidth - 2\tabcolsep) * \real{0.2000}}@{}}
\toprule()
\begin{minipage}[b]{\linewidth}\raggedright
A
\end{minipage} & 
\begin{minipage}[b]{\linewidth}\raggedright
B
\end{minipage} & 
\begin{minipage}[b]{\linewidth}\raggedright
A \&\& B
\end{minipage} & 
\begin{minipage}[b]{\linewidth}\raggedright
A \textbar\textbar{} B
\end{minipage} \\
\midrule()
\endhead
Falsy&
Falsy&
A&
B\\
Falsy&
Truthy&
A&
B\\
Truthy&
Falsy&
B&
A\\
Truthy&
Truthy&
B&
A\\
\bottomrule()
\end{longtable}

