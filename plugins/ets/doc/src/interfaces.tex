\hypertarget{interfaces}{%
\section{Interfaces}\label{inf:interfaces}}

An interface declaration declares an \emph{interface type}, i.e., a reference
type that:
\begin{itemize}
\item
  includes constants, properties and methods as its members;
\item
  has no instance variables;
\item
  usually declares one or more methods;
\item
  allows otherwise unrelated classes to provide implementations for the
  methods and so implement the interface.
\end{itemize}

Interfaces cannot be directly instantiated.

An interface can declare a \emph{direct extension} of one or more other
interfaces. In such case it inherits all members from the interfaces it
extends (except any members that it overrides or hides).

A class can be declared to \emph{directly implement} one or more
interfaces, and any instance of the class implements all methods
that the interface(s) specifies.
A  class implements all interfaces that its direct superclasses and direct
superinterfaces implement. Such interface inheritance allows objects to
support  common behaviors, while they do not share a superclass.

The value of a variable declared as an interface type can be a
reference to any instance of a class that implements the specified
interface. However, it is not enough for the class to implement
all methods of the interface: the class or one of its superclasses
must be actually declared to implement the interface, otherwise
such class is not considered to implement the interface.

Interfaces without \emph{interfaceExtendsClause} have class \emph{Object}
as their supertype. That enables assignments on the basis of reference types
conversions (see \See{cvt:reference-types-conversions}).

\hypertarget{interface-declarations}{%
\subsection{Interface Declarations}\label{inf:interface-declarations}}

An \emph{interface declaration} specifies a new named reference type.

\begin{grammar}
interfaceDeclaration
    : 'interface' identifier typeParameters?
      interfaceExtendsClause? '{' interfaceBody '}'
    ;

interfaceExtendsClause
    : 'extends' interfaceTypeList
    ;

interfaceTypeList
    : typeReference (',' typeReference)*
    ;

interfaceBody
    : interfaceMember*
    ;
\end{grammar}

The \emph{identifier} in an interface declaration specifies the
class name.

An interface declaration with \emph{typeParameters} introduces
a new generic interface (\See{gen:generic-declarations}).

An interface declaration's scope is defined in (\See{nds:scopes}).

An interface declaration's shadowing is specified in \needlink{shadowing}.

\hypertarget{superinterfaces-and-subinterfaces}{%
\subsection{Superinterfaces and
Subinterfaces}\label{inf:superinterfaces-and-subinterfaces}}

The interface declared with an \emph{extends} clause extends all other named
interfaces and thus inherits all their members.

Such other named interfaces are the \emph{direct superinterfaces}
of the declared interface.

A class that \emph{implements} the declared interface also
implements all the interfaces that the interface \emph{extends}.

A compile-time error occurs if:
\begin{itemize}
\item
  a \emph{typeReference} in an interface declaration's \emph{extends} clause
  names an interface type that is not accessible (\See{nds:accessible}).
\item
  type arguments of a \emph{typeReference} denote a parameterized type that
  is not well-formed (\See{gen:parameterized-declarations}).
\item
  there is a cycle in \emph{extends} graph.
\item
  at least one of \emph{typeReference}s is an alias of one of primitive or
  enum types.
\item
  any type argument is a wildcard type.
\end{itemize}

Each \emph{typeReference} in the \emph{extends} clause of an interface
declaration must name an accessible interface type (\See{nds:accessible});
a compile-time error occurs otherwise.

The \emph{direct superinterfaces} of the interface type $I<F\textsubscript{1},...,F\textsubscript{n}>$ are the types
given in the \emph{extends} clause of the declaration of \emph{I},
if an interface declaration (possibly generic) $I<F\textsubscript{1},...,F\textsubscript{n}>$ ($n\geq{}0$) contains
an \emph{extends} clause.

The \emph{direct superinterfaces} of the parameterized interface type
$I<T\textsubscript{1},...,T\textsubscript{n}>$ are all
types $J<U\textsubscript{1} \theta{},...,U\textsubscript{k} \theta{}>$,
if $T\textsubscript{i}$ ($1\leq{}i\leq{}n$) is a type of a generic interface
declaration $I<F\textsubscript{1},...,F\textsubscript{n}>$
($n > 0$), $J<U\textsubscript{1},...,U\textsubscript{k}>$
is a direct superinterface of $I<F\textsubscript{1},...,F\textsubscript{n}>$
and $\theta{}$ is the substitution ${[}F\textsubscript{1}:=T\textsubscript{1},...,F\textsubscript{n}:=T
\textsubscript{n}{]}$.

The transitive closure of the direct superinterface relationship results
in the \emph{superinterface} relationship.

Wherever \emph{K} is a superinterface of an interface \emph{I}, \emph{I}
is a \emph{subinterface} of \emph{K}.

An interface \emph{K} is a superinterface of interface \emph{I} if:

\begin{itemize}
\item
  \emph{I} is a direct subinterface of \emph{K}; or
\item
  \emph{K} is a superinterface of some interface \emph{J} to which
  \emph{I} is in its turn a subinterface.
\end{itemize}

An interface cannot have all interfaces as its extensions (unlike
class \emph{Object} has all classes as its extensions).

An interface \emph{I} \emph{directly depends} on a type \emph{T}
if the \emph{extends} clause of \emph{I} mentions \emph{T} as a
superinterface or as a qualifier in the superinterface name's
fully qualified form.

Moreover, an interface \emph{I} \emph{depends} on a reference
type \emph{T} if:

\begin{itemize}
\item
  \emph{I} directly depends on \emph{T}; or
\item
  \emph{I} directly depends on a class \emph{C}, which depends
  on \emph{T} (\See{cls:classes}); or
\item
  \emph{I} directly depends on an interface \emph{J}, which
  in its turn depends on \emph{T}.
\end{itemize}

A compile-time error occurs if an interface depends on itself.

A \emph{ClassCircularityError} is thrown if circularly declared
interfaces are detected as interfaces are loaded at run time.


\hypertarget{interface-body}{%
\subsection{Interface Body}\label{inf:interface-body}}

The body of an interface may declare members of the interface, that is,
properties (\See{inf:interface-properties}) and methods (\See{inf:interface-method-declarations}).

\begin{grammar}
interfaceMember
    : interfaceProperty
    | interfaceMethod
    ;
\end{grammar}

The scope of a declaration of a member \emph{m} that an interface
type \emph{I} declares or inherits is specified in (\See{nds:scopes}).


\hypertarget{interface-members}{%
\subsection{Interface Members}\label{inf:interface-members}}

Interface type members are as follows:

\begin{itemize}
\item
  Members declared in the interface body (\See{inf:interface-body}).
\item
  Members inherited from a direct superinterface (\See{inf:superinterfaces-and-subinterfaces}).
\item
  An interface without a direct superinterface implicitly declares an
  abstract member method \emph{m} (\See{inf:abstract-method-declarations})
  with signature \emph{s}, return type \emph{r} and \emph{throws} clause
  \emph{t} that correspond to each \emph{public} instance method \emph{m}
  with signature \emph{s}, return type \emph{r} and \emph{throws} clause
  \emph{t} declared in \emph{Object} (\See{typ:the-object-class}), unless
  the interface explicitly declares an abstract method (\See{inf:abstract-method-declarations})
  with the same signature and return type, and a compatible \emph{throws}
  clause.\\
  \strut \\
  A compile-time error occurs if the interface explicitly declares:
  \begin{itemize}
  \item
    a method \emph{m} that \emph{Object} declares as \emph{final}.
  \item
    a method with a signature that is override-equivalent (\See{nds:signatures})
    to an \emph{Object}'s \emph{public} method, but is not \emph{abstract},has
    a different return type or an incompatible \emph{throws} clause.
  \end{itemize}
\end{itemize}

An interface normally inherits all members of the interfaces it
extends. However, an interface does not inherit:
\begin{enumerate}
\def\labelenumi{\roman{enumi})}
\item
  fields it hides;
\item
  methods it overrides (\See{inf:inheritance-and-overriding});
\item
  private methods;
\item
  static methods.
\end{enumerate}

A name in a declaration scope must be unique, and the names
of an interface type's fields and methods must not be the
same (see \See{inf:interface-declarations}).


\hypertarget{interface-properties}{%
\subsection{Interface Properties}\label{inf:interface-properties}}

An interface property may be defined in the form of a field or an accessor
(a getter or a setter).

\begin{grammar}
interfaceProperty
    : readonly? identifier ':' type
    | 'get' identifier '(' ')' returnType
    | 'set' identifier '(' parameter ')'
    ;
\end{grammar}

If a property is defined in the form of a field, then it implicitly defines:
\begin{itemize}
\item
  a getter if a field is marked as \emph{readonly};
\item
  otherwise, both a getter and a setter with the same name.
\end{itemize}

So the following definitions have the same effect:

\begin{source}
interface Style {
    color: string
}
// is the same as
interface Style {
    get color(): string
    set color(s: string)
}
\end{source}

A class that implements an interface with properties may also use a field
or an accessor notation (\See{cls:properties-from-interface}).


\hypertarget{interface-method-declarations}{%
\subsection{Method Declarations}\label{inf:interface-method-declarations}}

An interface body can declare three kinds of methods:
\begin{enumerate}
\def\labelenumi{\arabic{enumi}.}
\item
  \emph{abstract} methods without body (\See{inf:abstract-method-declarations});
\item
  \emph{default} methods with body (\See{inf:default-method-declarations});
\item
  \emph{static} methods (\See{inf:static-method-declarations}).
\end{enumerate}

The grammar for the given definition is as follows:

\begin{grammar}
interfaceMethod
    : interfaceAbstractMethod
    | interfaceDefaultMethod
    | interfaceStaticMethod
    ;
\end{grammar}

The methods declared within interface bodies are implicitly \emph{public}.

A compile-time error occurs if the body of an interface declares:
\begin{itemize}
\item
  a method with a name already used for a field in this declaration.
\item
  two methods (explicitly or implicitly) with override-equivalent
  signatures (\See{nds:signatures}), unless such signatures are
  inherited along with abstract methods (\See{inf:inheritance-and-overriding}).
\end{itemize}

\hypertarget{abstract-method-declarations}{%
\subsubsection{Abstract Method Declarations}\label{inf:abstract-method-declarations}}

\begin{grammar}
interfaceAbstractMethod
    : identifier signature ';'
    ;
\end{grammar}

An abstract method in an interface body cannot be represented by a block of code.


\hypertarget{default-method-declarations}{%
\subsubsection{Default Method Declarations}\label{inf:default-method-declarations}}

\begin{grammar}
interfaceDefaultMethod
    : 'private'? identifier signature block
    ;
\end{grammar}

A default method can be explicitly declared \emph{private} in an interface body.

A block of code that represents the body of a default method in an interface
provides a default implementation for any class unless such class overrides
the method to implement the interface.


\hypertarget{static-method-declarations}{%
\subsubsection{\texorpdfstring{\emph{static} Method Declarations}{static Method Declarations}}\label{inf:static-method-declarations}}

\begin{grammar}
interfaceStaticMethod
    : 'static' 'private'? identifier signature block
    | 'private'? 'static' identifier signature block
    ;
\end{grammar}

A \emph{static} method in an interface body can be explicitly declared
\emph{private}.

\emph{static} interface method calls refer to no particular object.

In contrast to default methods, \emph{static} interface methods are not
instance methods.

A compile-time error occurs if:
\begin{itemize}
\item
  the body of a \emph{static} method attempts to use the keyword \code{this}
  or the keyword \code{super} to reference the current object.
\item
  the header or body of a \emph{static} method of an interface contains the
  name of any surrounding declaration's type parameter.
\end{itemize}


\hypertarget{inheritance-and-overriding}{%
\subsubsection{Inheritance and Overriding}\label{inf:inheritance-and-overriding}}

An interface \emph{I} inherits any abstract and default method \emph{m}
from its direct superinterfaces if all the following is true:

\begin{itemize}
\item
  \emph{m} is a member of \emph{I}'s direct superinterface \emph{J}.
\item
  \emph{I} declares no method with a signature that is a subsignature
  (\See{nds:signatures}) of \emph{m}'s signature.
\item
  No method \emph{m\textquotesingle{}} that is a member of an
  \emph{I}'s direct superinterface \emph{J\textquotesingle{}} (\emph{m}
  being distinct from \emph{m\textquotesingle{}}, and \emph{J} from \emph{J\textquotesingle{}})
  overrides the declaration of the method \emph{m} from \emph{J\textquotesingle{}}.
\end{itemize}

An interface cannot inherit \emph{private} or \emph{static} methods
from its superinterfaces.

A compile-time error occurs if:
\begin{itemize}
\item
  An interface \emph{I} declares a \emph{private} or \emph{static}
  method \emph{m}; and
\item
  \emph{m}'s signature is a subsignature of a \emph{public} instance method
  \emph{m\textquotesingle{}} in a superinterface of \emph{I}; and
\item
  \emph{m\textquotesingle{}} is otherwise accessible to code in \emph{I}.
\end{itemize}


\hypertarget{instance-method-overriding}{%
\subsubsection{Overriding (by Instance Methods)}\label{inf:instance-method-overriding}}

An instance method \emph{m\textsubscript{I}} (declared in or inherited by
interface \emph{I}) overrides another \emph{I}'s instance method
\emph{m\textsubscript{J}} (declared in interface \emph{J}), if all of the
following is true:

\begin{itemize}
\item
  \emph{J} is a superinterface of \emph{I}.
\item
  \emph{I} does not inherit \emph{m\textsubscript{J}}.
\item
  \emph{m\textsubscript{I}}'s signature is a subsignature
  (\See{nds:signatures}) of \emph{m\textsubscript{J}}'s signature.
\item
  \emph{m\textsubscript{J}} is \emph{public}.
\end{itemize}


\hypertarget{overriding-requirements}{%
\subsubsection{Overriding Requirements}
\label{inf:overriding-requirements}}

The relationship between the return types of an interface and of any
overridden interface methods is specified in \See{cls:requirements-in-overriding-and-hiding}.

The relationship between the \emph{throws} clauses of an interface
method and of any overridden interface methods is specified in
\See{cls:requirements-in-overriding-and-hiding}.

The relationship between the signatures of an interface method and
of any overridden interface methods is specified in \See{cls:requirements-in-overriding-and-hiding}.

The relationship between the accessibility of an interface method
and that of any overridden interface methods is specified in
\See{cls:requirements-in-overriding-and-hiding}.

A compile-time error occurs if a default method is override-
equivalent to a non-\emph{private} method of the class
\emph{Object}. Any class implementing the interface must
inherit its own implementation of the method.


\hypertarget{inheriting-methods-with-override-equivalent}{%
\subsubsection{Inheriting Methods with Override-Equivalent
Signatures}\label{inf:inheriting-methods-with-override-equivalent}}

An interface can inherit several methods with override-equivalent
signatures (\See{nds:override-equivalent}).

A compile-time error occurs, if an interface \emph{I} inherits
a default method with a signature that is override-equivalent
to another method inherited by \emph{I}, if that latter method
is abstract or default.

However, an interface \emph{I} inherits all methods that are abstract.

A compile-time error occurs unless one of the inherited methods for every
other inherited method is return-type-substitutable, while the \emph{throws}
clauses do not cause errors in such cases.

The same method declaration can use multiple paths of inheritance from an
interface, which causes no compile-time error on its own.

