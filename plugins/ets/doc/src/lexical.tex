\hypertarget{lexical-elements}{%
\section{Lexical Elements}\label{lex:lexical-elements}}

This chapter discusses the lexical structure of the \thelang{} programming
language and the analytical conventions.

\hypertarget{use-of-unicode-characters}{%
\subsection{Use of Unicode Characters}\label{lex:use-of-unicode-characters}}

The \thelang{} programming language uses the Unicode Character set
\footnote{Unicode Standard Version 15.0.0, \url{https://www.unicode.org/versions/Unicode15.0.0/}}
as its terminal
symbols and represents text in sequences of 16-bit code units, using the
Unicode UTF-16 encoding.

The term \emph{Unicode code point} is used in this specification only where such
representation is relevant to refer the reader to Unicode Character set
and UTF-16 encoding. Where such reference is irrelevant to the
discussion, the generic term \emph{character} is used.


\hypertarget{lexical-input-elements}{%
\subsection{Lexical Input Elements}\label{lex:lexical-input-elements}}

The language has lexical input elements of the following types: white
spaces (\See{lex:white-spaces}), line separators (\See{lex:line-separators}), tokens
(\See{lex:tokens}{lex:identifiers}{lex:keywords}{lex:operators-and-punctuation}{lex:literals}) and comments (\See{lex:comments}).


\hypertarget{lex:white-spaces}{%
\subsection{White Spaces}\label{lex:white-spaces}}

White spaces are lexical input elements that separate tokens from each
other to improve source code readability and avoid ambiguities. White
spaces are ignored by the syntactic grammar.

White spaces include the following:

\begin{itemize}
\item
Space (U+0020),
\item
Horizontal tabulation (U+0009),
\item
Vertical tabulation (U+000B),
\item
Form feed (U+000C),
\item
No-break space (U+00A0),
\item
Zero width no-break space (U+FEFF).
\end{itemize}

White spaces can occur within comments but not within any kind of token.

\hypertarget{line-separators}{%
\subsection{Line Separators}\label{lex:line-separators}}

Line separators are lexical input elements that divide sequences of
Unicode input characters into lines and separate tokens from each other
to improve source code readability.

Line separators include the following:

\begin{itemize}
\item
Newline character (U+000A or ASCII \textless LF\textgreater),
\item
Carriage return character (U+000D or ASCII \textless CR\textgreater),
\item
Line separator character (U+2028 or ASCII \textless LS\textgreater),
\item
Paragraph separator character (U+2029 or ASCII \textless PS\textgreater).
\end{itemize}

Any sequence of line separators is considered a single separator.


\hypertarget{tokens}{%
\subsection{Tokens}\label{lex:tokens}}

Tokens form the vocabulary of the language. There are four classes of
tokens: identifiers (\See{lex:identifiers}), keywords (\See{lex:keywords}),
operators and punctuation (\See{lex:operators-and-punctuation}), and literals (\See{lex:literals}).

Token is the only lexical input element which can act as terminal symbol
of the syntactic grammar. White space (\See{lex:white-spaces}) is ignored except as it
separates tokens that would otherwise merge into a single token. In the
process of tokenization, the next token is always the longest sequence
of characters that form a valid token.

In many cases line separators are treated as white spaces (\See{lex:semicolons})
except where line separators have special meanings.


\hypertarget{identifiers}{%
\subsection{Identifiers}\label{lex:identifiers}}

An identifier is a sequence of one or more valid Unicode characters. The
Unicode grammar of identifiers is based on character properties
specified by the Unicode Standard.

The first character in an identifier must be either `\code{\$}' or `\code{\_}'
or any Unicode code point with the Unicode property `\code{ID\_Start}'\footnote{\url{https://unicode.org/reports/tr31/}}.
Other characters must be Unicode code points with the Unicode property `\code{ID\_Continue}'
or one of the following characters: `\code{\$}' (\code{\textbackslash{}U+0024}), `\code{Zero Width Non-Joiner}' (\code{<ZWNJ>},
\code{\textbackslash{}U+200C}) or `\code{Zero Width Joiner}' (\code{<ZWJ>}, \code{\textbackslash{}U+200D}).

\begin{grammar}
Identifier
    : IdentifierStart IdentifierPart*
    ;

IdentifierStart
    : UnicodeIDStart
    | '$'
    | '_'
    | '\\' EscapeSequence
    ;

IdentifierPart
    : UnicodeIDContinue
    | '$'
    | <ZWNJ>
    | <ZWJ>
    | '\\' EscapeSequence
    ;
\end{grammar}


\hypertarget{keywords}{%
\subsection{Keywords}\label{lex:keywords}}

\emph{Keywords} are reserved words that have permanently predefined
meanings in the language.

The following keywords are reserved in any context (\emph{hard
keywords}) and cannot be used as identifiers:

\begin{longtable}[]{@{}
  >{\raggedright\arraybackslash}p{(\columnwidth - 6\tabcolsep) * \real{0.2499}}
  >{\raggedright\arraybackslash}p{(\columnwidth - 6\tabcolsep) * \real{0.2501}}
  >{\raggedright\arraybackslash}p{(\columnwidth - 6\tabcolsep) * \real{0.2499}}
  >{\raggedright\arraybackslash}p{(\columnwidth - 6\tabcolsep) * \real{0.2501}}@{}}
\toprule()
\begin{minipage}[b]{\linewidth}\raggedright
abstract

as
                                                    
assert

async 

await

break

case

class

const

constructor

continue

do

else

\end{minipage} & \begin{minipage}[b]{\linewidth}\raggedright
enum

export

extends

false

final

finally 

for

function

if

implements

import

instanceof

interface

\end{minipage} & \begin{minipage}[b]{\linewidth}\raggedright
internal

launch

let

native

new

null

override

package

private

protected

public

return

static

\end{minipage} & \begin{minipage}[b]{\linewidth}\raggedright
switch

super

this

throw

true

try

type

while
\end{minipage} \\
%\midrule()
\endhead
\bottomrule()
\end{longtable}

The following words have special meaning in certain contexts (\emph{soft
keywords}) but are valid identifiers in other places:

\begin{longtable}[]{@{}
  >{\raggedright\arraybackslash}p{(\columnwidth - 6\tabcolsep) * \real{0.2499}}
  >{\raggedright\arraybackslash}p{(\columnwidth - 6\tabcolsep) * \real{0.2501}}
  >{\raggedright\arraybackslash}p{(\columnwidth - 6\tabcolsep) * \real{0.2499}}
  >{\raggedright\arraybackslash}p{(\columnwidth - 6\tabcolsep) * \real{0.2501}}@{}}
\toprule()
\begin{minipage}[b]{\linewidth}\raggedright
catch

default 

from

\end{minipage} & \begin{minipage}[b]{\linewidth}\raggedright
get 

throws

in

\end{minipage} & \begin{minipage}[b]{\linewidth}\raggedright
of

out

readonly

\end{minipage} & \begin{minipage}[b]{\linewidth}\raggedright
rethrows

set 

\end{minipage} \\
%\midrule()
\endhead
\bottomrule()
\end{longtable}

The following words cannot be used as user defined type names but are
otherwise not restricted:

\begin{longtable}[]{@{}
  >{\raggedright\arraybackslash}p{(\columnwidth - 6\tabcolsep) * \real{0.2499}}
  >{\raggedright\arraybackslash}p{(\columnwidth - 6\tabcolsep) * \real{0.2501}}
  >{\raggedright\arraybackslash}p{(\columnwidth - 6\tabcolsep) * \real{0.2499}}
  >{\raggedright\arraybackslash}p{(\columnwidth - 6\tabcolsep) * \real{0.2501}}@{}}
\toprule()
\begin{minipage}[b]{\linewidth}\raggedright
boolean

byte

char
\end{minipage} & \begin{minipage}[b]{\linewidth}\raggedright
double

float

int

\end{minipage} & \begin{minipage}[b]{\linewidth}\raggedright
long

number

short
\end{minipage} & \begin{minipage}[b]{\linewidth}\raggedright
string 

undefined

void
\end{minipage} \\
%\midrule()
\endhead
\bottomrule()
\end{longtable}

The following keywords are reserved for future use (or used in TS):



\begin{longtable}[]{@{}
  >{\raggedright\arraybackslash}p{(\columnwidth - 6\tabcolsep) * \real{0.2499}}
  >{\raggedright\arraybackslash}p{(\columnwidth - 6\tabcolsep) * \real{0.2501}}
  >{\raggedright\arraybackslash}p{(\columnwidth - 6\tabcolsep) * \real{0.2499}}
  >{\raggedright\arraybackslash}p{(\columnwidth - 6\tabcolsep) * \real{0.2501}}@{}}
\toprule()
\begin{minipage}[b]{\linewidth}\raggedright
is
\end{minipage} & \begin{minipage}[b]{\linewidth}\raggedright
typeof
\end{minipage} & \begin{minipage}[b]{\linewidth}\raggedright
var
\end{minipage} & \begin{minipage}[b]{\linewidth}\raggedright
yield
\end{minipage} \\
%\midrule()
\endhead
\bottomrule()
\end{longtable}

Keywords are always lowercase.

\hypertarget{operators-and-punctuation}{%
\subsection{Operators and Punctuation}\label{lex:operators-and-punctuation}}

The following character sequences represent operators and punctuation:

\begin{longtable}[]{@{}
  >{\raggedright\arraybackslash}p{(\columnwidth - 16\tabcolsep) * \real{0.1111}}
  >{\raggedright\arraybackslash}p{(\columnwidth - 16\tabcolsep) * \real{0.1111}}
  >{\raggedright\arraybackslash}p{(\columnwidth - 16\tabcolsep) * \real{0.1111}}
  >{\raggedright\arraybackslash}p{(\columnwidth - 16\tabcolsep) * \real{0.1111}}
  >{\raggedright\arraybackslash}p{(\columnwidth - 16\tabcolsep) * \real{0.1111}}
  >{\raggedright\arraybackslash}p{(\columnwidth - 16\tabcolsep) * \real{0.1111}}
  >{\raggedright\arraybackslash}p{(\columnwidth - 16\tabcolsep) * \real{0.1111}}
  >{\raggedright\arraybackslash}p{(\columnwidth - 16\tabcolsep) * \real{0.1111}}
  >{\raggedright\arraybackslash}p{(\columnwidth - 16\tabcolsep) * \real{0.1111}}@{}}
\toprule()
\begin{minipage}[b]{\linewidth}\raggedright
+

-

*

/

\%
\end{minipage} & \begin{minipage}[b]{\linewidth}\raggedright
\&

\textbar{}

\^{}

\textless\textless{}

\textgreater\textgreater{}
\end{minipage} & \begin{minipage}[b]{\linewidth}\raggedright
+=

-=

*=

/=

\%=
\end{minipage} & \begin{minipage}[b]{\linewidth}\raggedright
\&=

\textbar=

\^{}=

\textless\textless=

\textgreater\textgreater=

\textgreater\textgreater\textgreater=
\end{minipage} & \begin{minipage}[b]{\linewidth}\raggedright
\&\&

\textbar\textbar{}

++

-\/-
\end{minipage} & \begin{minipage}[b]{\linewidth}\raggedright
==

\textless{}

\textgreater{}

===

=

!
\end{minipage} & \begin{minipage}[b]{\linewidth}\raggedright
??

?.

!=

\textless=

\textgreater=

\ldots{}
\end{minipage} & \begin{minipage}[b]{\linewidth}\raggedright
(

{[}

\{

,

.
\end{minipage} & \begin{minipage}[b]{\linewidth}\raggedright
)

{]}

\}

;

:
\end{minipage} \\
%\midrule()
\endhead
\bottomrule()
\end{longtable}

\hypertarget{literals}{%
\subsection{Literals}\label{lex:literals}}

\emph{Literals} are representations of values of certain types.

See (\See{lex:char-literals}) for experimental \emph{char literal}.

\hypertarget{integer-literals}{%
\subsubsection{Integer Literals}\label{lex:integer-literals}}

\emph{Integer literals} represent numbers that do not have a decimal
point or an exponential part. Integer literals can be written with bases
16 (hexadecimal), 10 (decimal), 8 (octal) and 2 (binary).

\begin{grammar}
DecimalIntegerLiteral
    : '0'
    | [1-9] [0-9_]* [0-9]
    ;

HexIntegerLiteral
    : '0' [xX]  ( HexDigit
        | HexDigit (HexDigit | '_')* HexDigit
        )
    ;

HexDigit
    : [0-9a-fA-F]
    ;

OctalIntegerLiteral
    : '0' [oO] ( [0-7]
               | [0-7] [0-7_]* [0-7]
               )
    ;

BinaryIntegerLiteral
    : '0' [bB] ( [01]
               | [01] [01_]* [01]
               )
    ;
\end{grammar}

Examples:

\begin{source}
153 // decimal literal
1_153 // decimal literal
0xBAD3 // hex literal
0xBAD_3 // hex literal
0o777 // octal literal
0b101 // binary literal
\end{source}

An underscore character `\_' can be used after a base prefix or between
successive digits in order to denote an integer literal and improve readability.
Underscore characters in such positions do not change the values of literals.
However, an underscore character must not be used as the very first symbol and
very last symbol of an integer literal.

Integer literals are of type \emph{int} if the value can be represented by a
32-bit number, otherwise it is of type \emph{long}. In variable and constant
declarations, an integer literal can be implicitly converted to other
\emph{integer} or \emph{char} type (see \See{nds:type-compatibility-with-initializer}\space ``Type Compatibility with Initializer'').
In all other places an explicit cast must be used (see \See{exp:cast-expressions}\space ``Cast Expressions'').


\hypertarget{floating-point-literals}{%
\subsubsection{Floating-Point Literals}\label{lex:floating-point-literals}}

\emph{Floating-point literals} represent decimal numbers and consist of
a whole-number part, a decimal or hexadecimal point, a fraction part and
an exponent.

\begin{grammar}
FloatLiteral
    : DecimalIntegerLiteral '.' FractionalPart? ExponentPart?
    | '.' FractionalPart ExponentPart?
    | DecimalIntegerLiteral ExponentPart
    ;

ExponentPart
    : [eE] [+-]? DecimalIntegerLiteral
    ;

FractionalPart
    : [0-9]
    | [0-9] [0-9_]* [0-9]
    ;
\end{grammar}

Examples:
\begin{source}
3.14
3.141_592
.5
1e10
\end{source}

In order to denote a floating-point literal, an underscore character `\_'
can be used after a base prefix or between successive digits for
readability. Underscore characters in such positions do not change the values
of literals. However, an underscore character must not be used as the very
first and very last symbol of an integer literal.

A floating-point literal is of the type \emph{double} (the type \emph{number} is an alias to \emph{double}). 
In variable and constant
declarations, a floating-point literal can be implicitly converted to the type
\emph{float} (see \See{nds:type-compatibility-with-initializer}\space ``Type Compatibility with Initializer'').


\hypertarget{boolean-literals}{%
\subsubsection{Boolean Literals}\label{lex:boolean-literals}}

There are two \emph{Boolean literal} values represented by the keywords
\code{true} and \code{false}.

\begin{grammar}
BooleanLiteral
    : 'true'
    | 'false'
    ;
\end{grammar}

Boolean literals are of the type \emph{boolean}.


\hypertarget{string-literals}{%
\subsubsection{String Literals}\label{lex:string-literals}}

\emph{String literals} consist of zero or more characters enclosed between
single or double quotes. The special form of a string literals are
\emph{template literals} (see \See{lex:template-literals}).

\begin{grammar}
StringLiteral
    : '"' DoubleQuoteCharacter* '"'
    | '\'' SingleQuoteCharacter* '\''
    ;

DoubleQuoteCharacter
    : ~["\\\r\n]
    | '\\' EscapeSequence
    ;
    
SingleQuoteCharacter
    : ~['\\\r\n]
    | '\\' EscapeSequence
    ;
    
EscapeSequence
    : ['"bfnrtv0\\]
    | 'x' HexDigit HexDigit
    | 'u' HexDigit HexDigit HexDigit HexDigit
    | 'u' '{' HexDigit+ '}'
    | ~[1-9xu\r\n]
    ;
\end{grammar}

An escape sequence always starts with the backslash character \verb+\+, followed by:

\begin{itemize}
\item
    \verb+"+ (double quote, U+0022)
\item
    \verb+'+ (single quote, U+0027)
\item
    \verb+b+ (backspace, U+0008)
\item
    \verb+f+ (form feed, U+000c)
\item
    \verb+n+ (linefeed, U+000a)
\item
    \verb+r+ (carriage return, U+000d)
\item
    \verb+t+ (horizontal tab, U+0009)
\item
    \verb+v+ (vertical tab, U+000b)
\item
    \verb+\\+ (backslash, U+005c)
\item 
    \verb+x+ and two hexadecimal digits, like \verb+7F+
\item 
    \verb+u+ and four hexadecimal digits, forming fixed Unicode escape sequence, like \verb+\u005c+
\item 
    \verb+u{+ and at least one hexadecimal digit, followed by \verb+}+, 
    forming a bounded Unicode escape sequence, like \verb+\u{5c}+
\item   
    any single character, except digits from `1' to `9' and characters `x', `u', CR and LF.
\end{itemize}

Examples:
\begin{source}
'Hello, world!'
"Hello, world!"
"\\"
""
"don't do it"
'don\'t do it'
'don\u0027t do it'
\end{source}

String literals are of type \emph{string}, which is a predefined reference type (\See{typ:the-string-class}).


\hypertarget{template-literals}{%
\subsubsection{Template Literals}\label{lex:template-literals}}

Multi-line string literals that can include embedded expressions are called
\emph{Template literals}. A template literal with embedded expression is
a \emph{template string}. A template string is not exactly a literal as
its value cannot be evaluated at compile-time. The evaluation of a template
string is called \emph{string interpolation} (see \See{exp:string-interpolation}).

\begin{grammar}
TemplateLiteral
    : '`' (BacktickCharacter | embeddedExpression)* '`'
    ;

BacktickCharacter
    : ~[`\\\r\n]
    | '\\' EscapeSequence
    | LineContinuation
    ;
\end{grammar}

For the grammar of \emph{embeddedExpression} see \See{exp:string-interpolation}.

Example of multi-line string:
\begin{source}
let sentence = `The is an example of multi-line string,
which should be enclosed in backticks (\`)`
\end{source}

Template literals are of type \emph{string}, which is a predefined reference type (\See{typ:the-string-class}).

\hypertarget{null-literal}{%
\subsubsection{Null (undefined) Literal}\label{lex:null-literal}}

There is only one literal, called \emph{null literal} that denotes a reference without
pointing at any entity. It is represented by keywords \code{null} and \code{undefined}.
Both keywords represent the same literal. It is recommended to use \code{null} 
for new source codes. In this specification \code{null} is used.

\begin{grammar}
NullLiteral
    : 'null' | 'undefined'
    ;
\end{grammar}

The null literal denotes the null reference which represents an absence of a
value and is a valid value only for nullable types (\See{typ:nullable-types}).
The null literal is of the type \emph{null} (see \See{typ:the-null-type}) and
is, by definition, the only value of this type.


\hypertarget{comments}{%
\subsection{Comments}\label{lex:comments}}

Text added in the stream to document and compliment the source code is a
\emph{Comment}. Comments are insignificant for the syntactic grammar.

\emph{Line comments} start with the character sequence `\code{//}' and stop at
the end of the line.

\emph{Multi-line comments} start with the character sequence `\code{/*}' and
stop with the first subsequent character sequence `\code{*/}'.

Comments can be nested.


\hypertarget{semicolons}{%
\subsection{Semicolons}\label{lex:semicolons}}

In most cases declarations and statements are terminated by a line separator
(see \See{lex:line-separators}). In some case a semicolon must be used to
separate syntax productions written in one line or to avoid ambiguity.

\begin{source}
function foo(x: number): number {
  x++;
  x *= x;
  return x
}
\end{source}

\begin{source}
let i = 1
i-i++  // one expression
i;-i++ // two expressions
\end{source}

\notwritten{TBD: specify exact rules - may use 262.ecma-international.org - sec-rules-of-automatic-semicolon-insertion}



