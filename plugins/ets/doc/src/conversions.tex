\hypertarget{conversions-and-contexts}{%
\section{Contexts and Conversions}\label{cvt:conversions-and-contexts}}

Every expression written in the \thelang{} programming language has a
type that is inferred at compile time. The \emph{target type} of an
expression is the type \emph{compatible} with the types expected in
most contexts that expression appears in.

There are two ways to facilitate the expression compatibility to its
surrounding context for more convenience:

\begin{enumerate}
\def\labelenumi{\arabic{enumi}.}
\item
  The type of some non-standalone expressions can be inferred from
  the target type (the expression types can be different in
  different contexts).
\item
  An implicit \emph{conversion} can be performed to ensure type
  compatibility, if the inferred expression type is different from
  the target type.
\end{enumerate}

A compile-time error occurs if neither produces the appropriate
expression type.

The form of an expression and the kind of its context indicate what
rules must apply to determine whether that expression is non-standalone,
and what such an expression's type and compatibility are in a particular
context. The target type can influence not only the expression type but,
in some cases, also the expression's runtime behavior in order to produce
the appropriate type of a value.

The rules that determine whether a target type allows an implicit
conversion vary for different kinds of context and expression
types, and for one particular case of the constant expression
value (\See{exp:constant-expressions}).

The conversion from type \emph{S} to type \emph{T} causes a type
\emph{S} expression to be treated as a type \emph{T} expression
at compile time.

Some cases of conversion can require a runtime action to check the
conversion validity or translate the runtime expression value into
a form that is appropriate for the new type \emph{T}.


\hypertarget{kinds-of-contexts}{%
\subsection{Kinds of Contexts}\label{cvt:kinds-of-contexts}}

Contexts can influence non-standalone expressions, and implicit
conversions may occur in various kinds of \emph{conversion contexts}.
The following kinds of contexts have different rules for non-standalone
expression typing and allow conversions in some, but not all expression
types:

\begin{itemize}
\item
  Assignment and call contexts: expression type must be compatible with
  the type of the target.
\begin{source}
  let variable: TargetType = expression /* Type of expression -> type of variable */
  foo (expression1, expression2) /* Type of expression -> type of function parameter */
\end{source}
\item
  Operator contexts: \code{string.+} (concatenation) and all primitive numeric
  type operators (\code{+}, \code{-} and so on).
\begin{source}
  let v1 = "a string" + 5 /* string, otherType pair */
  let v2 = 5 * 3.1415 /* two primary numeric types */
  let v3 = 5 - (new Number(6) as number)   /* two types */
\end{source}
\item
  Explicit casting contexts: conversion of an expression value to a type
  explicitly specified by a cast expression (\See{exp:cast-expressions}).
\begin{source}
  let v1 = "a string" as string
  let v2 = 5 as number
\end{source}
\end{itemize}


\hypertarget{assignment-call-contexts}{%
\subsubsection{Assignment and Call Contexts}\label{cvt:assignment-call-contexts}}

\emph{Assignment contexts} allow assigning (\See{exp:assignment}) an
expression value to a named variable, while the expression type must
be converted to the variable's type.

The term \label{cvt:assignment-compatible}\emph{assignment compatible}
means that one or more of the following conversions can be used to
convert the type of a valid expression as contained in the assignment
context to the type of the variable referred in the left part of the
assignment:

\begin{itemize}
\item
  identity conversion (\See{cvt:identity-conversion});
\item
  predefined numeric types conversions
  (\See{cvt:predefined-numeric-types-conversions}) (which can have
  a boxing conversion result in \emph{OutOfMemoryError});
\item
  reference types conversions (\See{cvt:reference-types-conversions});
\item
  function types conversions (\See{cvt:function-types-conversions});
\item
  raw types conversion (\See{cvt:raw-type-conversion}).
\end{itemize}

\emph{Call contexts} described by the \emph{assignment contexts}
rules allow assigning an argument value of a method or constructor
or function call (\See{cls:explicit-constructor-call}{exp:class-instance-creation-expressions}{exp:method-call-expression})
to a corresponding formal parameter.


\hypertarget{operator-contexts}{%
\subsubsection{Operator Contexts}\label{cvt:operator-contexts}}

\emph{String contexts} apply only to the non-\emph{string} operand of
a binary \code{+} operator where the other operand is a \emph{string}:

\begin{source}
Operator     :  expresssion1 + expression2
Operand types:    string     +  any_type
Operand types:  any_type     +  string
\end{source}

effectively transforms into

\begin{source}
Operator     :  expresssion1 + expression2
Operand types:    string     +  any_type.toString()
Operand types:  any_type.toString()  +  string
\end{source}

The \emph{string conversion}\label{cvt:string-conversion} can be of
the following kinds:
\begin{itemize}
\item
  Any reference type or enum type can convert directly to the
  \emph{string} type which is then performed as the \emph{toString()}
  method call.
\item
  Any primitive type must convert to a reference value (boxing
  \See{cvt:predefined-numeric-types-conversions}) before the
  \emph{toString()} method call is performed.
\end{itemize}

These contexts always have the type \emph{string} as the target type.

\emph{Numeric contexts} apply to the operands of an arithmetic operator;
\emph{numeric contexts} use combinations of predefined numeric type
conversions (see \See{cvt:predefined-numeric-types-conversions}) and
ensure that each argument expression can convert to the target type
\emph{T} while the arithmetic operation is defined for the values of
the type \emph{T}.

The {numeric contexts} are actually the forms of expressions as follows:

\begin{itemize}
\item
  Unary (\See{exp:unary-operators}),
\item
  Multiplicative (\See{exp:multiplicative-operators}),
\item
  Additive (\See{exp:additive-operators}),
\item
  Shift (\See{exp:shift-operators}),
\item
  Relational (\See{exp:relational-operators}),
\item
  Equality (\See{exp:equality-operators}),
\item
  Bitwise and Logical (\See{exp:bitwise-and-logical-operators}),
\item
  Conditional-And (\See{exp:conditional-and-operator}),
\item
  Conditional-Or (\See{exp:conditional-or-operator}).
\end{itemize}


\hypertarget{casting-contexts}{%
\subsubsection{Casting Contexts}\label{cvt:casting-contexts}}\label{cvt:casting-conversion}

The \emph{casting conversion} is the conversion of an operand of
a cast expression (\See{exp:cast-expressions}) to an explicitly
specified type by using any kind of conversion (\See{cvt:kinds-of-conversion}),
or a combination of such kinds, in a \emph{casting context}.

The \emph{casting conversion} for class and interface types allows
getting the subclass or subinterface from the variables declared by
the type of the superclass or superinterface:

\begin {source}
class Base {}
class Derived extends Base {}
let b: Base = new Derived()
let d: Derived = b as Derived 
\end{source}


\hypertarget{kinds-of-conversion}{%
\subsection{Kinds of Conversion}\label{cvt:kinds-of-conversion}}

The term `conversion' also describes any conversions allowed in a
particular context (for example, saying that an expression that
initializes a local variable is subject to `assignment conversion'
means that the rules for the assignment context define what specific
conversion is implicitly chosen for that expression).

The conversions allowed in \thelang{} are broadly grouped into the
following categories:

\begin{itemize}
\item
  \label{cvt:identity-conversion} Identity conversions: the type T
  is always compatible with itself.
\item
  Predefined numeric types conversions: all combinations allowed
  between numeric types.
\item
  Reference types conversions.
\item
  String conversions (See \See{cvt:string-conversion}).
\item
  Raw Types Conversion.
\end{itemize}

Conversions that are not in one of the above categories are forbidden.


\hypertarget{predefined-numeric-types-conversions}{%
\subsubsection{Predefined Numeric Types Conversions}\label{cvt:predefined-numeric-types-conversions}}

\emph{Widening conversions} cause no loss of information about the
overall magnitude of a numeric value (except conversions from integral
to floating-point types that can lose some least significant bits of
the value if the IEEE 754 `round-to-nearest' mode is used correctly
and the resultant floating-point value is properly rounded to the
integer value. Widening conversions never cause runtime errors.

\begin{itemize}
\item
  \emph{byte} -> \emph{short}, \emph{int}, \emph{long}, \emph{float} or \emph{double}.
\item
  \emph{short} -> \emph{int}, \emph{long}, \emph{float} or \emph{double}.
\item
  \emph{char} -> \emph{int}, \emph{long}, \emph{float} or \emph{double}.
\item
  \emph{int} -> \emph{long}, \emph{float} or \emph{double}.
\item
  \emph{long} -> \emph{float} or \emph{double}.
\item
  \emph{float} -> \emph{double}.
\end{itemize}

\emph{Narrowing conversions} (performed in compliance with IEEE 754
like in other programming languages) can lose information about the
overall magnitude of a numeric value potentially resulting in the
loss precision and range. Narrowing conversions never cause runtime errors.

\begin{itemize}
\item
  \emph{short} -> \emph{byte} or \emph{char}.
\item
  \emph{char} -> \emph{byte} or \emph{short}.
\item
  \emph{int} -> \emph{byte}, \emph{short} or \emph{char}.
\item
  \emph{long} -> \emph{byte}, \emph{short}, \emph{char} or \emph{int}.
\item
  \emph{float} -> \emph{byte}, \emph{short}, \emph{char}, \emph{int}
  or \emph{long}.
\item
  \emph{double} -> \emph{byte}, \emph{short}, \emph{char}, \emph{int},
  \emph{long} or \emph{float}.
\end{itemize}

\emph{Widening and narrowing} conversion is converting \emph{byte} to
an \emph{int} (widening), and the resultant \emph{int} to a \emph{char}
(narrowing).

\begin{itemize}
\item
  \emph{byte} -> \emph{char}.
\end{itemize}

\emph{Boxing and unboxing} conversion is converting reference into
value and value into reference for predefined types.

Boxing conversions treat primitive type expressions as expressions
of a corresponding reference type.

For example, the boxing conversion converts \emph{p} (\emph{p} being
a value of the type \emph{t}) into a reference \emph{r} of class and
type \emph{T} to have \emph{r.unboxed()} == \emph{p}.
An \emph{OutOfMemoryError} can occur as a result of this conversion
if the storage available for the creation of a new instance of the
wrapper class \emph{T} is insufficient.

Unboxing conversions treat reference type expressions as expressions
of a corresponding primitive type. The semantics of an unboxing conversion
is the same as that of the respective reference type's unboxed() function
call.

\begin{itemize}
\item
  \emph{byte} -> \emph{Byte}; \emph{Byte} -> \emph{byte}.
\item
  \emph{short} -> \emph{Short}; \emph{Short} -> \emph{short}.
\item
  \emph{char} -> \emph{Char}; \emph{char} -> \emph{Char}.
\item
  \emph{int} -> \emph{Int}; \emph{Int} -> \emph{int}.
\item
  \emph{long} -> \emph{Long}; \emph{Long} -> \emph{long}.
\item
  \emph{float} -> \emph{Float}; \emph{Float} -> \emph{float}.
\item
  \emph{double} -> \emph{Double}; \emph{Double} -> \emph{double}.
\end{itemize}

\hypertarget{reference-types-conversions}{%
\subsubsection{Reference Types Conversions}\label{cvt:reference-types-conversions}}

A \emph{widening reference conversion} from any subtype to the
supertype requires no special action at runtime
and therefore never causes an error.

\begin{source}
  interface BaseInterface {}
  class BaseClass {}
  interface DerivedInterface extends BaseInterface {}
  class DerivedClass extends BaseClass implements BaseInterface {}
  function foo (di: DerivedInterface) {
    let bi: BaseInterface = new DerivedClass() /* DerivedClass is a subtype of BaseInterface */
    bi = di /* DerivedInterface is a subtype of BaseInterface */
  }
\end{source}

In case of array types (\See{typ:array-types}) conversion also works according to widening style for array elements type.
Example below illustrates this

\begin{source}
  class Base {}
  class Derived extends Base {}
  function foo (da: Derived[]) {
    let ba: Base[] = da /* Derived[] is assigned into Base[] */
  }
\end{source}

Such array assignment may lead to runtime error (\emph{ArrayStoreError}) in case an object of incorrect type is put into an array. 
So, runtime system ensures type-safety applying run-time checks. Example below highlights this

\begin{source}
  class Base {}
  class Derived extends Base {}
  class AnotherDerived extends Base {}
  function foo (da: Derived[]) {
    let ba: Base[] = da // Derived[] is assigned into Base[]
    ba[0] = new AnotherDerived() // This assingment of array element will cause \emph{ArrayStoreError}
  }
\end{source}


\hypertarget{function-types-conversions}{%
\subsubsection{Function Types Conversions}\label{cvt:function-types-conversions}}

A \emph{function types conversion} occurs from one function type to another if the following conditions are held.

A throwing function type variable can have a non-throwing function value.

A compile-time error occurs if a throwing function value is assigned to
a non-throwing function type variable.

Types of arguments of both functions should be identical, but result types should be assignment compatible (\See{cvt:assignment-compatible}).

\begin{source}
  class Base {}
  class Derived extends Base {}
  let foo1: (p: Base) => Base = (p: Base): Derived => { return new Derived() } // OK
  let foo2: (p: Base) => Base = (p: Derived): Derived => { return new Derived() } // Compile-time error
\end{source}


\hypertarget{raw-type-conversion}{%
\subsubsection{Raw Types Conversion}\label{cvt:raw-type-conversion}}

Assuming \emph{G} is a generic type declaration with type parameters \emph{n},

\begin{source}
class|interface G<T1, T2, ... Tn> {}
\end{source}

any instantiation of G (\emph{G\textless Type\textsubscript{1},...,Type\textsubscript{n}\textgreater{}})
or its derived types can convert into G<>

\begin{source}
class|interface H<T1, T2, ... Tn> extends G<T1, T2, ... Tn> {}
let raw: G<> = new G<Type1, Type2, ... TypeN>
raw = new H<Type1, Type2, ... TypeN>
\end{source}
