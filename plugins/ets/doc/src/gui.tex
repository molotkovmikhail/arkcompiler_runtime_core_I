\hypertarget{gui}{%
\section{Support for GUI Programming}\label{inf:gui}}

This section describes built-in mechanisms that \thelang{} provides for
creating graphical user interface (GUI) programs. The section is based on
the ArkUI Declarative Core Language Specification available at
\url{https://gitee.com/arkui-finland/arkui-edsl-core-spec/blob/master/arkui-core-spec.md}.

ArkUI provides a set of extensions of the standard \theTS{} to declaratively
describe the programs' GUI and the interaction between the GUI components.
\thelang{} aims to adopt ArkUI syntax and semantics as long as they do not
contradict \thelang{}.

This section is under active development, and some of its parts can be still
under-specified. In such cases please refer to the original specification.


\hypertarget{gui-annotations}{%
\subsection{Annotations}\label{inf:gui-annotations}}

The term \emph{annotations} as used further in this section denotes special
language elements that alter semantics of classes (\emph{class-level anntotations}),
class fields (\emph{field-level annotations}) or functions (\emph{function-level annotations})
in a predefined manner. The syntax and semantics of each annotation are
defined in the below example that illustrates the idea in general:

\begin{source}
    @Component
    class MyComponent {
        @State @Watch("onValueChanged") value : string

        onValueChanged(value: string) : void {
            // ...
        }

        build() : void {
            // ...
        }
    }
\end{source}

\tobediscussed{Since the exact semantics of annotations as a language mechanism
is currently under active discussion and is not specified on intent. In
particular, it is not yet decided whether the annotations are a part of type
system or just some metadata "attached" to other language entities. This is to
be clarified in the upcoming updates of this specification.}


\hypertarget{gui-classes}{%
\subsection{GUI Classes}\label{inf:gui-classes}}

\emph{GUI classes} are used to define program's components. From the language
perspective, a GUI class is a restricted form of class type that is designed
to define GUI expressively and efficiently.

Each GUI class is required to implement its \emph{builder}, which is a method
responsible for visual rendering of components.

\begin{grammar}
    guiClassDeclaration
        : guiEntryAnnotation? guiComponentAnnotation 'class' identifier
          guiClassBody
        ;

    guiClassBody
        : '{'
           guiClassBodyDeclaration*
           guiMainComponentBuilderDeclaration
           guiClassBodyDeclaration*
          '}'
        ;

    guiClassBodyDeclaration
        : guiAccessModifier?
        (
        | guiClassFieldDeclaration
        | guiLifeCycleCallbackDeclaration
        | guiCustomComponentBuilderDeclaration
        | classFieldDeclaration
        | classMethodDeclaration
        )
        ;

    guiAccessModifier
        : 'private'
        ;

    guiClassFieldDeclaration
        : guiClassFieldAnnotationDeclaration
          variableDeclaration
        ;

    guiClassFieldAnnotationDeclaration
        : guiBuilderParamAnnotation
        | ( guiDataSynchronizationAnnotation guiWatchAnnotation? )
        ;

    guiDataSynchronizationAnnotation
        : guiConsumeAnnotation
        | guiLinkAnnotation
        | guiLocalStorageLinkAnnotation
        | guiLocalStoragePropAnnotation
        | guiObjectLinkAnnotation
        | guiPropAnnotation
        | guiProvideAnnotation
        | guiStateAnnotation
        | guiStorageLinkAnnotation
        | guiStoragePropAnnotation
        ;

    guiMainComponentBuilderDeclaration
        : guiAccessModifier?
          'build'
          '(' ')' (':' 'void')? block
        ;

    guiCustomComponentBuilderDeclaration
        : guiBuilderAnnotation
          guiAccessModifier?
          identifier
          '(' ')' (':' 'void')? block
        ;

    guiLifeCycleCallbackDeclaration
        : guiAccessModifier?
          ( 'aboutToAppear' | 'aboutToDisappear' )
          '(' ')' ':' 'void' block
        ;
\end{grammar}


\hypertarget{gui-builder-syntax-conventions}{%
\subsection{Builder Function Syntax Conventions}\label{inf:gui-builder-syntax-conventions}}

The following syntax conventions apply to any builder function (component's
main builder, component's custom builder or stand-alone global custom builder):

\begin{itemize}
    \item
        The required result of $C(\{...\})$ for any predefined or custom
        component \emph{C} is to initialize the component with the data from
        the $\{...\}$ block and render it. Concrete semantics depends on
        the implementation. For illustrative purpose, it may be expressed as
        $(new C(\{...\})).build()$, where the object literal $\{...\}$
        is handled as an initializer of the component's fields.
    \item
        The required result of $C() \{...\}$ for any predefined or custom
        component \emph{C} is to initialize the components and render it passing
        data from the $\{...\}$ block to the component's builder function.
        Concrete semantics depends on the implementation. For illustrative
        purpose, it may be expressed as $new C().build(\{...\})$, where the
        block $\{...\}$ is handled as a lambda to be passed as an argument to
        the builder.
\end{itemize}


\hypertarget{gui-builder-restrictions}{%
\subsection{Builder Function Restrictions}\label{inf:gui-builder-restrictions}}

Restrictions apply to any builder function (component's main builder, component's
custom builder or stand-alone global custom builder), and the following is not
allowed:

\begin{itemize}
    \item
        Declaring local variables.
    \item
        Constructing new objects.
    \item
        Function calls, except the following:
        \begin{itemize}
            \item
                Calling builders by name.
            \item
                Calling builders by reference stored in an \emph{@BuilderParam}
                annotated class field.
            \item
                Calling a \tobediscussed{predefined builder} \emph{Select}
                for conditional rendering.
            \item
                Calling a \tobediscussed{predefined builder} \emph{ForEach}
                for iterative rendering.
            \item
                Calling a function that
                \tobediscussed{does not mutate the program state} (please note
                that all logging functions do mutate the state and are
                therefore prohibited).
        \end{itemize}
    \item
        \tobediscussed{All other expressions are not allowed. - THE WORDING ABOVE MEANS THAT ONLY THE EXCEPTIONS ARE ALLOWED. MAYBE THEN WE LIST THE EXPRESSIONS ALLOWED, AND STATE THAT ALL OTHER ARE BANNED??}
\end{itemize}


\hypertarget{gui-annotations-list}{%
\subsection{Annotations List}\label{inf:gui-annotations-list}}

\hypertarget{gui-annotation-builder}{%
\subsubsection{@Builder Annotation}\label{inf:gui-annotation-builder}}

\emph{Function-level annotation for defining a custom builder} is applicable to:

\begin{itemize}
    \item
        Methods of GUI classes to define custom builder functions inside the
        GUI class.
    \item
        Stand-alone functions to define global custom builders.
\end{itemize}

\begin{grammar}
    guiBuilderAnnotation
        : '@' 'Builder'
        ;
\end{grammar}


\hypertarget{gui-annotation-builderparam}{%
\subsubsection{@BuilderParam Annotation}\label{inf:gui-annotation-builderparam}}

\emph{Field-level annotation for defining a reference to a custom builder}
is applicable only to member fields of GUI classes.

\begin{grammar}
    guiBuilderParamAnnotation
        : '@' 'BuilderParam'
        ;
\end{grammar}


\hypertarget{gui-annotation-component}{%
\subsubsection{@Component Annotation}\label{inf:gui-annotation-component}}


\emph{Class-level annotation for marking a class as a GUI class} is
applicable to any class as long as it complies with the limitations
imposed onto GUI classes.

\begin{grammar}
    guiComponentAnnotation
        : '@' 'Component'
        ;
\end{grammar}


\hypertarget{gui-annotation-consume}{%
\subsubsection{@Consume Annotation}\label{inf:gui-annotation-consume}}

\emph{@Consume} is a field-level annotation that establishes two-way
synchronization between a child component \emph{at an arbitrary nesting level}
and a parent component.

An \emph{@Consume}-annotated field in a child component shares the same value
with a field in the parent component; the parent component's
source field must be annotated with \emph{@Provide}.

The annotation \emph{@Consume} is applicable only to member fields of GUI classes.

\begin{grammar}
    guiConsumeAnnotation
        : '@' 'Consume'
        | '@' 'Consume' '(' StringLiteral ')'
        ;
\end{grammar}


\hypertarget{gui-annotation-entry}{%
\subsubsection{@Entry Annotation}\label{inf:gui-annotation-entry}}

\emph{Class-level annotation to indicate a top-most component on the page}
is applicable only to GUI classes.

\begin{grammar}
    guiEntryAnnotation
        : '@' 'Entry'
        | '@' 'Entry' '(' StringLiteral ')'
        ;
\end{grammar}


\hypertarget{gui-annotation-link}{%
\subsubsection{@Link Annotation}\label{inf:gui-annotation-link}}

\emph{@Link} is a field-level annotation that establishes two-way
synchronization between a child component and a parent component.

An \emph{@Link}-annotated field in a child component shares the same value with
a field in the parent component; the parent component's source field must be
annotated with \emph{@State}, \emph{@StorageLink} or \emph{@Link}.

The annotation \emph{@Link} is applicable only to member fields of
GUI classes.

\begin{grammar}
    guiLinkAnnotation
        : '@' 'Link'
        ;
\end{grammar}


\hypertarget{gui-annotation-localstoragelink}{%
\subsubsection{@LocalStorageLink Annotation}\label{inf:gui-annotation-localstoragelink}}

\emph{@LocalStorageLink} is a field-level annotation that establishes two-way
synchronization with a property inside a \emph{LocalStorage}.

The \emph{@LocalStorageLink} annotation is applicable only to member fields
of GUI classes.

\begin{grammar}
    guiLocalStorageLinkAnnotation
        : '@' 'LocalStorageLink' '(' StringLiteral ')'
        ;
\end{grammar}


\hypertarget{gui-annotation-localstorageprop}{%
\subsubsection{@LocalStorageProp Annotation}\label{inf:gui-annotation-localstorageprop}}

\emph{@LocalStorageProp} is a field-level annotation that establishes one-way
synchronization with a property inside a \emph{LocalStorage}. The synchronization
of value is uni-directional from the \emph{LocalStorage} to the annotated field.

The annotation \emph{@LocalStorageProp} is applicable only to member fields
of GUI classes.

\begin{grammar}
    guiLocalStoragePropAnnotation
        : '@' 'LocalStorageProp' '(' StringLiteral ')'
        ;
\end{grammar}


\hypertarget{gui-annotation-objectlink}{%
\subsubsection{@ObjectLink Annotation}\label{inf:gui-annotation-objectlink}}

\emph{@ObjectLink} is a field-level annotation that establishes two-way
synchronization with objects of \emph{@Observed}-annotated classes.

The annotation \emph{@ObjectLink} is applicable only to member fields of
GUI classes.

\begin{grammar}
    guiObjectLinkAnnotation
        : '@' 'ObjectLink'
        ;
\end{grammar}


\hypertarget{gui-annotation-observed}{%
\subsubsection{@Observed Annotation}\label{inf:gui-annotation-observed}}

\emph{@Observed} is a class-level annotation that establishes two-way
synchronization between instances of the \emph{@Observed}-annotated class
and \emph{@ObjectLink}-annotated member fields of GUI classes.

The annotation \emph{@Observed} is applicable only to non-GUI classes.

\begin{grammar}
    guiObservedAnnotation
        : '@' 'Observed'
        ;
\end{grammar}


\hypertarget{gui-annotation-prop}{%
\subsubsection{@Prop Annotation}\label{inf:gui-annotation-prop}}

The annotation \emph{@Prop} has the same semantics as \emph{@State} and only
differs in how the variable must be initialized and updated:

\begin{itemize}
    \item
        An \emph{@Prop}-annotated field must be initialized with a primitive or
        reference type value provided by its parent component; it must not be
        initialized locally.
    \item
        An \emph{@Prop}-annotated field can be modified locally, but
        the change does not propagate back to its parent component. Whenever
        that data source changes, the \emph{@Prop}-annotated field is updated,
        and any locally-made changes are overwritten; hence, the sync of the
        value is uni-directional from the parent to the owning component.
\end{itemize}

This annotation \emph{@Prop} is applicable only to member fields of GUI classes.

\begin{grammar}
    guiPropAnnotation
        : '@' 'Prop'
        ;
\end{grammar}


\hypertarget{gui-annotation-provide}{%
\subsubsection{@Provide Annotation}\label{inf:gui-annotation-provide}}

The annotation \emph{@Provide} has the same semantics as \emph{@State} with the
following additional features:

\begin{itemize}
    \item
        An \emph{@Provide}-annotated field automatically becomes available to
        all components that are descendents of the providing component.
\end{itemize}

The annotation \emph{@Provide} is applicable only to member fields of GUI classes.

\begin{grammar}
    guiProvideAnnotation
        : '@' 'Provide'
        | '@' 'Provide' '(' StringLiteral ')'
        ;
\end{grammar}


\hypertarget{gui-annotation-state}{%
\subsubsection{@State Annotation}\label{inf:gui-annotation-state}}

\emph{@State} is a field-level annotation, which indicates that the annotated
field holds a part of component's state. Changing any \emph{@State}-field
triggers component re-rendering.

The annotation \emph{@State} is applicable only to member fields of GUI classes.

\begin{grammar}
    guiStateAnnotation
        : '@' 'State'
        ;
\end{grammar}


\hypertarget{gui-annotation-storagelink}{%
\subsubsection{@StorageLink Annotation}\label{inf:gui-annotation-storagelink}}

\emph{@StorageLink} is a field-level annotation that establishes two-way
synchronization with a property inside an \emph{AppStorage}.

The annotation \emph{@StorageLink} is applicable only to member fields of GUI
classes.

\begin{grammar}
    guiStorageLinkAnnotation
        : '@' 'StorageLink' '(' StringLiteral ')'
        ;
\end{grammar}


\hypertarget{gui-annotation-storageprop}{%
\subsubsection{@StorageProp Annotation}\label{inf:gui-annotation-storageprop}}

\emph{@StorageProp} is a field-level annotation that establishes one-way
synchronization with a property inside an \emph{AppStorage}. The synchronization
of value is uni-directional from the \emph{AppStorage} to the annotated field.

The annotation \emph{@StorageProp} is applicable only to member fields of GUI
classes.

\begin{grammar}
    guiStoragePropAnnotation
        : '@' 'StorageProp' '(' StringLiteral ')'
        ;
\end{grammar}


\hypertarget{gui-annotation-watch}{%
\subsubsection{@Watch Annotation}\label{inf:gui-annotation-watch}}

\emph{@StorageProp} is a field-level annotation to specify a callback that
must be executed when the annotated field's value is changed.

The annotation \emph{@StorageProp} is applicable only to member fields of
GUI classes which are also annotated with:

\begin{itemize}
    \item
        \emph{@Consume}
    \item
        \emph{@Link}
    \item
        \emph{@LocalStorageLink}
    \item
        \emph{@LocalStorageProp}
    \item
        \emph{@ObjectLink}
    \item
        \emph{@Prop}
    \item
        \emph{@Provide}
    \item
        \emph{@State}
    \item
        \emph{@StorageLink}
    \item
        \emph{@StorageProp}
\end{itemize}

\begin{grammar}
    guiWatchAnnotation
        : '@' 'Watch' '(' StringLiteral ')'
        ;
\end{grammar}


\hypertarget{gui-differences}{%
\subsection{Differences from ArkUI}\label{inf:gui-differences}}

The differences from ArkUI are summarized below:

\begin{itemize}
    \item
        the keyword \code{struct} is not supported, \emph{class} must be used
        instead to specify GUI classes.
    \item
        the operator \code{\$\$} is not supported.
    \item
        the syntax \emph{if ... else ...} is not supported inside builders for
        conditional rendering. \tobediscussed{Predefined builder}
        \emph{Select(...)} must be used instead.
    \item
        the annotation \emph{@Entry} accepts its optional argument as a string
        literal and not as an identifier.
    \item
        the annotation \emph{@Extend} is not supported.
        \tobediscussed{Extension functions (to be specified later)}
        must be used instead to extend predefined components:
        \begin{source}
            extension function Text.fancy(color : string) {
                .backgroundColor(color)
            }
        \end{source}
\end{itemize}

\hypertarget{gui-example}{%
\subsection{Example}\label{inf:gui-example}}

\begin{source}
    // ViewModel classes ---------------------------

    let nextId : number = 0

    @Observed class ObservedArray<T> extends Array<T> {
        constructor(arr: T[]) {
            super(arr)
        }
    }

    @Observed class Address {
        street : string
        zip : number
        city : string

        constructor(street : string, zip: number, city : string) {
            this.street = street
            this.zip = zip
            this.city = city
        }
    }

    @Observed class Person {
        id_ : string
        name: string
        address : Address
        phones: ObservedArray<string>

        constructor(
            name: string,
            street : string,
            zip: number,
            city : string,
            phones: string[]
        ) {
            this.id_ = nextId as string
            nextId++

            this.name = name
            this.address = new Address(street, zip, city)
            this.phones = new ObservedArray<string>(phones)
        }
    }

    class AddressBook {
        me : Person
        contacts : ObservedArray<Person>

        constructor(me : Person, contacts : Person[]) {
            this.me = me
            this.contacts = new ObservedArray<Person>(contacts)
        }
    }

    // @Components -----------------------

    // Renders the name of a Person object and the first number in the phones
    // ObservedArray<string>
    // For also the phone number to update we need two @ObjectLink here,
    // person and phones, can not use this.person.phones.
    // Changes of inner Array not observed.
    // onClick updates selectedPerson also in AddressBookView, PersonEditView
    @Component
    class PersonView {

        @ObjectLink person : Person
        @ObjectLink phones : ObservedArray<string>

        @Link selectedPerson : Person

        build() {
            Flex({ direction: FlexDirection.Row, justifyContent: FlexAlign.SpaceBetween }) {
                Text(this.person.name)
                Select(this.phones.length != 0, Text(this.phones[0]), null)
            }
            .height(55)
            .backgroundColor(
                this.selectedPerson.name == this.person.name ? "#ffa0a0" : "#ffffff"
            )
            .onClick(() => {
                this.selectedPerson = this.person
            })
        }
    }

    // Renders all details
    // @Prop get initialized from parent AddressBookView, TextInput onChange
    // modifies local copies only on "Save Changes" copy all data from @Prop
    // to @ObjectLink, syncs to selectedPerson in other @Components.
    @Component
    class PersonEditView {

        @Consume addrBook : AddressBook

        /* Person object and sub-objects owned by the parent Component */
        @Link selectedPerson: Person

        /* editing on local copy until save is handled */
        @Prop name: string
        @Prop address : Address
        @Prop phones : ObservedArray<string>

        selectedPersonIndex() : number {
            return this.addrBook.contacts.findIndex(
                (person) => person.id_ == this.selectedPerson.id_
            )
        }

        build() {
            Column() {
                TextInput({ text: this.name})
                    .onChange((value) => {
                        this.name = value
                    })

                TextInput({text: this.address.street})
                    .onChange((value) => {
                        this.address.street = value
                    })

                TextInput({text: this.address.city})
                    .onChange((value) => {
                        this.address.city = value
                    })

                TextInput({text: this.address.zip.toString()})
                    .onChange((value) => {
                        const result = parseInt(value)
                        this.address.zip = isNaN(result) ? 0 : result
                    })

                Select(this.phones.length > 0,
                    ForEach(this.phones, (phone, index) => {
                        TextInput({text: phone})
                            .width(150)
                            .onChange((value) => {
                                console.log(index + ". " + value + " value has changed")
                                this.phones[index] = value
                            })
                    }, (phone, index) => index + "-" + phone),
                    null)

                Flex({
                    direction: FlexDirection.Row,
                    justifyContent: FlexAlign.SpaceBetween
                }) {
                    Text("Save Changes")
                        .onClick(() => {
                            // copy values from local copy to the provided ref
                            // to Person object owned by  parent Component.
                            // Avoid creating new Objects, modify properties of
                            // existing
                            this.selectedPerson.name           = this.name
                            this.selectedPerson.address.street = this.address.street
                            this.selectedPerson.address.city   = this.address.city
                            this.selectedPerson.address.zip    = this.address.zip
                            this.phones.forEach((phone : string, index : number) => {
                                this.selectedPerson.phones[index] = phone
                            })
                        })
                    Select(this.selectedPersonIndex() != -1,
                        Text("Delete Contact")
                            .onClick(() => {
                                let index = this.selectedPersonIndex()
                                console.log("delete contact at index " + index)

                                // delete found comtact
                                this.addrBook.contacts.splice(index, 1)

                                // determin new selectedPerson
                                index = (index < this.addrBook.contacts.length)
                                    ? index
                                    : index - 1

                                // if no contact left, set me as selectedPerson
                                this.selectedPerson = (index >= 0)
                                    ? this.addrBook.contacts[index]
                                    : this.addrBook.me
                            }),
                        null)
                }
            }
        }
    }

    @Component
    class AddressBookView {

        @ObjectLink me : Person
        @ObjectLink contacts : ObservedArray<Person>
        @State selectedPerson: Person = null

        aboutToAppear() {
            this.selectedPerson = this.me
        }

        build() {
            Flex({ direction: FlexDirection.Column, justifyContent: FlexAlign.Start}) {
                Text("Me:")
                PersonView({
                    person: this.me,
                    phones: this.me.phones,
                    selectedPerson: this.$selectedPerson})

                Divider().height(8)

                Flex({
                    direction: FlexDirection.Row,
                    justifyContent: FlexAlign.SpaceBetween
                }) {
                    Text("Contacts:")
                    Text("Add")
                        .onClick(() => {
                            this.selectedPerson = new Person ("", "", 0, "", ["+86"])
                            this.contacts.push(this.selectedPerson)
                        })
                }
                    .height(50)

                ForEach(this.contacts,
                    contact => {
                        PersonView({
                            person: contact,
                            phones: contact.phones,
                            selectedPerson: this.$selectedPerson
                        })
                    },
                    contact => contact.id_
                )

                Divider().height(8)

                Text("Edit:")
                PersonEditView({
                    selectedPerson: this.$selectedPerson,
                    name: this.selectedPerson.name,
                    address: this.selectedPerson.address,
                    phones: this.selectedPerson.phones
                })
            }
                .borderStyle(BorderStyle.Solid)
                .borderWidth(5)
                .borderColor(0xAFEEEE)
                .borderRadius(5)
        }
    }

    @Entry
    @Component
    class PageEntry {
        @Provide addrBook : AddressBook = new AddressBook(
            new Person(
                "Mighty Panda",
                "Wonder str., 8",
                888,
                "Shanghai",
                ["+8611122223333", "+8677788889999", "+8655566667777"]
            ),
            [
                new Person(
                    "Curious Squirrel",
                    "Wonder str., 8",
                    888,
                    "Hangzhou",
                    ["+8611122223332", "+8677788889998", "+8655566667776"]
                ),
                new Person(
                    "Wise Tiger",
                    "Wonder str., 8",
                    888,
                    "Nanjing",
                    ["+8610101010101", "+8620202020202", "+8630303030303"]
                ),
                new Person(
                    "Mysterious Dragon",
                    "Wonder str., 8",
                    888,
                    "Suzhou",
                    [ "+8610000000000", "+8680000000000"]
                ),
            ]);

        build() {
            AddressBookView({
                me: this.addrBook.me,
                contacts: this.addrBook.contacts,
                selectedPerson: this.addrBook.me
            })
        }
    }
\end{source}
