\hypertarget{modules-and-packages}{%
\section{Modules and Compilation Units}\label{pkg:modules-and-packages}\label{pkg:compilation-unit}}

Programs are structured as sequences of elements ready for compilation,
i.e., compilation units. Each compilation unit creates its own scope
(\See{nds:scopes}), and the compilation unit's variables, functions,
classes, interfaces or other declarations are only visible within such
scope unless explicitly exported.

A variable, function, class, interface or other declarations exported
from a different compilation unit must first be imported.

Compilation units are \textbf{separate modules} or \textbf{packages}.
Packages are described in the experimental section (\See{experimental:packages})

\begin{grammar}
compilationUnit
    : separateModuleDeclaration
    | packageDeclaration
    ;

packageDeclaration
    : packageModule+
    ;
\end{grammar}

All modules are stored in a file system or a database
(see \See{pkg:compilation-units-in-a-host-system}\space ``Compilation Units in a Host System'').

\hypertarget{separate-modules}{%
\subsection{Separate Modules}\label{pkg:separate-modules}}

A \emph{separate module} is a module that has no package header and
consists of two optional parts:

\begin{enumerate}
\def\labelenumi{\arabic{enumi}.}
\item
  Import declarations that enable referring imported declarations in a module
\item
  Top-level declarations, and
\item
  Top-level statements
\end{enumerate}

\begin{grammar}
separateModuleDeclaration
    : importDeclaration* (topDeclaration|topLevelStatements)*
    ;
\end{grammar}

Every module imports all exported entities from the predefined
package `std/core' automatically.

All entities from `std/core' are accessible as simple names, like the 
\emph{console} variable:

\begin{source}
// Hello, world! module
function main() {
  console.log("Hello, world!")
}
\end{source}

\hypertarget{compilation-units-in-a-host-system}{%
\subsection{Compilation Units in a Host System}\label{pkg:compilation-units-in-a-host-system}}

How modules and packages are created and stored is determined by a
host system.

The compiler and toolchain implementation of modules and packages stored
in a file system may impose restrictions on the placement of compilation
units for the sake of simplification.

In a simple implementation:

\begin{itemize}
\item
  a module (package module) is stored in a single file.
\item
  files corresponding to a package module are stored in a single folder.
\item
  a folder can store several separate modules (one source file to contain
  a separate module or a package module).
\item
  a folder that stores a single package must not contain separate module
  files or package modules from other packages.
\end{itemize}

\hypertarget{import-declarations}{%
\subsection{Import Declarations}\label{pkg:import-declarations}}

Import declarations import entities exported from other compilation units
and provide such entities with bindings in the current module.

Two parts of an import declaration are as follows:

\begin{enumerate}
\def\labelenumi{\arabic{enumi}.}
\item
  Import path that determines a compilation unit to import from;
\item
  Import binding that defines what entities and in what form (qualified or
  unqualified) the imported compilation unit can use.
\end{enumerate}

\begin{grammar}
importDeclaration
    : 'import' fileBinding|selectiveBindigns|defaultBinding 'from' importPath
    ;

fileBinding
    : '*' importAlias?
    | qualifiedName '.' '*'
    ;

selectiveBindigns
    : '{' importBinding (',' importBinding)* '}'
    ;

defaultBinding
    Identifier
    ;

importBinding
    : qualifiedName importAlias?
    ;

importAlias
    : 'as' Identifier
    ;

importPath
    : StringLiteral
    ;
\end{grammar}

Each binding adds a declaration or declarations to the package or separate module
scope (see \See{nds:scopes}).

Some import constructions are specific for packages and described in the experimental section (\See{experimental:packages}). 

\hypertarget{bind-all-with-unqualified-access}{%
\subsubsection{Bind All with Unqualified Access}\label{pkg:bind-all-with-unqualified-access}}

Unless an alias is set, the import binding `*' binds all entities exported
from the compilation unit defined by the \emph{import path} to the declaration
scope of the current module as simple names.

\begin{longtable}[]{@{}
  >{\raggedright\arraybackslash}p{(\columnwidth - 2\tabcolsep) * \real{0.5333}}
  >{\raggedright\arraybackslash}p{(\columnwidth - 2\tabcolsep) * \real{0.4667}}@{}}
\toprule()
\begin{minipage}[b]{\linewidth}\raggedright
Import
\end{minipage} & \begin{minipage}[b]{\linewidth}\raggedright
Usage
\end{minipage} \\
\midrule()
\endhead
\begin{source}
import * from "..."
\end{source} &
\begin{source}
let x = sin(1.0)
\end{source} \\
\bottomrule()
\end{longtable}


\hypertarget{bind-all-with-qualified-access}{%
\subsubsection{Bind All with Qualified
Access}\label{pkg:bind-all-with-qualified-access}}

The import binding `* as A' binds
the single named entity `A' to the
declaration scope of the current module. 

Use a qualified name that consists of `A'
and the name of entity: `\emph{A.name}' to access any entity exported from the
compilation unit defined by the \emph{import path}.

\begin{longtable}[]{@{}
  >{\raggedright\arraybackslash}p{(\columnwidth - 2\tabcolsep) * \real{0.5333}}
  >{\raggedright\arraybackslash}p{(\columnwidth - 2\tabcolsep) * \real{0.4667}}@{}}
\toprule()
\begin{minipage}[b]{\linewidth}\raggedright
Import
\end{minipage} & \begin{minipage}[b]{\linewidth}\raggedright
Usage
\end{minipage} \\
\midrule()
\endhead
\begin{source}
import * as Math from "..."
\end{source}&
\begin{source}
 let x = Math.sin(1.0)
\end{source} \\
\bottomrule()
\end{longtable}

This form of import is recommended for it simplifies the reading
and understanding of the source code.


\hypertarget{bind-a-simple-name}{%
\subsubsection{Bind a Simple Name}\label{pkg:bind-a-simple-name}}

There are two cases of the import binding `\emph{qualifiedName}' as follows:

\begin{itemize}
\item
  a simple name (like \emph{foo}); or
\item
  a name containing several identifiers (like \emph{A.foo}).
\end{itemize}

The import binding `\emph{ident}' binds an exported entity with the name
`\emph{ident}' to the declaration scope of the current module. The name `\emph{ident}' can
only correspond to several entities where `\emph{ident}' denotes several overloaded
functions (\See{xpm:overloading}).

The import binding `\emph{ident} as A' binds an exported entity (entities) with
the name A to the declaration scope of the current module.

The bound entity is not accessible as `\emph{ident}' because this binding does not
bind `\emph{ident}'.

Consider the following module:

\begin{source}
export const PI = 3.14
export function sin(d: number): number {}
\end{source}

The module's import path is irrelevant now:

\begin{longtable}[]{@{}
  >{\raggedright\arraybackslash}p{(\columnwidth - 2\tabcolsep) * \real{0.4659}}
  >{\raggedright\arraybackslash}p{(\columnwidth - 2\tabcolsep) * \real{0.5341}}@{}}
\toprule()
\begin{minipage}[b]{\linewidth}\raggedright
Import
\end{minipage} & \begin{minipage}[b]{\linewidth}\raggedright
Usage
\end{minipage} \\
\midrule()
\endhead

\begin{source}
import {sin} from "..."
\end{source} & 
\begin{source}
let x = sin(1.0)
let f: float = 1.0
\end{source} \\
\begin{source}
import {sin as Sine} from "..."
\end{source} &
\begin{source}
let x = Sine(1.0) // OK
let y = sin(1.0) /* Error `y' is not accessible */
\end{source} \\

\bottomrule()
\end{longtable}

One import statement can list several names:

\begin{longtable}[]{@{}
  >{\raggedright\arraybackslash}p{(\columnwidth - 2\tabcolsep) * \real{0.5333}}
  >{\raggedright\arraybackslash}p{(\columnwidth - 2\tabcolsep) * \real{0.4667}}@{}}
\toprule()
\begin{minipage}[b]{\linewidth}\raggedright
Import
\end{minipage} & \begin{minipage}[b]{\linewidth}\raggedright
Usage
\end{minipage} \\
\midrule()
\endhead
\begin{source}
import {sin, PI} from "..."
\end{source} &
\begin{source}
let x = sin(PI)
\end{source}
\\
\begin{source}
import {sin as Sine, PI} from "..."
\end{source}
&
\begin{source}
let x = Sine(PI)
\end{source}
\\
\bottomrule()
\end{longtable}

Complex cases with several bindings mixed for one import path are considered
in \See{pkg:several-bindings-for-one-import-path}\space ``Several Bindings for One Import Path''.

\hypertarget{several-bindings-for-one-import-path}{%
\subsubsection{Several Bindings for One Import Path}
\label{pkg:several-bindings-for-one-import-path}}

The same bound entities can use several import bindings. The same bound
entities can use one import declaration or several import declarations
with the same import path.

\begin{longtable}[]{@{}
  >{\raggedright\arraybackslash}p{(\columnwidth - 2\tabcolsep) * \real{0.4008}}
  >{\raggedright\arraybackslash}p{(\columnwidth - 2\tabcolsep) * \real{0.5992}}@{}}
\toprule()
\begin{minipage}[b]{\linewidth}\raggedright
In one import declaration
\end{minipage} & \begin{minipage}[b]{\linewidth}\raggedright
\begin{source}
import {sin, cos} from "..."
\end{source}
\end{minipage} \\
\midrule()
\endhead
In several import declarations &
\begin{source}
import {sin} from "..."
import {cos} from "..."
\end{source}
\\
\bottomrule()
\end{longtable}

No conflict occurs in the above example, because the import
bindings define disjoint sets of names.

The order of import bindings in an import declaration has
no influence on the outcome of the import.

The rules below prescribe what names must be used to add
bound entities to the declaration scope of the current
module if multiple bindings are applied to a single name:

\begin{longtable}[]{@{}
  >{\raggedright\arraybackslash}p{(\columnwidth - 4\tabcolsep) * \real{0.3333}}
  >{\raggedright\arraybackslash}p{(\columnwidth - 4\tabcolsep) * \real{0.3333}}
  >{\raggedright\arraybackslash}p{(\columnwidth - 4\tabcolsep) * \real{0.3334}}@{}}
\toprule()
\begin{minipage}[b]{\linewidth}\raggedright
Case
\end{minipage} & \begin{minipage}[b]{\linewidth}\raggedright
Sample
\end{minipage} & \begin{minipage}[b]{\linewidth}\raggedright
Rule
\end{minipage} \\
\midrule()
\endhead
A name is explicitly used without an alias in several bindings. &
\begin{source}
import {sin, sin} from "..."
\end{source}
& Ok. The compile-time warning is recommended. \\
A name is used explicitly without alias in one binding and implicitly
without an alias in another binding. &
\begin{source}
import {sin} from "..."
import * from "..."
\end{source}
& Ok.
No warning. \\
A name is explicitly used without alias and implicitly with alias. &
\begin{source}
import {sin} from "..."
import * as M from "..."
\end{source}
& Ok. Both the name and qualified name
can be used: sin and M.sin are accessible. \\
A name is explicitly used with alias and implicitly without alias. &
\begin{source}
import {sin as Sine} from "..."
import * from "..."
\end{source}
&
\begin{minipage}[t]{\linewidth}\raggedright
Ok. Only alias is accessible for the name, but not the original one:

\begin{itemize}
\item
  Sine is accessible
\item
  sin is not accessible
\end{itemize}
\end{minipage} \\
A name is explicitly used with alias and implicitly with alias.
&
\begin{source}
import {sin as Sine} from "..."
import * as M from "..."
\end{source}
&
\begin{minipage}[t]{\linewidth}\raggedright
Ok. Both variants can be used:

\begin{itemize}
\item
  Sine is accessible
\item
  M.sin is accessible
\end{itemize}
\end{minipage} \\
A name is explicitly used with alias several times. &
\begin{source}
import {sin as Sine, sin as SIN} from "..."
\end{source}
& Compile-time error.

Or warning? \\
\bottomrule()
\end{longtable}

\hypertarget{bind-default}{%
\subsubsection{Bind Default Import}\label{pkg:bind-default}}

Such binding allows to import the declaration which is exported from some
module as default export. Knowing the actual name of the declaration is not
required as the new name is given while importing.

\begin{source}
import DefaultExportedItemBindedName from ".../someFile"
function foo () {
  let v = new DefaultExportedItemBindedName() 
  // Object of class 'SomeClass' be created here
}

// SomeFile
export default class SomeClass {}
\end{source}


\hypertarget{import-path}{%
\subsubsection{Import Path}\label{pkg:import-path}}

An import path is a string literal (represented as a combination of
the slash character `/' and a sequence alpha-numeric characters)
that determines how an imported compilation unit must be placed.

Use the slash character `/' in import paths irrespective of the
host system. The backslash character is not used in this context.

In most file systems an import path looks like a file path;
\emph{relative} (see below) and \emph{non-relative} import paths
have different \emph{resolutions} that  map the import path to a
file path of the host system.

The compiler uses the following rule to define the kind of imported
compilation unit and the exact placement of the source code:

\begin{itemize}
\item
  Import the package that consists of all `*.\textless ext\textgreater'
  files in the folder if that \textbf{folder} is denoted by the last name
  in the resolved file path.
\item
  Otherwise, import the module placed in a source file
  `abs.\textless ext\textgreater' as the last name is considered to be
  a filename without extension.
\end{itemize}

A \textbf{relative import path} starts with `./' or `../' as in
the following examples:

\begin{source}
"./components/entry"
"../constants/http"
\end{source}

Resolving a relative import is relative to the importing file. Use relative
imports for the compilation units that maintain their relative location.

\begin{source}
package mytree
import * as Utils from "./mytreeutils"
\end{source}

Other import paths are \textbf{non-relative} as in the examples below:

\begin{source}
"/net/http"
"std/components/treemap"
\end{source}

Resolving a non-relative path depends on the compiler environment.
The definition of the compiler environment can be particularly
provided in a configuration file or environment variables.

Use the \emph{base URL} setting to resolve a path that starts with `/'.

Use the \emph{path mapping} in other cases.

The resolution details depend on the implementation.

For a simple implementation, use the configuration file \emph{stsconfig.json}
(file name, placement and format are implementation-specific) that contains
the following lines:

\begin{source}
"baseUrl": "/home/project",
"paths": {
"std": "/sts/stdlib"
}
\end{source}

In the example above `/net/http' is resolved to `/home/project/net/http'
and `std/components/treemap' to `/sts/stdlib/components/treemap'.

\hypertarget{default-import}{%
\subsection{Default Import}\label{pkg:default-import}}

A compilation unit automatically imports all entities exported from the
predefined package `core'. All entities from this package can be accessed
as simple names.

\begin{longtable}[]{@{}
  >{\raggedright\arraybackslash}p{(\columnwidth - 0\tabcolsep) * \real{1.0000}}@{}}
\toprule()
\begin{minipage}[b]{\linewidth}\raggedright
\begin{source}
function main() {

  let myException = new Exception { ... }
    // class Exception is defined in the `core' package

  console.log("Hello")
    // `console' variable is defined in the `core' package

}
\end{source}
\end{minipage} \\
\midrule()
\endhead
\bottomrule()
\end{longtable}


\hypertarget{top-level-declarations}{%
\subsection{Top-level Declarations}\label{pkg:top-level-declarations}}

\emph{Top-level type declarations} declare top-level types (class,
interface or enum) or top-level variables, constants or functions,
and can be exported.

\begin{grammar}
topDeclaration
    : ('export' 'default'?)?
    ( typeDeclaration
    | typeAlias
    | variableDeclaration
    | constantDeclaration
    | functionDeclaration
    )
    ;


\end{grammar}

\begin{source}
export let x: number[], y: number
\end{source}

\hypertarget{export-declarations}{%
\subsection{Export Declarations}\label{pkg:export-declarations}}

Top-level declarations can use export modifiers to `export' declared
names. A declared name is considered \emph{private} if not exported;
a \emph{private} declared name can be used only inside the package
it is declared in.

\begin{source}
export class Point {}
export let Origin = new Point(0, 0)
export function Distance(p1: Point, p2: Point): number {
  // ...
}
\end{source}

In addition, one of top-level declarations can be exported using default
export scheme. It allows not specifying the declared name while importing (see
\See{pkg:bind-default} for details). It is a compile time error if more than
one top-level declaration is marked as default.

\begin{source}
export default let PI = 3.141592653589
\end{source}

\hypertarget{top-level-statements}{%
\subsection{Top-Level Statements}\label{pkg:top-level-statements}}

Separate module may contain sequences of statements which are logically comprise one sequence of statements.

\begin{grammar}
topLevelStatements
    :
    statements
    ;
\end{grammar}

A module can contain any number of such top-level statements and they are merged
logically into a single sequence in the order of writing:

\begin{source}
    statements_1
    /* top-declarations */
    statements_2
\end{source}

The above sequence is equal to the following:

\begin{source}
    /* top-declarations */
    statements_1; statements_2
\end{source}

All top-level statements are executed once before the call to any other
function or access to any top-level variable of the separate module. This also works 
when \code{main} function of the module is used as the program entry point.

\begin{source}
    // Source file A 
    {
      console.log ("A.top-level statements")
    }

    // Source file B
    import * from "Source file A "
    function main () {
       console.log ("B.main")
    }
\end{source}

The output will be:

A.top-level statements
B.main

A compile-time error occurs if a top-level statement is the return statement (\See{stm:return-statements}).

\hypertarget{program-entry-point-main}{%
\subsection{Program Entry Point (\code{main})}\label{pkg:program-entry-point-main}}

A program (application) entry point is the top-level \code{main} function.
The \code{main} function must have either no parameters, or one parameter of  
string {[}{]} type.

A package can have several entry points. However, only one function can have
the \code{main} name, while all other functions' names must start with `main\$'.

An entry point with the \code{main} name is the default entry point.

Use a command line argument of the language toolchain to select non-default
entry points.

An example of a package with a single entry point: 

\begin{source}
package test
function main() {
  //
}
\end{source}

An example of a package with multiple entry points:

\begin{source}
package test
function main$Debug() { // additional entry point
  //
}

function main() { // default entry point
  //
}
\end{source}
