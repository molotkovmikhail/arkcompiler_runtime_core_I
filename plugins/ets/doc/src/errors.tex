\hypertarget{exceptions-and-errors}{%
\section{Errors}\label{exc:exceptions-and-errors}}

\thelang{} is designed to provide first-class support
in responding to and recovering from different erroneous conditions in a
program.

Two kinds of situations can occur and interrupt normal program
execution:

\begin{itemize}
\item
  Runtime errors, e.g., null pointer dereferencing, array bounds
  checking or division by zero;
\item
  Some operation completion failures; for example, the task of
  reading and processing data from a file on disk can fail in
  number of ways: the file not existing at a specified path,
  the file not having read permissions or else.
\end{itemize}

In this specification we use the terms
\begin{itemize}
\item
  `error' to denote runtime errors, and
\item
  `exception' to denote failures.
\end{itemize}

The difference between these two is that \emph{exceptions} are
the \emph{normal} and expected way for an operation to complete.
We expect a program to resolve some exceptions, and inform the
user if it cannot.

On the contrary, \emph{errors} indicate that there is a failure
of the program logic or even of the hardware. The program can
recover in some, but not all cases.

As a result, the \emph{exceptions} can be handled in a much
more effective manner than the \emph{errors}.

Some modern programming languages support only \emph{exceptions}; others
support only \emph{errors}. We are convinced that it is strongly necessary
to support both, therefore \thelang{} has `\emph{Exception}' and
`\emph{Error}' as the predefined types to be discussed below.

Exceptions are described in the experimental section (\See{experimental:exceptions}).

\hypertarget{errors}{%
\subsection{Errors}\label{exc:errors}}

\emph{Error} is the base class of all errors. Defining a new
error class is normally not required because error classes
for various cases (e.g., DivideByZeroError) are defined in
the standard library.

However, a developer can define a new error by using
\emph{Error} or any derived class as the base of the
new class. An example of the \emph{error} handling
is provided below:

\begin{source}
class DivideByZeroError extends Error {}

function divide(a: number, b: number): Number | null {
  try {
    return a / b
  }
  catch (x) {
    if (x instanceof DivideByZeroError)
      return null
    return 0
  }
}
\end{source}

In most case \emph{errors} are caused by the Virtual
Machine or the standard libraries.

It is not recommended to \emph{throw} errors in the
application code, because a structured way to handle
a range of unexpected situations is provided by
\emph{exceptions}.

However, \code{throw} statements (\See{stm:throw-statements})
allow to throw both.

Use \code{try} statements (\See{stm:try-statements}) to handle
\emph{errors} in a manner similar to \emph{exception}
handling.

Note that not every \emph{error} can be recovered.

\begin{source}
class Exception extends Error {}

function handleAll(
  actions : () => void,
  error_handling_actions : () => void,
  exception_handling_actions : () => void)
{
  try {
    actions()
  }
  catch (x) {
    if (x instanceof Exception)
      exception_handling_actions()
    else if (x instanceof Error)
      error_handling_actions()
  }
}
\end{source}
