\hypertarget{introduction}{%
\section{Introduction}\label{int:introduction}}


This document presents the complete information on the new common-purpose,
multi-paradigm programming language called \thelang{}.


\hypertarget{common-description}{%
\subsection{Common \thelang{} Description}\label{int:common-description}}

The \thelang{} language combines and supports many features that have
been in use in many well-known programming languages and proven to be
useful and powerful tools.

\thelang{} supports multiple programming paradigms as imperative,
object-oriented, functional and generic approaches, and combines them
in a safe and consistent way.

At the same time, \thelang{} does not support features that allow
software developers to write dangerous, unsafe and inefficient code.
In particular, the language uses the strong static typing principle
that allows no dynamic type changes as object types are determined
by their declarations and checked for semantic correctness at compile
time.

The following major aspects characterize the \thelang{} language as a whole:

\begin{itemize}
\item
Object orientation

The \thelang{} language supports conventional class-based
\emph{object-oriented approach} to programming. The major notions of
this approach are classes with single inheritance, interfaces as
abstractions to be implemented by classes, and virtual functions
(class members) with a dynamically dispatched overriding mechanism.
Common in many (if not all) modern programming languages, object-orientation
enables powerful, flexible, safe, clear and adequate software design.

\item
Modularity

The \thelang{} language supports the \emph{component programming approach}
which presumes that software is designed and implemented as a composition
of modules, or packages. A package in \thelang{} is a standalone,
independently compiled unit that combines various programming resources
(types, classes, functions and so on). A package can communicate with other
packages by exporting (some of) its resources to or importing from other
packages. This feature provides a high level of software development
process and software maintainability, supports flexible module reuse
and efficient version control.

\item
Genericity

Some program entities in \thelang{} can be \emph{type-parameterized}.
This means that an entity can represent a very high level (abstract)
concept, and providing more concrete information makes it specialized
for a particular use case. A classical illustration is the notion of
a list that represents the `idea' of the abstract data structure.
Providing additional information (the type of the list elements) can
turn this abstract notion into a concrete list.

Supported by many programming languages, a similar feature `generics'
or `templates') serves as the basis of the generic programming paradigm
and enables making programs and program structures more generic and
reusable.

It's worth mentioning that genericity is a completely compile-time
feature that does not reduce program efficiency. All actions related
to generic specializations are performed by the compiler.

\item
Multi-targeting

\thelang{} provides an efficient application development solution for
a wide range of devices. The language ecosystem is a developer-friendly
and uniform programming environment for popular platforms (cross-platform
development). It can generate optimized applications capable of operating
under the limitations of lightweight devices, or realizing the full
potential of any specific target hardware.

\end{itemize}

\thelang{} is designed as a part of the modern language manifold. To
provide an efficient and safely executable code, the language takes
flexibility and power from TypeScript and its predecessor, JavaScript,
and the static typing principle from Java and Kotlin.
The \thelang{}' overall design keeps its syntax style
similar to those languages, and some of its important constructs are
almost identical to theirs on purpose. In other words, there is a
significant \emph{common subset} of features of \thelang{} on the one
hand and of TypeScript, JavaScript, Java and Kotlin on the other.
Consequently, the \thelang{}' style and constructs are no puzzle
for TypeScript and Java users who can sense the meaning of
most constructs of the new language if not understand them completely.

This stylistic and semantic similarity permits smoothly migrating to
\thelang{} the applications originally written in TypeScript, Java or Kotlin.

Like its predecessors, \thelang{} is a relatively high-level language.
It means that the language provides no access to low-level machine
representations. As a high-level language, \thelang{} supports
automatic storage management: dynamically created objects are deallocated
automatically soon after they are no longer available, and their explicit
deallocation is not required.

\thelang{} is not merely a language, but rather a comprehensive software
development ecosystem that facilitates the creation of software solutions
in various application domains.

The \thelang{} ecosystem includes the language itself along with its
compiler, accompanying documents, guidelines, tutorials, the standard
library and a set of additional tools that perform automatic or semi-automatic
transition from other languages (currently, TypeScript and Java)
to \thelang{}.


\hypertarget{notation}{%
\subsection{Lexical and Syntactic Notation}\label{int:notation}}

This section introduces the notation (known as \emph{context-free grammar})
that this specification uses to define the lexical and syntactic structure
of a program.

The \thelang{} lexical notation defines a set of productions (rules) that
specify the structure of the elementary language parts called
\emph{tokens}. All tokens are defined in \See{lex:lexical-elements}\space
``Lexical Elements''. The set of tokens (identifiers, keywords, numbers/numeric
literals, operator signs, delimiters), special characters (white spaces and
line separators) and comments comprises the language's \emph{alphabet}.

The tokens defined by the lexical grammar are the terminal symbols of
syntactic notation, which defines a set of productions starting from the
goal symbol \emph{compilationUnit} (\See{pkg:compilation-unit}) that describes
how sequences of tokens can form syntactically correct programs.

Lexical and syntactic grammars are defined as a range of productions,
each comprised of an abstract symbol (\emph{nonterminal}) as its
\emph{left-hand side} and a sequence of one or more nonterminal and
\emph{terminal} symbols as its \emph{right-hand side}. Each production
has the `:' character as the separator between the left-hand and the
right-hand sides, and the `;' character  as its end marker.

Grammars draw the terminal symbols from a fixed width form. Starting
from a sentence that consists of a single distinguished nonterminal
(\emph{goal symbol}), grammars specify the language itself, i.e., the
set of possible sequences of terminal symbols that can result from
repeatedly replacing any nonterminal in the sequence for a right-hand
side of the production, to which that nonterminal is the left-hand
side.

There are additional symbols that can be used in right-hand size of
grammar productions along with terminal and non-terminal symbols.
Being no part of the terminal symbol sequences that comprise the
resultant program text, such additional symbols (sometimes called
\emph{metasymbols}) specify the structuring rules for terminal and
non-terminal sequences.

Grammars use the following metasymbols:

\begin{itemize}
\item
  Vertical line `|' to specify alternatives.
\item
  Question mark `?' to specify the optional appearance (zero or one time)
  of the preceding terminal or non-terminal.
\item
  Asterisk `*' to mark a terminal or non-terminal that can appear zero
  or more times.
\item
  Metasymbols `(' and `)' to enclose any sequence of terminals and/or
  non-terminals marked with the `?' or `*' metasymbols.
\end{itemize}

As an example, consider a production that specifies a list of expressions:

\begin{grammar}
expressionList
    : expression (',' expression)* ','?
    ;
\end{grammar}

This production introduces the following structure defined by the
non-terminal \emph{expressionList}: the expression list must consist of
the sequence of \emph{expression}s separated by the `,' terminal symbol.
The sequence must have at least one \emph{expression}, and the list is
optionally terminated by the `,' terminal symbol.

All grammar rules are presented in the Grammar section (\See{grm:grammar}).


