\hypertarget{types}{%
\section{Types}\label{typ:types}}

\thelang{} is a statically-typed language, and the type of every
declared entity and every expression is known at compile time
because it is either set explicitly by a developer, or inferred
implicitly by the compiler.

There are two categories of types:
\begin{enumerate}
\def\labelenumi{\arabic{enumi}.}
\item
  \emph{value types} (\See{typ:value-types}), and
\item
  \emph{reference types} (\See {typ:reference-types}).
\end{enumerate}

The types natural to \thelang{} are the \emph{predefined types}
(\See{typ:predefined-types}).

Other types that are additionally defined by a developer are
\emph{user-defined types}.


\hypertarget{predefined-types}{%
\subsection{Predefined Types}\label{typ:predefined-types}}

The set of the predefined types includes the following:
\begin{itemize}
\item
  basic numeric value type: \code{number}
\item
  additional numeric value types:
  \code{byte}, \code{short}, \code{int}, \code{long},
  \code{float}, \code{double} and \code{char}
\item
  boolean value type:
  \code{boolean}
\item
  predefined reference string type: 
  \code{string}
\item
  predefined class types: 
  \code{Object}, \code{never}, \code{string}, and \code{void}
\item
  each predefined value type corresponds to a predefined class
  type that wraps the value type:
  \code{Number},
  \code{Byte}, \code{Short}, \code{Int}, \code{Long},
  \code{Float}, \code{Double}, \code{Char} and \code{Boolean}.
\end{itemize}

The numeric value types and the boolean value type are called
\emph{primitive types}. Primitive type names are reserved and cannot
be used for user-defined type names.

The type \emph{double} is an alias to \emph{number}, 
the type \emph{Double} is an alias to \emph{Number}.


\hypertarget{user-defined-types}{%
\subsection{User-defined Types}\label{typ:user-defined-types}}

The \emph{user-defined} types include the following:

\begin{itemize}
\item
  class types (\See{cls:classes}),
\item
  interface types (\See{inf:interfaces}),
\item
  enumeration types (\See{enu:enums}),
\item
  array types (\See{typ:array-types}),
\item
  function types (\See{typ:function-types}),
\item
  nullable types (\See{typ:nullable-types}), and
\item
  type parameters (\See{gen:type-parameters}).
\end{itemize}


\hypertarget{types-category}{%
\subsection{Types by Category}\label{typ:category}}

The following table summarizes all \thelang{} types:
{\small
\begin{longtable}[]{p{2.5cm}|p{2.5cm}||p{2.5cm}|p{2.5cm}}
\toprule()
\multicolumn{2}{c||}{Predefined types} & \multicolumn{2}{c}{User-defined types} \\
\midrule()
Value types (Primitive types) & Reference types & Value types & Reference types \\
\midrule()
\endhead
\code{number}, & Number, & \code{enum} types & class types, \\
\code{byte}, \code{short}, & Byte, Short, & &  interface types, \\
\code{int}, \code{long},   & Int, Long,  & & array types, \\
\code{float}, \code{double}, & Float, Double,  & & function types,  \\
 \code{char}, \code{boolean} & Char, Boolean, & &  nullable types, \\
 & Object, string, &  &  type parameters \\
 & never, null, & & \\
 & void & & \\
\bottomrule()
\end{longtable}
}


\hypertarget{using-types}{%
\subsection{Using Types}\label{typ:using-types}}

To refer to a type in a source code, use:
\begin{itemize}
\item
  reserved name for a primitive type
\item
  type reference for a named type (\See{typ:named-types}) or a type
  alias (\See{nds:type-alias-declaration}).
\item
  an in-place type definition for an array type (\See{typ:array-types}),
  a function type (\See{typ:function-types}) or a nullable type
  (\See{typ:nullable-types}).
\end{itemize}

\begin{grammar}
type
    : predefinedType
    | typeReference 
    | arrayType 
    | functionType
    | nullableType
    | '(' type ')'
    ;

\end{grammar}

\begin{source}
let b: boolean   // using primitive value type name
let n: number    // using primitive value type name
let o: Object    // using predefined reference type name
let a: number[]  // using array type
\end{source}

Parentheses in types (where a type is a combination of array, function or
nullable types) are used to specify the required type structure.
Without parentheses, the \verb+'|'+ symbol that constructs a
nullable type has the lowest precedence.
% and right to left associativity.
\begin{source}
// a nullable array with elements of type string:
let a: string[] | null 
let s: string[] = []
a = s    // ok
a = null // ok, a is nullable

// an array with elements whose types are string or null:
let b: (string | null)[] 
b = null // error, b is not nullable
b = ["aa", null] // ok

// a function type that returns string or null
let c: () => string | null 
c = null // error, c is not nullable
c = (): string | null => { return null } // ok

// (a function type that returns string) or null
let d: (() => string) | null
d = null // ok, d is nullable
d = (): string => { return "hi" } // ok
\end{source}


\hypertarget{named-types}{%
\subsubsection{Named Types}\label{typ:named-types}}

Classes, interfaces, and enumerations are the named types that are
introduced through class declarations (\See{cls:classes}), interface
declarations (\See{inf:interfaces}) and enumeration declarations
(\See{enu:enums}) respectively.

Classes and interfaces that have type parameters are
\emph{generic types} (\See{gen:generics}).
Named types without type parameters are \emph{non-generic types}.

The \emph{type references} (\See{typ:type-references}) reference
named types by specifying their type names and (where applicable) type
arguments to be substituted for the type parameters of the named type.


\hypertarget{typ:type-references}{%
\subsubsection{Type References}\label{typ:type-references}}

A type reference uses a type name (either \emph{simple} or \emph{qualified},
see \See{nds:names}) or a type alias (\See{nds:type-alias-declaration})
to reference a type.

Optionally, each identifier in the name or alias can be followed by type
arguments (\See{gen:type-arguments}) if the referred type is a class
or an interface type.

\begin{grammar}
typeReference
    : typeReferencePart ('.' typeReferencePart)*
    ;

typeReferencePart
    : Identifier typeArguments?
    ;
\end{grammar}

\begin{source}
let map: Map<string, number>
\end{source}


\hypertarget{value-types}{%
\subsection{Value Types}\label{typ:value-types}}

The predefined \code{number} type (\See{typ:float-types}), integer types (\See{typ:integer-types}),
floating-point types (\See{typ:float-types}), the boolean
type (\See{typ:boolean-type-and-operations}) and user-defined
enumeration types (\See{enu:enums}) are \emph{value types}.

Such types' values do \emph{not} share state with other values.


\hypertarget{integer-types}{%
\subsubsection{Integer Types and Operations}\label{typ:integer-types}}

{\small
\begin{longtable}[]{@{}
  >{\raggedright\arraybackslash}p{(\columnwidth - 4\tabcolsep) * \real{0.1187}}
  >{\raggedright\arraybackslash}p{(\columnwidth - 4\tabcolsep) * \real{0.6000}}
  >{\raggedright\arraybackslash}p{(\columnwidth - 4\tabcolsep) * \real{0.2813}}@{}}
\toprule()
\textbf{Type} & \textbf{Set of values of the type} & \textbf{Corresponding class type} \\
\midrule()
\endhead
\code{byte} & All signed 8-bit integers ($-2^7$ to $2^7-1$) & \code{Byte} \\
\code{short} & All signed 16-bit integers ($-2^{15}$ to $2^{15}-1$) & \code{Short} \\
\code{int} & All signed 32-bit integers ($-2^{31}$ to $2^{31} - 1$) & \code{Int} \\
\code{long} & All signed 64-bit integers ($-2^{63}$ to $2^{63} - 1$) & \code{Long} \\
\code{char} & Unsigned integers from \code{\textbackslash{}U+0000} to \code{\textbackslash{}U+FFFF} inclusive, that is, from \code{0} to \code{65,535} & \code{Char} \\
\bottomrule()
\end{longtable}
}

\thelang{} provides a number of operators to act on integer values:

\begin{itemize}
\item
  Comparison operators that produce a value of the type \emph{boolean}:
  \begin{itemize}
  \item
    Numerical comparison operators `\code{\textless{}}', `\code{\textless{}=}',
    `\code{\textgreater{}}' and `\code{\textgreater{}=}'
    (\See{exp:numerical-comparison-operators});
  \item
    Numerical equality operators `\code{==}' and `\code{!=}'
    (\See{exp:value-equality-operators});
  \end{itemize}
\item
  Numerical operators that produce a value of the type \emph{int}
  or \emph{long};
  \begin{itemize}
  \item
    Unary plus `\code{+}' and minus `\code{-}' operators (\See{exp:unary-plus-operator}{exp:unary-minus-operator});
  \item
    Multiplicative operators `\code{*}', `\code{/}' and `\code{\%}' (\See{exp:multiplicative-operators});
  \item
    Additive operators `\code{+}' and `\code{-}' (\See{exp:additive-operators});
  \item
    Increment operator `\code{++}' used both as prefix (\See{exp:prefix-increment-operator})
    and postfix (\See{exp:postfix-increment-operator});
  \item
    Decrement operator `\code{-\/-}' used both as prefix (\See{exp:prefix-decrement-operator})
    and postfix (\See{exp:postfix-decrement-operator});
  \item
    Signed and unsigned shift operators `\code{\textless\textless{}}',
    `\code{\textgreater{}\textgreater{}}' and `\code{\textgreater{}\textgreater{}\textgreater{}}'
    (\See{exp:shift-operators});
  \item
    Bitwise complement operator `\code{\textasciitilde{}}' (\See{exp:bitwise-complement-operator});
  \item
    Integer bitwise operators `\code{\&}', `\code{\^{}}' and
    `\code{\textbar{}}' (\See{exp:integer-bitwise-operators-and});
  \end{itemize}
\item
  Conditional operator `\code{? :}' (\See{exp:conditional-expressions});
\item
  Cast operator (\See{exp:cast-expressions}), which can convert
  an integer value to a value of any specified numeric type;
\item
  The string concatenation operator `\code{+}' (\See{exp:string-concatenation-operator}),
  which (if a \code{string} operand and an integer operand are
  available) converts the integer operand to a \code{string} (the
  character of a \code{char} operand, or the decimal form of a
  \code{byte}, \code{short}, \code{int} or \code{long} operand)
  and then creates a concatenation of the two strings as a new
  \code{string}.
\end{itemize}

The classes \emph{Byte}, \emph{Short}, \emph{Int}, \emph{Long} and \emph{Char}
also predefine other useful constructors, methods and constants.

The operation implementation uses 64-bit precision if at least one
operand of an integer operator (other than a shift operator) is of
the type \emph{long}; the result of the numerical operator is then
of the type \emph{long}.

If the other operand is not \emph{long}, the numeric promotion
(\See{cvt:predefined-numeric-types-conversions}) must be used first to widen it
 to the type \emph{long}.

The operation implementation uses 32-bit precision if no operand
is of the type \emph{long}; the result of such numerical operator
is then of the type \emph{int}. Any operand that is not \emph{int},
the numeric promotion must be used first to widen it to the type
\emph{int}.

Any integer type value can be cast to or from any numeric type.

Casts between the integer types and the type \emph{boolean} are
not possible.

The integer operators cannot indicate an overflow or an underflow.

An integer operator can throw errors (\See{exc:exceptions-and-errors})
as follows:

\begin{itemize}
\item
  An integer division operator `\code{/}' \See{exp:division-operator}\space and an
  integer remainder operator `\code{\%}' \See{exp:remainder-operator}\space throw the
  \emph{ArithmeticError} if their right-hand operand is zero.
\item
  An increment operator `\code{++}' and a decrement operator `\code{-\/-}`
  \See{exp:additive-operators}\space throw an \emph{OutOfMemoryError} if
  boxing conversion (\See{cvt:predefined-numeric-types-conversions}) is
  required but the available memory is not sufficient to perform it.
  \notwritten{TBD: Do we need to clarify that let I: Int;
  x = I++ should clone the object?}
\end{itemize}


\hypertarget{float-types}{%
    \subsubsection{Floating-Point Types and Operations}\label{typ:float-types}}

\begin{longtable}[]{@{}
  >{\raggedright\arraybackslash}p{(\columnwidth - 4\tabcolsep) * \real{0.1187}}
  >{\raggedright\arraybackslash}p{(\columnwidth - 4\tabcolsep) * \real{0.6184}}
  >{\raggedright\arraybackslash}p{(\columnwidth - 4\tabcolsep) * \real{0.2629}}@{}}
\toprule()
\begin{minipage}[b]{\linewidth}\raggedright
\textbf{Type}
\end{minipage} & \begin{minipage}[b]{\linewidth}\raggedright
\textbf{The set of values of the type}
\end{minipage} & \begin{minipage}[b]{\linewidth}\raggedright
\textbf{Corresponding class type}
\end{minipage} \\
\midrule()
\endhead
float & The set of all IEEE-754 32-bit floating-point numbers & Float \\
number, double  & The set of all IEEE-754 64-bit floating-point numbers & Number, Double \\
\bottomrule()
\end{longtable}

A number of \thelang{} operators act on the floating-point type values:

\begin{itemize}
\item
  Comparison operators that produce a type \emph{boolean} value:
  \begin{itemize}
  \item
    Numerical comparison operators `\code{\textless{}}', `\code{\textless{}=}',
    `\code{\textgreater{}}' and `\code{\textgreater{}=}'
    (\See{exp:numerical-comparison-operators});
  \item
    Numerical equality operators `\code{==}' and `\code{!=}'
    (\See{exp:value-equality-operators});
  \end{itemize}
\item
  Numerical operators that produce values of the type \emph{float} or
  \emph{double}:
  \begin{itemize}
  \item
    Unary plus `\code{+}' and minus `\code{-}' operators (\See{exp:unary-plus-operator}{exp:unary-minus-operator});
  \item
    Multiplicative operators `\code{*}', `\code{/}' and `\code{\%}'
    (\See{exp:multiplicative-operators});
  \item
    Additive operators `\code{+}' and `\code{-}' (\See{exp:additive-operators});
  \item
    Increment operator `\code{++}' used both as prefix (\See{exp:prefix-increment-operator})
    and postfix (\See{exp:postfix-increment-operator});
  \item
    Decrement operator `\code{-\/-}' used both as prefix (\See{exp:prefix-decrement-operator})
    and postfix (\See{exp:postfix-decrement-operator});
  \end{itemize}
\item
  Conditional operator `\code{? :}' (\See{exp:conditional-expressions});
\item
  Cast operator \See{exp:cast-expressions}, which converts a floating-point
  value to a value of any specified numeric type;
\item
  The string concatenation operator `\code{+}' (\See{exp:string-concatenation-operator}),
  which (if a \code{string} operand and a floating-point operand are
  available) converts the floating-point operand to a \code{string}
  with a value represented in the decimal form (without the loss of
  information) and then creates a concatenation of the two strings
  as a new \code{string}.
\end{itemize}

The classes \emph{Float} and \emph{Double}, and the math library
also predefine other useful constructors, methods and constants.

The operation is a floating-point operation if at least one of the
operands in a binary operator is of the floating-point type, even
if the other operand is integer.

The operation implementation uses 64-bit floating-point arithmetic
if at least one operand of the numerical operator is of the type
\emph{double}; the result of the numerical operator is then a type
\emph{double} value.

If the other operand is not \emph{double}, the numeric promotion
(\See{cvt:predefined-numeric-types-conversions}) must be used first
 to widen it to the type \emph{double}.

The operation implementation uses 32-bit floating-point arithmetic
of no operand is of the type \emph{double}; the result of the
numerical operator is then a type \emph{float} value. If the other
operand is not \emph{float}, the numeric promotion must be used
first to widen it to the type  \emph{float}.

Any floating-point type value can be cast to or from any numeric type. 

Casts between floating-point types and the type \emph{boolean}
are not possible.

Operators on floating-point numbers (except the remainder operator, see
\See{exp:remainder-operator}) behave in compliance with the IEEE 754 Standard.
For example, \thelang{} requires the support of IEEE 754 \emph{denormalized}
floating-point numbers and \emph{gradual underflow} that make it easier to
prove a particular numerical algorithms' desirable properties.
Floating-point operations do not `flush to zero' if the calculated result is a
denormalized number.

\thelang{} requires that floating-point arithmetic behave as if the
floating-point result of every floating-point operator is rounded to the
result precision. An \emph{inexact} result is rounded to the representable
value nearest to the infinitely precise result; \thelang{} uses the
\emph{round to nearest} principle (the default rounding mode in IEEE-754)
and prefers the representable value with the least significant bit zero
out of any two equally near representable values.

\thelang{} uses \emph{round toward zero} to convert a floating-point value
to an integer (\See{cvt:predefined-numeric-types-conversions}), which in this case acts
as if the number is truncated and the mantissa bits discarded. The format's
value closest to and no greater in magnitude than the infinitely precise
result is chosen as the result of rounding toward zero.

A floating-point operation with overflow produces a signed infinity.

A floating-point operation with underflow produces a denormalized
value or a signed zero.

A floating-point operation with no mathematically definite result
produces NaN.

All numeric operations with a NaN operand result in NaN.

A floating-point operator (the increment operator `\code{++}' and
decrement operator `\code{-\/-}', see \See{exp:additive-operators}) can throw
the \emph{OutOfMemoryError} (see \See{exc:errors}) if boxing conversion
(see \See{cvt:predefined-numeric-types-conversions}) is needed but sufficient
memory is not available to perform it.


\hypertarget{numeric-hierarchy}{%
\subsubsection{Numeric Types Hierarchies}\label{typ:numeric-hierarchy}}

Integer types and floating-point types are the \emph{numeric types}.

Larger types include smaller types (of their values):

\begin{itemize}
\item
  \emph{double} \textgreater{} \emph{float} \textgreater{} \emph{long}
  \textgreater{} \emph{int} \textgreater{} \emph{short} \textgreater{} \emph{byte}
\end{itemize}

A smaller type's value can be assigned to a larger type's variable.

\hypertarget{boolean-type-and-operations}{%
\subsubsection{Boolean Type and Operations}\label{typ:boolean-type-and-operations}}

The type \emph{boolean} represents logical values \code{true}
and \code{false} that correspond to the class type \emph{Boolean}.

The boolean operators are as follows:

\begin{itemize}
\item
  Relational operators `\code{==}' and `\code{!=}' (\See{exp:relational-operators});
\item
  Logical complement operator `\code{!}' (\See{exp:logical-complement-operator});
\item
  Logical operators `\code{\&}', `\code{\^{}}' and `\code{\textbar{}}'
  (\See{exp:integer-bitwise-operators-and});
\item
  Conditional-and and conditional-or operators `\code{\&\&}'(\See{exp:conditional-and-operator})
  and `\code{\textbar\textbar{}}' (\See{exp:conditional-or-operator});
\item
  Conditional operator `\code{? :}' (\See{exp:conditional-expressions});
\item
  String concatenation operator `\code{+}' (\See{exp:string-concatenation-operator})
  that converts the \emph{boolean} operand to a \emph{string}
  (\code{true} or \code{false}) and then creates a concatenation
  of the two strings as a new \code{string}.
\end{itemize}

The conversion of an integral or floating-point expression \emph{x}
to a \emph{boolean} value must follow the \emph{C} language convention
that any nonzero value is \code{true} by the expression \emph{x != 0}.


\hypertarget{reference-types}{%
\subsection{Reference Types}\label{typ:reference-types}}

\emph{Reference types} can be of the following kinds:

\begin{itemize}
\item
  class types (\See{cls:classes}),
\item
  interface types (\See{inf:interfaces}),
\item
  array types (\See{typ:array-types}),
\item
  function types (\See{typ:function-types}),
\item
  nullable types (\See{typ:nullable-types}),
\item
  string type (\See{typ:the-string-class}),
\item
  never type (\See{typ:the-never-class}), null type
  (\See{typ:the-null-type}), void type (\See{typ:the-void-type}), and
\item
  type parameters (\See{gen:type-parameters}).
\end{itemize}


\hypertarget{objects}{%
\subsubsection{Objects}\label{typ:objects}}

An \emph{object} can be a \emph{class instance},
\emph{function instance} or an \emph{array}.

The pointers to these objects are called \emph{references}
or reference values.

A class instance creation expression
(\See{exp:class-instance-creation-expressions}) explicitly
creates a class instance.

Referring to a declared function by its name, qualified name
or lambda expression (\See{exp:lambda-expressions}) explicitly creates
a function instance.

An array creation expression explicitly creates an array (\See{exp:array-creation-expressions}).

A string literal initialization explicitly creates a string.

Other expressions can implicitly create a class instance
(\See{exp:class-instance-creation-expressions}) or an array
(\See{exp:array-creation-expressions}).

The operators on references to objects of class, interface
and type parameter are as follows:

\begin{itemize}
\item
  Field access that uses a qualified name 
  or a field access expression (\See{exp:field-access-expressions});
\item
  Call expression (\See{exp:method-call-expression}\space and
  \See{exp:function-call-expression});
\item
  Cast expression (\See{exp:cast-expressions});
\item
  String concatenation operator `\code{+}' (\See{exp:string-concatenation-operator})
  that (given a \emph{string} operand and a reference) calls the
  \emph{toString} method of the referenced object, converts the
  reference to a \emph{string} and creates a concatenation of
  the two strings as a new \code{string};
\item
  \code{instanceof} operator (\See{exp:type-comparison-operator-instanceof});
\item
  Reference equality operators `\code{==}' and `\code{!=}'
  (\See{exp:reference-equality-operators});
\item
  Conditional operator `\code{?:}' (\See{exp:conditional-expressions}).
\end{itemize}

Multiple references to an object are possible. 
Most objects have state. The state is stored in the field if an
object is an instances of class, or in a variable that is a
component of an array object.
If two variables contain references to the same object, and the
state of that object is modified in one variable's reference, then then
the state so modified can be seen in the other variable's reference.


\hypertarget{the-object-class}{%
\subsubsection{The \emph{Object} Class Type}\label{typ:the-object-class}}

The class \emph{Object} is a supertype of all other classes, interfaces, and arrays.

Thus all class, interface, and array types inherit (\See{cls:inheritance}) the class
\emph{Object}'s methods as summarized below:

\begin{itemize}
\item
  The method \emph{equals} defines a notion of object equality,
  which is based on the comparison of values rather than references.
\item
  The method \emph{hashCode} returns the integer hash code of the object.
\item
  The method \emph{toString} returns a \emph{string} representation
  of the object.
\end{itemize}


\hypertarget{the-string-class}{%
\subsubsection{\texorpdfstring{The \emph{string}
Type}{The String Type}}\label{typ:the-string-class}}

The \emph{string} type stores sequences of characters as
Unicode UTF-16 code units.

String is the predefined type and a part of \thelang{}
which the compiler gives some dedicated support with
extended semantics.

The type \emph{string} includes all string literals, e.g., `\emph{abc}'.
The value of a \emph{string} object cannot be changed after it is created,
i.e., \emph{string} object is immutable and can be shared.

The string concatenation operator `\code{+}' (\See{exp:string-concatenation-operator})
implicitly creates a new \emph{string} object if the result is not a constant
expression (\See{exp:constant-expressions}).

While the \emph{String} type is the alias to \emph{string}, it
is recommended to use \emph{string} in all cases.


\hypertarget{the-never-class}{%
\subsubsection{\texorpdfstring{The \emph{never}
Type}{The never Class Type}}\label{typ:the-never-class}}

The class \emph{never} is a subclass (see \See{smt:subtyping}) of any other
class.

The \emph{never} class has no instances and is used to represent
values that never exist (a function with this return type never
returns but only throws an error or exception).


\hypertarget{the-void-type}{%
\subsubsection{The \emph{void} Type}\label{typ:the-void-type}}

The \emph{void} type has a single value and is typically used
as the return type if a function returns no value of another
type (similarly to  type \emph{Unit} in other languages), or if void
is the type argument that instantiates a generic type where
a specific type parameter argument value is irrelevant.

Following code:

\begin{source}
function foo () {}
\end{source}

is equivalent to

\begin{source}
function foo (): void {}
\end{source}

\begin{source}
class A<G> {}
let a = new A<void>()
\end{source}

\hypertarget{array-types}{%
\subsubsection{Array Types}\label{typ:array-types}}

An array is an object that contains elements with similar data types.

The array elements are stored in a contiguous memory location,
the first element is stored at the 0th index, 2nd element at
the 1st and so on.

The basic two operations with array elements are getting elements
out of and putting elements into an array. Both use \code{[ ]} to
perform and index.

\begin{grammar}
arrayType
    : type '[' ']'
\end{grammar}

Examples:

\begin{source}
let a : number[] = [0, 0, 0, 0, 0] /* allocate array with 5 elements of type number */
a[1] = 7 /* put 7 as the 2nd element of the array, index of this element is 1 */
let y = a[4] /* get the last element of array 'a' */
\end{source}

A type alias can define a name for an array type
(see \See{nds:type-alias-declaration}\space ``Type Alias Declaration'').

\begin{source}
type Matrix = number[][] /* Two-dimensional array */
\end{source}

As an object an array is assignable to a variable of type Object:

\begin{source}
let a: number[] = [1, 2, 3]
let o: Object = a
\end{source}


\hypertarget{function-types}{%
\subsubsection{Function Types}\label{typ:function-types}}

A \emph{function type} can be used to express the expected
signature of a function. A \emph{function type} consists of the following:

\begin{itemize}
\item
  list of parameters (may be empty);
\item
  optional return type;
\item
  optional keyword \code{throws}.
\end{itemize}

\begin{grammar}
functionType
    : '(' ftParameterList? ')' ftReturnType? 'throws'?
    ;

ftParameterList
    : ftParameter (',' ftParameter)* (',' restParameter)?
    | restParameter
    ;

ftParameter
    : identifier ':' type
    ;

restParameter
    : '...' ftParameter
    ;

ftReturnType
    : '=>' type
    ;
\end{grammar}

The \emph{rest} parameter is described in \See{nds:rest-parameter}.

\begin{source}
let binaryOp: (x: number, y: number) => number
function evaluate(f: (x: number, y: number) => number) { }
\end{source}

A name for a function type can be defined by a type
alias (see \See{nds:type-alias-declaration}).

\begin{source}
type BinaryOp = (x: number, y: number) => number
let op: BinaryOp
\end{source}

The type \emph{void} (\See{typ:the-void-type}) is the implied
return type of a function type if \emph{ftReturnType} is
omitted.

The function type is the \emph{throwing function type} if it contains the
`\code{throws}' mark (see \See{exc:throwing-functions}\space ``Throwing Functions'').

Assignability and conversions for function types are described in 
\See{cvt:assignment-call-contexts}\space and \See{cvt:function-types-conversions}\space respectively.

\hypertarget{the-null-type}{%
\subsubsection{The \emph{null} (\emph{undefined}) Type}\label{typ:the-null-type}}

The \emph{null} type is used to create nullable types
(\See{typ:nullable-types}). The \emph{undefined} type is the alias to \emph{null}
and has exactly the same meaning.

The \emph{null} type's single value is represented by the \emph{null} or \emph{undefined} keyword.

It is not recommended to use the \emph{null} type as a type annotation.

\hypertarget{nullable-types}{%
\subsubsection{Nullable Types}\label{typ:nullable-types}}

\begin{grammar}
nullableType
    : type '|' 'null'
    | type '|' 'undefined'
    ;
\end{grammar}

\thelang{} has nullable and non-nullable type universes to support
null safety.

All predefined and user-defined type declarations create non-nullable
types that cannot have a \emph{null} value at runtime.

Use \emph{T | null} as a type to specify a nullable version of
type \emph{T}. Any redundant nullability specifiers are disregarded:

\emph{T | null | null  = T | null}.

\begin{quote}
A variable declared to have the type \emph{T | null} can hold values
of type \emph{T} and its derived types, or the value \emph{null}.
\end{quote}

\begin{source}
class T {}
class T_derived extends T {}

let x: T | null = new T_derived()
x = null
\end{source}

A compile-time error occurs if \emph{T} is not a reference type.

An operation that is safe with no regard to the presence or absence
of \emph{null} (e.g., re-assigning one nullable value to another)
can be used as-is for nullable types.

The following null-safe options exist for operations on
\emph{T | null} that can potentially violate null safety
(e.g., access to a property):

\begin{itemize}
\item
  Use safe operations:
  \begin{itemize}
  \item
    safe method call (see details in \See{exp:method-call-expression});
  \item
    safe field access expression (see details in \See{exp:field-access-expressions});
  \item
    safe array access expression (see details in \See{exp:array-access-expressions});
  \item
    safe function call (see details in \See{exp:function-call-expression});
  \end{itemize}
\item
  Downcast from \emph{T | null} to \emph{T}:
  \begin{itemize}
  \item
    cast expression (see details in \See{exp:cast-expressions});
  \item
    ensure-not-null expression (see details in \See{exp:ensure-not-null-expression});
  \end{itemize}
\item
  Supply a default value to use if \emph{null} is present:
  \begin{itemize}
  \item
    null-coalescing expression (see details in \See{exp:null-coalescing-expression}).
  \end{itemize}
\end{itemize}


\hypertarget{default-values-for-types}{%
\subsection{Default Values for Types}\label{typ:default-values-for-types}}

Some types use so-called default values for variables that have no
explicit initialization (\See{nds:variable-declarations}), including
the following:

\begin{itemize}
\item
  all primitive types (see the table below).
\item
  nullable reference types have the default value \emph{null}
  (\See{lex:literals}).
\end{itemize}

All other types, including reference types and enumeration types,
have no default values.

A variable of those types must be initialized explicitly with a value
before its first use.

The primitive types' default values are as follows:

\begin{longtable}[]{@{}
  >{\raggedright\arraybackslash}p{(\columnwidth - 2\tabcolsep) * \real{0.4389}}
  >{\raggedright\arraybackslash}p{(\columnwidth - 2\tabcolsep) * \real{0.5611}}@{}}
%\caption{Default values for primitive types}\tabularnewline
\toprule()
\begin{minipage}[b]{\linewidth}\raggedright
Data Type
\end{minipage} & \begin{minipage}[b]{\linewidth}\raggedright
Default Value
\end{minipage} \\
\midrule()
\endfirsthead
\toprule()
\begin{minipage}[b]{\linewidth}\raggedright
Data Type
\end{minipage} & \begin{minipage}[b]{\linewidth}\raggedright
Default Value
\end{minipage} \\
\midrule()
\endhead
\code{number} & \code{0 as number}\\
\code{byte} & \code{0 as byte}\\
\code{short} & \code{0 as short}\\
\code{int} & \code{0 as int}\\
\code{long} & \code{0 as long}\\
\code{float} & \code{+0.0 as float}\\
\code{double} & \code{+0.0 as double}\\
\code{char} & \code{\textquotesingle{}\textbackslash{}u0000\textquotesingle{}} \\
\code{boolean} & \code{false} \\
\bottomrule()
\end{longtable}

