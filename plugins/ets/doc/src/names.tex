\hypertarget{names-declarations-and-scopes}{%
\section{Names, Declarations and Scopes}\label{nds:names-declarations-and-scopes}}

\hypertarget{names}{%
\subsection{Names}\label{nds:names}}

A name refers to any declared entity.

\emph{Simple} names consist of a single identifier; \emph{qualified} names
consist of a sequence of identifiers with `\code{.}' tokens as separators.

A qualified name \emph{N.x} (where \emph{N} is a simple or qualified name,
and \emph{x} is an identifier) can refer to a \emph{member} of a package or
reference type (see \See {typ:reference-types}). \emph{N} can name:

\begin{itemize}
\item
  a package, of which \emph{x} is a member.
\item
  a reference type or a variable of a reference type (see
  \See {typ:reference-types}), and then \emph{x} names a member (a field or a method) of that type.
\end{itemize}


\protect\hyperlink{qualifiedName}{qualifiedName: (Identifier
\textquotesingle.\textquotesingle)* Identifier;}

\hypertarget{declarations}{%
\subsection{Declarations}\label{nds:declarations}}

Declarations introduce named entities in appropriate \emph{\textbf{declaration scope}} (See \See{nds:scopes}). 
Any declaration in its declaration scope must be \emph{distinguishable}.

\hypertarget{distinguishable-declarations}{%
\subsection{Distinguishable
Declarations}\label{nds:distinguishable-declarations}}

Declarations must be distinguishable in a declaration scope otherwise
a compile-time error is produced.

Declarations are distinguishable if:

\begin{itemize}
\item
  They have different names;
\item
  They are distinguishable by signatures (\See{xpm:distinguishable-by-signatures})
\end{itemize}

Example of declarations distinguishable by names:

\begin{source}
const PI = 3.14
const pi = 3
function Pi() {}
type IP = number[]
\end{source}

Example of declarations undistinguishable by names
that cause a compile time error:

\begin{source}
// The constant and the function have the same name.
const PI = 3.14               // compile-time error
function PI() { return 3.14 } // compile-time error

// The type and the variable have the same name.
class P {}
type Person = P              // compile-time error
let Person: Person           // compile-time error

// The field and the method have the same name.
class C {
  counter: number            // compile-time error
  counter(): number {        // compile-time error
    return this.counter
  }
}
\end{source}

\hypertarget{scopes}{%
\subsection{Scopes}\label{nds:scopes}\label{nds:accessible}}

The \emph{\textbf{scope}} of a name is the region of program code within
which the entity declared by that name can be referred to without the
qualification of the name. In other words, the name is accessible if it
can be used in some context, and the nature of its usage depends on the
kind of the name. A type name can be used to declare variables or constants;
a name of a function can be used to call that function.
The scope of a name depends on the context the name is declared in:

\begin{itemize}
\item\label{nds:package-access}
  A name declared on the package level (\emph{package level scope})
  is accessible throughout the entire package. The name can
  be accessed in other packages if exported.
\item\label{nds:module-access}
  \emph{Module level scope} is apllicable for separate modules only. 
  A name declared on the module level s accessible throughout the entire module.
  The name can be accessed in other packages if exported.
\item\label{nds:class-access}
  A name declared within a class (\emph{class level scope}) is accessible in
  that class and sometimes, depending on the access modifier, outside
  the class or by methods of derived classes.
\item\label{nds:interface-access}
  A name declared within an interface (\emph{interface level scope}) is
  accessible inside and outside that interface (default public).
\item\label{nds:enum-access}
  \emph{Enum level scope}: as every enumeration defines a type within a package or module then
  its scope is identical to the package or module level scope. All enumeration constants have the 
  same scope as the enumeration itself.  
\item\label{nds:class-or-interface-type-parameter-access}
  \emph{The scope of a type parameter} name in a class or interface
  declaration is that entire declaration, excluding static member
  declarations.
\item\label{nds:function-type-parameter-access}
  The scope of a type parameter name in a function 
  declaration is that entire declaration (\emph{function parameter scope}).
\item\label{nds:function-access}
  The scope of a name declared immediately within the body of a function
  (method) declaration is the body of that function declaration (\emph{method or function scope})
  from the place of declaration and up to the end of the body.
\item\label{nds:block-access}
  The scope of a name declared within a statement block is the body
  of such statement block from the place of declaration and up to
  the end of the block (\emph{block scope}).
\end{itemize}

\begin{source}
function foo() {
  let x = y // compile-time error -- y is not accessible
  let y = 1
}
\end{source}

\tobediscussed{TBD: 
- name accessibility in DS, order is irrelevant
- nested declaration space
- may be shadowed in nested}

Scopes of two names can overlap (e.g., when statements are nested),
in this case access to the outer name is not accessible because
the name with the innermost declaration takes precedence.

Class, interface and enum members are not directly accessible in a scope;
in order to access them, apply the dot operator `\code{.}' to an instance.

\hypertarget{type-declarations}{%
\subsection{Type Declarations}\label{nds:type-declarations}}

An interface declaration (\See{inf:interfaces}), a class declaration
(\See{cls:classes}) or an enum declaration (\See{enu:enums}) can be
type declarations.

\begin{grammar}
typeDeclaration
    : classDeclaration
    | interfaceDeclaration
    | enumDeclaration
    ;
\end{grammar}


\hypertarget{type-alias-declaration}{%
\subsection{Type Alias Declaration}\label{nds:type-alias-declaration}}

Type aliases enable using meaningful and concise notations by providing:
\begin{itemize}
\item
  names for anonymous types (array, function, nullable types); or
\item
  alternative names for existing types.
\end{itemize}

Scopes of type aliases are package level or module level scopes.
Names of all type aliases must be unique across all types in the
current context. 

\begin{grammar}
typeAlias: 
  'type' identifier typeParameters? '=' type
  ;
\end{grammar}

Meaningful names can be provided for anonymous types as follows:

\begin{source}
type Matrix = number[][]
type Handler = (s: string, no: number) => string
type Predicate<T> = (x: T) => Boolean
type NullableNumber = Number | null
\end{source}

Use a type alias if the existing type name is too long in order to
introduce a shorter new name, particularly for a generic type.

\begin{source}
type Dictionary = Map<string, string>
type MapOfString<T> = Map<T, string>
\end{source}

A type alias introduces no new type; it is only a new name and its
meaning is the same as the original type's.

\begin{source}
type Vector = number[]
function max(x: Vector): number {
  let m = x[0]
  for (let v of x)
    if (v > m)
      v = m
  return m
}

function main() {
  let x: Vector = [3, 2, 1]
  console.log(max(x)) // ok
}
\end{source}

\hypertarget{variable-and-constant-declarations}{%
\subsection{Variable and Constant Declarations}\label{nds:variable-and-constant-declarations}}

\hypertarget{variable-declarations}{%
\subsubsection{Variable Declarations}\label{nds:variable-declarations}}

A \emph{variable declaration} introduces a named variable which it can
assign an initial value.

\begin{grammar}
variableDeclarations
    : 'let' varDeclarationList ';'
    ;

variableDeclarationList
    : variableDeclaration (',' variableDeclaration)*
    ;

variableDeclaration
    : identifier ':' type initializer?
    | identifier initializer
    ;

initializer
    : '=' expression
    ;
\end{grammar}

Determine the type \emph{T} of a variable introduced by a variable
declaration as follows:

\begin{itemize}
\item
  \emph{T} is the type specified in a type annotation (if any) of the
  declaration. If the declaration also has an initializer, the initializer
  expression must be compatible with that \emph{T} (see \See{nds:type-compatibility-with-initializer}\space
  ``Type Compatibility with Initializer'').
\item
  If no type annotation is available, \emph{T} is inferred from the initializer
  expression (see \See{nds:type-inference-from-initializer}\space
  ``Type Inference from Initializer'').
\end{itemize}

\begin{source}
let a: number // ok
let b = 1 // ok, number type is inferred
let c: number = 6, d = 1, e = "hello" // ok

// ok, type of lambda and type of 'f' can be inferred
let f = (p: number) => b + p
let x // compile-time error -- either type or initializer
\end{source}

Every variable in a program must get an initial value before its value is used.

There are several ways to identify such an initial value:

\begin{itemize}
\item
  An initial value is explicitly specified using \emph{initializer}.
\item
  Each method or function parameter is initialized to the corresponding
  argument value provided by the caller of the method or function.
\item
  Each constructor parameter is initialized to the corresponding
  argument value provided by a class instance creation expression
  (\See{exp:class-instance-creation-expressions}) or an explicit
  constructor call (\See{cls:explicit-constructor-call}).
\item
  An exception parameter is initialized to the thrown object
  (\See{stm:throw-statements}) that represents the exception or error.
\item
  Each class, instance, local variable or array component is initialized
  with a \emph{default value} (\See{typ:default-values-for-types}) when
  it is created.
\end{itemize}

Otherwise, a compile-time error occurs and a variable is not initialized.


\hypertarget{constant-declarations}{%
\subsubsection{Constant Declarations}\label{nds:constant-declarations}}

A \emph{constant declaration} introduces a named variable with
a mandatory explicit value.

The value of a constant cannot be changed by using an assignment
expression (\See{exp:assignment}). However, its properties or items
can be modified if the constant is an object or array.

\begin{grammar}
constantDeclarations
    : 'const' constantDeclarationList ';'
    ;

constantDeclarationList
    : constantDeclaration (',' constantDeclaration)*
    ;

constantDeclaration
    : identifier (':' type)?  initializer
    ;
\end{grammar}

Determine the type \emph{T} of a constant declaration as follows:

\begin{itemize}
\item
  \emph{T} is the type specified in a type annotation (if any) of
  the declaration. The initializer expression must be compatible with that
  \emph{T} (see \See{nds:type-compatibility-with-initializer}\space ``Type
  Compatibility with Initializer'').
\item
  If no type annotation is available, \emph{T} is inferred from the
  initializer expression (see \See{nds:type-inference-from-initializer}\space
  ``Type Inference from Initializer'')
\end{itemize}

\begin{source}
const a: number = 1 // ok
const b = 1 // ok, int type is inferred
const c: number = 1, d = 2, e = "hello" // ok
const x // compile-time error -- initializer is mandatory
const y: number // compile-time error -- initializer is mandatory
\end{source}


\hypertarget{type-compatibility-with-initializer}{%
\subsubsection{Type Compatibility with Initializer}\label{nds:type-compatibility-with-initializer}}

If a variable or constant declaration contains the type annotation
\emph{T} and the initializer expression \emph{E}, then the type of \emph{E}
must be equal to that of \emph{T}; otherwise, one of the following assertions
must be true:

\begin{longtable}[]{@{}
  >{\raggedright\arraybackslash}p{(\columnwidth - 4\tabcolsep) * \real{0.2598}}
  >{\raggedright\arraybackslash}p{(\columnwidth - 4\tabcolsep) * \real{0.3146}}
  >{\raggedright\arraybackslash}p{(\columnwidth - 4\tabcolsep) * \real{0.4256}}@{}}
\toprule()
\begin{minipage}[b]{\linewidth}\raggedright
\textbf{T is}
\end{minipage} & \begin{minipage}[b]{\linewidth}\raggedright
\textbf{E is}
\end{minipage} & \begin{minipage}[b]{\linewidth}\raggedright
\textbf{Assertion}
\end{minipage} \\
\midrule()
\endhead
One of integer types & integer literal or compile-time constant
expression of some integer type & Value of \emph{E} is in bound of type \emph{T} \\
Type \emph{char} & integer literal & Value of \emph{E} is in bounds of type \emph{T} \\
Float type (\emph{float} or \emph{double}) & floating-point literal or compile-time
constant expression of a float type & Value of \emph{E} is in bounds of type \emph{T}.
\emph{This conversion may lead to a loss of precision, see ``Narrowing
Primitive Conversion''.} \\
Class type & of a class type & Type of \emph{E} is derived class of \emph{T} \\
\bottomrule()
\end{longtable}

An error must be thrown unless at least one of these conditions is fulfilled.


\hypertarget{type-inference-from-initializer}{%
\subsubsection{Type Inference from Initializer}\label{nds:type-inference-from-initializer}}

The type of a declared entity is:
\begin{itemize}
\item
  the type of the initializer expression if a variable or constant
  declaration contains no explicit type annotation; or
\item
  \emph{Object | null} if the initializer expression is a null
  literal (\emph{null}).
\end{itemize}

\hypertarget{function-declarations}{%
\subsection{Function Declarations}\label{nds:function-declarations}}

\emph{\textbf{Function declarations}} specify names, signatures and bodies
when introducing \textbf{named functions}.

\begin{grammar}
functionDeclaration
    : functionOverloadSignature*
	  modifiers? 'function' identifier
      typeParameters? signature block?
    ;

modifiers
    : 'native'
    | 'async'
    ;

\end{grammar}

Function \emph{overload signatures} allows to call a function in a different ways (\See{nds:function-overload-signatures}).

The body is omitted in a \emph{\textbf{native function}} (\See{experimental:native-functions}).

Specify type parameters if a function is declared as \emph{\textbf{generic}}.
See \See{gen:generics}\space ``Generics'' for details.

Native functions are described in the experimental section (\See{experimental:native-functions}).

Declare functions on the top-level (see \See{pkg:top-level-statements}\space
``Top-level Declarations'').

Use function expressions to define lambdas (see \See{exp:lambda-expressions}\space
``Lambda expressions'').

\hypertarget{signatures}{%
  \subsubsection{Signatures}\label{nds:signatures}}

A signature defines parameters (\See{nds:parameters}) and a return type (\See{nds:return-type})
of a function, method or constructor.

\begin{grammar}
signature
    : '(' parameterList? ')' returnType? throwMark?
    ;

returnType
    : ':' type
    ;

throwMark
    : 'throws'
    | 'rethrows'
    ;
\end{grammar}

For the details of \code{throws} and \code{rethrows} marks see
\See{exc:throwing-functions}\space and \See{exc:rethrowing-functions}\space respectively.

Overloading (\See{xpm:overloading}) is supported for functions and methods. The signatures of functions
and methods are important for their unique identification.

\hypertarget{parameters}{%
\subsubsection{Parameter List}\label{nds:parameters}}

A signature contains a \emph{parameter list} that specifies an identifier and a type of each parameter. 
Every parameter's type must be explicitly defined.
The last parameter of a function can be a \emph{rest parameter}.

\begin{grammar}
parameterList
    : parameter (',' parameter)* (',' restParameter)?
    | restParameter
    ;

parameter
    : identifier ':' type
    | optionalParameter
    ;

restParameter
    : '...' parameter
    ;
\end{grammar}

An \emph{optional parameter} (\See{nds:optional-parameters}) allows to omit
the corresponding argument when calling a function.
If a parameter is not an \emph{optional parameter}, then each function call
must contain a corresponding argument for it.
Non-optional parameters are called the \emph{required parameters}.

This function has required parameters:
\begin{source}
  function power(base: number, exponent: number): number {
    return Math.pow(base, exponent)
  }
  power(2, 3) // both arguments are required in the call
\end{source}

A compile-time error occurs if an optional parameter precedes a required parameter in the parameter list.


\hypertarget{optional-parameters}{%
\subsubsection{Optional Parameters}\label{nds:optional-parameters}}

There are two forms of \emph{optional parameters}.

\begin{grammar}
optionalParameter
    : identifier ':' type '=' expression
    | identifier '?' ':' type 
    ;
\end{grammar}

The first form contains an expression that specifies a \emph{default value}.
Such parameter is called a \emph{parameter with default values}.
If the corresponding argument to such parameter is omitted in a function call,
then this parameter has the default value.

\begin{source}
function pair(x: number, y: number = 7)
{
  console.log(x, y)
}
pair(1, 2) // prints: 1 2
pair(1) // prints: 1 7
\end{source}

The second form is a short notation for a parameter of a nullable type with \code{null} default value.

The following two functions can be used in the same way:
\begin{source}
function hello1(name: string | null = null) {}
function hello2(name?: string) {}

hello1() // 'name' has 'null' value
hello1("John") // 'name' has a string value
hello2() // 'name' has 'null' value
hello2("John") // 'name' has a string value
\end{source}

A compile-time error occurs in \verb+p?: T+ if \verb+T+ is not a reference type.

\hypertarget{rest-parameter}{%
\subsubsection{Rest Parameter}\label{nds:rest-parameter}}

A \emph{rest parameter} allows functions or methods to take unbounded
numbers of arguments.

\emph{Rest parameters} have the \verb+...+ symbol mark before a parameter name.

\begin{source}
function sum(...numbers: number[]): number {
  let res = 0
  for (let n of numbers)
    res += n
  return res
}
\end{source}

A compile-time error occurs if a rest parameter:
\begin{itemize}
\item
  is not the last parameter in a parameter list;
\item
  has a type that is not an array type.
\end{itemize}

A function that has a rest parameter of the type \verb+T[]+ can accept
any number of arguments of the type \verb+T+.

\begin{source}
function sum(...numbers: number[]): number {
  let res = 0
  for (let n of numbers)
    res += n
  return res
}

sum() // returns 0
sum(1) // returns 1
sum(1, 2, 3) // returns 6
\end{source}

Only one argument can be accepted if such argument of the \verb+T[]+ type
is prefixed with the \verb+...+ operator (\emph{spread} operator).

\begin{source}
function sum(...numbers: number[]): number {
  let res = 0
  for (let n of numbers)
    res += n
  return res
}

let x: number[] = [1, 2, 3]
sum(...x) // returns 6
\end{source}

\hypertarget{shadowing-parameters}{%
\subsubsection{Shadowing Parameters}\label{nds:shadowing-parameters}}

If the name of the parameter is identical to the name of the top-level
variable accessible witin the body of the function or method with such
paramter, then the name of that parameter shadows the name of the top-level
variable within the body of such function or method.

\begin{source}
class T1 {}
class T2 {}
class T3 {}

let variable: T1
function foo (variable: T2) {
    // 'variable' has type T2 and refers to the function parameter
}
class SomeClass {
  method (variable: T3) {
    // 'variable' has type T3 and refers to the method parameter
  }
}
\end{source}

\hypertarget{return-type}{%
\subsubsection{Return Type}\label{nds:return-type}}

If a function or a method return type is omitted, it is inferred from the
function or the method body.
A compile-time error occurs if a return type is omitted in the native functions (\See{experimental:native-functions}).

The current version of \thelang{} allows type inference of a return type
under certain conditions only.
A compile-time error occurs unless one of the following conditions is met:
\begin{itemize}
\item
    The return type is \emph{void} (see \See{typ:the-void-type}) if there
    is no return statement, or if all return statements have no expressions.
\item
    The return type is \emph{R} if there is at least one return statement
    with an expression, and the type of each expressions of each return
    statements is \emph{R}.
\item
    The return type is \emph{R} if there are \emph{k} return statements
    (\emph{k} is two or more) with expressions of types \emph{(T\textsubscript{1}, ..., T\textsubscript{k})},
    and \emph{R} is the \emph{least upper bound} (\See{smt:least-upper-bound})
    of these types.
\end{itemize}

In all other cases the type is not inferred (including when the expression in
the return statement is a call to a function or method whose return value type
is omitted), and a compile-time error occurs.

\hypertarget{function-overload-signatures}{%
\subsubsection{Function Overload Signatures}\label{nds:function-overload-signatures}}

The \thelang{} language allows specifying a function that can be called in different ways
by writing \emph{overload signatures}. To do so, several functions headers are
written that have the same name but different signatures, followed by the single
implementation function. See also \See{cls:method-overload-signatures}\space
for \emph{method overload signatures}.

\begin{grammar}
functionOverloadSignature
    : 'async'? 'function' identifier
      typeParameters? signature ';'
    ;
\end{grammar}

A compile-time error occurs if the function implementation is missing or does
not immediately follow the declaration.

A call of a function with overload signatures is always a call of the
implementation function.

The example below has two overload signatures defined: one is parameterless,
and the other has one parameter:
\begin{source}
function foo(): void; /*1st signature*/
function foo(x: string): void; /*2nd signature*/
function foo(x?: string): void {
    console.log(x)
}

foo() // ok, 1st signature is used
foo("aa") // ok, 2nd signature is used
\end{source}

The call of \code{foo()} is executed as a call of the implementation function
with the \code{null} argument, while the call of \code{foo(x)} is executed
as a call of the implementation function with the \code{x} argument

A compile-time error occurs if the signature of function implementation is not
\emph{overload signature compatible} with each overload signature.
It means that a call of each overload signature must be replacable for the
correct call of the implementation function.
Using optional parameters (\See{nds:optional-parameters}) or \emph{least upper bound}
types (\See{smt:least-upper-bound}) can achieve this.
See \See{smt:overload-signature-compatibility}\space for the exact semantic rules.

A compile-time error occurs unless all overload signatures are exported or
non-exported.

