\hypertarget{generics}{%
\section{Generics}\label{gen:generics}}

\hypertarget{generic-declarations}{%
  \subsection{Generic Declarations}\label{gen:generic-declarations}}

To be \emph{generic}, a class, interface or function must be parameterized
by at least one named type (a \emph{type parameter}); a type parameter can
also be used inside a class, interface or function as an ordinary type.

\begin{grammar}
typeParameters
    : '<' typeParameterList '>'
    ;

typeParameterList
    : typeParameter (',' typeParameter)*
    ;

typeParameter
    : ('in' | 'out')? identifier constraint?
    ;

constraint
    : 'extends' typeReference 
    ;
\end{grammar}

A generic class, interface, method, constructor or function defines
a set of parameterized classes, interfaces, methods, constructors
or functions respectively (\See{gen:parameterized-declarations}).
One type argument can only define one such set for each possible
parametrization of the type parameter section.


\hypertarget{type-parameters}{%
\subsection{Type Parameters}\label{gen:type-parameters}}

In a generic declaration a \emph{type parameter} is an unqualified
identifier that is used as a type.

For the scope of the type parameter see \See{nds:scopes}.

Type parameters are introduced by \emph{type parameters section}
declarations (\See{gen:generic-declarations}).

Each declared type parameter has a \emph{bound}
(\See{gen:type-parameter-bound}).

A type parameter \emph{T} \emph{directly depends} on a type parameter
\emph{S} in a type parameter section, if \emph{S} constraints \emph{T},
while \emph{T}:
\begin{itemize}
\item
  directly depends on \emph{S}; or
\item
  directly depends on a type parameter \emph{U} that depends on \emph{S}.
\end{itemize}

A compile-time error occurs if a type parameter in a type parameter section
depends on itself.


\hypertarget{type-parameter-bound}{%
\subsection{Type Parameter Bound}\label{gen:type-parameter-bound}}

Every type parameter's \emph{bound} follows the keyword \code{extends}.
A bound is a single type parameter \emph{T}; \emph{Object | null} is
assumed if no bound is declared.

\hypertarget{parameterized-declarations}{%
\subsection{Parameterized Declarations}\label{gen:parameterized-declarations}}

The set of parameterized declarations is defined by a generic class,
interface or function declaration (\See{gen:generic-declarations}).
A parameterized declaration \emph{G\textless T\textsubscript{1}, ..., T\textsubscript{n}\textgreater{}} (\emph{\textless T\textsubscript{1}, ..., T\textsubscript{n}\textgreater{}}
being the list of \emph{type arguments} applied to the generic declaration
\emph{G}) denotes a particular \emph{parameterization} of the
generic declaration.

The bounds \emph{B\textsubscript{1}, ..., B\textsubscript{n}}
correspond to type parameters \emph{F\textsubscript{1}, ..., F\textsubscript{n}}
of a generic declaration.
\emph{T\textsubscript{i }} is a subtype of each bound type
\emph{B\textsubscript{i}} (\See{smt:subtyping}); all such subtypes
of the type listed in the corresponding bound have each type argument
\emph{T\textsubscript{i}} of a parameterized declaration ranging over
them.

For a parameterized declaration
\emph{G\textless T\textsubscript{1}, ..., T\textsubscript{n}\textgreater{}}
to be \emph{well-formed}, all of the following must be true:

\begin{itemize}
\item
  The generic declaration name is \emph{G}.
\item
  The numbers of type arguments and of \emph{G}'s type parameters is the same.
\item
  All type arguments are subtypes of a corresponding type parameter bound.
\end{itemize}

A compile-time error occurs if a parameterized declaration is not
well-formed.

The generic version is included (unless explicitly excluded) with
a class type, interface type or a function in this specification.

Any two parameterized declarations are considered \emph{provably distinct} if:

\begin{itemize}
\item
  Both are parameterizations of distinct generic declarations; or
\item
  Any of their type argument are provable distinct.
\end{itemize}


\hypertarget{type-arguments}{%
\subsection{Type Arguments}\label{gen:type-arguments}}

Type arguments can be reference types or wildcards.

\begin{grammar}
typeArguments
    : '<' typeArgumentList? '>'
    ;
\end{grammar}

typeArguments denote a raw type (\See{gen:raw-types}) unless
a typeArgumentList is provided.

A compile-time error occurs if type arguments are omitted in a
parametrized function.

\begin{grammar}
typeArgumentList
    : typeArgument (',' typeArgument)*
    ;

typeArgument
    : typeReference
    | arrayType
    | wildcardType
    ;

wildcardType
    : 'in' typeReference
    | 'out' typeReference?
    ;
\end{grammar}

The variance for type arguments can be specified with wildcards
(\emph{use-site variance}). It allows specifying the type variance
on the corresponding type argument and changing the type variance
of an \emph{invariant} type parameter.

\textbf{NOTE: This description of use-site variance modifiers is preliminary.
The details are to be specified in the future language versions.
}

The syntax to signify a covariant type argument, or a wildcard with
an upper bound (emph{T} is a \emph{typeReference}) is as follows:

\emph{out T}

This syntax restricts the methods available so that the only methods
that can be accessed are those not using \emph{T}, or using it in
out-position only.

The syntax to signify a contravariant type argument, or a wildcard
with a lower bound (emph{T} is a \emph{typeReference}) is as follows:

\emph{in T}

This syntax restricts the methods available so that the only methods
that can be accessed are those not using \emph{T}, or using it in
in-position only.

The unbounded wildcard \emph{out} and the wildcard
\emph{out Object | null} are equivalent.

A compile-time error occurs if:
\begin{itemize}
\item
  a wildcard is used in a parameterization of a function;
\item
  a covariant wildcard is specified for a contravariant type parameter;
\item
  a contravariant wildcard is specified for a covariant type parameter.
\end{itemize}

The rules below apply for the subtyping (\See{smt:subtyping}) of two
non-equivalent types \emph{A \textless: B} and an invariant type parameter
\emph{F} in case of use-site variance:

\begin{itemize}
\item
  \emph{T\textless out A\textgreater{} \textless: T\textless out B\textgreater{}}
\item
  \emph{T\textless in A\textgreater{} :\textgreater{} T\textless in B\textgreater{}}
\item
  \emph{T\textless A\textgreater{} \textless: T\textless out A\textgreater{}}
\item
  \emph{T\textless A\textgreater{} \textless: T\textless in A\textgreater{}}
\end{itemize}

Any two type arguments are considered \emph{provably distinct} if:

\begin{itemize}
\item
  The two arguments are not of the same type, and neither is a type
  parameter nor a wildcard; or
\item
  One type argument is a type parameter or wildcard with an upper
  bound   of \emph{S}, while the other \emph{T} is not a type
  parameter or wildcard, and neither is a subtype of another
  (\See{smt:subtyping}); or
\item
  Each type argument is a type parameter or wildcard with upper
  bounds S and T, and neither is a subtype of another (\See{smt:subtyping}).
\end{itemize}


\hypertarget{raw-types}{%
\subsection{Raw Types}\label{gen:raw-types}}

\begin{grammar}
rawType
    : identifier '<' '>'
    ;
\end{grammar}

\textbf{Raw types are added below to simplify the migration from other languages that support this notion.
The future versions of the language will disallow the use of raw types.}

A raw type is one of the following:

\begin{itemize}
\item
  The reference type formed by taking a generic type
  declaration's name without the accompanying
  type argument list.
\item
  An array type with a raw type element.
\item
  A non-\emph{static} member type of a raw type \emph{R} not
  inherited from an \emph{R}'s superclass or
  superinterface.
\end{itemize}

Only a generic class or interface type can be a raw type.

Raw type superclasses and superinterfaces are the raw versions
of respective superclasses and superinterfaces of any generic
type instantiations.

The type of a constructor (\See{cls:constructor-declaration}),
instance method (class (\See{cls:instance-methods}) and
interface (\See{inf:abstract-method-declarations}\space and
\See{inf:default-method-declarations})), as well as non-\emph{static}
field (\See{cls:field-declarations}) of a raw type \emph{C} that is
not inherited from respective superclasses or superinterfaces, is
the raw version of its type in the generic declaration corresponding
to \emph{C}.

The type of a \emph{static} method or \emph{static} field of a raw
type \emph{C} and the type of a method or field in the generic
declaration corresponding to \emph{C} are the same.


A compile-time error occurs if:
\begin{itemize}
\item
  Type arguments are passed to a non-\emph{static} type member of a raw
  type that is not inherited from its superclasses or superinterfaces.
\item
  An attempt is made to use a type member of a parameterized type
  as a raw type.
\end{itemize}


A class' supertype can be a raw type.

Member accesses for the class are treated as normal, while member
accesses for the supertype are treated as for raw types.
Calls to \emph{super} are treated as method calls on a raw type
in the class constructor.

The use of raw types is a concession for the sake of the legacy code
compatibility, and is to be disallowed in the future versions of the
language.

The use of a raw type always results in compile-time diagnostics in
order to ensure that potential typing rules violations are flagged.
