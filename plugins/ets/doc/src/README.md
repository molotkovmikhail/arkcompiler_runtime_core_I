## LaTeX source for language specification

TL;DR

> Install [TeXLive](https://www.tug.org/texlive/) or [MiKTeX](https://miktex.org/), then run
> `xelatex spec.tex` three times and you should get `spec.pdf`.
> For simplicity install all packages in LaTeX distributins (in TeXLive it is called
> `full scheme`.

This folder contains LaTeX sources of STS language specification document.

Source files are intended to be built with XeLaTeX (part of XeTeX package)

### How to build

To build `*.pdf` file issue command

```
$ xelatex spec.tex
```

You may see LaTeX warning:

```
LaTeX Warning: There were undefined references.
```

or


```
LaTeX Warning: Label(s) may have changed. Rerun to get cross-references right.
```

This means TOC, index, heading numbering and so on were not correctly calculated.
Simply run build command once again. It is normal LaTeX (and derived systems) behaviour.

Multiple additional files will be generated, like `*.aux`, `*.log`. They're added to `.gitignore`
and can be deleted after build.

### Source code structure

Specification is divided into three parts:

* Main file (`spec.tex`)
* Class file (`stsspec.cls`)
* Chapter files

#### Main file
This file is an entry point of a whole document. It contains:
* authors list
* `\include{}` clauses for chapter files
* date of modification

#### Authors list
List of authors is a part of title page and is bound to `\author{}` command.

#### Chapters
Each chapter is stored in its own `*.tex` file and is a part of whole document.
Chapters are included in order of appearance of include clauses by `\include{}` command.
Headings ordering and numbering strictly follows this order.

It is not necessary to use `\usepackage{}` or similar things in files.
If something is required, use class file instead (see below).

#### Date of modification
LaTeX automatically inserts current date to title page if date is not set.
To change this behaviour we define `\specDate{}` command, returning wanted date.

This date if further used in title page and possibly in other places.

### Class file
For convenience purpose and for aggregation of all specification's LaTeX stuff, we have
created `LaTeX class`, called `stsspec.cls`.

It contains style information for grammar rules, source code snippets, headings.
Used package are listed (some with conditions and options) in the class file.

Also, several specification specific commands are introduced, like `\thelang{}`, 
returning `STS` as language name.

If one want to add some package, class file is a right place for that.

### How to edit document

#### Document structure
Logically document chapter consist of sections, subsections and subsubsections.

So, one have to put something like

```latex

\hypertarget{lexical-elements}{%
\section{Lexical Elements}\label{lex:lexical-elements}}

...

\hypertarget{use-of-unicode-characters}{%
\subsection{Use of Unicode Characters}\label{lex:use-of-unicode-characters}}

...

\hypertarget{integer-literals}{%
\subsubsection{Integer Literals}\label{lex:integer-literals}}
```

into chapter file.

It will produce 

```text
2 Lexical Elements. . . . . . . . . . . . . 11
2.1 Use of Unicode Characters . . . . . . . 11
...
   2.9.1 Integer Literals . . . . . . . . . 14
```
in table of content.

`\hypertarget{}` command is left for compartibility and may be removed later.

`\label{}` command must contain mnemonic name for cross-document links
(`\See`, `\ref` and similar commands). Important to have 3-letter prefix (`lex:`) here
for consistency.

#### Source code and grammar

Specification contains a lot of source code snippets and language grammar clauses.

To syntax highlight both there are environments created -- `source` and `grammar`:

> Style of both environments is subject to discuss and change.

##### Grammar environment
```latex
\begin{grammar}
expressionStatement
    : expression ';'?
    ;
\end{grammar}
```

The result is similar to

![Grammar in PDF](grammar.png)

##### Source environment

```latex
\begin{source}
// existing variable
for (i = 1; i < 10; i++) { ... }

// new variable, explicit type
for (let i: int = 1; i < 10; i++) { ... }

// new variable, implicit type
// inferred from variable declaration
for (let i = 1; i < 10; i++) { ... }
\end{source}
```
The result is similar to

![Source in PDF](source.png)


##### How to add source and grammar

To add source code or grammar snippet, add text

```latex
\begin{grammar}
  Grammar text goes here
\end{grammar}

\begin{source}
  Source code text goes here
\end{source}
```

Despite of possible syntax highlighting in different text editors,
LaTeX does not analyze and executes commands or text substitutions
inside the environment.

So, simply paste text of grammar or source file into the environment,
and that's it.


#### Tables
LaTeX tables are horrible at first glance, but has a lot of options to tweak.

So, do not use tables.

If you still here, try to copy-paste some table from a document and modify.

If you still here and want something special, try to use online editors:

* https://www.tablesgenerator.com/latex_tables
* https://www.latex-tables.com/
* https://tableconvert.com/latex-generator
* ...

You've been warned.

#### Misc commands
##### Code
Use `\code{stuff}` to mark (and make monotyped) some text as code.
`stuff` should not contain `{` and `}` (or it must be escaped, like `\{` and `\}`)

##### `console-output` environment
There is a special environment for text output of running program: `console-output`.
```latex
\begin{console-output}
end of try
defer in try
\end{console-output}
```

The result is similar to

![Console output in PDF](console-output.png)


##### Math and math-like text
It is LaTeX!

Use math mode on all cylinders!

`$T_i$` is T<sub>i</sub>

`$T^i$` is T<sup>i</sup>

and so on...

See

* http://mirrors.ctan.org/info/lshort/russian/lshortru.pdf
* https://ru.overleaf.com/learn


##### Some colorization

There are commands for semantical colorization:

* `\notsolved{}` -- Magenta background (for not yet solved things)
* `\needlink{}` -- Yellow background (link placeholder)
* `\notwritten{}` -- Gray background / White color (absent part)
* `\tobediscussed{}` -- Cyan background (not yet discussed / no decision)

For technical reasons these commands should not cross paragraph border.

Good luck!


