/*
 * Copyright (c) 2021-2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <cstdint>
#include "compiler_logger.h"
#include "optimizer/ir_builder/inst_builder.h"
#include "optimizer/ir_builder/ir_builder.h"
#include "optimizer/ir/inst.h"
#include "bytecode_instruction.h"
#include "bytecode_instruction-inl.h"

namespace panda::compiler {

template <Opcode OPCODE>
void InstBuilder::BuildLaunch(const BytecodeInstruction *bc_inst, bool is_range, bool acc_read)
{
    if (graph_->GetArch() == Arch::AARCH32) {
        failed_ = true;
        return;
    }
    auto pc = GetPc(bc_inst->GetAddress());
    auto inst = graph_->CreateInstLoadRuntimeClass(DataType::REFERENCE, pc);
    inst->SetTypeId(TypeIdMixin::MEM_PROMISE_CLASS_ID);
    inst->SetMethod(GetGraph()->GetMethod());
    inst->SetClass(nullptr);
    auto save_state = CreateSaveState(Opcode::SaveState, pc);
    auto new_obj = CreateNewObjectInst(pc, TypeIdMixin::MEM_PROMISE_CLASS_ID, save_state, inst);
    AddInstruction(save_state, inst, new_obj);
    BuildCall<OPCODE>(bc_inst, is_range, acc_read, new_obj);
    UpdateDefinitionAcc(new_obj);
}

template void InstBuilder::BuildLaunch<Opcode::CallLaunchStatic>(const BytecodeInstruction *bc_inst, bool is_range,
                                                                 bool acc_read);
template void InstBuilder::BuildLaunch<Opcode::CallLaunchVirtual>(const BytecodeInstruction *bc_inst, bool is_range,
                                                                  bool acc_read);

}  // namespace panda::compiler