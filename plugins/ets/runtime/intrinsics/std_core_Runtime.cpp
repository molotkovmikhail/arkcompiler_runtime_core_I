#include "intrinsics.h"
#include "libpandabase/utils/logger.h"
#include "runtime/handle_scope-inl.h"

namespace panda::ets::intrinsics {

uint8_t StdCoreRuntimeEquals(ObjectHeader *header [[maybe_unused]], EtsObject *source, EtsObject *target)
{
    return (source == target) ? UINT8_C(1) : UINT8_C(0);
}

}  // namespace panda::ets::intrinsics