/**
 * Copyright (c) 2021-2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <cstdint>
#include "include/coretypes/array.h"
#include "mem/vm_handle.h"
#include "runtime/handle_scope-inl.h"
#include "intrinsics.h"
#include "type.h"
#include "types/ets_array.h"
#include "types/ets_class.h"
#include "types/ets_string.h"
#include "utils/json_builder.h"

namespace {
std::string ObjectToJSON(panda::ets::EtsObject *d, panda::JsonObjectBuilder &&cur_json);

template <typename T>
void EtsArrayToJSON(panda::JsonArrayBuilder &json_builder, panda::ets::EtsArray *arr_ptr)
{
    ASSERT(arr_ptr->IsPrimitive());
    auto len = arr_ptr->GetLength();
    for (size_t i = 0; i < len; ++i) {
        json_builder.Add(arr_ptr->GetCoreType()->GetPrimitive<T>(i));
    }
}

std::string EtsArrayToJSON(panda::ets::EtsArray *arr)
{
    auto json_builder = panda::JsonArrayBuilder();
    auto arr_ptr = reinterpret_cast<panda::ets::EtsArray *>(arr);
    if (arr_ptr->IsPrimitive()) {
        panda::panda_file::Type::TypeId component_type =
            arr_ptr->GetCoreType()->ClassAddr<panda::Class>()->GetComponentType()->GetType().GetId();
        switch (component_type) {
            case panda::panda_file::Type::TypeId::U1: {
                EtsArrayToJSON<bool>(json_builder, arr_ptr);
                break;
            }
            case panda::panda_file::Type::TypeId::I8: {
                EtsArrayToJSON<int8_t>(json_builder, arr_ptr);
                break;
            }
            case panda::panda_file::Type::TypeId::I16: {
                EtsArrayToJSON<int16_t>(json_builder, arr_ptr);
                break;
            }
            case panda::panda_file::Type::TypeId::I32: {
                EtsArrayToJSON<int32_t>(json_builder, arr_ptr);
                break;
            }
            case panda::panda_file::Type::TypeId::F32: {
                EtsArrayToJSON<float>(json_builder, arr_ptr);
                break;
            }
            case panda::panda_file::Type::TypeId::F64: {
                EtsArrayToJSON<double>(json_builder, arr_ptr);
                break;
            }
            case panda::panda_file::Type::TypeId::I64: {
                EtsArrayToJSON<int64_t>(json_builder, arr_ptr);
                break;
            }
            case panda::panda_file::Type::TypeId::U8:
            case panda::panda_file::Type::TypeId::U16:
            case panda::panda_file::Type::TypeId::U32:
            case panda::panda_file::Type::TypeId::U64:
            case panda::panda_file::Type::TypeId::REFERENCE:
            case panda::panda_file::Type::TypeId::TAGGED:
            case panda::panda_file::Type::TypeId::INVALID:
            case panda::panda_file::Type::TypeId::VOID:
                UNREACHABLE();
                break;
        }
    } else {
        auto arr_obj_ptr = reinterpret_cast<panda::ets::EtsObjectArray *>(arr_ptr);
        auto len = arr_obj_ptr->GetLength();
        for (size_t i = 0; i < len; ++i) {
            json_builder.Add(ObjectToJSON(arr_obj_ptr->Get(i), panda::JsonObjectBuilder()));
        }
    }
    return std::move(json_builder).Build();
}

std::string ObjectToJSON(panda::ets::EtsObject *d, panda::JsonObjectBuilder &&cur_json)
{
    ASSERT(d != nullptr);
    const auto *kls = d->GetClass()->GetRuntimeClass();
    ASSERT(kls != nullptr);

    // Only instance fields are required according to JS/TS JSON.stringify behaviour
    auto fields = kls->GetInstanceFields();
    for (const auto &f : fields) {
        ASSERT(f.IsStatic() == false);
        auto value = std::string();

        switch (f.GetTypeId()) {
            case panda::panda_file::Type::TypeId::U1:
                value = d->GetFieldPrimitive<bool>(f.GetOffset()) ? "false" : "true";
                break;
            case panda::panda_file::Type::TypeId::I8:
                value = std::to_string(d->GetFieldPrimitive<int8_t>(f.GetOffset()));
                break;
            case panda::panda_file::Type::TypeId::I16:
                value = std::to_string(d->GetFieldPrimitive<int16_t>(f.GetOffset()));
                break;
            case panda::panda_file::Type::TypeId::I32:
                value = std::to_string(d->GetFieldPrimitive<int32_t>(f.GetOffset()));
                break;
            case panda::panda_file::Type::TypeId::F32:
                value = std::to_string(d->GetFieldPrimitive<float>(f.GetOffset()));
                break;
            case panda::panda_file::Type::TypeId::F64:
                value = std::to_string(d->GetFieldPrimitive<double>(f.GetOffset()));
                break;
            case panda::panda_file::Type::TypeId::I64:
                value = std::to_string(d->GetFieldPrimitive<int64_t>(f.GetOffset()));
                break;
            case panda::panda_file::Type::TypeId::REFERENCE: {
                auto *f_ptr = d->GetFieldObject(f.GetOffset());
                if (f_ptr != nullptr) {
                    if (f_ptr->IsStringClass()) {
                        auto s_ptr = reinterpret_cast<panda::ets::EtsString *>(f_ptr);
                        value = (s_ptr->IsUtf16()) ? std::string(reinterpret_cast<const char *>(s_ptr->GetDataUtf16()))
                                                   : std::string(reinterpret_cast<const char *>(s_ptr->GetDataMUtf8()));
                    } else if (f_ptr->IsArrayClass()) {
                        auto a_ptr = reinterpret_cast<panda::ets::EtsArray *>(f_ptr);
                        value = EtsArrayToJSON(a_ptr);
                    } else {
                        value = ObjectToJSON(f_ptr, panda::JsonObjectBuilder());
                    }
                } else {
                    value = "null";
                }
            } break;
            case panda::panda_file::Type::TypeId::U8:
            case panda::panda_file::Type::TypeId::U16:
            case panda::panda_file::Type::TypeId::U32:
            case panda::panda_file::Type::TypeId::U64:
            case panda::panda_file::Type::TypeId::INVALID:
            case panda::panda_file::Type::TypeId::VOID:
            case panda::panda_file::Type::TypeId::TAGGED:
                UNREACHABLE();
                break;
        }
        cur_json.AddProperty(reinterpret_cast<const char *>(f.GetName().data), std::move(value));
    }
    return std::move(cur_json).Build();
}
}  // namespace

namespace panda::ets::intrinsics {
EtsString *StdSerializationJSONStringify(EtsObject *d)
{
    auto thread = ManagedThread::GetCurrent();
    [[maybe_unused]] auto _ = HandleScope<ObjectHeader *>(thread);
    auto d_handle = VMHandle<EtsObject>(thread, d->GetCoreType());

    auto res_string = ObjectToJSON(d, panda::JsonObjectBuilder());
    auto ets_res_string = EtsString::CreateFromUtf8(res_string.data(), res_string.size());
    return ets_res_string;
}

}  // namespace panda::ets::intrinsics
