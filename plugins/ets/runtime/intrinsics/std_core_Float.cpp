/**
 * Copyright (c) 2021-2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "intrinsics.h"
#include "plugins/ets/runtime/types/ets_string.h"
#include "plugins/ets/runtime/intrinsics/helpers/ets_intrinsics_helpers.h"

namespace panda::ets::intrinsics {

EtsString *StdCoreFloatToString(float number, int radix)
{
    return helpers::FpToString(number, radix);
}

extern "C" EtsBoolean StdCoreFloatIsNan(float v)
{
    return ToEtsBoolean(v != v);
}

extern "C" EtsBoolean StdCoreFloatIsFinite(float v)
{
    static const float POSITIVE_INFINITY = 1.0 / 0.0;
    static const float NEGATIVE_INFINITY = -1.0 / 0.0;

    return ToEtsBoolean(v == v && v != POSITIVE_INFINITY && v != NEGATIVE_INFINITY);
}

}  // namespace panda::ets::intrinsics
