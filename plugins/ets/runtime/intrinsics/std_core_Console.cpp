/**
 * Copyright (c) 2021-2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <iostream>
#include <string_view>
#include "libpandabase/utils/utf.h"

namespace panda::ets::intrinsics {

extern "C" void StdConsolePrintln()
{
    std::cout << std::endl;
}

extern "C" void StdConsolePrintBool(uint8_t b)
{
    if (b != 0U) {
        std::cout << "true";
    } else {
        std::cout << "false";
    }
}

extern "C" void StdConsolePrintChar(uint16_t c)
{
    const utf::Utf8Char utf8_ch = utf::ConvertUtf16ToUtf8(c, 0, false);
    std::cout << std::string_view(reinterpret_cast<const char *>(utf8_ch.ch.data()), utf8_ch.n);
}

}  // namespace panda::ets::intrinsics
