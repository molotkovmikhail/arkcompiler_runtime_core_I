/**
 * Copyright (c) 2021-2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "intrinsics.h"
#include "plugins/ets/runtime/lambda_utils.h"
#include "plugins/ets/runtime/ets_coroutine.h"
#include "plugins/ets/runtime/ets_vm.h"
#include "runtime/coroutine_manager.h"
#include "plugins/ets/runtime/types/ets_promise.h"
#include "runtime/handle_scope-inl.h"
#include "runtime/handle_scope.h"

#include "plugins/ets/runtime/job_queue.h"

namespace panda::ets::intrinsics {

static void OnPromiseCompletion(EtsCoroutine *coro, VMHandle<EtsPromise> &promise, VMHandle<EtsObjectArray> &queue)
{
    VMMutableHandle<ObjectHeader> exception(coro, nullptr);
    coretypes::ArraySizeT length = 0;
    if (queue.GetPtr() != nullptr) {
        length = queue->GetLength();
    }
    for (coretypes::ArraySizeT i = 0; i < length; ++i) {
        EtsObject *callback = queue->Get(i);
        if (callback != nullptr) {
            queue->Set(i, nullptr);
            LambdaUtils::InvokeVoid(coro, callback);
            if (coro->HasPendingException() && exception.GetPtr() == nullptr) {
                exception.Update(coro->GetException());
                coro->ClearException();
            }
        }
    }
    promise->ClearQueues(coro);
    if (exception.GetPtr() != nullptr) {
        coro->SetException(exception.GetPtr());
    }
    coro->GetPandaVM()->FirePromiseStateChanged(promise);
}

void EtsPromiseResolve(EtsPromise *promise, EtsObject *value)
{
    EtsCoroutine *coro = EtsCoroutine::GetCurrent();
    if (promise == nullptr) {
        LanguageContext ctx = Runtime::GetCurrent()->GetLanguageContext(panda_file::SourceLang::ETS);
        ThrowNullPointerException(ctx, coro);
        return;
    }
    if (promise->GetState() != EtsPromise::STATE_PENDING) {
        return;
    }
    [[maybe_unused]] HandleScope<ObjectHeader *> scope(coro);
    VMHandle<EtsPromise> hpromise(coro, promise);
    VMHandle<EtsObjectArray> then_queue(coro, promise->GetThenQueue(coro)->GetCoreType());
    hpromise->Resolve(coro, value);
    OnPromiseCompletion(coro, hpromise, then_queue);
}

void EtsPromiseReject(EtsPromise *promise, EtsObject *error)
{
    EtsCoroutine *coro = EtsCoroutine::GetCurrent();
    if (promise == nullptr) {
        LanguageContext ctx = Runtime::GetCurrent()->GetLanguageContext(panda_file::SourceLang::ETS);
        ThrowNullPointerException(ctx, coro);
        return;
    }
    if (promise->GetState() != EtsPromise::STATE_PENDING) {
        return;
    }
    [[maybe_unused]] HandleScope<ObjectHeader *> scope(coro);
    VMHandle<EtsPromise> hpromise(coro, promise);
    VMHandle<EtsObjectArray> catch_queue(coro, promise->GetCatchQueue(coro)->GetCoreType());
    hpromise->Reject(coro, error);
    OnPromiseCompletion(coro, hpromise, catch_queue);
}

void EtsPromiseAddToJobQueue(EtsObject *callback)
{
    auto *job_queue = EtsCoroutine::GetCurrent()->GetPandaVM()->GetJobQueue();
    if (job_queue != nullptr) {
        job_queue->AddJob(callback);
    }
}

EtsObject *EtsAwaitPromise(EtsPromise *promise)
{
    EtsCoroutine *current_coro = EtsCoroutine::GetCurrent();
    if (promise == nullptr) {
        LanguageContext ctx = Runtime::GetCurrent()->GetLanguageContext(panda_file::SourceLang::ETS);
        ThrowNullPointerException(ctx, current_coro);
        return nullptr;
    }
    [[maybe_unused]] HandleScope<ObjectHeader *> scope(current_coro);
    VMHandle<EtsPromise> promise_handle(current_coro, promise);
    if (promise_handle.GetPtr()->IsResolved()) {
        /**
         * The promise is already resolved. Further actions:
         *      STS mode: get Promise.value, return it
         *      JS mode:
         *          JQ::put(current_coro, promise)
         *          CM::Await()
         */
        LOG(DEBUG, COROUTINES) << "Promise::await: promise is already resolved!";
        return promise_handle.GetPtr()->GetValue(current_coro);
    }

    // the promise is not resolved yet
    auto *cm = static_cast<CoroutineManager *>(EtsCoroutine::GetCurrent()->GetPandaVM()->GetThreadManager());
    CompletionEvent *e = promise_handle.GetPtr()->GetEventPtr();
    if (e != nullptr) {
        /**
         * The promise is linked to come coroutine return value.
         * Further actions:
         *      STS mode: return P.value
         *      JS mode: ???
         */
        cm->Await(e);
        // NB: here the event(e) is already deleted (consumed by await).
        promise_handle.GetPtr()->SetEventPtr(nullptr);
        return promise_handle.GetPtr()->GetValue(current_coro);
    }

    LOG(DEBUG, COROUTINES) << "Promise::await: promise is not linked to an event (standalone)";
    /**
     * This promise is not linked to any coroutine return value (standalone promise).
     * Further actions:
     *      STS mode:
     *          create Event, connect it to promise
     *          CM::Await(event) // who will resolve P and P.event?
     *      JS mode: ???
     */

    return nullptr;
}
}  // namespace panda::ets::intrinsics
