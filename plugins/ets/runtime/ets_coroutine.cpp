/**
 * Copyright (c) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "plugins/ets/runtime/ets_coroutine.h"
#include "runtime/include/object_header.h"
#include "plugins/ets/runtime/types/ets_promise.h"
#include "plugins/ets/runtime/ets_vm.h"
#include "runtime/include/panda_vm.h"
#include "plugins/ets/runtime/ets_class_linker_extension.h"
#include "plugins/ets/runtime/types/ets_object.h"

namespace panda::ets {

EtsCoroutine::EtsCoroutine(ThreadId id, mem::InternalAllocatorPtr allocator, PandaVM *vm, PandaString name,
                           CoroutineContext *context, std::optional<EntrypointInfo> &&ep_info)
    : Coroutine(id, allocator, vm, panda::panda_file::SourceLang::ETS, std::move(name), context, std::move(ep_info))
{
    ASSERT(vm != nullptr);
}

PandaEtsVM *EtsCoroutine::GetPandaVM() const
{
    return static_cast<PandaEtsVM *>(GetVM());
}

void EtsCoroutine::Initialize()
{
    auto allocator = GetVM()->GetHeapManager()->GetInternalAllocator();
    auto ets_napi_env = PandaEtsNapiEnv::Create(this, allocator);
    if (!ets_napi_env) {
        LOG(FATAL, RUNTIME) << "Cannot create PandaEtsNapiEnv: " << ets_napi_env.Error();
    }
    ets_napi_env_ = std::move(ets_napi_env.Value());
    // Main EtsCoroutine is Initialized before class linker and promise_class_ptr_ will be set after creating the class
    if (HasEntrypoint()) {
        promise_class_ptr_ = GetPandaVM()->GetClassLinker()->GetPromiseClass()->GetRuntimeClass();
    }
    ASSERT(promise_class_ptr_ != nullptr || !HasEntrypoint());

    Coroutine::Initialize();
}

void EtsCoroutine::FreeInternalMemory()
{
    ets_napi_env_->FreeInternalMemory();
    ManagedThread::FreeInternalMemory();
}

void EtsCoroutine::RequestCompletion(Value return_value)
{
    if (!return_value.IsReference()) {
        // TODO(konstanting, #I67QXC): what if coro returns a primitive? looks like we have to do (auto)boxing here
        LOG(FATAL, COROUTINES) << "Currently a coroutine should return an object. Autoboxing for primitive types"
                                  " will be supported later on, i.e. Promise<void> is illegal now.";
        UNREACHABLE();
    }
    auto ret_obj_ptr = return_value.GetAs<ObjectHeader *>();
    auto promise_ref = GetCompletionEvent()->GetPromise();
    auto promise = reinterpret_cast<EtsPromise *>(GetVM()->GetGlobalObjectStorage()->Get(promise_ref));
    if (promise != nullptr) {
        promise->Resolve(this, EtsObject::FromCoreType(ret_obj_ptr));
    }
    LOG(DEBUG, COROUTINES) << "Coroutine " << GetName() << " has completed with return value = ObjectPtr<"
                           << reinterpret_cast<uintptr_t>(ret_obj_ptr) << ">";
    Coroutine::RequestCompletion(return_value);
}

}  // namespace panda::ets
