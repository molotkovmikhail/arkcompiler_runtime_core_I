/**
 * Copyright (c) 2021-2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "plugins/ets/runtime/types/ets_string.h"
#include "plugins/ets/runtime/types/ets_method.h"
#include "plugins/ets/runtime/interop_js/js_value_call.h"
#include "plugins/ets/runtime/interop_js/napi_env_scope.h"
#include "plugins/ets/runtime/interop_js/js_convert.h"
#include "plugins/ets/runtime/interop_js/ts2ets_common.h"
#include "runtime/include/panda_vm.h"
#include "runtime/include/class_linker-inl.h"
#include "runtime/handle_scope-inl.h"

namespace panda::ets::interop::js {

template <typename FClsResolv, typename FStorePrim, typename FStoreRef>
[[nodiscard]] static ALWAYS_INLINE inline bool ConvertNapiVal(InteropCtx *ctx, FClsResolv &resolve_ref_cls,
                                                              FStorePrim &store_prim, FStoreRef &store_ref,
                                                              panda_file::Type type, napi_value js_val)
{
    auto env = ctx->GetJSEnv();

    auto unwrap_val = [&](auto conv_tag) {
        using Convertor = typename decltype(conv_tag)::type;  // conv_tag acts as lambda template parameter
        using cpptype = typename Convertor::cpptype;          // NOLINT(readability-identifier-naming)
        auto res = Convertor::Unwrap(ctx, env, js_val);
        if (UNLIKELY(!res.has_value())) {
            return false;
        }
        if constexpr (std::is_pointer_v<cpptype>) {
            store_ref(AsEtsObject(res.value())->GetCoreType());
        } else {
            store_prim(Value(res.value()).GetAsLong());
        }
        return true;
    };

    switch (type.GetId()) {
        case panda_file::Type::TypeId::VOID: {
            // do nothing
            return true;
        }
        case panda_file::Type::TypeId::U1: {
            return unwrap_val(helpers::TypeIdentity<JSConvertU1>());
        }
        case panda_file::Type::TypeId::I8: {
            return unwrap_val(helpers::TypeIdentity<JSConvertI32>());
        }
        case panda_file::Type::TypeId::U16: {
            return unwrap_val(helpers::TypeIdentity<JSConvertI32>());
        }
        case panda_file::Type::TypeId::I16: {
            return unwrap_val(helpers::TypeIdentity<JSConvertI32>());
        }
        case panda_file::Type::TypeId::I32: {
            return unwrap_val(helpers::TypeIdentity<JSConvertI32>());
        }
        case panda_file::Type::TypeId::I64: {
            return unwrap_val(helpers::TypeIdentity<JSConvertI64>());
        }
        case panda_file::Type::TypeId::F32: {
            return unwrap_val(helpers::TypeIdentity<JSConvertF32>());
        }
        case panda_file::Type::TypeId::F64: {
            return unwrap_val(helpers::TypeIdentity<JSConvertF64>());
        }
        case panda_file::Type::TypeId::REFERENCE: {
            if (UNLIKELY(IsNullOrUndefined(env, js_val))) {
                store_ref(nullptr);
                return true;
            }
            auto klass = resolve_ref_cls();
            // start fastpath
            if (klass == ctx->JSValueClass()) {
                return unwrap_val(helpers::TypeIdentity<JSConvertJSValue>());
            }
            if (klass == ctx->GetStringClass()) {
                return unwrap_val(helpers::TypeIdentity<JSConvertString>());
            }
            // start slowpath
            auto refconv = JSRefConvertResolve(ctx, klass);
            ObjectHeader *res = refconv->Unwrap(ctx, js_val)->GetCoreType();
            if (UNLIKELY(res == nullptr)) {
                return false;
            }
            store_ref(res);
            return true;
        }
        default: {
            ctx->Fatal(std::string("ConvertNapiVal: unsupported typeid ") +
                       panda_file::Type::GetSignatureByTypeId(type));
        }
    }
    UNREACHABLE();
}

template <typename FClsResolv, typename FStore, typename FRead>
static ALWAYS_INLINE inline void ConvertEtsVal(InteropCtx *ctx, [[maybe_unused]] FClsResolv &resolve_ref_cls,
                                               FStore &store_res, panda_file::Type type, FRead &read_val)
{
    auto env = ctx->GetJSEnv();

    auto wrap_prim = [&](auto conv_tag) {
        using Convertor = typename decltype(conv_tag)::type;  // conv_tag acts as lambda template parameter
        using cpptype = typename Convertor::cpptype;          // NOLINT(readability-identifier-naming)
        store_res(Convertor::Wrap(env, read_val(helpers::TypeIdentity<cpptype>())));
    };

    auto wrap_ref = [&](auto conv_tag, ObjectHeader *ref) -> void {
        using Convertor = typename decltype(conv_tag)::type;  // conv_tag acts as lambda template parameter
        using cpptype = typename Convertor::cpptype;          // NOLINT(readability-identifier-naming)
        cpptype value = std::remove_pointer_t<cpptype>::FromEtsObject(EtsObject::FromCoreType(ref));
        store_res(Convertor::Wrap(env, value));
    };

    switch (type.GetId()) {
        case panda_file::Type::TypeId::VOID: {
            store_res(nullptr);
            return;
        }
        case panda_file::Type::TypeId::U1: {
            return wrap_prim(helpers::TypeIdentity<JSConvertU1>());
        }
        case panda_file::Type::TypeId::I8: {
            return wrap_prim(helpers::TypeIdentity<JSConvertI32>());
        }
        case panda_file::Type::TypeId::U16: {
            return wrap_prim(helpers::TypeIdentity<JSConvertI32>());
        }
        case panda_file::Type::TypeId::I16: {
            return wrap_prim(helpers::TypeIdentity<JSConvertI32>());
        }
        case panda_file::Type::TypeId::I32: {
            return wrap_prim(helpers::TypeIdentity<JSConvertI32>());
        }
        case panda_file::Type::TypeId::I64: {
            return wrap_prim(helpers::TypeIdentity<JSConvertI64>());
        }
        case panda_file::Type::TypeId::F32: {
            return wrap_prim(helpers::TypeIdentity<JSConvertF32>());
        }
        case panda_file::Type::TypeId::F64: {
            return wrap_prim(helpers::TypeIdentity<JSConvertF64>());
        }
        case panda_file::Type::TypeId::REFERENCE: {
            ObjectHeader *ref = read_val(helpers::TypeIdentity<ObjectHeader *>());
            if (UNLIKELY(ref == nullptr)) {
                store_res(GetNull(env));
                return;
            }
            auto klass = ref->template ClassAddr<Class>();
            ASSERT(klass == resolve_ref_cls());  // TODO(vpukhov): inheritance
            // start fastpath
            if (klass == ctx->JSValueClass()) {
                return wrap_ref(helpers::TypeIdentity<JSConvertJSValue>(), ref);
            }
            if (klass == ctx->GetStringClass()) {
                return wrap_ref(helpers::TypeIdentity<JSConvertString>(), ref);
            }
            // start slowpath
            auto refconv = JSRefConvertResolve(ctx, klass);
            store_res(refconv->Wrap(ctx, EtsObject::FromCoreType(ref)));
            return;
        }
        default: {
            ctx->Fatal(std::string("ConvertEtsVal: unsupported typeid ") +
                       panda_file::Type::GetSignatureByTypeId(type));
        }
    }
    UNREACHABLE();
}

using ArgValueBox = std::variant<uint64_t, ObjectHeader **>;

template <bool IS_STATIC>
static ALWAYS_INLINE inline napi_value EtsCallImpl(EtsCoroutine *coro, InteropCtx *ctx, Method *method,
                                                   Span<napi_value> jsargv, mem::Reference *ets_fnobj_ref)
{
    ScopedManagedCodeThread managed_scope(coro);

    auto class_linker = Runtime::GetCurrent()->GetClassLinker();

    auto pf = method->GetPandaFile();
    panda_file::ProtoDataAccessor pda(*pf, panda_file::MethodDataAccessor::GetProtoId(*pf, method->GetFileId()));
    pda.EnumerateTypes([](panda_file::Type /* unused */) {});  // preload reftypes span

    auto resolve_ref_cls = [&](uint32_t idx) {
        auto klass = class_linker->GetLoadedClass(*pf, pda.GetReferenceType(idx), ctx->LinkerCtx());
        ASSERT(klass != nullptr);
        return klass;
    };

    panda_file::ShortyIterator it(method->GetShorty());
    auto ref_arg_idx = static_cast<uint32_t>(!(*it++).IsPrimitive());  // skip retval

    ASSERT(method->IsStatic() == IS_STATIC);
    static constexpr size_t ETS_ARGS_DISP = IS_STATIC ? 0 : 1;

    auto const num_args = method->GetNumArgs() - ETS_ARGS_DISP;
    auto &ets_args = ctx->GetTempArgs<Value>(method->GetNumArgs());
    {
        HandleScope<ObjectHeader *> ets_handle_scope(coro);

        auto &ets_boxed_args = ctx->GetTempArgs<ArgValueBox>(num_args);

        // Convert and box in VMHandle if necessary
        for (uint32_t arg_idx = 0; arg_idx < num_args; ++arg_idx, it.IncrementWithoutCheck()) {
            panda_file::Type type = *it;
            auto js_val = jsargv[arg_idx];
            auto cls_resolver = [&]() { return resolve_ref_cls(ref_arg_idx++); };
            auto store_prim = [&](uint64_t val) { ets_boxed_args[arg_idx] = val; };
            auto store_ref = [&](ObjectHeader *obj) {
                uintptr_t addr = VMHandle<ObjectHeader>(coro, obj).GetAddress();
                ets_boxed_args[arg_idx] = reinterpret_cast<ObjectHeader **>(addr);
            };
            if (UNLIKELY(!ConvertNapiVal(ctx, cls_resolver, store_prim, store_ref, type, js_val))) {
                INTEROP_LOG(DEBUG) << "EtsCall: exit with pending exception";
                return nullptr;
            }
        }

        // Unbox VMHandles
        for (size_t i = 0; i < num_args; ++i) {
            auto const &box = ets_boxed_args[i];
            if (std::holds_alternative<ObjectHeader **>(box)) {
                ASSERT(std::get<1>(box) != nullptr);
                ets_args[ETS_ARGS_DISP + i] = Value(*std::get<1>(box));
            } else {
                ets_args[ETS_ARGS_DISP + i] = Value(std::get<0>(box));
            }
        }
    }
    if constexpr (!IS_STATIC) {
        ets_args[0] = Value(ctx->Refstor()->Get(ets_fnobj_ref));
    } else {
        (void)ets_fnobj_ref;
    }

    Value ets_res = method->Invoke(coro, ets_args.data());

    if (UNLIKELY(coro->HasPendingException())) {
        ctx->ForwardEtsException(coro);
        INTEROP_LOG(DEBUG) << "EtsCall: exit with pending exception";
        return nullptr;
    }

    napi_value js_res;
    {
        auto type = method->GetReturnType();
        auto cls_resolver = [&]() { return resolve_ref_cls(0); };
        auto store_res = [&](napi_value res) { js_res = res; };
        auto read_val = [&](auto type_tag) { return ets_res.GetAs<typename decltype(type_tag)::type>(); };
        ConvertEtsVal(ctx, cls_resolver, store_res, type, read_val);
    }
    return js_res;
}

napi_value CallEtsFunctionImpl(napi_env env, Span<napi_value> jsargv)
{
    auto coro = EtsCoroutine::GetCurrent();
    auto ctx = InteropCtx::Current(coro);
    [[maybe_unused]] EtsJSNapiEnvScope scope(ctx, env);
    INTEROP_LOG(DEBUG) << "CallEtsFunction: enter";

    // NOLINTNEXTLINE(readability-container-size-empty)
    if (UNLIKELY(jsargv.size() < 1)) {
        InteropCtx::ThrowJSError(env, "CallEtsFunction: method name required");
        return nullptr;
    }

    if (UNLIKELY(GetValueType(env, jsargv[0]) != napi_string)) {
        InteropCtx::ThrowJSError(env, "CallEtsFunction: method name is not a string");
        return nullptr;
    }
    auto method_name = std::string("ETSGLOBAL::") + GetString(env, jsargv[0]);
    INTEROP_LOG(DEBUG) << "CallEtsFunction: method name: " << method_name;

    auto method_res = Runtime::GetCurrent()->ResolveEntryPoint(method_name);
    if (UNLIKELY(!method_res)) {
        InteropCtx::ThrowJSError(env, "CallEtsFunction: can't resolve method " + method_name);
        return nullptr;
    }
    auto method = method_res.Value();

    auto const num_args = method->GetNumArgs();
    if (UNLIKELY(num_args != jsargv.size() - 1)) {
        InteropCtx::ThrowJSError(env, std::string("CallEtsFunction: wrong argc"));
        return nullptr;
    }

    auto js_ret = EtsCallImpl<true>(coro, ctx, method, jsargv.SubSpan(1), nullptr);
    INTEROP_LOG(DEBUG) << "CallEtsFunction: exit";
    return js_ret;
}

napi_value EtsLambdaProxyInvoke(napi_env env, napi_callback_info cbinfo)
{
    auto coro = EtsCoroutine::GetCurrent();
    auto ctx = InteropCtx::Current(coro);
    [[maybe_unused]] EtsJSNapiEnvScope envscope(ctx, env);
    INTEROP_LOG(DEBUG) << "EtsProxyInvoke: enter";

    size_t argc;
    napi_value athis;
    void *data;
    NAPI_CHECK_FATAL(napi_get_cb_info(env, cbinfo, &argc, nullptr, &athis, &data));
    auto &js_args = ctx->GetTempArgs<napi_value>(argc);
    NAPI_CHECK_FATAL(napi_get_cb_info(env, cbinfo, &argc, js_args.data(), &athis, &data));

    auto ets_ref = static_cast<mem::Reference *>(data);
    ASSERT(ets_ref != nullptr);
    auto ets_obj = EtsObject::FromCoreType(ctx->Refstor()->Get(ets_ref));
    auto method = ets_obj->GetClass()->GetMethod("invoke");
    ASSERT(method != nullptr);

    if (UNLIKELY(argc != method->GetNumArgs() - 1)) {
        InteropCtx::ThrowJSError(env, std::string("EtsProxyInvoke: wrong argc"));
        return nullptr;
    }

    auto js_args_span = Span(js_args.data(), js_args.size());
    auto js_ret = EtsCallImpl<false>(coro, ctx, method->GetPandaMethod(), js_args_span, ets_ref);
    INTEROP_LOG(DEBUG) << "EtsProxyInvoke: exit";
    return js_ret;
}

template <char DELIM = '.', typename F>
static void WalkQualifiedName(std::string_view name, F const &f)
{
    for (const char *p = name.data(); *p == DELIM;) {
        // NOLINTNEXTLINE(cppcoreguidelines-pro-bounds-pointer-arithmetic)
        auto e = ++p;
        while (*e != '\0' && *e != DELIM) {
            e++;
        }
        f(std::string(p, ToUintPtr(e) - ToUintPtr(p)));
        p = e;
    }
}

static inline std::pair<napi_value, napi_value> ResolveQualifiedJSCall(napi_env env, napi_value js_val,
                                                                       coretypes::String *qname_str)
{
    napi_value js_this {};
    auto qname = std::string_view(utf::Mutf8AsCString(qname_str->GetDataMUtf8()), qname_str->GetMUtf8Length());

    auto resolve_name = [&](const std::string &name) {
        js_this = js_val;
        INTEROP_LOG(DEBUG) << "JSValueJSCall: resolve name: " << name;
        napi_status rc = napi_get_named_property(env, js_val, name.c_str(), &js_val);
        if (UNLIKELY(NapiThrownGeneric(rc))) {
            NapiFatal("Qualified name resolution throws");  // TODO(vpukhov): handle different failure types
        }
    };
    js_this = js_val;
    WalkQualifiedName(qname, resolve_name);

    return {js_this, js_val};
}

template <bool IS_NEWCALL, bool CALL_BY_VALUE>
static ALWAYS_INLINE inline uint64_t JSValueJSCallImpl(Method *method, uint8_t *args, uint8_t *in_stack_args)
{
    INTEROP_LOG(DEBUG) << "JSValueJSCall: enter";
    auto coro = EtsCoroutine::GetCurrent();
    auto ctx = InteropCtx::Current(coro);
    auto class_linker = Runtime::GetCurrent()->GetClassLinker();

    auto pf = method->GetPandaFile();
    panda_file::ProtoDataAccessor pda(*pf, panda_file::MethodDataAccessor::GetProtoId(*pf, method->GetFileId()));
    pda.EnumerateTypes([](panda_file::Type /* unused */) {});  // preload reftypes span

    auto resolve_ref_cls = [&](uint32_t idx) {
        auto klass = class_linker->GetLoadedClass(*pf, pda.GetReferenceType(idx), ctx->LinkerCtx());
        ASSERT(klass != nullptr);
        return klass;
    };

    Span<uint8_t> in_gpr_args(args, arch::ExtArchTraits<RUNTIME_ARCH>::GP_ARG_NUM_BYTES);
    Span<uint8_t> in_fpr_args(in_gpr_args.end(), arch::ExtArchTraits<RUNTIME_ARCH>::FP_ARG_NUM_BYTES);
    arch::ArgReader<RUNTIME_ARCH> arg_reader(in_gpr_args, in_fpr_args, in_stack_args);
    arg_reader.Read<Method *>();  // skip method

    napi_env env = ctx->GetJSEnv();
    NapiScope js_handle_scope(env);

    ASSERT(method->IsStatic());
    panda_file::ShortyIterator it(method->GetShorty());
    uint32_t num_args = method->GetNumArgs();
    uint32_t ref_arg_idx = !(*it++).IsPrimitive();

    napi_value js_this;
    napi_value js_fn;
    it.IncrementWithoutCheck();
    it.IncrementWithoutCheck();
    num_args -= 2;
    ref_arg_idx += 2;
    if constexpr (CALL_BY_VALUE) {  // jsvalue(fn), jsvalue(this), ...args
        js_fn = JSConvertJSValue::Wrap(env, arg_reader.Read<JSValue *>());
        js_this = JSConvertJSValue::Wrap(env, arg_reader.Read<JSValue *>());
    } else {  // jsvalue(top), string(qual.name), ...args
        napi_value js_val = JSConvertJSValue::Wrap(env, arg_reader.Read<JSValue *>());
        auto qname_str = arg_reader.Read<coretypes::String *>();
        ASSERT(qname_str->ClassAddr<Class>()->IsStringClass());
        std::tie(js_this, js_fn) = ResolveQualifiedJSCall(env, js_val, qname_str);
    }

    auto &jsargs = ctx->GetTempArgs<napi_value>(num_args);

    for (uint32_t arg_idx = 0; arg_idx < num_args; ++arg_idx, it.IncrementWithoutCheck()) {
        auto cls_resolver = [&]() { return resolve_ref_cls(ref_arg_idx++); };
        auto store_res = [&](napi_value res) { jsargs[arg_idx] = res; };
        auto read_val = [&](auto type_tag) { return arg_reader.Read<typename decltype(type_tag)::type>(); };
        ConvertEtsVal(ctx, cls_resolver, store_res, *it, read_val);
    }

    napi_value js_ret;
    napi_status js_status;
    {
        ScopedNativeCodeThread native_scope(coro);
        if constexpr (IS_NEWCALL) {
            js_status = napi_new_instance(env, js_fn, jsargs.size(), jsargs.data(), &js_ret);
        } else {
            js_status = napi_call_function(env, js_this, js_fn, jsargs.size(), jsargs.data(), &js_ret);
        }
    }

    if (UNLIKELY(js_status != napi_ok)) {
        NAPI_FATAL_IF(js_status != napi_pending_exception);
        ctx->ForwardJSException(coro);
        INTEROP_LOG(DEBUG) << "JSValueJSCall: exit with pending exception";
        return 0;
    }

    Value ets_ret;

    if constexpr (IS_NEWCALL) {
        NAPI_FATAL_IF(resolve_ref_cls(0) != ctx->JSValueClass());
        auto res = JSConvertJSValue::Unwrap(ctx, env, js_ret);
        if (!res.has_value()) {
            ctx->Fatal("newcall result unwrap failed, but shouldnt");
        }
        ets_ret = Value(res.value()->GetCoreType());
    } else {
        panda_file::Type type = method->GetReturnType();
        auto cls_resolver = [&]() { return resolve_ref_cls(0); };
        auto store_prim = [&](uint64_t val) { ets_ret = Value(val); };
        auto store_ref = [&](ObjectHeader *obj) { ets_ret = Value(obj); };
        if (UNLIKELY(!ConvertNapiVal(ctx, cls_resolver, store_prim, store_ref, type, js_ret))) {
            ctx->ForwardJSException(coro);
            INTEROP_LOG(DEBUG) << "JSValueJSCall: exit with pending exception";
            return 0;
        }
    }

    INTEROP_LOG(DEBUG) << "JSValueJSCall: exit";
    return static_cast<uint64_t>(ets_ret.GetAsLong());
}

extern "C" uint64_t JSValueJSCall(Method *method, uint8_t *args, uint8_t *in_stack_args)
{
    return JSValueJSCallImpl<false, false>(method, args, in_stack_args);
}

extern "C" uint64_t JSValueJSCallByValue(Method *method, uint8_t *args, uint8_t *in_stack_args)
{
    return JSValueJSCallImpl<false, true>(method, args, in_stack_args);
}

extern "C" uint64_t JSValueJSNew(Method *method, uint8_t *args, uint8_t *in_stack_args)
{
    return JSValueJSCallImpl<true, false>(method, args, in_stack_args);
}

extern "C" void JSValueJSCallBridge(Method *method, ...);
extern "C" void JSValueJSCallByValueBridge(Method *method, ...);
extern "C" void JSValueJSNewBridge(Method *method, ...);

template <bool IS_NEWCALL>
static void InitJSCallSignatures(coretypes::String *cls_str)
{
    auto coro = EtsCoroutine::GetCurrent();
    auto ctx = InteropCtx::Current(coro);
    auto class_linker = Runtime::GetCurrent()->GetClassLinker();

    const char *class_descriptor = utf::Mutf8AsCString(cls_str->GetDataMUtf8());
    EtsClass *ets_class = coro->GetPandaVM()->GetClassLinker()->GetClass(class_descriptor);
    NAPI_FATAL_IF(ets_class == nullptr);
    auto klass = ets_class->GetRuntimeClass();

    INTEROP_LOG(DEBUG) << "Bind bridge call methods for " << utf::Mutf8AsCString(klass->GetDescriptor());

    for (auto &method : klass->GetMethods()) {
        if (method.IsConstructor()) {
            continue;
        }
        ASSERT(method.IsStatic());
        auto pf = method.GetPandaFile();
        panda_file::ProtoDataAccessor pda(*pf, panda_file::MethodDataAccessor::GetProtoId(*pf, method.GetFileId()));
        pda.EnumerateTypes([](panda_file::Type /* unused */) {});  // preload reftypes span

        void *method_ep = nullptr;
        if constexpr (IS_NEWCALL) {
            method_ep = reinterpret_cast<void *>(JSValueJSNewBridge);
        } else {
            uint32_t const arg_reftype_shift = method.GetReturnType().IsReference() ? 1 : 0;
            ASSERT(method.GetArgType(0).IsReference());  // arg0 is always a reference
            ASSERT(method.GetArgType(1).IsReference());  // arg1 is always a reference
            auto cls1 = class_linker->GetClass(*pf, pda.GetReferenceType(1 + arg_reftype_shift), ctx->LinkerCtx());
            if (cls1->IsStringClass()) {
                method_ep = reinterpret_cast<void *>(JSValueJSCallBridge);
            } else {
                ASSERT(cls1 == ctx->JSValueClass());
                method_ep = reinterpret_cast<void *>(JSValueJSCallByValueBridge);
            }
        }
        method.SetCompiledEntryPoint(method_ep);
        method.SetNativePointer(nullptr);
    }
}

uint8_t JSRuntimeInitJSCallClass(EtsString *cls_str)
{
    InitJSCallSignatures<false>(cls_str->GetCoreType());
    return 1;
}

uint8_t JSRuntimeInitJSNewClass(EtsString *cls_str)
{
    InitJSCallSignatures<true>(cls_str->GetCoreType());
    return 1;
}

}  // namespace panda::ets::interop::js
