/**
 * Copyright (c) 2021-2023 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <node_api.h>
#include "runtime/handle_scope-inl.h"
#include "runtime/mem/refstorage/reference.h"
#include "plugins/ets/runtime/ets_vm.h"
#include "plugins/ets/runtime/lambda_utils.h"
#include "plugins/ets/runtime/types/ets_method.h"
#include "plugins/ets/runtime/interop_js/js_job_queue.h"
#include "plugins/ets/runtime/interop_js/ts2ets_common.h"
#include "plugins/ets/runtime/interop_js/interop_context.h"

namespace panda::ets::interop::js {
static napi_value ThenCallback(napi_env env, napi_callback_info info)
{
    mem::Reference *callback_ref = nullptr;
    [[maybe_unused]] napi_status status =
        napi_get_cb_info(env, info, nullptr, nullptr, nullptr, reinterpret_cast<void **>(&callback_ref));
    ASSERT(status == napi_ok);

    EtsCoroutine *coro = EtsCoroutine::GetCurrent();
    PandaEtsVM *vm = coro->GetPandaVM();
    ScopedManagedCodeThread scope(coro);
    [[maybe_unused]] HandleScope<ObjectHeader *> handle_scope(ManagedThread::GetCurrent());
    VMHandle<EtsObject> callback(coro, vm->GetGlobalObjectStorage()->Get(callback_ref));
    vm->GetGlobalObjectStorage()->Remove(callback_ref);

    LambdaUtils::InvokeVoid(coro, callback.GetPtr());
    if (coro->HasPendingException()) {
        napi_throw_error(env, nullptr, "EtsVM internal error");
    }
    napi_value undefined;
    napi_get_undefined(env, &undefined);
    return undefined;
}

void JsJobQueue::AddJob(EtsObject *callback)
{
    EtsCoroutine *coro = EtsCoroutine::GetCurrent();
    PandaEtsVM *vm = coro->GetPandaVM();
    napi_env env = InteropCtx::Current(coro)->GetJSEnv();
    napi_deferred deferred;
    napi_value undefined;
    napi_value js_promise;
    napi_value then_fn;

    napi_get_undefined(env, &undefined);
    napi_status status = napi_create_promise(env, &deferred, &js_promise);
    if (status != napi_ok) {
        NapiFatal("Cannot allocate a Promise instance");
    }
    status = napi_get_named_property(env, js_promise, "then", &then_fn);
    ASSERT(status == napi_ok);
    (void)status;

    mem::Reference *callback_ref =
        vm->GetGlobalObjectStorage()->Add(callback->GetCoreType(), mem::Reference::ObjectType::GLOBAL);

    napi_value then_callback;
    status = napi_create_function(env, nullptr, 0, ThenCallback, callback_ref, &then_callback);
    if (status != napi_ok) {
        NapiFatal("Cannot create a function");
    }

    napi_value then_promise;
    status = napi_call_function(env, js_promise, then_fn, 1, &then_callback, &then_promise);
    ASSERT(status == napi_ok);
    (void)status;

    napi_resolve_deferred(env, deferred, undefined);
}
}  // namespace panda::ets::interop::js
