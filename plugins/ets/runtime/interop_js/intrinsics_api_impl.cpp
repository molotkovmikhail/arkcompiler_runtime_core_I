/*
 * Copyright (c) 2023 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "plugins/ets/runtime/interop_js/js_value_call.h"
#include "plugins/ets/runtime/interop_js/js_convert.h"
#include "plugins/ets/runtime/interop_js/ts2ets_common.h"
#include "plugins/ets/runtime/interop_js/intrinsics_api.h"
#include "plugins/ets/runtime/interop_js/intrinsics_api_impl.h"
#include "plugins/ets/runtime/interop_js/napi_env_scope.h"
#include "plugins/ets/runtime/types/ets_string.h"

namespace panda::ets::interop::js {

static JSValue *JSRuntimeNewJSValueDouble(double v)
{
    auto coro = EtsCoroutine::GetCurrent();
    auto ctx = InteropCtx::Current(coro);
    return JSValue::CreateNumber(coro, ctx, v);
}

static JSValue *JSRuntimeNewJSValueString(EtsString *v)
{
    auto coro = EtsCoroutine::GetCurrent();
    auto ctx = InteropCtx::Current(coro);
    std::string str;
    if (v->IsUtf16()) {
        NapiFatal("not implemented");
    } else {
        str = std::string(utf::Mutf8AsCString(v->GetDataMUtf8()));
    }
    return JSValue::CreateString(coro, ctx, std::move(str));
}

static double JSRuntimeGetValueDouble(JSValue *ets_js_value)
{
    return ets_js_value->GetNumber();
}

static uint8_t JSRuntimeGetValueBoolean(JSValue *ets_js_value)
{
    return static_cast<uint8_t>(ets_js_value->GetBoolean());
}

static EtsString *JSRuntimeGetValueString(JSValue *ets_js_value)
{
    auto coro = EtsCoroutine::GetCurrent();
    auto ctx = InteropCtx::Current(coro);
    auto env = ctx->GetJSEnv();

    NapiScope js_handle_scope(env);

    napi_value js_val = ets_js_value->GetNapiValue(env);
    auto res = JSConvertString::Unwrap(ctx, env, js_val);
    if (UNLIKELY(!res)) {
        ctx->ForwardJSException(coro);
        return {};
    }

    return res.value();
}

template <typename T>
static typename T::cpptype JSValueNamedGetter(JSValue *ets_js_value, EtsString *ets_prop_name)
{
    auto coro = EtsCoroutine::GetCurrent();
    auto ctx = InteropCtx::Current(coro);
    auto env = ctx->GetJSEnv();
    NapiScope js_handle_scope(env);

    PandaString prop_name = ets_prop_name->GetMutf8();
    auto res = JSValueGetByName<T>(ctx, ets_js_value, prop_name.c_str());
    if (UNLIKELY(!res)) {
        ctx->ForwardJSException(coro);
        return {};
    }
    return res.value();
}

template <typename T>
static void JSValueNamedSetter(JSValue *ets_js_value, EtsString *ets_prop_name, typename T::cpptype ets_prop_val)
{
    auto coro = EtsCoroutine::GetCurrent();
    auto ctx = InteropCtx::Current(coro);
    auto env = ctx->GetJSEnv();
    NapiScope js_handle_scope(env);

    PandaString prop_name = ets_prop_name->GetMutf8();
    bool res = JSValueSetByName<T>(ctx, ets_js_value, prop_name.c_str(), ets_prop_val);
    if (UNLIKELY(!res)) {
        ctx->ForwardJSException(coro);
    }
}

static JSValue *JSRuntimeGetUndefined()
{
    auto coro = EtsCoroutine::GetCurrent();
    auto ctx = InteropCtx::Current(coro);
    return JSValue::CreateUndefined(coro, ctx);
}

static JSValue *JSRuntimeGetNull()
{
    auto coro = EtsCoroutine::GetCurrent();
    auto ctx = InteropCtx::Current(coro);
    return JSValue::CreateNull(coro, ctx);
}

static JSValue *JSRuntimeGetGlobal()
{
    auto coro = EtsCoroutine::GetCurrent();
    auto ctx = InteropCtx::Current(coro);
    auto env = ctx->GetJSEnv();
    NapiScope js_handle_scope(env);

    napi_value global;
    NAPI_CHECK_FATAL(napi_get_global(env, &global));
    return JSValue::CreateRefValue(coro, ctx, global, napi_object);
}

static JSValue *JSRuntimeCreateObject()
{
    auto coro = EtsCoroutine::GetCurrent();
    auto ctx = InteropCtx::Current(coro);
    auto env = ctx->GetJSEnv();
    NapiScope js_handle_scope(env);

    napi_value obj;
    NAPI_CHECK_FATAL(napi_create_object(env, &obj));
    return JSValue::CreateRefValue(coro, ctx, obj, napi_object);
}

uint8_t JSRuntimeInstanceOf(JSValue *object, JSValue *ctor)
{
    auto coro = EtsCoroutine::GetCurrent();
    auto ctx = InteropCtx::Current(coro);
    auto env = ctx->GetJSEnv();
    NapiScope js_handle_scope(env);

    auto js_obj = object->GetNapiValue(env);
    auto js_ctor = ctor->GetNapiValue(env);
    bool res;
    napi_status rc = napi_instanceof(env, js_obj, js_ctor, &res);
    if (UNLIKELY(NapiThrownGeneric(rc))) {
        ctx->ForwardJSException(coro);
        return 0;
    }
    return static_cast<uint8_t>(res);
}

static void FinalizeEtsGlobalRef([[maybe_unused]] napi_env env, void *fdata, [[maybe_unused]] void *fhint)
{
    auto coro = EtsCoroutine::GetCurrent();
    auto ctx = InteropCtx::Current(coro);

    ctx->Refstor()->Remove(static_cast<mem::Reference *>(fdata));
}

static JSValue *JSRuntimeCreateLambdaProxy(EtsObject *callable)
{
    auto coro = EtsCoroutine::GetCurrent();
    auto ctx = InteropCtx::Current(coro);
    auto env = ctx->GetJSEnv();

    ASSERT(callable->GetClass()->GetMethod("invoke") != nullptr);

    auto ets_ref = ctx->Refstor()->Add(callable->GetCoreType(), mem::Reference::ObjectType::GLOBAL);
    ASSERT(ets_ref != nullptr);

    NapiScope js_handle_scope(env);

    napi_value js_fn;
    NAPI_CHECK_FATAL(napi_create_function(env, "invoke", NAPI_AUTO_LENGTH, EtsLambdaProxyInvoke, ets_ref, &js_fn));
    NAPI_CHECK_FATAL(napi_add_finalizer(env, js_fn, ets_ref, FinalizeEtsGlobalRef, nullptr, nullptr));

    return JSValue::CreateRefValue(coro, ctx, js_fn, napi_function);
}

static std::pair<std::string_view, std::string_view> ResolveModuleName(std::string_view module)
{
    static const std::unordered_set<std::string_view> NATIVE_MODULE_LIST = {
        "@system.app",  "@ohos.app",       "@system.router", "@system.curves",
        "@ohos.curves", "@system.matrix4", "@ohos.matrix4"};

    constexpr std::string_view REQUIRE = "require";
    constexpr std::string_view REQUIRE_NAPI = "requireNapi";
    constexpr std::string_view REQUIRE_NATIVE_MODULE = "requireNativeModule";
    constexpr std::string_view SYSTEM_PLUGIN_PREFIX = "@system.";
    constexpr std::string_view OHOS_PLUGIN_PREFIX = "@ohos.";

    if (NATIVE_MODULE_LIST.count(module) != 0) {
        return {module.substr(1), REQUIRE_NATIVE_MODULE};
    }

    if (module.compare(0, SYSTEM_PLUGIN_PREFIX.size(), SYSTEM_PLUGIN_PREFIX) == 0) {
        return {module.substr(SYSTEM_PLUGIN_PREFIX.size()), REQUIRE_NAPI};
    }

    if (module.compare(0, OHOS_PLUGIN_PREFIX.size(), OHOS_PLUGIN_PREFIX) == 0) {
        return {module.substr(OHOS_PLUGIN_PREFIX.size()), REQUIRE_NAPI};
    }

    return {module, REQUIRE};
}

static JSValue *JSRuntimeLoadModule(EtsString *module)
{
    auto coro = EtsCoroutine::GetCurrent();
    auto ctx = InteropCtx::Current(coro);
    auto env = ctx->GetJSEnv();

    PandaString module_name = module->GetMutf8();
    auto [mod, func] = ResolveModuleName(module_name);

    NapiScope js_handle_scope(env);

    napi_value js_glob;
    napi_value require_fn;
    NAPI_CHECK_FATAL(napi_get_global(env, &js_glob));
    NAPI_CHECK_FATAL(napi_get_named_property(env, js_glob, func.data(), &require_fn));

    NAPI_FATAL_IF(GetValueType(env, require_fn) != napi_function);
    napi_value mod_obj;
    {
        napi_value js_name;
        NAPI_CHECK_FATAL(napi_create_string_utf8(env, mod.data(), NAPI_AUTO_LENGTH, &js_name));
        std::array<napi_value, 1> args = {js_name};
        napi_value recv;
        NAPI_CHECK_FATAL(napi_get_undefined(env, &recv));
        auto status = (napi_call_function(env, recv, require_fn, args.size(), args.data(), &mod_obj));

        if (status == napi_pending_exception) {
            napi_value exp;
            NAPI_CHECK_FATAL(napi_get_and_clear_last_exception(env, &exp));
            NAPI_CHECK_FATAL(napi_fatal_exception(env, exp));
            std::abort();
        }

        NAPI_FATAL_IF(status != napi_ok);
    }
    NAPI_FATAL_IF(IsUndefined(env, mod_obj));

    return JSValue::CreateRefValue(coro, ctx, mod_obj, napi_object);
}

const IntrinsicsAPI G_INTRINSICS_API = {
    JSValue::JSRuntimeFinalizationQueueCallback,
    JSRuntimeNewJSValueDouble,
    JSRuntimeNewJSValueString,
    JSRuntimeGetValueDouble,
    JSRuntimeGetValueBoolean,
    JSRuntimeGetValueString,
    JSValueNamedGetter<JSConvertJSValue>,
    JSValueNamedGetter<JSConvertF64>,
    JSValueNamedGetter<JSConvertString>,
    JSValueNamedSetter<JSConvertJSValue>,
    JSValueNamedSetter<JSConvertF64>,
    JSValueNamedSetter<JSConvertString>,
    JSRuntimeGetUndefined,
    JSRuntimeGetNull,
    JSRuntimeGetGlobal,
    JSRuntimeCreateObject,
    JSRuntimeInstanceOf,
    JSRuntimeInitJSCallClass,
    JSRuntimeInitJSNewClass,
    JSRuntimeCreateLambdaProxy,
    JSRuntimeLoadModule,
};

const IntrinsicsAPI *GetIntrinsicsAPI()
{
    return &G_INTRINSICS_API;
}

}  // namespace panda::ets::interop::js
