/**
 * Copyright (c) 2023 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef PANDA_PLUGINS_ETS_RUNTIME_INTEROP_JS_INTEROP_CONTEXT_H_
#define PANDA_PLUGINS_ETS_RUNTIME_INTEROP_JS_INTEROP_CONTEXT_H_

#include "plugins/ets/runtime/ets_vm.h"
#include "plugins/ets/runtime/ets_coroutine.h"
#include "plugins/ets/runtime/interop_js/js_refconvert.h"
#include "plugins/ets/runtime/interop_js/js_job_queue.h"
#include "plugins/ets/runtime/interop_js/intrinsics_api_impl.h"
#include "libpandabase/macros.h"
#include "runtime/include/value.h"
#include <node_api.h>
#include <unordered_map>

#include "plugins/ets/runtime/interop_js/intrinsics/std_js_jsruntime.h"

namespace panda {

class Class;
class ClassLinkerContext;

namespace mem {
class GlobalObjectStorage;
class Reference;
}  // namespace mem

}  // namespace panda

namespace panda::ets::interop::js {

class JSValue;

// Work-around for String JSValue and node_api
class JSValueStringStorage {
public:
    class CachedEntry {
    public:
        std::string const *Data()
        {
            return data_;
        }

    private:
        friend class JSValue;
        friend class JSValueStringStorage;

        explicit CachedEntry(std::string const *data) : data_(data) {}

        std::string const *data_ {};
    };

    explicit JSValueStringStorage() = default;

    CachedEntry Get(std::string &&str)
    {
        auto [it, inserted] = string_tab_.insert({str, 0});
        it->second++;
        return CachedEntry(&it->first);
    }

    void Release(CachedEntry str)
    {
        auto it = string_tab_.find(*str.Data());
        ASSERT(it != string_tab_.end());
        if (--(it->second) == 0) {
            string_tab_.erase(it);
        }
    }

private:
    std::unordered_map<std::string, uint64_t> string_tab_;
};

using ArgValueBox = std::variant<uint64_t, ObjectHeader **>;

class InteropCtx {
public:
    static void Init(EtsCoroutine *coro)
    {
        // Initialize InteropCtx in VM ExternalData
        new (InteropCtx::Current(coro)) InteropCtx(coro);
    }

    static InteropCtx *Current(PandaEtsVM *ets_vm)
    {
        static_assert(sizeof(PandaEtsVM::ExternalData) >= sizeof(InteropCtx));
        static_assert(alignof(PandaEtsVM::ExternalData) >= alignof(InteropCtx));
        return reinterpret_cast<InteropCtx *>(ets_vm->GetExternalData());
    }

    static InteropCtx *Current(EtsCoroutine *coro)
    {
        return Current(coro->GetPandaVM());
    }

    static InteropCtx *Current()
    {
        return Current(EtsCoroutine::GetCurrent());
    }

    PandaEtsVM *GetPandaEtsVM()
    {
        return PandaEtsVM::FromExternalData(reinterpret_cast<void *>(this));
    }

    napi_env GetJSEnv() const
    {
        return js_env_;
    }

    void SetJSEnv(napi_env env)
    {
        js_env_ = env;
    }

    mem::GlobalObjectStorage *Refstor() const
    {
        return refstor_;
    }

    ClassLinkerContext *LinkerCtx() const
    {
        return linker_ctx_;
    }

    JSValueStringStorage *GetStringStor()
    {
        return &js_value_string_stor_;
    }

    mem::Reference *GetJSValueFinalizationQueue() const
    {
        return jsvalue_fqueue_ref_;
    }

    Method *GetRegisterFinalizerMethod() const
    {
        return jsvalue_fqueue_register_;
    }

    template <typename T>
    std::vector<T> &GetTempArgs(size_t sz)
    {
        auto &args = std::get<std::vector<T>>(tmp_args_);
        if (UNLIKELY(args.capacity() < sz)) {
            args.reserve(sz);
        }
        args.resize(sz);
        return args;
    }

    JSRefConvertCache *GetRefConvertCache()
    {
        return &refconvert_cache_;
    }

    Class *JSValueClass() const
    {
        return jsvalue_class_;
    }

    Class *JSExceptionClass() const
    {
        return jsexception_class_;
    }

    Class *GetStringClass() const
    {
        return string_class_;
    }

    Class *GetPromiseClass() const
    {
        return promise_class_;
    }

    EtsObject *CreateJSException(EtsCoroutine *coro, JSValue *jsvalue);

    static void ThrowETSException(EtsCoroutine *coro, napi_value val);
    static void ThrowETSException(EtsCoroutine *coro, const char *msg);
    static void ThrowETSException(EtsCoroutine *coro, const std::string &msg)
    {
        ThrowETSException(coro, msg.c_str());
    }

    static void ThrowJSError(napi_env env, const std::string &msg);
    static void ThrowJSException(napi_env env, napi_value val);

    void ForwardEtsException(EtsCoroutine *coro);
    void ForwardJSException(EtsCoroutine *coro);

    [[noreturn]] static void Fatal(const char *msg);
    [[noreturn]] static void Fatal(const std::string &msg)
    {
        Fatal(msg.c_str());
    }

private:
    explicit InteropCtx(EtsCoroutine *coro);

    napi_env js_env_ {};

    mem::GlobalObjectStorage *refstor_ {};
    ClassLinkerContext *linker_ctx_ {};
    JSValueStringStorage js_value_string_stor_ {};
    mem::Reference *jsvalue_fqueue_ref_ {};

    // Temporary storages for arg convertors, std::tuple<std::vector<Types>...>
    std::tuple<std::vector<Value>, std::vector<ArgValueBox>, std::vector<napi_value>> tmp_args_;

    JSRefConvertCache refconvert_cache_;

    Class *jsruntime_class_ {};
    Class *jsvalue_class_ {};
    Class *jsexception_class_ {};
    Class *string_class_ {};
    Class *promise_class_ {};

    Method *jsvalue_fqueue_register_ {};

    friend class EtsJSNapiEnvScope;
};

inline JSRefConvertCache *RefConvertCacheFromInteropCtx(InteropCtx *ctx)
{
    return ctx->GetRefConvertCache();
}
inline napi_env JSEnvFromInteropCtx(InteropCtx *ctx)
{
    return ctx->GetJSEnv();
}

}  // namespace panda::ets::interop::js

#endif  // !PANDA_PLUGINS_ETS_RUNTIME_INTEROP_JS_INTEROP_CONTEXT_H_
