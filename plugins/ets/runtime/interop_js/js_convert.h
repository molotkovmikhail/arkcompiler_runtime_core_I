/**
 * Copyright (c) 2021-2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef PANDA_PLUGINS_ETS_RUNTIME_INTEROP_JS_JS_CONVERT_H_
#define PANDA_PLUGINS_ETS_RUNTIME_INTEROP_JS_JS_CONVERT_H_

#include "plugins/ets/runtime/ets_panda_file_items.h"
#include "plugins/ets/runtime/interop_js/ts2ets_common.h"
#include "plugins/ets/runtime/interop_js/js_value.h"
#include "runtime/handle_scope-inl.h"
#include "runtime/include/coretypes/class.h"
#include "plugins/ets/runtime/types/ets_string.h"
#include "plugins/ets/runtime/types/ets_promise.h"
#include "plugins/ets/runtime/interop_js/pending_promise_listener.h"
#include "plugins/ets/runtime/types/ets_method.h"

namespace panda::ets::interop::js {

template <typename T>
inline EtsObject *AsEtsObject(T *obj)
{
    static_assert(std::is_base_of_v<ObjectHeader, T>);
    return reinterpret_cast<EtsObject *>(obj);
}

template <typename T>
inline T *FromEtsObject(EtsObject *obj)
{
    static_assert(std::is_base_of_v<ObjectHeader, T>);
    return reinterpret_cast<T *>(obj);
}

void JSConvertTypeCheckFailed(const char *type_name);
inline void JSConvertTypeCheckFailed(const std::string &s)
{
    JSConvertTypeCheckFailed(s.c_str());
}

// Base mixin class of JSConvert interface
// Represents primitive types and some built-in classes, has no state
template <typename Impl, typename ImplCpptype>
struct JSConvertBase {
    JSConvertBase() = delete;
    using cpptype = ImplCpptype;
    static constexpr bool IS_REFTYPE = std::is_pointer_v<cpptype>;
    static constexpr size_t TYPE_SIZE = IS_REFTYPE ? ClassHelper::OBJECT_POINTER_SIZE : sizeof(cpptype);

    static void TypeCheckFailed()
    {
        JSConvertTypeCheckFailed(Impl::type_name);
    }

    // Convert ets->js, returns nullptr if failed
    static napi_value Wrap(napi_env env, cpptype ets_val)
    {
        if constexpr (IS_REFTYPE) {
            ASSERT(ets_val != nullptr);
        }
        return Impl::WrapImpl(env, ets_val);
    }

    // Convert js->ets, returns nullopt if failed
    static std::optional<cpptype> Unwrap(InteropCtx *ctx, napi_env env, napi_value js_val)
    {
        if constexpr (IS_REFTYPE) {
            ASSERT(!IsNullOrUndefined(env, js_val));
        }
        return Impl::UnwrapImpl(ctx, env, js_val);
    }

    static napi_value WrapWithNullCheck(napi_env env, cpptype ets_val)
    {
        if constexpr (IS_REFTYPE) {
            if (UNLIKELY(ets_val == nullptr)) {
                return GetNull(env);
            }
        }
        return Impl::WrapImpl(env, ets_val);
    }

    static std::optional<cpptype> UnwrapWithNullCheck(InteropCtx *ctx, napi_env env, napi_value js_val)
    {
        if constexpr (IS_REFTYPE) {
            if (UNLIKELY(IsNullOrUndefined(env, js_val))) {
                return nullptr;
            }
        }
        return Impl::UnwrapImpl(ctx, env, js_val);
    }
};

// NOLINTNEXTLINE(cppcoreguidelines-macro-usage)
#define JSCONVERT_DEFINE_TYPE(type, cpptype_)                                                                 \
    struct JSConvert##type : public JSConvertBase<JSConvert##type, cpptype_> {                                \
        static constexpr const char *type_name = #type;                                                       \
        /* Must not fail */                                                                                   \
        [[maybe_unused]] static inline napi_value WrapImpl([[maybe_unused]] napi_env env,                     \
                                                           [[maybe_unused]] cpptype ets_val);                 \
        /* May fail */                                                                                        \
        [[maybe_unused]] static inline std::optional<cpptype> UnwrapImpl([[maybe_unused]] InteropCtx *ctx,    \
                                                                         [[maybe_unused]] napi_env env,       \
                                                                         [[maybe_unused]] napi_value js_val); \
    };

// NOLINTNEXTLINE(cppcoreguidelines-macro-usage)
#define JSCONVERT_WRAP(type) \
    inline napi_value JSConvert##type::WrapImpl([[maybe_unused]] napi_env env, [[maybe_unused]] cpptype ets_val)

// NOLINTNEXTLINE(cppcoreguidelines-macro-usage)
#define JSCONVERT_UNWRAP(type)                                                  \
    inline std::optional<JSConvert##type::cpptype> JSConvert##type::UnwrapImpl( \
        [[maybe_unused]] InteropCtx *ctx, [[maybe_unused]] napi_env env, [[maybe_unused]] napi_value js_val)

JSCONVERT_DEFINE_TYPE(U1, bool)
JSCONVERT_WRAP(U1)
{
    napi_value js_val;
    NAPI_CHECK_FATAL(napi_get_boolean(env, static_cast<bool>(ets_val), &js_val));
    return js_val;
}
JSCONVERT_UNWRAP(U1)
{
    if (UNLIKELY(GetValueType(env, js_val) != napi_boolean)) {
        TypeCheckFailed();
        return {};
    }
    bool val;
    NAPI_CHECK_FATAL(napi_get_value_bool(env, js_val, &val));
    return val;
}

JSCONVERT_DEFINE_TYPE(I32, int32_t)
JSCONVERT_WRAP(I32)
{
    napi_value js_val;
    NAPI_CHECK_FATAL(napi_create_int32(env, ets_val, &js_val));
    return js_val;
}
JSCONVERT_UNWRAP(I32)
{
    if (UNLIKELY(GetValueType(env, js_val) != napi_number)) {
        TypeCheckFailed();
        return {};
    }
    int32_t val;
    NAPI_CHECK_FATAL(napi_get_value_int32(env, js_val, &val));
    return val;
}

JSCONVERT_DEFINE_TYPE(F32, float)
JSCONVERT_WRAP(F32)
{
    napi_value js_val;
    NAPI_CHECK_FATAL(napi_create_double(env, ets_val, &js_val));
    return js_val;
}
JSCONVERT_UNWRAP(F32)
{
    if (UNLIKELY(GetValueType(env, js_val) != napi_number)) {
        TypeCheckFailed();
        return {};
    }
    double val;
    NAPI_CHECK_FATAL(napi_get_value_double(env, js_val, &val));
    return static_cast<float>(val);
}

JSCONVERT_DEFINE_TYPE(F64, double)
JSCONVERT_WRAP(F64)
{
    napi_value js_val;
    NAPI_CHECK_FATAL(napi_create_double(env, ets_val, &js_val));
    return js_val;
}
JSCONVERT_UNWRAP(F64)
{
    if (UNLIKELY(GetValueType(env, js_val) != napi_number)) {
        TypeCheckFailed();
        return {};
    }
    double val;
    NAPI_CHECK_FATAL(napi_get_value_double(env, js_val, &val));
    return val;
}

JSCONVERT_DEFINE_TYPE(I64, int64_t)
JSCONVERT_WRAP(I64)
{
    napi_value js_val;
    NAPI_CHECK_FATAL(napi_create_int64(env, ets_val, &js_val));
    return js_val;
}
JSCONVERT_UNWRAP(I64)
{
    if (UNLIKELY(GetValueType(env, js_val) != napi_number)) {
        TypeCheckFailed();
        return {};
    }
    int64_t val;
    NAPI_CHECK_FATAL(napi_get_value_int64(env, js_val, &val));
    return val;
}

JSCONVERT_DEFINE_TYPE(String, EtsString *)
JSCONVERT_WRAP(String)
{
    napi_value js_val;
    if (UNLIKELY(ets_val->IsUtf16())) {
        auto str = reinterpret_cast<char16_t *>(ets_val->GetDataUtf16());
        NAPI_CHECK_FATAL(napi_create_string_utf16(env, str, ets_val->GetUtf16Length(), &js_val));
    } else {
        auto str = utf::Mutf8AsCString(ets_val->GetDataMUtf8());
        // -1 for NULL terminated Mutf8 string!!!
        NAPI_CHECK_FATAL(napi_create_string_utf8(env, str, ets_val->GetMUtf8Length() - 1, &js_val));
    }
    return js_val;
}
JSCONVERT_UNWRAP(String)
{
    if (UNLIKELY(GetValueType(env, js_val) != napi_string)) {
        TypeCheckFailed();
        return {};
    }
    std::string value = GetString(env, js_val);
    return EtsString::CreateFromUtf8(value.data(), value.length());
}

JSCONVERT_DEFINE_TYPE(JSValue, JSValue *)
JSCONVERT_WRAP(JSValue)
{
    return ets_val->GetNapiValue(env);
}
JSCONVERT_UNWRAP(JSValue)
{
    return JSValue::Create(EtsCoroutine::GetCurrent(), ctx, js_val);
}

JSCONVERT_DEFINE_TYPE(JSException, EtsObject *)  // TODO(vpukhov): associate with js Error?
JSCONVERT_WRAP(JSException)
{
    auto coro = EtsCoroutine::GetCurrent();
    auto ctx = InteropCtx::Current(coro);

    auto klass = ets_val->GetClass();
    NAPI_FATAL_IF(klass->GetRuntimeClass() != ctx->JSExceptionClass());

    // TODO(vpukhov): remove call after adding a mirror-class for JSException
    auto method = klass->GetMethod("getValue");
    ASSERT(method != nullptr);
    std::array args = {Value(ets_val->GetCoreType())};
    auto val = JSValue::FromCoreType(method->GetPandaMethod()->Invoke(coro, args.data()).GetAs<ObjectHeader *>());
    NAPI_FATAL_IF(val == nullptr);
    return val->GetNapiValue(env);
}
JSCONVERT_UNWRAP(JSException)
{
    auto coro = EtsCoroutine::GetCurrent();
    auto value = JSValue::Create(coro, ctx, js_val);
    if (UNLIKELY(value == nullptr)) {
        return {};
    }
    auto res = ctx->CreateJSException(coro, value);
    if (UNLIKELY(res == nullptr)) {
        return {};
    }
    return res;
}

JSCONVERT_DEFINE_TYPE(Promise, EtsPromise *)

JSCONVERT_WRAP(Promise)
{
    napi_deferred deferred;
    napi_value js_promise;
    NAPI_CHECK_FATAL(napi_create_promise(env, &deferred, &js_promise));
    auto *coro = EtsCoroutine::GetCurrent();
    if (ets_val->GetState() != EtsPromise::STATE_PENDING) {
        EtsObject *value = ets_val->GetValue(coro);
        napi_value completion_value;
        auto ctx = InteropCtx::Current();
        if (value == nullptr) {
            napi_get_null(env, &completion_value);
        } else if (value->GetClass()->IsStringClass()) {
            completion_value = JSConvertString::Wrap(env, reinterpret_cast<EtsString *>(value));
        } else if (value->GetClass()->GetRuntimeClass() == ctx->GetPromiseClass()) {
            completion_value = JSConvertPromise::Wrap(env, reinterpret_cast<EtsPromise *>(value));
        } else {
            NapiFatal("unsupported type");
        }
        if (ets_val->IsResolved()) {
            NAPI_CHECK_FATAL(napi_resolve_deferred(env, deferred, completion_value));
        } else {
            NAPI_CHECK_FATAL(napi_reject_deferred(env, deferred, completion_value));
        }
    } else {
        coro->GetPandaVM()->AddPromiseListener(ets_val, MakePandaUnique<PendingPromiseListener>(deferred));
    }
    return js_promise;
}

JSCONVERT_UNWRAP(Promise)
{
    UNREACHABLE();
}

#undef JSCONVERT_DEFINE_TYPE
#undef JSCONVERT_WRAP
#undef JSCONVERT_UNWRAP

template <typename T>
static ALWAYS_INLINE inline std::optional<typename T::cpptype> JSValueGetByName(InteropCtx *ctx, JSValue *jsvalue,
                                                                                const char *name)
{
    auto env = ctx->GetJSEnv();
    napi_value js_val = jsvalue->GetNapiValue(env);
    napi_status rc = napi_get_named_property(env, js_val, name, &js_val);
    if (UNLIKELY(NapiThrownGeneric(rc))) {
        return {};
    }
    return T::UnwrapWithNullCheck(ctx, env, js_val);
}

template <typename T>
[[nodiscard]] static ALWAYS_INLINE inline bool JSValueSetByName(InteropCtx *ctx, JSValue *jsvalue, const char *name,
                                                                typename T::cpptype ets_prop_val)
{
    auto env = ctx->GetJSEnv();
    napi_value js_val = jsvalue->GetNapiValue(env);
    napi_value js_prop_val = T::WrapWithNullCheck(env, ets_prop_val);
    if (UNLIKELY(js_prop_val == nullptr)) {
        return false;
    }
    napi_status rc = napi_set_named_property(env, js_val, name, js_prop_val);
    return !NapiThrownGeneric(rc);
}

}  // namespace panda::ets::interop::js

#endif  // !PANDA_PLUGINS_ETS_RUNTIME_INTEROP_JS_JS_CONVERT_H_
