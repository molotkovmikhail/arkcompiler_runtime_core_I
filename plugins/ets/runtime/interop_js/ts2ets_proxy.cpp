/**
 * Copyright (c) 2021-2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "runtime/include/coretypes/class.h"
#include "plugins/ets/runtime/interop_js/interop_context.h"
#include "plugins/ets/runtime/interop_js/napi_env_scope.h"
#include "plugins/ets/runtime/interop_js/ts2ets_common.h"
#include "plugins/ets/runtime/interop_js/ets_type_visitor-inl.h"
#include "plugins/ets/runtime/types/ets_method.h"
#include "runtime/handle_scope-inl.h"

namespace panda::ets::interop::js {

#ifndef NDEBUG
// Debug log
// NOLINTNEXTLINE(cppcoreguidelines-macro-usage)
#define PROXY_DLOG(msg) INTEROP_LOG_INFO(msg)
// NOLINTNEXTLINE(cppcoreguidelines-macro-usage)
#define PROXY_DLOG_A(msg, ...) INTEROP_LOG_INFO_A(msg, __VA_ARGS__)
// Wrappers cache log
// NOLINTNEXTLINE(cppcoreguidelines-macro-usage)
#define PROXY_CLOG(msg) INTEROP_LOG_INFO(msg)
// NOLINTNEXTLINE(cppcoreguidelines-macro-usage)
#define PROXY_CLOG_A(msg, ...) INTEROP_LOG_INFO_A(msg, __VA_ARGS__)
#else
#define PROXY_DLOG(msg)
#define PROXY_DLOG_A(msg, ...)
#define PROXY_CLOG(msg)
#define PROXY_CLOG_A(msg, ...)
#endif

// Lazy tagged pointer
template <typename U, typename R>
struct WrapperLink {
    static_assert(alignof(U) >= (1U << 1U));

    WrapperLink() = default;

    explicit WrapperLink(U *ptr)
    {
        Set(ptr, true);
        ASSERT(!IsResolved());
    }
    explicit WrapperLink(R *ptr)
    {
        Set(ptr, false);
        ASSERT(IsResolved());
    }

    bool IsResolved() const
    {
        return !GetTag();
    }

    auto GetUnresolved() const
    {
        ASSERT(!IsResolved());
        return reinterpret_cast<U *>(GetUPtr());
    }

    auto GetResolved() const
    {
        ASSERT(IsResolved());
        return reinterpret_cast<R *>(ptr_);
    }

private:
    auto static constexpr MASK = ~static_cast<uintptr_t>(1);

    bool GetTag() const
    {
        return static_cast<bool>(ptr_ & ~MASK);
    }

    uintptr_t GetUPtr() const
    {
        return ptr_ & MASK;
    }

    template <typename T>
    void Set(T *ptr, bool tag)
    {
        ptr_ = reinterpret_cast<uintptr_t>(ptr);
        ASSERT(!GetTag());
        ptr_ |= static_cast<uintptr_t>(tag);
    }

    uintptr_t ptr_ {};
};

struct WrapperClass;
struct WrapperMethod;
using WrapperClassLink = WrapperLink<panda::Class, WrapperClass>;
using WrapperMethodLink = WrapperLink<panda::Method, WrapperMethod>;

struct WrapperField {
    WrapperClass *wclass_owner {};
    WrapperClassLink wclass_field {};  // used if reftype
    uint32_t ets_offs {};
};

// NOLINTBEGIN(misc-non-private-member-variables-in-classes)
struct WrapperClass {
    panda::Class *ets_klass {};
    WrapperMethodLink ets_init {};
    napi_ref js_ctor {};
    panda::ObjectHeader *pending_ets_obj {};     // passed to js_ctor
    std::vector<WrapperField> wfields {};        // invalidates cached values if reallocated
    std::vector<WrapperMethodLink> wmethods {};  // invalidates cached values if reallocated

    bool shareable {};  // subtype of Shareable, enables reference tracking for ETS objects

    NO_COPY_SEMANTIC(WrapperClass);
    NO_MOVE_SEMANTIC(WrapperClass);

    explicit WrapperClass() = default;
    ~WrapperClass() = default;
};
// NOLINTEND(misc-non-private-member-variables-in-classes)

class WrapperClassCache {
public:
    static WrapperClass *Lookup(panda::Class *klass)
    {
        auto it = cache_.find(klass);
        if (UNLIKELY(it == cache_.end())) {
            return nullptr;
        }
        return &it->second;
    }

    static WrapperClass *Insert(panda::Class *klass)
    {
        ASSERT(Lookup(klass) == nullptr);
        auto [it, found] =
            cache_.emplace(std::piecewise_construct, std::forward_as_tuple(klass), std::forward_as_tuple());
        ASSERT(found);

        auto &res = it->second;

        // NOLINTNEXTLINE(cppcoreguidelines-pro-type-vararg)
        PROXY_DLOG_A("WrapperClassCache: inserted: %p -> %p", (void *)klass, (void *)&res);

        return &res;
    }

    static void Remove(panda::Class *klass)
    {
        ASSERT(Lookup(klass) != nullptr);
        cache_.erase(klass);
    }

    WrapperClassCache() = delete;

private:
    // NOLINTNEXTLINE(fuchsia-statically-constructed-objects)
    static std::unordered_map<panda::Class *, WrapperClass> cache_;
};

// NOLINTNEXTLINE(fuchsia-statically-constructed-objects)
std::unordered_map<panda::Class *, WrapperClass> WrapperClassCache::cache_;

struct WrapperRef {
    // NOLINTNEXTLINE(misc-non-private-member-variables-in-classes)
    panda::mem::Reference *ets_ref {};
    // NOLINTNEXTLINE(misc-non-private-member-variables-in-classes)
    napi_ref js_ref {};

    static constexpr size_t ETS_SHAREABLE_DATA_OFFSET = sizeof(panda::ObjectHeader);

    void LinkShareable(panda::ObjectHeader *ets_obj)
    {
        auto val = reinterpret_cast<uint64_t>(this);
        ets_obj->SetFieldPrimitive(ETS_SHAREABLE_DATA_OFFSET, val);
    }

    static void UnlinkShareable(panda::ObjectHeader *ets_obj)
    {
        uint64_t val = 0;
        ets_obj->SetFieldPrimitive(ETS_SHAREABLE_DATA_OFFSET, val);
    }

    static WrapperRef *FromShareable(panda::ObjectHeader *ets_obj)
    {
        auto val = ets_obj->GetFieldPrimitive<uint64_t>(ETS_SHAREABLE_DATA_OFFSET);
        return reinterpret_cast<WrapperRef *>(val);
    }
};

class WrapperRefCache {
public:
    static WrapperRef *Alloc()
    {
        // NOLINTNEXTLINE(cppcoreguidelines-no-malloc)
        void *mem = malloc(sizeof(WrapperClass));
        assert(mem);
        return new (mem) WrapperRef {};
    }

    static void Free(WrapperRef *ref)
    {
        ref->~WrapperRef();
        // NOLINTNEXTLINE(cppcoreguidelines-no-malloc)
        free(ref);
    }

    WrapperRefCache() = delete;
};

struct WrapperType;

// NOLINTBEGIN(misc-non-private-member-variables-in-classes)
struct WrapperValueConvertors {
    napi_value (*wrap)(napi_env env, WrapperType *wtype, panda::Value ets_val);
    bool (*unwrap)(napi_env env, WrapperType *wtype, napi_value js_val, panda::Value &ets_val);
    WrapperValueConvertors() = default;
};

struct WrapperType {
    WrapperValueConvertors conv {};
    WrapperClass *wclass {};  // used if reftype
    WrapperType() = default;
};

struct WrapperMethod {
    panda::Method *ets_method {};
    napi_ref js_func {};                      // only for functions (ETSGLOBAL::)
    WrapperType wclass_this {};               // this class wrapper, only for instance methods
    WrapperType wclass_ret {};                // return class value wrapper
    std::vector<WrapperType> wclass_args {};  // invalidates cached values if reallocated
};
// NOLINTEND(misc-non-private-member-variables-in-classes)

class WrapperMethodCache {
public:
    static WrapperMethod *Lookup(panda::Method *method)
    {
        auto it = cache_.find(method);
        if (UNLIKELY(it == cache_.end())) {
            return nullptr;
        }
        return &it->second;
    }

    static WrapperMethod *Insert(panda::Method *method)
    {
        ASSERT(Lookup(method) == nullptr);
        auto [it, found] =
            cache_.emplace(std::piecewise_construct, std::forward_as_tuple(method), std::forward_as_tuple());
        ASSERT(found);
        auto &res = it->second;
        // NOLINTNEXTLINE(cppcoreguidelines-pro-type-vararg)
        PROXY_DLOG_A("WrapperMethodCache: inserted: %p -> %p", (void *)method, (void *)&res);
        return &res;
    }

    static void Remove(panda::Method *method)
    {
        ASSERT(Lookup(method) != nullptr);
        cache_.erase(method);
    }

    WrapperMethodCache() = delete;

private:
    // NOLINTNEXTLINE(fuchsia-statically-constructed-objects)
    static std::unordered_map<panda::Method *, WrapperMethod> cache_;
};

// NOLINTNEXTLINE(fuchsia-statically-constructed-objects)
std::unordered_map<panda::Method *, WrapperMethod> WrapperMethodCache::cache_;

// unwrap object with typecheck
static inline WrapperRef *WrapperObjUnwrap(napi_env env, napi_value jsval)
{
    void *data;
    NAPI_CHECK_FATAL(napi_unwrap(env, jsval, &data));
    ASSERT(data != nullptr);
    return reinterpret_cast<WrapperRef *>(data);
}

static WrapperClass *GetWrapperClass(napi_env env, panda::Class *klass);

// allocate js wrapper for existing ets object
static napi_value WrapperCreateForExisting(napi_env env, WrapperClass *wclass, panda::ObjectHeader *ets_obj)
{
    napi_value js_ctor {};
    napi_value js_val {};
    NAPI_CHECK_FATAL(napi_get_reference_value(env, wclass->js_ctor, &js_ctor));
    wclass->pending_ets_obj = ets_obj;
    NAPI_CHECK_FATAL(napi_new_instance(env, js_ctor, 0, nullptr, &js_val));
    return js_val;
}

#ifndef NDEBUG
static void DumpField(WrapperField *wfield)
{
    [[maybe_unused]] panda::Class *owner_klass = wfield->wclass_owner->ets_klass;
    panda::Class *field_class;
    auto wclass_field = wfield->wclass_field;
    bool resolved = wclass_field.IsResolved();
    if (resolved) {
        field_class = wclass_field.GetResolved()->ets_klass;
    } else {
        field_class = wclass_field.GetUnresolved();
    }

    if (field_class != nullptr) {
        // NOLINTNEXTLINE(cppcoreguidelines-pro-type-vararg)
        PROXY_DLOG_A("DumpField: own_cls=%p offs=%u field_cls=%p", (void *)owner_klass, wfield->ets_offs,
                     (void *)field_class);
    } else {
        // NOLINTNEXTLINE(cppcoreguidelines-pro-type-vararg)
        PROXY_DLOG_A("DumpField: own_cls=%p offs=%u <primitive>", (void *)owner_klass, wfield->ets_offs);
    }
}
#else
static void DumpField([[maybe_unused]] WrapperField *wfield) {}
#endif

// getter owner check
static bool WrapperFieldGetCheck(napi_env env, napi_callback_info cinfo, WrapperField *&wfield, WrapperRef *&wref)
{
    // NOLINTNEXTLINE(cppcoreguidelines-pro-type-vararg)
    PROXY_DLOG("WrapperFieldGetCheck");

    size_t argc = 0;
    napi_value athis;
    void *data;
    NAPI_CHECK_FATAL(napi_get_cb_info(env, cinfo, &argc, nullptr, &athis, &data));
    if (UNLIKELY(argc != 0)) {
        InteropCtx::ThrowJSError(env, "getter called in wrong context");
        return false;
    }

    wfield = reinterpret_cast<WrapperField *>(data);
    DumpField(wfield);
    wref = WrapperObjUnwrap(env, athis);
    return wref != nullptr;
}

// setter owner check
static bool WrapperFieldSetCheck(napi_env env, napi_callback_info cinfo, WrapperField *&wfield, WrapperRef *&wref,
                                 napi_value &js_val)
{
    // NOLINTNEXTLINE(cppcoreguidelines-pro-type-vararg)
    PROXY_DLOG("WrapperFieldSetCheck");

    size_t argc = 1;
    napi_value athis;
    void *data;
    NAPI_CHECK_FATAL(napi_get_cb_info(env, cinfo, &argc, &js_val, &athis, &data));
    if (UNLIKELY(argc != 1)) {
        InteropCtx::ThrowJSError(env, "setter called in wrong context");
        return false;
    }

    wfield = reinterpret_cast<WrapperField *>(data);
    DumpField(wfield);
    wref = WrapperObjUnwrap(env, athis);
    return wref != nullptr;
}

// static getter owner check
static bool WrapperStaticFieldGetCheck(napi_env env, napi_callback_info cinfo, WrapperField *&wfield)
{
    // NOLINTNEXTLINE(cppcoreguidelines-pro-type-vararg)
    PROXY_DLOG("WrapperStaticFieldGetCheck");

    size_t argc = 0;
    napi_value athis;
    void *data;
    NAPI_CHECK_FATAL(napi_get_cb_info(env, cinfo, &argc, nullptr, &athis, &data));
    if (UNLIKELY(argc != 0)) {
        InteropCtx::ThrowJSError(env, "getter called in wrong context");
        return false;
    }

    wfield = reinterpret_cast<WrapperField *>(data);
    DumpField(wfield);
    return true;
}

// static setter owner check
static bool WrapperStaticFieldSetCheck(napi_env env, napi_callback_info cinfo, WrapperField *&wfield,
                                       napi_value &js_val)
{
    // NOLINTNEXTLINE(cppcoreguidelines-pro-type-vararg)
    PROXY_DLOG("WrapperStaticFieldSetCheck");

    size_t argc = 1;
    napi_value athis;
    void *data;
    NAPI_CHECK_FATAL(napi_get_cb_info(env, cinfo, &argc, &js_val, &athis, &data));
    if (UNLIKELY(argc != 1)) {
        InteropCtx::ThrowJSError(env, "setter called in wrong context");
        return false;
    }

    wfield = reinterpret_cast<WrapperField *>(data);
    DumpField(wfield);
    return true;
}

WrapperClass *WrapperClassLinkResolve(napi_env env, WrapperClassLink &link)
{
    if (LIKELY(link.IsResolved())) {
        // NOLINTNEXTLINE(cppcoreguidelines-pro-type-vararg)
        PROXY_DLOG_A("WrapperClassLinkResolve: hit: %p -> %p", (void *)link.GetResolved()->ets_klass,
                     (void *)link.GetResolved());
        return link.GetResolved();
    }
    // NOLINTNEXTLINE(cppcoreguidelines-pro-type-vararg)
    PROXY_CLOG_A("WrapperClassLinkResolve: miss: %p", (void *)link.GetUnresolved());
    WrapperClass *wclass = GetWrapperClass(env, link.GetUnresolved());
    if (UNLIKELY(wclass == nullptr)) {
        return nullptr;
    }
    link = WrapperClassLink(wclass);
    // NOLINTNEXTLINE(cppcoreguidelines-pro-type-vararg)
    PROXY_DLOG_A("WrapperClassLinkResolve: resolved: %p -> %p", (void *)link.GetResolved()->ets_klass,
                 (void *)link.GetResolved());
    return wclass;
}

// internal ets->js
// NOLINTNEXTLINE(cppcoreguidelines-macro-usage)
#define TS2ETS_WRAPPER_WRAP(type, cpptype)                                                                   \
    static inline napi_value WrapperValueWrapImpl##type(napi_env env, [[maybe_unused]] WrapperClass *wclass, \
                                                        cpptype ets_val);                                    \
    static napi_value WrapperValueWrap##type(napi_env env, WrapperType *wtype, panda::Value ets_val)         \
    {                                                                                                        \
        return WrapperValueWrapImpl##type(env, wtype->wclass, ets_val.GetAs<cpptype>());                     \
    }                                                                                                        \
    static inline napi_value WrapperValueWrapImpl##type(                                                     \
        [[maybe_unused]] napi_env env, [[maybe_unused]] WrapperClass *wclass, [[maybe_unused]] cpptype ets_val)

// internal ets->js
// NOLINTNEXTLINE(cppcoreguidelines-macro-usage)
#define TS2ETS_WRAPPER_UNWRAP(type, cpptype)                                                                          \
    static inline bool WrapperValueUnwrapImpl##type(napi_env env, [[maybe_unused]] WrapperClass *wclass,              \
                                                    napi_value js_val, cpptype &ets_val);                             \
    static bool WrapperValueUnwrap##type(napi_env env, WrapperType *wtype, napi_value js_val, panda::Value &ets_val)  \
    {                                                                                                                 \
        cpptype val;                                                                                                  \
        if (UNLIKELY(!WrapperValueUnwrapImpl##type(env, wtype->wclass, js_val, val))) {                               \
            return false;                                                                                             \
        }                                                                                                             \
        ets_val = panda::Value(val);                                                                                  \
        return true;                                                                                                  \
    }                                                                                                                 \
    /* so wrap wase also defined */                                                                                   \
    [[maybe_unused]] static constexpr WrapperValueConvertors WrapperValueConvertors##type {WrapperValueWrap##type,    \
                                                                                           WrapperValueUnwrap##type}; \
    static inline bool WrapperValueUnwrapImpl##type([[maybe_unused]] napi_env env,                                    \
                                                    [[maybe_unused]] WrapperClass *wclass,                            \
                                                    [[maybe_unused]] napi_value js_val, cpptype &ets_val)

// getter accessor
// NOLINTNEXTLINE(cppcoreguidelines-macro-usage)
#define TS2ETS_WRAPPER_FIELD_GET(type)                                                                        \
    static inline napi_value WrapperFieldGetImpl##type(napi_env env, WrapperField *wfield, WrapperRef *wref); \
    static napi_value WrapperFieldGet##type(napi_env env, napi_callback_info cinfo)                           \
    {                                                                                                         \
        auto coro = EtsCoroutine::GetCurrent();                                                               \
        ScopedManagedCodeThreadSwitch managed_scope(coro);                                                    \
        WrapperField *wfield;                                                                                 \
        WrapperRef *wref;                                                                                     \
        if (UNLIKELY(!WrapperFieldGetCheck(env, cinfo, wfield, wref))) {                                      \
            return napi_value {nullptr};                                                                      \
        }                                                                                                     \
        return WrapperFieldGetImpl##type(env, wfield, wref);                                                  \
    }                                                                                                         \
    static inline napi_value WrapperFieldGetImpl##type(napi_env env, WrapperField *wfield, WrapperRef *wref)

// setter accessor
// NOLINTNEXTLINE(cppcoreguidelines-macro-usage)
#define TS2ETS_WRAPPER_FIELD_SET(type)                                                                 \
    static inline void WrapperFieldSetImpl##type(napi_env env, WrapperField *wfield, WrapperRef *wref, \
                                                 napi_value js_val);                                   \
    static napi_value WrapperFieldSet##type(napi_env env, napi_callback_info cinfo)                    \
    {                                                                                                  \
        auto coro = EtsCoroutine::GetCurrent();                                                        \
        ScopedManagedCodeThreadSwitch managed_scope(coro);                                             \
        WrapperField *wfield;                                                                          \
        WrapperRef *wref;                                                                              \
        napi_value js_val;                                                                             \
        if (UNLIKELY(!WrapperFieldSetCheck(env, cinfo, wfield, wref, js_val))) {                       \
            return napi_value {nullptr};                                                               \
        }                                                                                              \
        WrapperFieldSetImpl##type(env, wfield, wref, js_val);                                          \
        return napi_value {nullptr};                                                                   \
    }                                                                                                  \
    static inline void WrapperFieldSetImpl##type(napi_env env, WrapperField *wfield, WrapperRef *wref, \
                                                 napi_value js_val)

// static getter accessor
// NOLINTNEXTLINE(cppcoreguidelines-macro-usage)
#define TS2ETS_WRAPPER_STATIC_FIELD_GET(type)                                                     \
    static inline napi_value WrapperStaticFieldGetImpl##type(napi_env env, WrapperField *wfield); \
    static napi_value WrapperStaticFieldGet##type(napi_env env, napi_callback_info cinfo)         \
    {                                                                                             \
        auto coro = EtsCoroutine::GetCurrent();                                                   \
        ScopedManagedCodeThreadSwitch managed_scope(coro);                                        \
        WrapperField *wfield;                                                                     \
        if (UNLIKELY(!WrapperStaticFieldGetCheck(env, cinfo, wfield))) {                          \
            return napi_value {nullptr};                                                          \
        }                                                                                         \
        return WrapperStaticFieldGetImpl##type(env, wfield);                                      \
    }                                                                                             \
    static inline napi_value WrapperStaticFieldGetImpl##type(napi_env env, WrapperField *wfield)

// static setter accessor
// NOLINTNEXTLINE(cppcoreguidelines-macro-usage)
#define TS2ETS_WRAPPER_STATIC_FIELD_SET(type)                                                                   \
    static inline void WrapperStaticFieldSetImpl##type(napi_env env, WrapperField *wfield, napi_value &js_val); \
    static napi_value WrapperStaticFieldSet##type(napi_env env, napi_callback_info cinfo)                       \
    {                                                                                                           \
        auto coro = EtsCoroutine::GetCurrent();                                                                 \
        ScopedManagedCodeThreadSwitch managed_scope(coro);                                                      \
        WrapperField *wfield;                                                                                   \
        napi_value js_val;                                                                                      \
        if (UNLIKELY(!WrapperStaticFieldSetCheck(env, cinfo, wfield, js_val))) {                                \
            return napi_value {nullptr};                                                                        \
        }                                                                                                       \
        WrapperStaticFieldSetImpl##type(env, wfield, js_val);                                                   \
        return napi_value {nullptr};                                                                            \
    }                                                                                                           \
    static inline void WrapperStaticFieldSetImpl##type([[maybe_unused]] napi_env env,                           \
                                                       [[maybe_unused]] WrapperField *wfield, napi_value &js_val)

// accessors for primitive fields
// NOLINTNEXTLINE(cppcoreguidelines-macro-usage)
#define TS2ETS_WRAPPER_FIELD_ACCESSOR_PRIMITIVE(type, cpptype)                         \
    TS2ETS_WRAPPER_FIELD_GET(type)                                                     \
    {                                                                                  \
        auto ets_obj = InteropCtx::Current()->Refstor()->Get(wref->ets_ref);           \
        auto ets_val = ets_obj->GetFieldPrimitive<cpptype>(wfield->ets_offs);          \
        return WrapperValueWrapImpl##type(env, nullptr, ets_val);                      \
    }                                                                                  \
    TS2ETS_WRAPPER_FIELD_SET(type)                                                     \
    {                                                                                  \
        cpptype ets_val;                                                               \
        if (UNLIKELY(!WrapperValueUnwrapImpl##type(env, nullptr, js_val, ets_val))) {  \
            InteropCtx::ThrowJSError(env, "unwrap type mismatch, " #type " expected"); \
            return;                                                                    \
        }                                                                              \
        auto ets_obj = InteropCtx::Current()->Refstor()->Get(wref->ets_ref);           \
        ets_obj->SetFieldPrimitive(wfield->ets_offs, ets_val);                         \
    }

// NOLINTNEXTLINE(cppcoreguidelines-macro-usage)
#define TS2ETS_WRAPPER_STATIC_FIELD_ACCESSOR_PRIMITIVE(type, cpptype)                                 \
    TS2ETS_WRAPPER_STATIC_FIELD_GET(type)                                                             \
    {                                                                                                 \
        auto ets_val = wfield->wclass_owner->ets_klass->GetFieldPrimitive<cpptype>(wfield->ets_offs); \
        return WrapperValueWrapImpl##type(env, nullptr, ets_val);                                     \
    }                                                                                                 \
    TS2ETS_WRAPPER_STATIC_FIELD_SET(type)                                                             \
    {                                                                                                 \
        cpptype ets_val;                                                                              \
        if (UNLIKELY(!WrapperValueUnwrapImpl##type(env, nullptr, js_val, ets_val))) {                 \
            InteropCtx::ThrowJSError(env, "unwrap type mismatch, " #type " expected");                \
            return;                                                                                   \
        }                                                                                             \
        wfield->wclass_owner->ets_klass->SetFieldPrimitive<cpptype>(wfield->ets_offs, ets_val);       \
    }

TS2ETS_WRAPPER_WRAP(VOID, int8_t)
{
    napi_value js_val;
    NAPI_CHECK_FATAL(napi_get_undefined(env, &js_val));
    return js_val;
}
TS2ETS_WRAPPER_UNWRAP(VOID, int8_t)
{
    static_cast<void>(ets_val);
    UNREACHABLE();  // "unwrap void" shouldnt happen
}

TS2ETS_WRAPPER_WRAP(F32, float)
{
    // NOLINTNEXTLINE(cppcoreguidelines-pro-type-vararg)
    PROXY_DLOG_A("TS2ETS_WRAPPER_WRAP: float %g", ets_val);
    napi_value js_val;
    NAPI_CHECK_FATAL(napi_create_double(env, static_cast<double>(ets_val), &js_val));
    return js_val;
}
TS2ETS_WRAPPER_UNWRAP(F32, float)
{
    if (UNLIKELY(GetValueType(env, js_val) != napi_number)) {
        InteropCtx::ThrowJSError(env, "unwrap type mismatch, number expected");
        return false;
    }
    double val;
    NAPI_CHECK_FATAL(napi_get_value_double(env, js_val, &val));
    ets_val = val;
    return true;
}
TS2ETS_WRAPPER_FIELD_ACCESSOR_PRIMITIVE(F32, float)
TS2ETS_WRAPPER_STATIC_FIELD_ACCESSOR_PRIMITIVE(F32, float)

TS2ETS_WRAPPER_WRAP(U1, bool)
{
    // NOLINTNEXTLINE(cppcoreguidelines-pro-type-vararg)
    PROXY_DLOG_A("TS2ETS_WRAPPER_WRAP: bool %g", ets_val);
    napi_value js_val;
    NAPI_CHECK_FATAL(napi_get_boolean(env, static_cast<bool>(ets_val), &js_val));
    return js_val;
}
TS2ETS_WRAPPER_UNWRAP(U1, bool)
{
    if (UNLIKELY(GetValueType(env, js_val) != napi_boolean)) {
        InteropCtx::ThrowJSError(env, "unwrap type mismatch, number expected");
        return false;
    }
    bool val;
    NAPI_CHECK_FATAL(napi_get_value_bool(env, js_val, &val));
    ets_val = val;
    return true;
}
TS2ETS_WRAPPER_FIELD_ACCESSOR_PRIMITIVE(U1, bool)
TS2ETS_WRAPPER_STATIC_FIELD_ACCESSOR_PRIMITIVE(U1, bool)

TS2ETS_WRAPPER_WRAP(F64, double)
{
    // NOLINTNEXTLINE(cppcoreguidelines-pro-type-vararg)
    PROXY_DLOG_A("TS2ETS_WRAPPER_WRAP: double %lg", ets_val);
    napi_value js_val;
    NAPI_CHECK_FATAL(napi_create_double(env, static_cast<double>(ets_val), &js_val));
    return js_val;
}
TS2ETS_WRAPPER_UNWRAP(F64, double)
{
    if (UNLIKELY(GetValueType(env, js_val) != napi_number)) {
        InteropCtx::ThrowJSError(env, "unwrap type mismatch, number expected");
        return false;
    }
    double val;
    NAPI_CHECK_FATAL(napi_get_value_double(env, js_val, &val));
    ets_val = val;
    return true;
}
TS2ETS_WRAPPER_FIELD_ACCESSOR_PRIMITIVE(F64, double)
TS2ETS_WRAPPER_STATIC_FIELD_ACCESSOR_PRIMITIVE(F64, double)

TS2ETS_WRAPPER_WRAP(I32, int32_t)
{
    // NOLINTNEXTLINE(cppcoreguidelines-pro-type-vararg)
    PROXY_DLOG_A("TS2ETS_WRAPPER_WRAP: int %d", ets_val);
    napi_value js_val;
    NAPI_CHECK_FATAL(napi_create_int32(env, static_cast<int32_t>(ets_val), &js_val));
    return js_val;
}
TS2ETS_WRAPPER_UNWRAP(I32, int32_t)
{
    if (UNLIKELY(GetValueType(env, js_val) != napi_number)) {
        InteropCtx::ThrowJSError(env, "unwrap type mismatch, number expected");
        return false;
    }
    int32_t val;
    NAPI_CHECK_FATAL(napi_get_value_int32(env, js_val, &val));
    ets_val = val;
    return true;
}
TS2ETS_WRAPPER_FIELD_ACCESSOR_PRIMITIVE(I32, int32_t)
TS2ETS_WRAPPER_STATIC_FIELD_ACCESSOR_PRIMITIVE(I32, int32_t)

TS2ETS_WRAPPER_WRAP(I64, int64_t)
{
    // NOLINTNEXTLINE(cppcoreguidelines-pro-type-vararg)
    PROXY_DLOG_A("TS2ETS_WRAPPER_WRAP: long %ld", ets_val);
    napi_value js_val;
    NAPI_CHECK_FATAL(napi_create_int64(env, static_cast<int64_t>(ets_val), &js_val));
    return js_val;
}
TS2ETS_WRAPPER_UNWRAP(I64, int64_t)
{
    if (UNLIKELY(GetValueType(env, js_val) != napi_number)) {
        InteropCtx::ThrowJSError(env, "unwrap type mismatch, number expected");
        return false;
    }
    int64_t val;
    NAPI_CHECK_FATAL(napi_get_value_int64(env, js_val, &val));
    ets_val = val;
    return true;
}
TS2ETS_WRAPPER_FIELD_ACCESSOR_PRIMITIVE(I64, int64_t)
TS2ETS_WRAPPER_STATIC_FIELD_ACCESSOR_PRIMITIVE(I64, int64_t)

TS2ETS_WRAPPER_WRAP(OBJ, panda::ObjectHeader *)
{
    if (UNLIKELY(ets_val == nullptr)) {
        napi_value js_val;
        // NOLINTNEXTLINE(cppcoreguidelines-pro-type-vararg)
        PROXY_DLOG("TS2ETS_WRAPPER_WRAP: null reference");
        NAPI_CHECK_FATAL(napi_get_null(env, &js_val));
        return js_val;
    }

    if (LIKELY(wclass->shareable)) {
        auto wref = WrapperRef::FromShareable(ets_val);
        if (LIKELY(wref != nullptr)) {
            // NOLINTNEXTLINE(cppcoreguidelines-pro-type-vararg)
            PROXY_CLOG_A("TS2ETS_WRAPPER_WRAP: shareable&hit: oh=%p", (void *)ets_val);
            napi_value js_val;
            NAPI_CHECK_FATAL(napi_get_reference_value(env, wref->js_ref, &js_val));
            return js_val;
        }
        // NOLINTNEXTLINE(cppcoreguidelines-pro-type-vararg)
        PROXY_CLOG_A("TS2ETS_WRAPPER_WRAP: shareable&miss: oh=%p", (void *)ets_val);
    }

    return WrapperCreateForExisting(env, wclass, ets_val);
}
TS2ETS_WRAPPER_UNWRAP(OBJ, panda::ObjectHeader *)
{
    auto wref = WrapperObjUnwrap(env, js_val);
    if (UNLIKELY(wref == nullptr)) {
        return false;
    }
    ets_val = InteropCtx::Current()->Refstor()->Get(wref->ets_ref);
    return true;
}

TS2ETS_WRAPPER_FIELD_GET(OBJ)
{
    DumpField(wfield);

    auto wclass_field = WrapperClassLinkResolve(env, wfield->wclass_field);
    if (UNLIKELY(wclass_field == nullptr)) {
        return nullptr;
    }
    auto ets_obj = InteropCtx::Current()->Refstor()->Get(wref->ets_ref);
    auto ets_val = ets_obj->GetFieldObject(wfield->ets_offs);

    if (UNLIKELY(ets_val == nullptr)) {
        // NOLINTNEXTLINE(cppcoreguidelines-pro-type-vararg)
        PROXY_DLOG_A("TS2ETS_WRAPPER_FIELD_GET: null from wref=%p offs=%u", (void *)wref, wfield->ets_offs);
    }
    return WrapperValueWrapImplOBJ(env, wclass_field, ets_val);
}
TS2ETS_WRAPPER_FIELD_SET(OBJ)
{
    DumpField(wfield);

    auto wclass_field = WrapperClassLinkResolve(env, wfield->wclass_field);
    if (UNLIKELY(wclass_field == nullptr)) {
        return;
    }

    panda::ObjectHeader *ets_val;
    if (UNLIKELY(!WrapperValueUnwrapImplOBJ(env, wclass_field, js_val, ets_val))) {
        return;
    }

    auto ets_obj = InteropCtx::Current()->Refstor()->Get(wref->ets_ref);
    if (ets_val == nullptr) {
        // NOLINTNEXTLINE(cppcoreguidelines-pro-type-vararg)
        PROXY_DLOG_A("TS2ETS_WRAPPER_FIELD_SET: null to wref=%p offs=%u", (void *)wref, wfield->ets_offs);
    }
    ets_obj->SetFieldObject(wfield->ets_offs, ets_val);
}
TS2ETS_WRAPPER_WRAP(STRING, panda::ObjectHeader *)
{
    napi_value js_val;
    if (UNLIKELY(ets_val == nullptr)) {
        // NOLINTNEXTLINE(cppcoreguidelines-pro-type-vararg)
        PROXY_DLOG("TS2ETS_WRAPPER_WRAP: null reference");
        NAPI_CHECK_FATAL(napi_get_null(env, &js_val));

        return js_val;
    }

    ASSERT(wclass == nullptr && "No wrapper class for strings");
    auto ets_string = EtsString::FromEtsObject(EtsObject::FromCoreType(ets_val));
    if (ets_string->IsUtf16()) {
        auto str = reinterpret_cast<char16_t *>(ets_string->GetDataUtf16());
        auto length = ets_string->GetUtf16Length() - 1;
        NAPI_CHECK_FATAL(napi_create_string_utf16(env, reinterpret_cast<char16_t *>(str), length, &js_val));
    } else {
        auto str = utf::Mutf8AsCString(ets_string->GetDataMUtf8());
        auto length = ets_string->GetMUtf8Length() - 1;
        NAPI_CHECK_FATAL(napi_create_string_utf8(env, str, length, &js_val));
    }

    return js_val;
}
TS2ETS_WRAPPER_UNWRAP(STRING, panda::ObjectHeader *)
{
    size_t length;
    NAPI_CHECK_FATAL(napi_get_value_string_utf16(env, js_val, nullptr, 0, &length));
    std::vector<char16_t> buf;
    buf.resize(length + 1);
    NAPI_CHECK_FATAL(napi_get_value_string_utf16(env, js_val, buf.data(), buf.size(), &length));
    ets_val = EtsString::CreateFromUtf16(reinterpret_cast<uint16_t *>(buf.data()), length)->AsObject()->GetCoreType();
    return true;
}

TS2ETS_WRAPPER_FIELD_GET(STRING)
{
    DumpField(wfield);

    auto ets_obj = InteropCtx::Current()->Refstor()->Get(wref->ets_ref);
    auto ets_val = ets_obj->GetFieldObject(wfield->ets_offs);

    if (UNLIKELY(ets_val == nullptr)) {
        // NOLINTNEXTLINE(cppcoreguidelines-pro-type-vararg)
        PROXY_DLOG_A("TS2ETS_WRAPPER_FIELD_GET: null from wref=%p offs=%u", (void *)wref, wfield->ets_offs);
    }
    return WrapperValueWrapImplSTRING(env, nullptr, ets_val);
}
TS2ETS_WRAPPER_FIELD_SET(STRING)
{
    DumpField(wfield);

    panda::ObjectHeader *ets_val;
    if (UNLIKELY(!WrapperValueUnwrapImplSTRING(env, nullptr, js_val, ets_val))) {
        return;
    }

    auto ets_obj = InteropCtx::Current()->Refstor()->Get(wref->ets_ref);
    if (ets_val == nullptr) {
        // NOLINTNEXTLINE(cppcoreguidelines-pro-type-vararg)
        PROXY_DLOG_A("TS2ETS_WRAPPER_FIELD_SET: null to wref=%p offs=%u", (void *)wref, wfield->ets_offs);
    }
    ets_obj->SetFieldObject(wfield->ets_offs, ets_val);
}
TS2ETS_WRAPPER_STATIC_FIELD_GET(STRING)
{
    DumpField(wfield);

    auto ets_val = wfield->wclass_owner->ets_klass->GetFieldObject(wfield->ets_offs);

    if (UNLIKELY(ets_val == nullptr)) {
        // NOLINTNEXTLINE(cppcoreguidelines-pro-type-vararg)
        PROXY_DLOG_A("TS2ETS_WRAPPER_STATIC_FIELD_GET: null from offs=%u", wfield->ets_offs);
    }
    return WrapperValueWrapImplSTRING(env, nullptr, ets_val);
}
TS2ETS_WRAPPER_STATIC_FIELD_SET(STRING)
{
    DumpField(wfield);

    panda::ObjectHeader *ets_val;
    if (UNLIKELY(!WrapperValueUnwrapImplSTRING(env, nullptr, js_val, ets_val))) {
        // NOLINTNEXTLINE(cppcoreguidelines-pro-type-vararg)
        PROXY_DLOG_A("TS2ETS_WRAPPER_STATIC_FIELD_SET: null to offs=%u", wfield->ets_offs);
    }
    wfield->wclass_owner->ets_klass->SetFieldObject(wfield->ets_offs, ets_val);
}

static void WrapperFinalize([[maybe_unused]] napi_env env, void *data, [[maybe_unused]] void *hint)
{
    // NOLINTNEXTLINE(cppcoreguidelines-pro-type-vararg)
    PROXY_DLOG_A("WrapperFinalize: %p", (void *)data);
    auto coro = EtsCoroutine::GetCurrent();
    ScopedManagedCodeThreadSwitch managed_scope(coro);
    auto wref = reinterpret_cast<WrapperRef *>(data);
    InteropCtx::Current(coro)->Refstor()->Remove(wref->ets_ref);
    WrapperRefCache::Free(wref);
}

static void WrapperFinalizeShareable([[maybe_unused]] napi_env env, void *data, [[maybe_unused]] void *hint)
{
    // NOLINTNEXTLINE(cppcoreguidelines-pro-type-vararg)
    PROXY_DLOG_A("WrapperFinalizeShareable: %p", (void *)data);
    auto coro = EtsCoroutine::GetCurrent();
    auto ctx = InteropCtx::Current(coro);
    ScopedManagedCodeThreadSwitch managed_scope(coro);
    auto wref = reinterpret_cast<WrapperRef *>(data);
    WrapperRef::UnlinkShareable(ctx->Refstor()->Get(wref->ets_ref));
    ctx->Refstor()->Remove(wref->ets_ref);
    WrapperRefCache::Free(wref);
}

static bool WrapperCtorInit(napi_env env, napi_callback_info cinfo, size_t argc, WrapperClass *wclass,
                            mem::Reference *ets_ref);

// napi_wrap existing or newly allocated ets object
static napi_value WrapperCtor(napi_env env, napi_callback_info cinfo)
{
    // NOLINTNEXTLINE(cppcoreguidelines-pro-type-vararg)
    PROXY_DLOG("WrapperCtor");
    auto coro = EtsCoroutine::GetCurrent();
    ScopedManagedCodeThreadSwitch managed_scope(coro);
    size_t argc = 0;
    napi_value athis;
    void *data;
    NAPI_CHECK_FATAL(napi_get_cb_info(env, cinfo, &argc, nullptr, &athis, &data));

    auto wclass = reinterpret_cast<WrapperClass *>(data);
    panda::ObjectHeader *ets_obj;
    WrapperRef *wref = WrapperRefCache::Alloc();
    auto ctx = InteropCtx::Current(coro);

    if (wclass->pending_ets_obj != nullptr) {
        // nonexisting ref for existing ets obj
        ets_obj = wclass->pending_ets_obj;
        wclass->pending_ets_obj = nullptr;
        // commit changes
        wref->ets_ref = ctx->Refstor()->Add(ets_obj, panda::mem::Reference::ObjectType::GLOBAL);
    } else {
        // nonexisting ets object ~ allocate and call <ctor>
        ets_obj = panda::ObjectHeader::Create(coro, wclass->ets_klass);
        if (UNLIKELY(ets_obj == nullptr)) {
            WrapperRefCache::Free(wref);
            return nullptr;
        }
        // commit changes
        wref->ets_ref = ctx->Refstor()->Add(ets_obj, panda::mem::Reference::ObjectType::GLOBAL);
        if (UNLIKELY(!WrapperCtorInit(env, cinfo, argc, wclass, wref->ets_ref))) {
            ctx->Refstor()->Remove(wref->ets_ref);
            WrapperRefCache::Free(wref);
            return nullptr;
        }
    }

    // link wref with js and ets refs
    if (LIKELY(wclass->shareable)) {
        wref->LinkShareable(ctx->Refstor()->Get(wref->ets_ref));
    }
    // NOLINTNEXTLINE(cppcoreguidelines-pro-type-vararg)
    PROXY_DLOG_A("WrapperCtor: add finalizer for %p", (void *)wref);

    auto cb = wclass->shareable ? WrapperFinalizeShareable : WrapperFinalize;

    NAPI_CHECK_FATAL(
        napi_wrap(env, athis, wref, cb, wref, nullptr));  // can't create weakref with napi_wrap in ace napi
    NAPI_CHECK_FATAL(napi_create_reference(env, athis, 0, &wref->js_ref));
    NAPI_CHECK_FATAL(napi_object_seal(env, athis));  // TODO(vpukhov): broken in ace napi
    return athis;
}

template <bool IS_STATIC, bool IS_FUNC = false>
napi_value CallWrapperMethod(napi_env env, napi_callback_info cinfo);

class WrapperClassCreator final : public EtsTypeVisitor {
    using Base = EtsTypeVisitor;

public:
    WrapperClassCreator(napi_env env, panda::Class *klass) : env_(env)
    {
        ASSERT(WrapperClassCache::Lookup(klass) == nullptr);

        wclass_ = WrapperClassCache::Insert(klass);
        wclass_->ets_klass = klass;
    }

    WrapperClass *Process()
    {
        auto res = ProcessInternal();
        if (Error()) {
            WrapperClassCache::Remove(wclass_->ets_klass);
            return nullptr;
        }
        return res;
    }

private:
    static constexpr auto METHOD_ATTR = static_cast<napi_property_attributes>(0);
    // TODO(vpukhov): napi_static methods are added to instance
    static constexpr auto STATIC_METHOD_ATTR = static_cast<napi_property_attributes>(napi_static);
    static constexpr auto FIELD_ATTR = static_cast<napi_property_attributes>(napi_writable | napi_enumerable);
    // TODO(vpukhov): napi_static fields are added to instance
    static constexpr auto STATIC_FIELD_ATTR = static_cast<napi_property_attributes>(
        napi_static | napi_writable | napi_enumerable);  // NOLINT(hicpp-signed-bitwise)
    static constexpr const char *INIT_NAME = "<ctor>";

    WrapperClass *ProcessInternal()
    {
        auto klass = wclass_->ets_klass;
        auto klass_name = klass->GetName();  // mangled name
        // NOLINTNEXTLINE(cppcoreguidelines-pro-type-vararg)
        PROXY_DLOG_A("WrapperClassCreator: %p %s", (void *)klass, klass_name.c_str());
        ASSERT(!klass->IsPrimitive());
        if (klass->IsArrayClass()) {
            Error() = "arrays are not supported";
            return nullptr;
        }

        if (klass->IsStringClass()) {
            NapiFatal("Unreachable");
        }

        auto const n_fields = klass->GetFields().size();
        auto const n_methods = klass->GetMethods().size();
        wclass_->wfields.reserve(n_fields);
        wclass_->wmethods.reserve(n_methods);
        js_props_.reserve(n_fields + n_methods);

        VisitClass(wclass_->ets_klass);
        if (Error()) {
            return nullptr;
        }
        ProcessMethods();
        if (Error()) {
            return nullptr;
        }
        HandleShareable();

        napi_value js_ctor;
        NAPI_CHECK_FATAL(napi_define_class(env_, klass_name.c_str(), NAPI_AUTO_LENGTH, WrapperCtor, wclass_,
                                           js_props_.size(), js_props_.data(), &js_ctor));
        NAPI_CHECK_FATAL(napi_create_reference(env_, js_ctor, 1, &wclass_->js_ctor));
        return wclass_;
    }

    void HandleShareable()
    {
        if (UNLIKELY(shareable_klass_ == nullptr)) {
            PandaEtsVM *vm = EtsCoroutine::GetCurrent()->GetPandaVM();
            shareable_klass_ = vm->GetClassLinker()->GetClass("Lstd/interop/js/Shareable;");
            NAPI_FATAL_IF(shareable_klass_ == nullptr);  // Shareable must be defined somewhere
            // NOLINTNEXTLINE(cppcoreguidelines-pro-type-vararg)
            PROXY_DLOG_A("HandleShareable: klass is: %p %s", (void *)shareable_klass_,
                         shareable_klass_->GetDescriptor());
        }

        auto klass = EtsClass::FromRuntimeClass(wclass_->ets_klass);
        NAPI_FATAL_IF(klass == shareable_klass_);  // user cannot construct shareable directly

        for (auto k = klass; k != nullptr; k = k->GetBase()) {
            // NOLINTNEXTLINE(cppcoreguidelines-pro-type-vararg)
            PROXY_DLOG_A("HandleShareable: test for: %p %s", (void *)k, k->GetDescriptor());
            if (k == shareable_klass_) {
                // NOLINTNEXTLINE(cppcoreguidelines-pro-type-vararg)
                PROXY_DLOG_A("HandleShareable: is shareable: %p", (void *)klass);
                wclass_->shareable = true;
                break;
            }
        }
    }

    // For object fields klass_field != nullptr
    void AddField(panda::Class *klass_field, napi_callback getter, napi_callback setter)
    {
        {
            WrapperField wfield {};
            wfield.wclass_owner = wclass_;
            wfield.wclass_field = WrapperClassLink(klass_field);  // lazy
            wfield.ets_offs = ets_field_->GetOffset();
            wclass_->wfields.push_back(wfield);
        }
        // NOLINTNEXTLINE(cppcoreguidelines-pro-type-vararg)
        PROXY_DLOG_A("AddField: lazy: %p %p", (void *)klass_field,
                     (void *)wclass_->wfields.back().wclass_field.GetUnresolved());
        {
            napi_property_descriptor prop {};
            prop.utf8name = reinterpret_cast<const char *>(ets_field_->GetName().data);
            prop.getter = getter;
            prop.setter = setter;
            prop.attributes = InStatic() ? STATIC_FIELD_ATTR : FIELD_ATTR;
            prop.data = &wclass_->wfields.back();
            js_props_.emplace_back(prop);
        }
        DumpField(&wclass_->wfields.back());
    }

    void AddMethod(panda::Method *method)
    {
        auto name = reinterpret_cast<const char *>(method->GetName().data);

        napi_callback impl;
        if (method->IsStatic()) {
            impl = CallWrapperMethod<true>;
        } else {
            impl = CallWrapperMethod<false>;
        }

        wclass_->wmethods.emplace_back(WrapperMethodLink(method));  // lazy
        // NOLINTNEXTLINE(cppcoreguidelines-pro-type-vararg)
        PROXY_DLOG_A("AddMethod: lazy: %s %p", name, (void *)wclass_->wmethods.back().GetUnresolved());

        {
            napi_property_descriptor prop {};
            prop.utf8name = name;
            prop.method = impl;
            prop.attributes = method->IsStatic() ? STATIC_METHOD_ATTR : METHOD_ATTR;
            prop.data = &wclass_->wmethods.back();
            js_props_.emplace_back(prop);
        }
    }

    void ProcessMethods()
    {
        auto klass = wclass_->ets_klass;
        auto methods = klass->GetMethods();

        std::unordered_set<uint8_t const *> names;

        for (auto &method : methods) {
            auto name = method.GetName().data;
            auto it = names.insert(name);
            NAPI_FATAL_IF(!it.second);  // no overloading
            AddMethod(&method);
            if (strcmp(reinterpret_cast<const char *>(name), INIT_NAME) == 0) {
                wclass_->ets_init = WrapperMethodLink(&method);  // lazy
            }
        }
        ASSERT(wclass_->ets_init.GetUnresolved() != nullptr);
    }

    void VisitU1() override
    {
        if (InStatic()) {
            AddField(nullptr, WrapperStaticFieldGetU1, WrapperStaticFieldSetU1);
        } else {
            AddField(nullptr, WrapperFieldGetU1, WrapperFieldSetU1);
        }
    }

    void VisitI8() override
    {
        NAPI_FATAL_IF("not implemented");
    }

    void VisitI16() override
    {
        NAPI_FATAL_IF("not implemented");
    }

    void VisitU16() override
    {
        NAPI_FATAL_IF("not implemented");
    }

    void VisitI32() override
    {
        if (InStatic()) {
            AddField(nullptr, WrapperStaticFieldGetI32, WrapperStaticFieldSetI32);
        } else {
            AddField(nullptr, WrapperFieldGetI32, WrapperFieldSetI32);
        }
    }

    void VisitI64() override
    {
        if (InStatic()) {
            AddField(nullptr, WrapperStaticFieldGetI64, WrapperStaticFieldSetI64);
        } else {
            AddField(nullptr, WrapperFieldGetI64, WrapperFieldSetI64);
        }
    }

    void VisitF32() override
    {
        if (InStatic()) {
            AddField(nullptr, WrapperStaticFieldGetF32, WrapperStaticFieldSetF32);
        } else {
            AddField(nullptr, WrapperFieldGetF32, WrapperFieldSetF32);
        }
    }

    void VisitF64() override
    {
        if (InStatic()) {
            AddField(nullptr, WrapperStaticFieldGetF64, WrapperStaticFieldSetF64);
        } else {
            AddField(nullptr, WrapperFieldGetF64, WrapperFieldSetF64);
        }
    }

    void VisitString([[maybe_unused]] panda::Class *klass) override
    {
        if (InStatic()) {
            AddField(nullptr, WrapperStaticFieldGetSTRING, WrapperStaticFieldSetSTRING);
        } else {
            AddField(nullptr, WrapperFieldGetSTRING, WrapperFieldSetSTRING);
        }
    }

    void VisitArray([[maybe_unused]] panda::Class *klass) override
    {
        NAPI_FATAL_IF("not implemented");
    }

    void VisitFieldReference([[maybe_unused]] panda::Field const *field, panda::Class *klass) override
    {
        if (klass->IsStringClass()) {
            VisitString(klass);
            return;
        }

        if (InStatic()) {
            NAPI_FATAL_IF("not implemented");
        } else {
            AddField(klass, WrapperFieldGetOBJ, WrapperFieldSetOBJ);
        }
    }

    void VisitFieldPrimitive([[maybe_unused]] panda::Field const *field, panda::panda_file::Type type) override
    {
        VisitPrimitive(type);
    }

    void VisitField(const panda::Field *field) override
    {
        ets_field_ = field;
        Base::VisitField(field);
    }

private:
    napi_env env_;
    WrapperClass *wclass_ {};

    std::vector<napi_property_descriptor> js_props_;
    const panda::Field *ets_field_ {};  // to pass to field visitor

    static EtsClass *shareable_klass_;
};
EtsClass *WrapperClassCreator::shareable_klass_ {};

static WrapperClass *GetWrapperClass(napi_env env, panda::Class *klass)
{
    ASSERT(klass != nullptr);
    auto wclass = WrapperClassCache::Lookup(klass);
    if (LIKELY(wclass != nullptr)) {
        // NOLINTNEXTLINE(cppcoreguidelines-pro-type-vararg)
        PROXY_DLOG_A("GetWrapperClass: cache hit: %p -> %p", (void *)klass, (void *)wclass);
        return wclass;
    }
    // NOLINTNEXTLINE(cppcoreguidelines-pro-type-vararg)
    PROXY_DLOG_A("GetWrapperClass: cache miss: %p", (void *)klass);
    // call convertor
    WrapperClassCreator creator(env, klass);
    wclass = creator.Process();
    if (UNLIKELY(wclass == nullptr)) {
        std::string msg("can't create proxy wrapper of class '" + klass->GetName() +
                        "' Error: " + creator.Error().value_or("empty"));
        InteropCtx::ThrowJSError(env, msg);
        return nullptr;
    }
    return wclass;
}

static WrapperMethod *GetWrapperMethod(napi_env env, panda::Method *method);

static inline WrapperMethod *WrapperMethodLinkResolve(napi_env env, WrapperMethodLink &link)
{
    if (LIKELY(link.IsResolved())) {
        // NOLINTNEXTLINE(cppcoreguidelines-pro-type-vararg)
        PROXY_CLOG_A("WrapperMethodLinkResolve: hit: %p -> %p", (void *)link.GetResolved()->ets_method,
                     (void *)link.GetResolved());
        return link.GetResolved();
    }
    // NOLINTNEXTLINE(cppcoreguidelines-pro-type-vararg)
    PROXY_CLOG_A("WrapperMethodLinkResolve: miss: %p", (void *)link.GetUnresolved());
    WrapperMethod *wmethod = GetWrapperMethod(env, link.GetUnresolved());
    if (UNLIKELY(wmethod == nullptr)) {
        return nullptr;
    }
    link = WrapperMethodLink(wmethod);
    // NOLINTNEXTLINE(cppcoreguidelines-pro-type-vararg)
    PROXY_DLOG_A("WrapperMethodLinkResolve: resolved: %p -> %p", (void *)link.GetResolved()->ets_method,
                 (void *)link.GetResolved());
    return wmethod;
}

[[maybe_unused]] static void DumpInvoke(WrapperMethod *wmethod, std::vector<panda::Value> &args)
{
    auto klass_name = wmethod->ets_method->GetClass()->GetName();
    auto method_name = reinterpret_cast<const char *>(wmethod->ets_method->GetName().data);
    std::string msg = "DumpInvoke: " + klass_name + "::" + method_name + " with args:";

    for (size_t i = 0; i < args.size(); ++i) {
        msg += "\n[" + std::to_string(i) + "] = " + std::to_string(args[i].GetAsLong());
    }
    // NOLINTNEXTLINE(cppcoreguidelines-pro-type-vararg)
    PROXY_DLOG_A("%s", msg.c_str());
}

// internal call <ctor> on wrapped object
static bool WrapperCtorInit(napi_env env, napi_callback_info cinfo, size_t argc, WrapperClass *wclass,
                            mem::Reference *ets_ref)
{
    // NOLINTNEXTLINE(cppcoreguidelines-pro-type-vararg)
    PROXY_DLOG("WrapperCtorInit");
    auto coro = EtsCoroutine::GetCurrent();
    auto ctx = InteropCtx::Current(coro);
    [[maybe_unused]] EtsJSNapiEnvScope envscope(ctx, env);
    auto &wmethod_link = wclass->ets_init;
    auto wmethod = WrapperMethodLinkResolve(env, wmethod_link);
    if (UNLIKELY(wmethod == nullptr)) {
        return false;
    }

    if (UNLIKELY(wmethod->wclass_args.size() != argc)) {
        InteropCtx::ThrowJSError(env, "constructor called in wrong context");
        return false;
    }

    auto &ets_args = ctx->GetTempArgs<Value>(argc + 1);  // this
    auto &js_args = ctx->GetTempArgs<napi_value>(argc);

    NAPI_CHECK_FATAL(napi_get_cb_info(env, cinfo, &argc, js_args.data(), nullptr, nullptr));

    ets_args[0] = panda::Value(ctx->Refstor()->Get(ets_ref));  // this

    for (size_t i = 0; i < argc; ++i) {
        auto &wtype = wmethod->wclass_args[i];
        auto res = wtype.conv.unwrap(env, &wtype, js_args[i], ets_args[i + 1]);
        if (UNLIKELY(!res)) {
            return false;
        }
    }

    wmethod->ets_method->Invoke(coro, ets_args.data());
    if (UNLIKELY(coro->HasPendingException())) {
        NAPI_FATAL_IF("exception proxy not implemented");
        return false;
    }
    return true;
}

template <bool IS_STATIC, bool IS_FUNC>
napi_value CallWrapperMethod(napi_env env, napi_callback_info cinfo)
{
    // NOLINTNEXTLINE(cppcoreguidelines-pro-type-vararg)
    PROXY_DLOG_A("CallWrapperMethod static=%d", static_cast<int>(IS_STATIC));
    auto coro = EtsCoroutine::GetCurrent();
    auto ctx = InteropCtx::Current(coro);
    [[maybe_unused]] EtsJSNapiEnvScope envscope(ctx, env);
    static constexpr auto ARGS_START = static_cast<size_t>(!IS_STATIC);

    size_t constexpr ARGS_OPT_SZ = 16U;
    size_t argc = ARGS_OPT_SZ;
    auto &ets_args = ctx->GetTempArgs<Value>(argc);
    auto &js_args = ctx->GetTempArgs<napi_value>(argc);
    napi_value athis;
    void *data;
    NAPI_CHECK_FATAL(napi_get_cb_info(env, cinfo, &argc, js_args.data(), &athis, &data));

    WrapperMethod *wmethod;

    if constexpr (IS_FUNC) {
        wmethod = reinterpret_cast<WrapperMethod *>(data);
    } else {
        auto wmethod_link = reinterpret_cast<WrapperMethodLink *>(data);
        wmethod = WrapperMethodLinkResolve(env, *wmethod_link);
        if (UNLIKELY(wmethod == nullptr)) {
            return nullptr;
        }
    }

    // NOLINTNEXTLINE(cppcoreguidelines-pro-type-vararg)
    PROXY_DLOG_A("CallWrapperMethod: %s", reinterpret_cast<const char *>(wmethod->ets_method->GetName().data));

    if (UNLIKELY(wmethod->wclass_args.size() != argc)) {
        InteropCtx::ThrowJSError(env, "wrong argc: " + std::to_string(wmethod->wclass_args.size()) + " expected");
        return nullptr;
    }

    if (UNLIKELY(argc > js_args.size())) {
        ets_args.resize(argc + ARGS_START);
        js_args.resize(argc);
        NAPI_CHECK_FATAL(napi_get_cb_info(env, cinfo, &argc, js_args.data(), &athis, &data));
    }

    ScopedManagedCodeThreadSwitch managed_scope(coro);

    if constexpr (!IS_STATIC) {
        auto &wtype = wmethod->wclass_this;
        auto res = wtype.conv.unwrap(env, &wtype, athis, ets_args[0]);
        if (UNLIKELY(!res)) {
            return nullptr;
        }
    }
    auto &wargs = wmethod->wclass_args;
    for (size_t i = 0; i < argc; ++i) {
        auto &wtype = wargs[i];
        auto res = wtype.conv.unwrap(env, &wtype, js_args[i], ets_args[i + ARGS_START]);
        if (UNLIKELY(!res)) {
            return nullptr;
        }
    }

    panda::Value ets_res = wmethod->ets_method->Invoke(coro, ets_args.data());
    if (UNLIKELY(coro->HasPendingException())) {
        NAPI_FATAL_IF("exception proxy not implemented");
        return nullptr;
    }
    auto &wtype_ret = wmethod->wclass_ret;

    return wtype_ret.conv.wrap(env, &wtype_ret, ets_res);
}

class WrapperMethodCreator final : public EtsMethodVisitor {
public:
    WrapperMethodCreator(napi_env env, panda::Method *method) : EtsMethodVisitor(method), env_(env)
    {
        NAPI_FATAL_IF(WrapperMethodCache::Lookup(method));

        wmethod_ = WrapperMethodCache::Insert(method);
        wmethod_->ets_method = method;
    }

    WrapperMethod *ProcessMethod()
    {
        Process();
        if (Error()) {
            WrapperMethodCache::Remove(wmethod_->ets_method);
            return nullptr;
        }
        return wmethod_;
    }

    WrapperMethod *ProcessFunction()
    {
        Process();
        if (Error()) {
            WrapperMethodCache::Remove(wmethod_->ets_method);
            return nullptr;
        }
        auto method = wmethod_->ets_method;
        auto const method_name = method->GetName();
        napi_value js_func;
        NAPI_CHECK_FATAL(napi_create_function(env_, reinterpret_cast<const char *>(method_name.data), NAPI_AUTO_LENGTH,
                                              &CallWrapperMethod<true, true>, wmethod_, &js_func));
        NAPI_CHECK_FATAL(napi_create_reference(env_, js_func, 1, &wmethod_->js_func));
        return wmethod_;
    }

private:
    void Process()
    {
        auto method = wmethod_->ets_method;
        auto const n_args = method->GetNumArgs();
        wmethod_->wclass_args.reserve(n_args);
        VisitMethod();
        TYPEVIS_ABRUPT_ON_ERROR();

        if (!method->IsStatic()) {
            // NOLINTNEXTLINE(cppcoreguidelines-pro-type-vararg)
            PROXY_DLOG("vis this");
            wmethod_->wclass_this = GetConvertors(method->GetClass());
            TYPEVIS_ABRUPT_ON_ERROR();
        }
    }

    WrapperType GetConvertors(panda::panda_file::Type type)
    {
        WrapperValueConvertors conv {};

        switch (type.GetId()) {
            case panda::panda_file::Type::TypeId::VOID:
                conv = WrapperValueConvertorsVOID;
                break;
            case panda::panda_file::Type::TypeId::U1:
                conv = WrapperValueConvertorsU1;
                break;
            case panda::panda_file::Type::TypeId::I32:
                conv = WrapperValueConvertorsI32;
                break;
            case panda::panda_file::Type::TypeId::I64:
                conv = WrapperValueConvertorsI64;
                break;
            case panda::panda_file::Type::TypeId::F32:
                conv = WrapperValueConvertorsF32;
                break;
            case panda::panda_file::Type::TypeId::F64:
                conv = WrapperValueConvertorsF64;
                break;
            default:
                NAPI_FATAL_IF("not implemented");
        }
        return {conv, nullptr};
    }

    WrapperType GetConvertors(panda::Class *klass)
    {
        ASSERT(klass != nullptr);

        if (klass->IsStringClass()) {
            return {WrapperValueConvertorsSTRING, nullptr};
        }

        auto wclass = GetWrapperClass(env_, klass);
        if (UNLIKELY(wclass == nullptr)) {
            Error() = "wrapper class creation failed";
            return {};
        }
        return {WrapperValueConvertorsOBJ, wclass};
    }

    void VisitReturn(panda::panda_file::Type type) override
    {
        // NOLINTNEXTLINE(cppcoreguidelines-pro-type-vararg)
        PROXY_DLOG("VisitReturn: primitive");
        wmethod_->wclass_ret = GetConvertors(type);
        TYPEVIS_ABRUPT_ON_ERROR();
    }
    void VisitReturn(panda::Class *klass) override
    {
        // NOLINTNEXTLINE(cppcoreguidelines-pro-type-vararg)
        PROXY_DLOG("VisitReturn: klass");
        wmethod_->wclass_ret = GetConvertors(klass);
        TYPEVIS_ABRUPT_ON_ERROR();
    }
    void VisitArgument([[maybe_unused]] uint32_t idx, panda::panda_file::Type type) override
    {
        // NOLINTNEXTLINE(cppcoreguidelines-pro-type-vararg)
        PROXY_DLOG_A("VisitArgument: %u primitive", idx);
        wmethod_->wclass_args.push_back(GetConvertors(type));
        TYPEVIS_ABRUPT_ON_ERROR();
    }
    void VisitArgument([[maybe_unused]] uint32_t idx, panda::Class *klass) override
    {
        // NOLINTNEXTLINE(cppcoreguidelines-pro-type-vararg)
        PROXY_DLOG_A("VisitArgument: %u klass", idx);
        wmethod_->wclass_args.push_back(GetConvertors(klass));
        TYPEVIS_ABRUPT_ON_ERROR();
    }

    napi_env env_;
    WrapperMethod *wmethod_ {};
};

static WrapperMethod *GetWrapperFunction(napi_env env, panda::Method *method)
{
    auto wmethod = WrapperMethodCache::Lookup(method);
    if (LIKELY(wmethod != nullptr)) {
        return wmethod;
    }
    // call convertor
    WrapperMethodCreator creator(env, method);
    wmethod = creator.ProcessFunction();
    if (UNLIKELY(wmethod == nullptr)) {
        InteropCtx::ThrowJSError(env, std::string("can't create proxy wrapper of function ") +
                                          reinterpret_cast<const char *>(method->GetName().data));
        return nullptr;
    }
    return wmethod;
}

static WrapperMethod *GetWrapperMethod(napi_env env, panda::Method *method)
{
    auto wmethod = WrapperMethodCache::Lookup(method);
    if (LIKELY(wmethod != nullptr)) {
        return wmethod;
    }
    // call convertor
    WrapperMethodCreator creator(env, method);
    wmethod = creator.ProcessMethod();
    if (UNLIKELY(wmethod == nullptr)) {
        InteropCtx::ThrowJSError(env, std::string("can't create proxy wrapper of method ") +
                                          reinterpret_cast<const char *>(method->GetName().data));
        return nullptr;
    }
    return wmethod;
}

/******************************************************************************/

napi_value RegisterETSClassImpl(napi_env env, napi_value *jsargv, uint32_t jsargc)
{
    // NOLINTNEXTLINE(cppcoreguidelines-pro-type-vararg)
    PROXY_DLOG("RegisterETSClass: enter");

    if (jsargc != 1) {
        InteropCtx::ThrowJSError(env, "RegisterETSClass: bad args");
        return nullptr;
    }

    // NOLINTNEXTLINE(cppcoreguidelines-pro-bounds-pointer-arithmetic)
    if (GetValueType(env, jsargv[0]) != napi_string) {
        InteropCtx::ThrowJSError(env, "RegisterETSClass: class name is not a string");
        return nullptr;
    }
    // NOLINTNEXTLINE(cppcoreguidelines-pro-bounds-pointer-arithmetic)
    std::string klass_name = GetString(env, jsargv[0]);

    auto coro = EtsCoroutine::GetCurrent();
    auto ets_klass = coro->GetPandaVM()->GetClassLinker()->GetClass(klass_name.c_str());
    if (UNLIKELY(ets_klass == nullptr)) {
        InteropCtx::ThrowJSError(env, "RegisterETSClass: unresolved klass " + klass_name);
        return nullptr;
    }

    ScopedManagedCodeThreadSwitch managed_scope(coro);
    auto wclass = GetWrapperClass(env, ets_klass->GetRuntimeClass());
    if (UNLIKELY(wclass == nullptr)) {
        return nullptr;
    }

    napi_value js_ctor;
    NAPI_CHECK_FATAL(napi_get_reference_value(env, wclass->js_ctor, &js_ctor));
    // NOLINTNEXTLINE(cppcoreguidelines-pro-type-vararg)
    PROXY_DLOG("RegisterETSClass: exit");
    return js_ctor;
}

napi_value RegisterETSFunctionImpl(napi_env env, napi_value *jsargv, uint32_t jsargc)
{
    // NOLINTNEXTLINE(cppcoreguidelines-pro-type-vararg)
    PROXY_DLOG("RegisterETSFunction: enter");

    if (jsargc != 2) {
        InteropCtx::ThrowJSError(env, "RegisterETSFunction: bad args");
        return nullptr;
    }
    // NOLINTNEXTLINE(cppcoreguidelines-pro-bounds-pointer-arithmetic)
    if (GetValueType(env, jsargv[0]) != napi_string) {
        InteropCtx::ThrowJSError(env, "RegisterETSFunction: function name is not a string");
        return nullptr;
    }
    // NOLINTNEXTLINE(cppcoreguidelines-pro-bounds-pointer-arithmetic)
    if (GetValueType(env, jsargv[1]) != napi_string) {
        InteropCtx::ThrowJSError(env, "RegisterETSFunction: function name is not a string");
        return nullptr;
    }

    // NOLINTNEXTLINE(cppcoreguidelines-pro-bounds-pointer-arithmetic)
    std::string class_descriptor = GetString(env, jsargv[0]);
    // NOLINTNEXTLINE(cppcoreguidelines-pro-bounds-pointer-arithmetic)
    std::string method_name = GetString(env, jsargv[1]);
    // NOLINTNEXTLINE(cppcoreguidelines-pro-type-vararg)
    PROXY_DLOG_A("RegisterETSFunction: method name: %s", method_name.c_str());

    auto coro = EtsCoroutine::GetCurrent();

    ScopedManagedCodeThreadSwitch managed_scope(coro);

    EtsClass *ets_class = coro->GetPandaVM()->GetClassLinker()->GetClass(class_descriptor.data());
    if (UNLIKELY(ets_class == nullptr)) {
        InteropCtx::ThrowJSError(env, "RegisterETSFunctionImpl: unresolved class " + std::string(class_descriptor));
        return nullptr;
    }

    EtsMethod *ets_method = ets_class->GetDirectMethod(method_name.data());
    if (UNLIKELY(ets_method == nullptr)) {
        InteropCtx::ThrowJSError(env, "RegisterETSFunctionImpl: class " + std::string(class_descriptor) +
                                          " doesn't contain " + std::string(method_name) + " method");
        return nullptr;
    }

    auto wmethod = GetWrapperFunction(env, ets_method->GetPandaMethod());
    if (UNLIKELY(wmethod == nullptr)) {
        return nullptr;
    }

    napi_value js_func;
    NAPI_CHECK_FATAL(napi_get_reference_value(env, wmethod->js_func, &js_func));
    // NOLINTNEXTLINE(cppcoreguidelines-pro-type-vararg)
    PROXY_DLOG("RegisterETSFunction: exit");
    return js_func;
}

}  // namespace panda::ets::interop::js
