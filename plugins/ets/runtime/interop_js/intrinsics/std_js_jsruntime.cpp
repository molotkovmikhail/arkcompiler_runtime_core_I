/*
 * Copyright (c) 2023 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "intrinsics.h"
#include "plugins/ets/runtime/interop_js/intrinsics_api.h"
#include "plugins/ets/runtime/types/ets_string.h"

namespace panda::ets::interop::js {

namespace notimpl {

[[noreturn]] static void NotImplementedHook()
{
    LOG(FATAL, ETS) << "NotImplementedHook called for JSRuntime intrinsic";
    UNREACHABLE();
}

template <typename R, typename... Args>
static R NotImplementedAdapter([[maybe_unused]] Args... args)
{
    NotImplementedHook();
}

static const IntrinsicsAPI S_INTRINSICS_API = {
    // clang-format off
    NotImplementedAdapter,
    NotImplementedAdapter,
    NotImplementedAdapter,
    NotImplementedAdapter,
    NotImplementedAdapter,
    NotImplementedAdapter,
    NotImplementedAdapter,
    NotImplementedAdapter,
    NotImplementedAdapter,
    NotImplementedAdapter,
    NotImplementedAdapter,
    NotImplementedAdapter,
    NotImplementedAdapter,
    NotImplementedAdapter,
    NotImplementedAdapter,
    NotImplementedAdapter,
    NotImplementedAdapter,
    NotImplementedAdapter,
    NotImplementedAdapter,
    NotImplementedAdapter,
    NotImplementedAdapter,
    // clang-format on
};

}  // namespace notimpl

static const IntrinsicsAPI *S_INTRINSICS_API = &notimpl::S_INTRINSICS_API;

void JSRuntimeIntrinsicsSetIntrinsicsAPI(const IntrinsicsAPI *intrinsics_api)
{
    S_INTRINSICS_API = intrinsics_api;
}

namespace intrinsics {

void JSRuntimeFinalizationQueueCallbackIntrinsic(EtsObject *obj)
{
    return S_INTRINSICS_API->JSRuntimeFinalizationQueueCallback(obj);
}

JSValue *JSRuntimeNewJSValueDoubleIntrinsic(double v)
{
    return S_INTRINSICS_API->JSRuntimeNewJSValueDouble(v);
}

JSValue *JSRuntimeNewJSValueStringIntrinsic(EtsString *v)
{
    return S_INTRINSICS_API->JSRuntimeNewJSValueString(v);
}

double JSRuntimeGetValueDoubleIntrinsic(JSValue *ets_js_value)
{
    return S_INTRINSICS_API->JSRuntimeGetValueDouble(ets_js_value);
}

uint8_t JSRuntimeGetValueBooleanIntrinsic(JSValue *ets_js_value)
{
    return S_INTRINSICS_API->JSRuntimeGetValueBoolean(ets_js_value);
}

EtsString *JSRuntimeGetValueStringIntrinsic(JSValue *ets_js_value)
{
    return S_INTRINSICS_API->JSRuntimeGetValueString(ets_js_value);
}

JSValue *JSRuntimeGetPropertyJSValueIntrinsic(JSValue *ets_js_value, EtsString *ets_prop_name)
{
    return S_INTRINSICS_API->JSRuntimeGetPropertyJSValue(ets_js_value, ets_prop_name);
}

double JSRuntimeGetPropertyDoubleIntrinsic(JSValue *ets_js_value, EtsString *ets_prop_name)
{
    return S_INTRINSICS_API->JSRuntimeGetPropertyDouble(ets_js_value, ets_prop_name);
}

EtsString *JSRuntimeGetPropertyStringIntrinsic(JSValue *ets_js_value, EtsString *ets_prop_name)
{
    return S_INTRINSICS_API->JSRuntimeGetPropertyString(ets_js_value, ets_prop_name);
}

void JSRuntimeSetPropertyJSValueIntrinsic(JSValue *ets_js_value, EtsString *ets_prop_name, JSValue *value)
{
    S_INTRINSICS_API->JSRuntimeSetPropertyJSValue(ets_js_value, ets_prop_name, value);
}

void JSRuntimeSetPropertyDoubleIntrinsic(JSValue *ets_js_value, EtsString *ets_prop_name, double value)
{
    S_INTRINSICS_API->JSRuntimeSetPropertyDouble(ets_js_value, ets_prop_name, value);
}

void JSRuntimeSetPropertyStringIntrinsic(JSValue *ets_js_value, EtsString *ets_prop_name, EtsString *value)
{
    S_INTRINSICS_API->JSRuntimeSetPropertyString(ets_js_value, ets_prop_name, value);
}

JSValue *JSRuntimeGetUndefinedIntrinsic()
{
    return S_INTRINSICS_API->JSRuntimeGetUndefined();
}

JSValue *JSRuntimeGetNullIntrinsic()
{
    return S_INTRINSICS_API->JSRuntimeGetNull();
}

JSValue *JSRuntimeGetGlobalIntrinsic()
{
    return S_INTRINSICS_API->JSRuntimeGetGlobal();
}

JSValue *JSRuntimeCreateObjectIntrinsic()
{
    return S_INTRINSICS_API->JSRuntimeCreateObject();
}

uint8_t JSRuntimeInstanceOfIntrinsic(JSValue *object, JSValue *ctor)
{
    return S_INTRINSICS_API->JSRuntimeInstanceOf(object, ctor);
}

uint8_t JSRuntimeInitJSCallClassIntrinsic(EtsString *cls_name)
{
    return S_INTRINSICS_API->JSRuntimeInitJSCallClass(cls_name);
}

uint8_t JSRuntimeInitJSNewClassIntrinsic(EtsString *cls_name)
{
    return S_INTRINSICS_API->JSRuntimeInitJSNewClass(cls_name);
}

JSValue *JSRuntimeCreateLambdaProxyIntrinsic(EtsObject *lambda)
{
    return S_INTRINSICS_API->JSRuntimeCreateLambdaProxy(lambda);
}

JSValue *JSRuntimeLoadModuleIntrinsic(EtsString *module)
{
    return S_INTRINSICS_API->JSRuntimeLoadModule(module);
}

}  // namespace intrinsics
}  // namespace panda::ets::interop::js
