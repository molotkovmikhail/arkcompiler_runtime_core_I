/**
 * Copyright (c) 2021-2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "plugins/ets/runtime/interop_js/interop_context.h"
#include "plugins/ets/runtime/interop_js/ts2ets_common.h"

namespace panda::ets::interop::js {

[[noreturn]] void NapiFatal(const char *message)
{
    InteropCtx::Fatal(message);
    UNREACHABLE();
}

[[noreturn]] void NapiFatal(const std::string &message)
{
    InteropCtx::Fatal(message.c_str());
    UNREACHABLE();
}

void InteropTrace(const char *func, const char *file, int line)
{
    INTEROP_LOG(DEBUG) << "trace: " << func << ":" << file << ":" << line;
}

extern "C" __attribute__((weak)) napi_status napi_check_object_type_tag([[maybe_unused]] napi_env env,
                                                                        [[maybe_unused]] napi_value js_object,
                                                                        [[maybe_unused]] const napi_type_tag *type_tag,
                                                                        [[maybe_unused]] bool *result)
{
    *result = true;
    return napi_ok;
}

extern "C" __attribute__((weak)) napi_status napi_object_seal([[maybe_unused]] napi_env env,
                                                              [[maybe_unused]] napi_value object)
{
    return napi_ok;
}

}  // namespace panda::ets::interop::js

namespace panda::ets::ts2ets::GlobalCtx {  // NOLINT(readability-identifier-naming)

void Init()
{
    interop::js::InteropCtx::Init(EtsCoroutine::GetCurrent());
}

}  // namespace panda::ets::ts2ets::GlobalCtx
