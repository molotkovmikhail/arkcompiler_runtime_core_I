/**
 * Copyright (c) 2021-2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef PANDA_PLUGINS_ETS_RUNTIME_INTEROP_JS_JS_REFCONVERT_H_
#define PANDA_PLUGINS_ETS_RUNTIME_INTEROP_JS_JS_REFCONVERT_H_

#include "plugins/ets/runtime/ets_coroutine.h"
#include "plugins/ets/runtime/interop_js/ts2ets_common.h"
#include "libpandabase/macros.h"
#include <node_api.h>
#include <unordered_map>

namespace panda::ets::interop::js {

class InteropCtx;
class JSRefConvertCache;

// Forward declarations to avoid cyclic deps.
inline JSRefConvertCache *RefConvertCacheFromInteropCtx(InteropCtx *ctx);
inline napi_env JSEnvFromInteropCtx(InteropCtx *ctx);

// Conversion interface for some panda::Class objects
class JSRefConvert {
public:
    // Returns nullptr if failed
    napi_value Wrap(InteropCtx *ctx, EtsObject *obj) const
    {
        ASSERT(obj != nullptr);
        return (this->*(this->wrap_))(ctx, obj);
    }

    // Returns nullptr if failed
    EtsObject *Unwrap(InteropCtx *ctx, napi_value js_value) const
    {
        ASSERT(!IsNullOrUndefined(JSEnvFromInteropCtx(ctx), js_value));
        return (this->*(this->unwrap_))(ctx, js_value);
    }

    JSRefConvert() = delete;
    NO_COPY_SEMANTIC(JSRefConvert);
    NO_MOVE_SEMANTIC(JSRefConvert);
    virtual ~JSRefConvert() = default;

protected:
    template <typename D>
    explicit JSRefConvert(D * /*unused*/)
        : wrap_(static_cast<WrapT>(&D::WrapImpl)), unwrap_(static_cast<UnwrapT>(&D::UnwrapImpl))
    {
    }

private:
    using WrapT = decltype(&JSRefConvert::Wrap);
    using UnwrapT = decltype(&JSRefConvert::Unwrap);

    const WrapT wrap_;
    const UnwrapT unwrap_;
};

// Fast cache to find convertor for some panda::Class
class JSRefConvertCache {
public:
    JSRefConvert *Lookup(Class *klass)
    {
        auto *entry = GetDirCacheEntry(klass);
        if (LIKELY(entry->klass == klass)) {
            return entry->value;
        }
        auto value = LookupFull(klass);
        *entry = {klass, value};
        return value;
    }

    JSRefConvert *Insert(Class *klass, std::unique_ptr<JSRefConvert> value)
    {
        auto owned_value = value.get();
        auto [it, inserted] = cache_.insert_or_assign(klass, std::move(value));
        ASSERT(inserted);
        (void)inserted;
        *GetDirCacheEntry(klass) = {klass, owned_value};
        return owned_value;
    }

    JSRefConvertCache() : dircache_(new DirCachePair[DIRCACHE_SZ]) {}
    ~JSRefConvertCache() = default;
    NO_COPY_SEMANTIC(JSRefConvertCache);
    NO_MOVE_SEMANTIC(JSRefConvertCache);

private:
    __attribute__((noinline)) JSRefConvert *LookupFull(Class *klass)
    {
        auto it = cache_.find(klass);
        if (UNLIKELY(it == cache_.end())) {
            return nullptr;
        }
        return it->second.get();
    }

    struct DirCachePair {
        Class *klass {};
        JSRefConvert *value {};
    };

    static constexpr uint32_t DIRCACHE_SZ = 1024;

    DirCachePair *GetDirCacheEntry(Class *klass)
    {
        static_assert(helpers::math::IsPowerOfTwo(DIRCACHE_SZ));
        auto hash = helpers::math::PowerOfTwoTableSlot<uint32_t>(ToUintPtr(klass), DIRCACHE_SZ,
                                                                 GetLogAlignment(alignof(Class)));
        return &dircache_[hash];
    }

    std::unique_ptr<DirCachePair[]> dircache_;  // NOLINT(modernize-avoid-c-arrays)
    std::unordered_map<Class *, std::unique_ptr<JSRefConvert>> cache_;
};

void RegisterBuiltinJSRefConvertors(InteropCtx *ctx);

// Try to create JSRefConvert for nonexisting cache entry
JSRefConvert *JSRefConvertCreate(InteropCtx *ctx, Class *klass);

// Find or create JSRefConvert for some Class
inline JSRefConvert *JSRefConvertResolve(InteropCtx *ctx, Class *klass)
{
    JSRefConvertCache *cache = RefConvertCacheFromInteropCtx(ctx);
    auto conv = cache->Lookup(klass);
    if (LIKELY(conv != nullptr)) {
        return conv;
    }
    return JSRefConvertCreate(ctx, klass);
}

}  // namespace panda::ets::interop::js

#endif  // !PANDA_PLUGINS_ETS_RUNTIME_INTEROP_JS_JS_REFCONVERT_H_
