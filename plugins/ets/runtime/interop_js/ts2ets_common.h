/**
 * Copyright (c) 2021-2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef PANDA_PLUGINS_ETS_RUNTIME_TS2ETS_TS2ETS_COMMON_H_
#define PANDA_PLUGINS_ETS_RUNTIME_TS2ETS_TS2ETS_COMMON_H_

#include "runtime/include/thread_scopes.h"
#include "runtime/mem/refstorage/global_object_storage.h"

#include <node_api.h>

// NOLINTNEXTLINE(cppcoreguidelines-macro-usage)
#define INTEROP_LOG(level) LOG(level, ETS_INTEROP_JS)

#ifdef PANDA_TARGET_OHOS
#include <hilog/log.h>

#define INTEROP_LOG_DEBUG(msg) OH_LOG_Print(LOG_APP, LOG_DEBUG, 0xFF00, "ts2ets", "%s", msg)
#define INTEROP_LOG_DEBUG_A(msg, ...) OH_LOG_Print(LOG_APP, LOG_DEBUG, 0xFF00, "ts2ets", msg, __VA_ARGS__)
#define INTEROP_LOG_INFO(msg) OH_LOG_Print(LOG_APP, LOG_INFO, 0xFF00, "ts2ets", "%s", msg)
#define INTEROP_LOG_INFO_A(msg, ...) OH_LOG_Print(LOG_APP, LOG_INFO, 0xFF00, "ts2ets", msg, __VA_ARGS__)
#define INTEROP_LOG_ERROR(msg) OH_LOG_Print(LOG_APP, LOG_ERROR, 0xFF00, "ts2ets", msg)
#define INTEROP_LOG_ERROR_A(msg, ...) OH_LOG_Print(LOG_APP, LOG_ERROR, 0xFF00, "ts2ets", msg, __VA_ARGS__)

#else

inline std::string EtsLogMakeString(const char *fmt, ...)
{
    va_list ap;       // NOLINT(cppcoreguidelines-pro-type-vararg)
    va_list ap_copy;  // NOLINT(cppcoreguidelines-pro-type-vararg)
    va_start(ap, fmt);
    va_copy(ap_copy, ap);

    int len = vsnprintf(nullptr, 0, fmt, ap);
    if (len < 0) {
        LOG(FATAL, ETS) << "interop_js: Cannot convert message to log buffer";
        UNREACHABLE();
    }

    std::string res;
    res.resize(static_cast<size_t>(len));
    vsnprintf(res.data(), len + 1, fmt, ap_copy);

    va_end(ap_copy);
    va_end(ap);
    return res;
}

// NOLINTNEXTLINE(cppcoreguidelines-macro-usage)
#define TS2ETS_LOGGER(level, ...)                                      \
    do {                                                               \
        std::string msg_ts2ets_logger = EtsLogMakeString(__VA_ARGS__); \
        LOG(level, ETS) << "interop_js: " << msg_ts2ets_logger;        \
    } while (0)

// NOLINTNEXTLINE(cppcoreguidelines-macro-usage)
#define INTEROP_LOG_DEBUG(msg) TS2ETS_LOGGER(DEBUG, msg)
// NOLINTNEXTLINE(cppcoreguidelines-macro-usage)
#define INTEROP_LOG_DEBUG_A(msg, ...) TS2ETS_LOGGER(DEBUG, msg, __VA_ARGS__)
// NOLINTNEXTLINE(cppcoreguidelines-macro-usage)
#define INTEROP_LOG_INFO(msg) TS2ETS_LOGGER(INFO, msg)
// NOLINTNEXTLINE(cppcoreguidelines-macro-usage)
#define INTEROP_LOG_INFO_A(msg, ...) TS2ETS_LOGGER(INFO, msg, __VA_ARGS__)
// NOLINTNEXTLINE(cppcoreguidelines-macro-usage)
#define INTEROP_LOG_ERROR(msg) TS2ETS_LOGGER(INFO, msg)
// NOLINTNEXTLINE(cppcoreguidelines-macro-usage)
#define INTEROP_LOG_ERROR_A(msg, ...) TS2ETS_LOGGER(INFO, msg, __VA_ARGS__)
#endif

namespace panda::ets::interop::js {

[[noreturn]] void NapiFatal(const char *message);
[[noreturn]] void NapiFatal(const std::string &message);

// NOLINTNEXTLINE(cppcoreguidelines-macro-usage)
#define NAPI_FATAL_IF(expr)    \
    do {                       \
        bool _expr = (expr);   \
        if (UNLIKELY(_expr)) { \
            NapiFatal(#expr);  \
            UNREACHABLE();     \
        }                      \
    } while (0)

#if !defined(NDEBUG)
// NOLINTNEXTLINE(cppcoreguidelines-macro-usage)
#define NAPI_ASSERT_OK(expr) NAPI_FATAL_IF(expr)
#else
// NOLINTNEXTLINE(cppcoreguidelines-macro-usage)
#define NAPI_ASSERT_OK(expr) \
    do {                     \
        (expr);              \
    } while (0)
#endif

// NOLINTNEXTLINE(cppcoreguidelines-macro-usage)
#define NAPI_CHECK_FATAL(status) NAPI_FATAL_IF((status) != napi_ok)
// NOLINTNEXTLINE(cppcoreguidelines-macro-usage)
#define TYPEVIS_NAPI_CHECK(expr) TYPEVIS_CHECK_ERROR((expr) == napi_ok, #expr)

inline bool NapiThrownGeneric(napi_status rc)
{
    NAPI_FATAL_IF(rc != napi_ok && rc != napi_generic_failure);
    return rc == napi_generic_failure;
}

void InteropTrace(const char *func, const char *file, int line);

#ifndef NDEBUG
// NOLINTNEXTLINE(cppcoreguidelines-macro-usage)
#define INTEROP_TRACE()                             \
    do {                                            \
        InteropTrace(__func__, __FILE__, __LINE__); \
    } while (0)
#else
// NOLINTNEXTLINE(cppcoreguidelines-macro-usage)
#define INTEROP_TRACE()
#endif

class NapiScope {
public:
    explicit NapiScope(napi_env env) : env_(env)
    {
        [[maybe_unused]] auto status = napi_open_handle_scope(env_, &scope_);
        ASSERT(status == napi_ok);
    }

    ~NapiScope()
    {
        [[maybe_unused]] auto status = napi_close_handle_scope(env_, scope_);
        ASSERT(status == napi_ok);
    }

    NO_COPY_SEMANTIC(NapiScope);
    NO_MOVE_SEMANTIC(NapiScope);

private:
    napi_env env_ {};
    napi_handle_scope scope_ {};
};

class NapiEscapableScope {
public:
    explicit NapiEscapableScope(napi_env env) : env_(env)
    {
        [[maybe_unused]] auto status = napi_open_escapable_handle_scope(env_, &scope_);
        ASSERT(status == napi_ok);
    }

    void Escape(napi_value &val)
    {
        [[maybe_unused]] auto status = napi_escape_handle(env_, scope_, val, &val);
        ASSERT(status == napi_ok);
    }

    ~NapiEscapableScope()
    {
        [[maybe_unused]] auto status = napi_close_escapable_handle_scope(env_, scope_);
        ASSERT(status == napi_ok);
    }

    NO_COPY_SEMANTIC(NapiEscapableScope);
    NO_MOVE_SEMANTIC(NapiEscapableScope);

private:
    napi_env env_ {};
    napi_escapable_handle_scope scope_ {};
};

struct ScopedManagedCodeThreadSwitch {
    explicit ScopedManagedCodeThreadSwitch(panda::ManagedThread *thread) : thread_(thread)
    {
        if (!thread_->IsManagedCode()) {
            thread_->ManagedCodeBegin();
            switched_ = true;
        }
    }
    ~ScopedManagedCodeThreadSwitch()
    {
        if (switched_) {
            thread_->ManagedCodeEnd();
        }
    }

    NO_COPY_SEMANTIC(ScopedManagedCodeThreadSwitch);
    NO_MOVE_SEMANTIC(ScopedManagedCodeThreadSwitch);

private:
    panda::ManagedThread *thread_ = nullptr;
    bool switched_ = false;
};

inline napi_valuetype GetValueType(napi_env env, napi_value val)
{
    napi_valuetype vtype;
    NAPI_CHECK_FATAL(napi_typeof(env, val, &vtype));
    return vtype;
}

inline bool IsUndefined(napi_env env, napi_value val)
{
    return GetValueType(env, val) == napi_undefined;
}

inline napi_value GetNull(napi_env env)
{
    napi_value val;
    NAPI_CHECK_FATAL(napi_get_null(env, &val));
    return val;
}

inline bool IsNullOrUndefined(napi_env env, napi_value val)
{
    napi_valuetype vtype = GetValueType(env, val);
    return vtype == napi_undefined || vtype == napi_null;
}

inline std::string GetString(napi_env env, napi_value js_val)
{
    size_t length;
    NAPI_CHECK_FATAL(napi_get_value_string_utf8(env, js_val, nullptr, 0, &length));
    std::string value;
    value.resize(length);
    // +1 for NULL terminated string!!!
    NAPI_CHECK_FATAL(napi_get_value_string_utf8(env, js_val, value.data(), value.size() + 1, &length));
    return value;
}

}  // namespace panda::ets::interop::js

#endif  // !PANDA_PLUGINS_ETS_RUNTIME_TS2ETS_TS2ETS_COMMON_H_
