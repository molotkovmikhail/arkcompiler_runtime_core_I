/**
 * Copyright (c) 2021-2023 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <node_api.h>
#include "plugins/ets/runtime/ets_panda_file_items.h"
#include "plugins/ets/runtime/ets_vm_api.h"
#include "plugins/ets/runtime/interop_js/interop_context.h"
#include "plugins/ets/runtime/interop_js/js_value_call.h"
#include "plugins/ets/runtime/interop_js/ts2ets_common.h"
#include "plugins/ets/runtime/interop_js/ts2ets_copy.h"
#include "plugins/ets/runtime/interop_js/ts2ets_proxy.h"
#include "generated/base_options.h"

#ifdef PANDA_TARGET_OHOS
#include <hilog/log.h>
#endif  // PANDA_TARGET_OHOS

static void LogError(const std::string &msg)
{
#ifdef PANDA_TARGET_OHOS
    OH_LOG_Print(LOG_APP, LOG_ERROR, 0xFF00, "ArkEtsVm", "%s: %s", "ETS_VM_PLUGIN", msg.c_str());
#else   // PANDA_TARGET_OHOS
    std::cerr << msg << std::endl;
#endif  // PANDA_TARGET_OHOS
}

namespace panda::ets::interop::js {

static napi_value Version(napi_env env, [[maybe_unused]] napi_callback_info info)
{
    constexpr std::string_view MSG = "0.1";

    napi_value result;
    [[maybe_unused]] napi_status status = napi_create_string_utf8(env, MSG.data(), MSG.size(), &result);
    assert(status == napi_ok);

    return result;
}

static napi_value Call(napi_env env, napi_callback_info info)
{
    size_t argc = 0;
    [[maybe_unused]] napi_status status;
    status = napi_get_cb_info(env, info, &argc, nullptr, nullptr, nullptr);
    assert(status == napi_ok);

    auto coro = EtsCoroutine::GetCurrent();
    auto &argv = InteropCtx::Current(coro)->GetTempArgs<napi_value>(argc);
    napi_value this_arg {};
    void *data = nullptr;
    status = napi_get_cb_info(env, info, &argc, argv.data(), &this_arg, &data);
    assert(status == napi_ok);

    return CallEtsFunctionImpl(env, {argv.data(), argc});
}

static napi_value CallWithCopy(napi_env env, napi_callback_info info)
{
    size_t argc = 0;
    [[maybe_unused]] napi_status status;
    status = napi_get_cb_info(env, info, &argc, nullptr, nullptr, nullptr);
    assert(status == napi_ok);

    auto coro = EtsCoroutine::GetCurrent();
    auto &argv = InteropCtx::Current(coro)->GetTempArgs<napi_value>(argc);
    napi_value this_arg {};
    void *data = nullptr;
    status = napi_get_cb_info(env, info, &argc, argv.data(), &this_arg, &data);
    assert(status == napi_ok);

    return InvokeEtsMethodImpl(env, argv.data(), argc, false);
}

static napi_value CreateEtsRuntime(napi_env env, napi_callback_info info)
{
    [[maybe_unused]] napi_status status;
    napi_value napi_false;
    status = napi_get_boolean(env, false, &napi_false);
    assert(status == napi_ok);

    size_t argc = 0;
    status = napi_get_cb_info(env, info, &argc, nullptr, nullptr, nullptr);
    assert(status == napi_ok);

    std::vector<napi_value> argv(argc);
    napi_value this_arg {};
    void *data = nullptr;
    status = napi_get_cb_info(env, info, &argc, argv.data(), &this_arg, &data);
    assert(status == napi_ok);

    if (argc != 4) {
        LogError("CreateEtsRuntime: exactly 4 arguments are required");
        return napi_false;
    }

    napi_valuetype type;
    napi_typeof(env, argv[0], &type);
    if (type != napi_string) {
        LogError("CreateEtsRuntime: first argument is not a string");
        return napi_false;
    }
    auto index_path = GetString(env, argv[0]);

    napi_typeof(env, argv[1], &type);
    if (type != napi_string) {
        LogError("CreateEtsRuntime: second argument is not a string");
        return napi_false;
    }
    auto stdlib_path = GetString(env, argv[1]);

    napi_typeof(env, argv[2], &type);
    if (type != napi_boolean) {
        LogError("CreateEtsRuntime: third argument is not a boolean");
        return napi_false;
    }
    bool use_jit;
    napi_get_value_bool(env, argv[2], &use_jit);

    napi_typeof(env, argv[3], &type);
    if (type != napi_boolean) {
        LogError("CreateEtsRuntime: fourth argument is not a boolean");
        return napi_false;
    }
    bool use_aot;
    napi_get_value_bool(env, argv[3], &use_aot);

    bool res = panda::ets::CreateRuntime(stdlib_path, index_path, use_jit, use_aot);
    if (res) {
        auto coro = EtsCoroutine::GetCurrent();
        ScopedManagedCodeThread scoped(coro);
        InteropCtx::Init(coro);
    }
    napi_value napi_res;
    status = napi_get_boolean(env, res, &napi_res);
    assert(status == napi_ok);
    return napi_res;
}

static napi_value CreateRuntime(napi_env env, napi_callback_info info)
{
    [[maybe_unused]] napi_status status;

    size_t constexpr ARGC = 1;
    std::array<napi_value, ARGC> argv {};

    size_t argc = ARGC;
    NAPI_ASSERT_OK(napi_get_cb_info(env, info, &argc, argv.data(), nullptr, nullptr));

    napi_value napi_false;
    NAPI_ASSERT_OK(napi_get_boolean(env, false, &napi_false));

    if (argc != ARGC) {
        LogError("CreateEtsRuntime: bad args number");
        return napi_false;
    }

    napi_value args_map = argv[0];
    if (GetValueType(env, args_map) != napi_object) {
        LogError("CreateEtsRuntime: argument is not an object");
        return napi_false;
    }

    napi_value prop_names;
    NAPI_ASSERT_OK(napi_get_property_names(env, args_map, &prop_names));

    uint32_t num_props;
    NAPI_ASSERT_OK(napi_get_array_length(env, prop_names, &num_props));

    std::vector<std::string> arg_strings;
    arg_strings.reserve(num_props + 1);
    arg_strings.emplace_back("argv[0] placeholder");

    for (uint32_t i = 0; i < num_props; ++i) {
        napi_value key;
        napi_value value;
        NAPI_ASSERT_OK(napi_get_element(env, prop_names, i, &key));
        NAPI_ASSERT_OK(napi_get_property(env, args_map, key, &value));
        if (napi_coerce_to_string(env, value, &value) != napi_ok) {
            LogError("Option values must be coercible to string");
            return napi_false;
        }
        arg_strings.push_back("--" + GetString(env, key) + "=" + GetString(env, value));
    }

    auto add_opts = [&](base_options::Options *base_options, panda::RuntimeOptions *runtime_options) {
        panda::PandArgParser pa_parser;
        base_options->AddOptions(&pa_parser);
        runtime_options->AddOptions(&pa_parser);

        std::vector<const char *> fake_argv;
        fake_argv.reserve(arg_strings.size());
        for (auto const &arg : arg_strings) {
            fake_argv.push_back(arg.c_str());  // Be careful, do not reallocate referenced strings
        }

        if (!pa_parser.Parse(fake_argv.size(), fake_argv.data())) {
            LogError("Parse options failed. Optional arguments:\n" + pa_parser.GetHelpString());
            return false;
        }

        auto runtime_options_err = runtime_options->Validate();
        if (runtime_options_err) {
            LogError("Parse options failed: " + runtime_options_err.value().GetMessage());
            return false;
        }
        return true;
    };

    bool res = ets::CreateRuntime(add_opts);
    if (res) {
        auto coro = EtsCoroutine::GetCurrent();
        ScopedManagedCodeThread scoped(coro);
        InteropCtx::Init(coro);
    }
    napi_value napi_res;
    NAPI_ASSERT_OK(napi_get_boolean(env, res, &napi_res));
    return napi_res;
}

static napi_value RegisterETSFunction(napi_env env, napi_callback_info info)
{
    size_t argc = 0;
    [[maybe_unused]] napi_status status;
    NAPI_CHECK_FATAL(napi_get_cb_info(env, info, &argc, nullptr, nullptr, nullptr));

    auto coro = EtsCoroutine::GetCurrent();
    auto &argv = InteropCtx::Current(coro)->GetTempArgs<napi_value>(argc);
    napi_value this_arg {};
    void *data = nullptr;
    NAPI_CHECK_FATAL(napi_get_cb_info(env, info, &argc, argv.data(), &this_arg, &data));

    return RegisterETSFunctionImpl(env, argv.data(), argc);
}

static napi_value RegisterETSClass(napi_env env, napi_callback_info info)
{
    size_t argc = 0;
    NAPI_CHECK_FATAL(napi_get_cb_info(env, info, &argc, nullptr, nullptr, nullptr));

    auto coro = EtsCoroutine::GetCurrent();
    auto &argv = InteropCtx::Current(coro)->GetTempArgs<napi_value>(argc);
    napi_value this_arg {};
    void *data = nullptr;
    NAPI_CHECK_FATAL(napi_get_cb_info(env, info, &argc, argv.data(), &this_arg, &data));

    return RegisterETSClassImpl(env, argv.data(), argc);
}

static napi_value Init(napi_env env, napi_value exports)
{
    const std::array desc = {
        napi_property_descriptor {"version", 0, Version, 0, 0, 0, napi_enumerable, 0},
        napi_property_descriptor {"call", 0, Call, 0, 0, 0, napi_enumerable, 0},
        napi_property_descriptor {"callWithCopy", 0, CallWithCopy, 0, 0, 0, napi_enumerable, 0},
        napi_property_descriptor {"createEtsRuntime", 0, CreateEtsRuntime, 0, 0, 0, napi_enumerable, 0},
        napi_property_descriptor {"createRuntime", 0, CreateRuntime, 0, 0, 0, napi_enumerable, 0},
        napi_property_descriptor {"registerEtsFunction", 0, RegisterETSFunction, 0, 0, 0, napi_enumerable, 0},
        napi_property_descriptor {"registerEtsClass", 0, RegisterETSClass, 0, 0, 0, napi_enumerable, 0},
    };

    NAPI_CHECK_FATAL(napi_define_properties(env, exports, desc.size(), desc.data()));
    return exports;
}

}  // namespace panda::ets::interop::js

NAPI_MODULE(ETS_INTEROP_JS_NAPI, panda::ets::interop::js::Init)
