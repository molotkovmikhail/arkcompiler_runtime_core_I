/**
 * Copyright (c) 2021-2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "plugins/ets/runtime/ets_class_linker_extension.h"
#include "plugins/ets/runtime/interop_js/interop_context.h"
#include "plugins/ets/runtime/interop_js/ts2ets_common.h"
#include "plugins/ets/runtime/interop_js/js_refconvert.h"
#include "plugins/ets/runtime/interop_js/js_convert.h"
#include "runtime/mem/local_object_handle.h"

namespace panda::ets::interop::js {

// JSRefConvert adapter for builtin reference types
template <typename Conv>
class JSRefConvertBuiltin : public JSRefConvert {
public:
    JSRefConvertBuiltin() : JSRefConvert(this) {}

    napi_value WrapImpl(InteropCtx *ctx, EtsObject *obj) const
    {
        using ObjType = std::remove_pointer_t<typename Conv::cpptype>;
        return Conv::Wrap(ctx->GetJSEnv(), FromEtsObject<ObjType>(obj));
    }

    EtsObject *UnwrapImpl(InteropCtx *ctx, napi_value js_value) const
    {
        auto res = Conv::Unwrap(ctx, ctx->GetJSEnv(), js_value);
        if (!res) {
            return nullptr;
        }
        return AsEtsObject(res.value());
    }
};

// JSRefConvert adapter for builtin[] types
template <typename Conv>
class JSRefConvertBuiltinArray : public JSRefConvert {
public:
    explicit JSRefConvertBuiltinArray(Class *klass) : JSRefConvert(this), klass_(klass) {}

private:
    using elem_cpptype = typename Conv::cpptype;  // NOLINT(readability-identifier-naming)

    static elem_cpptype GetElem(coretypes::Array *arr, size_t idx)
    {
        if constexpr (Conv::IS_REFTYPE) {
            auto elem = EtsObject::FromCoreType(arr->Get<ObjectHeader *>(idx));
            return FromEtsObject<std::remove_pointer_t<elem_cpptype>>(elem);
        } else {
            return arr->Get<elem_cpptype>(idx);
        }
    }

    static void SetElem(coretypes::Array *arr, size_t idx, elem_cpptype value)
    {
        if constexpr (Conv::IS_REFTYPE) {
            arr->Set(idx, AsEtsObject(value)->GetCoreType());
        } else {
            arr->Set(idx, value);
        }
    }

public:
    napi_value WrapImpl(InteropCtx *ctx, EtsObject *obj) const
    {
        auto env = ctx->GetJSEnv();

        auto ets_arr = static_cast<coretypes::Array *>(obj->GetCoreType());
        auto len = ets_arr->GetLength();

        NapiEscapableScope js_handle_scope(env);
        napi_value js_arr;
        NAPI_CHECK_FATAL(napi_create_array_with_length(env, len, &js_arr));

        for (size_t idx = 0; idx < len; ++idx) {
            elem_cpptype ets_elem = GetElem(ets_arr, idx);
            auto js_elem = Conv::WrapWithNullCheck(env, ets_elem);
            if (UNLIKELY(js_elem == nullptr)) {
                return nullptr;
            }
            napi_status rc = napi_set_element(env, js_arr, idx, js_elem);
            if (UNLIKELY(NapiThrownGeneric(rc))) {
                return nullptr;
            }
        }
        js_handle_scope.Escape(js_arr);
        return js_arr;
    }

    EtsObject *UnwrapImpl(InteropCtx *ctx, napi_value js_arr) const
    {
        auto coro = EtsCoroutine::GetCurrent();
        auto env = ctx->GetJSEnv();
        {
            bool is_array;
            NAPI_CHECK_FATAL(napi_is_array(env, js_arr, &is_array));
            if (UNLIKELY(!is_array)) {
                JSConvertTypeCheckFailed("array");
                return nullptr;
            }
        }

        uint32_t len;
        napi_status rc = napi_get_array_length(env, js_arr, &len);
        if (UNLIKELY(NapiThrownGeneric(rc))) {
            return nullptr;
        }

        // TODO(vpukhov): elide handles for primitive arrays
        LocalObjectHandle<coretypes::Array> ets_arr(coro, coretypes::Array::Create(klass_, len));
        NapiScope js_handle_scope(env);

        for (size_t idx = 0; idx < len; ++idx) {
            napi_value js_elem;
            rc = napi_get_element(env, js_arr, idx, &js_elem);
            if (UNLIKELY(NapiThrownGeneric(rc))) {
                return nullptr;
            }
            auto res = Conv::UnwrapWithNullCheck(ctx, env, js_elem);
            if (UNLIKELY(!res)) {
                return nullptr;
            }
            SetElem(ets_arr.GetPtr(), idx, res.value());
        }

        return EtsObject::FromCoreType(ets_arr.GetPtr());
    }

private:
    Class *klass_ {};
};

// JSRefConvert adapter for reference[] types
class JSRefConvertReftypeArray : public JSRefConvert {
public:
    JSRefConvertReftypeArray(Class *klass, JSRefConvert *elem_conv)
        : JSRefConvert(this), klass_(klass), elem_conv_(elem_conv)
    {
    }

    napi_value WrapImpl(InteropCtx *ctx, EtsObject *obj) const
    {
        auto coro = EtsCoroutine::GetCurrent();
        auto env = ctx->GetJSEnv();

        LocalObjectHandle<coretypes::Array> ets_arr(coro, obj->GetCoreType());
        auto len = ets_arr->GetLength();

        NapiEscapableScope js_handle_scope(env);
        napi_value js_arr;
        NAPI_CHECK_FATAL(napi_create_array_with_length(env, len, &js_arr));

        for (size_t idx = 0; idx < len; ++idx) {
            EtsObject *ets_elem = EtsObject::FromCoreType(ets_arr->Get<ObjectHeader *>(idx));
            napi_value js_elem;
            if (LIKELY(ets_elem != nullptr)) {
                js_elem = elem_conv_->Wrap(ctx, ets_elem);
                if (UNLIKELY(js_elem == nullptr)) {
                    return nullptr;
                }
            } else {
                js_elem = GetNull(env);
            }
            napi_status rc = napi_set_element(env, js_arr, idx, js_elem);
            if (UNLIKELY(NapiThrownGeneric(rc))) {
                return nullptr;
            }
        }
        js_handle_scope.Escape(js_arr);
        return js_arr;
    }

    EtsObject *UnwrapImpl(InteropCtx *ctx, napi_value js_arr) const
    {
        auto coro = EtsCoroutine::GetCurrent();
        auto env = ctx->GetJSEnv();
        {
            bool is_array;
            NAPI_CHECK_FATAL(napi_is_array(env, js_arr, &is_array));
            if (UNLIKELY(!is_array)) {
                JSConvertTypeCheckFailed("array");
                return nullptr;
            }
        }

        uint32_t len;
        napi_status rc = napi_get_array_length(env, js_arr, &len);
        if (UNLIKELY(NapiThrownGeneric(rc))) {
            return nullptr;
        }

        LocalObjectHandle<coretypes::Array> ets_arr(coro, coretypes::Array::Create(klass_, len));
        NapiScope js_handle_scope(env);

        for (size_t idx = 0; idx < len; ++idx) {
            napi_value js_elem;
            rc = napi_get_element(env, js_arr, idx, &js_elem);
            if (UNLIKELY(NapiThrownGeneric(rc))) {
                return nullptr;
            }
            if (LIKELY(!IsNullOrUndefined(env, js_elem))) {
                EtsObject *ets_elem = elem_conv_->Unwrap(ctx, js_elem);
                if (UNLIKELY(ets_elem == nullptr)) {
                    return nullptr;
                }
                ets_arr->Set(idx, ets_elem->GetCoreType());
            }
        }

        return EtsObject::FromCoreType(ets_arr.GetPtr());
    }

private:
    static constexpr auto ELEM_SIZE = ClassHelper::OBJECT_POINTER_SIZE;

    Class *klass_ {};
    JSRefConvert *elem_conv_ {};
};

static std::unique_ptr<JSRefConvert> JSRefConvertCreateImpl(InteropCtx *ctx, Class *klass)
{
    std::string descriptor(utf::Mutf8AsCString(klass->GetDescriptor()));
    if (descriptor.rfind("Lstd/", 0) == 0) {
        NapiFatal("Convertor for " + descriptor + " requested, must add placeholder");
    }

    if (klass->IsArrayClass()) {
        auto type = klass->GetComponentType()->GetType().GetId();
        NAPI_FATAL_IF(type != panda_file::Type::TypeId::REFERENCE);  // Unhandled primitive array
        JSRefConvert *elem_conv = JSRefConvertResolve(ctx, klass->GetComponentType());
        if (UNLIKELY(elem_conv == nullptr)) {
            return nullptr;
        }
        return std::unique_ptr<JSRefConvert>(new JSRefConvertReftypeArray(klass, elem_conv));
    }

    // TODO(vpukhov): ets_proxy
    return nullptr;
}

JSRefConvert *JSRefConvertCreate(InteropCtx *ctx, Class *klass)
{
    auto conv = JSRefConvertCreateImpl(ctx, klass);
    if (UNLIKELY(conv == nullptr)) {
        NapiFatal(std::string("Can't create convertor for ") + utf::Mutf8AsCString(klass->GetDescriptor()));
    }
    return ctx->GetRefConvertCache()->Insert(klass, std::move(conv));
}

template <typename Conv>
static inline void RegisterBuiltinRefConvertor(JSRefConvertCache *cache, Class *klass)
{
    cache->Insert(klass, std::unique_ptr<JSRefConvert>(new JSRefConvertBuiltin<Conv>()));
}

template <ClassRoot CLASS_ROOT, typename Conv>
static inline void RegisterBuiltinArrayConvertor(JSRefConvertCache *cache, EtsClassLinkerExtension *ext)
{
    auto aklass = ext->GetClassRoot(CLASS_ROOT);
    cache->Insert(aklass, std::unique_ptr<JSRefConvert>(new JSRefConvertBuiltinArray<Conv>(aklass)));
}

void RegisterBuiltinJSRefConvertors(InteropCtx *ctx)
{
    auto cache = ctx->GetRefConvertCache();
    RegisterBuiltinRefConvertor<JSConvertJSValue>(cache, ctx->JSValueClass());
    RegisterBuiltinRefConvertor<JSConvertJSException>(cache, ctx->JSExceptionClass());
    RegisterBuiltinRefConvertor<JSConvertString>(cache, ctx->GetStringClass());
    RegisterBuiltinRefConvertor<JSConvertPromise>(cache, ctx->GetPromiseClass());

    auto coro = EtsCoroutine::GetCurrent();
    PandaEtsVM *vm = coro->GetPandaVM();
    EtsClassLinkerExtension *linker_ext = vm->GetClassLinker()->GetEtsClassLinkerExtension();

    RegisterBuiltinArrayConvertor<ClassRoot::ARRAY_U1, JSConvertU1>(cache, linker_ext);
    RegisterBuiltinArrayConvertor<ClassRoot::ARRAY_I32, JSConvertI32>(cache, linker_ext);
    RegisterBuiltinArrayConvertor<ClassRoot::ARRAY_F32, JSConvertF32>(cache, linker_ext);
    RegisterBuiltinArrayConvertor<ClassRoot::ARRAY_I64, JSConvertI64>(cache, linker_ext);
    RegisterBuiltinArrayConvertor<ClassRoot::ARRAY_F64, JSConvertF64>(cache, linker_ext);
    RegisterBuiltinArrayConvertor<ClassRoot::ARRAY_STRING, JSConvertString>(cache, linker_ext);
    // TODO(vpukhov): jsvalue[], currently uses JSRefConvertArrayRef
}

}  // namespace panda::ets::interop::js
