/**
 * Copyright (c) 2021-2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef PANDA_PLUGINS_ETS_RUNTIME_INTEROP_JS_JSVALUE_CALL_H_
#define PANDA_PLUGINS_ETS_RUNTIME_INTEROP_JS_JSVALUE_CALL_H_

#include <node_api.h>
#include "utils/span.h"

namespace panda::ets {
class EtsString;
}  // namespace panda::ets

namespace panda::ets::interop::js {

napi_value CallEtsFunctionImpl(napi_env env, Span<napi_value> jsargv);
napi_value EtsLambdaProxyInvoke(napi_env env, napi_callback_info cbinfo);

uint8_t JSRuntimeInitJSCallClass(EtsString *cls_str);
uint8_t JSRuntimeInitJSNewClass(EtsString *cls_str);

}  // namespace panda::ets::interop::js

#endif  // !PANDA_PLUGINS_ETS_RUNTIME_INTEROP_JS_JSVALUE_CALL_H_
