/**
 * Copyright (c) 2023 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "plugins/ets/runtime/interop_js/interop_context.h"

#include "plugins/ets/runtime/ets_exceptions.h"
#include "plugins/ets/runtime/ets_class_linker_extension.h"
#include "plugins/ets/runtime/ets_vm.h"
#include "plugins/ets/runtime/interop_js/js_convert.h"
#include "plugins/ets/runtime/interop_js/ts2ets_common.h"
#include "plugins/ets/runtime/types/ets_method.h"
#include "runtime/include/runtime.h"

namespace panda::ets::interop::js {

InteropCtx::InteropCtx(EtsCoroutine *coro)
{
    PandaEtsVM *vm = coro->GetPandaVM();
    EtsClassLinker *ets_class_linker = vm->GetClassLinker();
    refstor_ = vm->GetGlobalObjectStorage();
    linker_ctx_ = ets_class_linker->GetEtsClassLinkerExtension()->GetBootContext();

    JSRuntimeIntrinsicsSetIntrinsicsAPI(GetIntrinsicsAPI());
    auto *job_queue = Runtime::GetCurrent()->GetInternalAllocator()->New<JsJobQueue>();
    vm->InitJobQueue(job_queue);

    auto cache_class = [&](const char *descriptor) {
        auto klass = ets_class_linker->GetClass(descriptor)->GetRuntimeClass();
        ASSERT(klass != nullptr);
        return klass;
    };
    jsruntime_class_ = cache_class("Lstd/interop/js/JSRuntime;");
    jsvalue_class_ = cache_class("Lstd/interop/js/JSValue;");
    jsexception_class_ = cache_class("Lstd/interop/js/JSException;");
    string_class_ = cache_class("Lstd/core/String;");
    promise_class_ = cache_class("Lstd/core/Promise;");

    RegisterBuiltinJSRefConvertors(this);

    {
        auto method = EtsClass::FromRuntimeClass(jsruntime_class_)->GetMethod("createFinalizationQueue");
        ASSERT(method != nullptr);
        auto res = method->GetPandaMethod()->Invoke(coro, nullptr);
        ASSERT(!coro->HasPendingException());
        auto queue = EtsObject::FromCoreType(res.GetAs<ObjectHeader *>());
        jsvalue_fqueue_ref_ = Refstor()->Add(queue->GetCoreType(), mem::Reference::ObjectType::GLOBAL);
        ASSERT(jsvalue_fqueue_ref_ != nullptr);

        jsvalue_fqueue_register_ = queue->GetClass()
                                       ->GetMethod("register", "Lstd/core/Object;Lstd/core/Object;Lstd/core/Object;:V")
                                       ->GetPandaMethod();
        ASSERT(jsvalue_fqueue_register_ != nullptr);
    }
}

EtsObject *InteropCtx::CreateJSException(EtsCoroutine *coro, JSValue *jsvalue)
{
    [[maybe_unused]] HandleScope<ObjectHeader *> scope(coro);
    VMHandle<ObjectHeader> jsvalue_handle(coro, jsvalue->GetCoreType());

    Method::Proto proto(Method::Proto::ShortyVector {panda_file::Type(panda_file::Type::TypeId::VOID),
                                                     panda_file::Type(panda_file::Type::TypeId::REFERENCE)},
                        Method::Proto::RefTypeVector {utf::Mutf8AsCString(JSValueClass()->GetDescriptor())});
    auto ctor_name = utf::CStringAsMutf8(panda_file_items::CTOR.data());
    auto ctor = JSExceptionClass()->GetDirectMethod(ctor_name, proto);
    ASSERT(ctor != nullptr);

    auto exc_obj = ObjectHeader::Create(coro, JSExceptionClass());
    if (UNLIKELY(exc_obj == nullptr)) {
        return nullptr;
    }
    VMHandle<ObjectHeader> exc_handle(coro, exc_obj);

    std::array<Value, 2> args {Value(exc_handle.GetPtr()), Value(jsvalue_handle.GetPtr())};
    ctor->InvokeVoid(coro, args.data());
    auto res = EtsObject::FromCoreType(exc_handle.GetPtr());
    if (UNLIKELY(coro->HasPendingException())) {
        return nullptr;
    }
    return res;
}

void InteropCtx::ThrowETSException(EtsCoroutine *coro, napi_value val)
{
    auto ctx = Current(coro);

    if (coro->IsUsePreAllocObj()) {
        coro->SetUsePreAllocObj(false);
        coro->SetException(coro->GetVM()->GetOOMErrorObject());
        return;
    }
    ASSERT(!coro->HasPendingException());

    auto exc = JSConvertJSException::Unwrap(ctx, ctx->GetJSEnv(), val);
    if (LIKELY(exc.has_value())) {
        ASSERT(exc != nullptr);
        coro->SetException(exc.value()->GetCoreType());
    }  // otherwise exception is already set
}

void InteropCtx::ThrowETSException(EtsCoroutine *coro, const char *msg)
{
    ASSERT(!coro->HasPendingException());
    ets::ThrowEtsException(coro, "Lstd/interop/js/JSException;", msg);
}

void InteropCtx::ThrowJSError(napi_env env, const std::string &msg)
{
    INTEROP_LOG(INFO) << "ThrowJSError: " << msg;
    bool pending;
    NAPI_CHECK_FATAL(napi_is_exception_pending(env, &pending));
    if (!pending) {
        napi_throw_error(env, nullptr, msg.c_str());
    }
}

void InteropCtx::ThrowJSException(napi_env env, napi_value val)
{
    INTEROP_LOG(INFO) << "ThrowJSException";
    napi_throw(env, val);
}

void InteropCtx::ForwardEtsException(EtsCoroutine *coro)
{
    auto env = GetJSEnv();
    ASSERT(coro->HasPendingException());
    auto exc = EtsObject::FromCoreType(coro->GetException());
    coro->ClearException();
    auto klass = exc->GetClass();
    if (LIKELY(klass->GetRuntimeClass() == JSExceptionClass())) {
        napi_value js_exc = JSConvertJSException::Wrap(env, exc);
        ThrowJSException(env, js_exc);
        return;
    }
    // TODO(vpukhov): put proxy-object instead
    ThrowJSError(env, klass->GetDescriptor());
}

void InteropCtx::ForwardJSException(EtsCoroutine *coro)
{
    napi_value excval;
    NAPI_CHECK_FATAL(napi_get_and_clear_last_exception(GetJSEnv(), &excval));
    ThrowETSException(coro, excval);
}

void JSConvertTypeCheckFailed(const char *type_name)
{
    auto ctx = InteropCtx::Current();
    auto env = ctx->GetJSEnv();
    std::string str = type_name + std::string(" expected");
    napi_value js_val;
    NAPI_CHECK_FATAL(napi_create_string_utf8(env, str.data(), str.length(), &js_val));
    InteropCtx::ThrowJSException(env, js_val);
}

[[noreturn]] void InteropCtx::Fatal(const char *msg)
{
    INTEROP_LOG(ERROR) << "InteropCtx::Fatal: " << msg;
    napi_fatal_error("ets::interop::js", NAPI_AUTO_LENGTH, msg, NAPI_AUTO_LENGTH);
    std::abort();
}

}  // namespace panda::ets::interop::js
