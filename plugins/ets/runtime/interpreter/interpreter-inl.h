/*
 * Copyright (c) 2021-2023 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef PLUGINS_ETS_INTERPRETER_INTERPRETER_INL_H
#define PLUGINS_ETS_INTERPRETER_INTERPRETER_INL_H

#include "runtime/coroutine_manager.h"
#include "runtime/interpreter/interpreter-inl.h"
#include "runtime/mem/internal_allocator.h"
#include "plugins/ets/runtime/ets_class_linker_extension.h"
#include "plugins/ets/runtime/ets_coroutine.h"
#include "plugins/ets/runtime/ets_vm.h"
#include "plugins/ets/runtime/ets_handle_scope.h"
#include "plugins/ets/runtime/ets_handle.h"
#include "plugins/ets/runtime/types/ets_promise.h"
#include "runtime/mem/refstorage/global_object_storage.h"

namespace panda::ets {
template <class RuntimeIfaceT, bool IS_DYNAMIC, bool IS_DEBUG, bool IS_PROFILE_ENABLED>
class InstructionHandler : public interpreter::InstructionHandler<RuntimeIfaceT, IS_DYNAMIC, IS_DEBUG> {
public:
    ALWAYS_INLINE inline explicit InstructionHandler(interpreter::InstructionHandlerState *state)
        : interpreter::InstructionHandler<RuntimeIfaceT, IS_DYNAMIC, IS_DEBUG>(state)
    {
    }

    template <BytecodeInstruction::Format FORMAT>
    ALWAYS_INLINE void HandleEtsLaunchShort()
    {
        auto id = this->GetInst().template GetId<FORMAT>();
        LOG_INST() << "launch.short v" << this->GetInst().template GetVReg<FORMAT, 0>() << ", v"
                   << this->GetInst().template GetVReg<FORMAT, 1>() << ", " << std::hex << "0x" << id;
        HandleLaunch<FORMAT, false>(id);
    }

    template <BytecodeInstruction::Format FORMAT>
    ALWAYS_INLINE void HandleEtsLaunch()
    {
        auto id = this->GetInst().template GetId<FORMAT>();
        LOG_INST() << "launch v" << this->GetInst().template GetVReg<FORMAT, 0>() << ", v"
                   << this->GetInst().template GetVReg<FORMAT, 1>() << ", v"
                   << this->GetInst().template GetVReg<FORMAT, 2>() << ", v"
                   << this->GetInst().template GetVReg<FORMAT, 3>() << ", " << std::hex << "0x" << id;
        HandleLaunch<FORMAT, false>(id);
    }

    template <BytecodeInstruction::Format FORMAT>
    ALWAYS_INLINE void HandleEtsLaunchRange()
    {
        auto id = this->GetInst().template GetId<FORMAT>();
        LOG_INST() << "launch.range v" << this->GetInst().template GetVReg<FORMAT, 0>() << ", " << std::hex << "0x"
                   << id;
        HandleLaunch<FORMAT, true>(id);
    }

    template <BytecodeInstruction::Format FORMAT>
    ALWAYS_INLINE void HandleEtsLaunchVirtShort()
    {
        auto id = this->GetInst().template GetId<FORMAT>();
        LOG_INST() << "launch.virt.short v" << this->GetInst().template GetVReg<FORMAT, 0>() << ", v"
                   << this->GetInst().template GetVReg<FORMAT, 1>() << ", " << std::hex << "0x" << id;
        HandleLaunchVirt<FORMAT, false>(id);
    }

    template <BytecodeInstruction::Format FORMAT>
    ALWAYS_INLINE void HandleEtsLaunchVirt()
    {
        auto id = this->GetInst().template GetId<FORMAT>();
        LOG_INST() << "launch.virt v" << this->GetInst().template GetVReg<FORMAT, 0>() << ", v"
                   << this->GetInst().template GetVReg<FORMAT, 1>() << ", v"
                   << this->GetInst().template GetVReg<FORMAT, 2>() << ", v"
                   << this->GetInst().template GetVReg<FORMAT, 3>() << ", " << std::hex << "0x" << id;
        HandleLaunchVirt<FORMAT, false>(id);
    }

    template <BytecodeInstruction::Format FORMAT>
    ALWAYS_INLINE void HandleEtsLaunchVirtRange()
    {
        auto id = this->GetInst().template GetId<FORMAT>();
        LOG_INST() << "launch.virt.range v" << this->GetInst().template GetVReg<FORMAT, 0>() << ", " << std::hex << "0x"
                   << id;
        HandleLaunchVirt<FORMAT, true>(id);
    }

private:
    template <BytecodeInstruction::Format FORMAT, bool IS_RANGE>
    ALWAYS_INLINE void HandleLaunchVirt(BytecodeId method_id)
    {
        auto *method = ResolveMethod(method_id);
        if (LIKELY(method != nullptr)) {
            ObjectHeader *obj = this->GetCallerObject<FORMAT>();
            if (UNLIKELY(obj == nullptr)) {
                return;
            }
            auto *cls = obj->ClassAddr<Class>();
            ASSERT(cls != nullptr);
            method = cls->ResolveVirtualMethod(method);
            ASSERT(method != nullptr);
            HandleLaunch<FORMAT, IS_RANGE>(method);
        } else {
            this->MoveToExceptionHandler();
        }
    }

    template <BytecodeInstruction::Format FORMAT, bool IS_RANGE>
    ALWAYS_INLINE void HandleLaunch(BytecodeId method_id)
    {
        auto *method = ResolveMethod(method_id);
        if (LIKELY(method != nullptr)) {
            HandleLaunch<FORMAT, IS_RANGE>(method);
        } else {
            this->MoveToExceptionHandler();
        }
    }

    template <BytecodeInstruction::Format FORMAT, bool IS_RANGE>
    ALWAYS_INLINE void HandleLaunch(Method *method)
    {
        if (UNLIKELY(!method->Verify())) {
            RuntimeIfaceT::ThrowVerificationException(method->GetFullName());
            this->MoveToExceptionHandler();
            return;
        }

        size_t num_args = method->GetNumArgs();
        PandaVector<Value> args(num_args);
        FillArgs<FORMAT, IS_RANGE>(args);

        // this promise is going to be resolved on coro completion
        EtsCoroutine *coroutine = EtsCoroutine::GetCurrent();
        EtsPromise *promise = EtsPromise::Create(coroutine);
        if (UNLIKELY(promise == nullptr)) {
            this->MoveToExceptionHandler();
            return;
        }
        PandaEtsVM *ets_vm = coroutine->GetPandaVM();
        auto promise_ref = ets_vm->GetGlobalObjectStorage()->Add(promise, mem::Reference::ObjectType::WEAK);
        auto evt = Runtime::GetCurrent()->GetInternalAllocator()->New<CompletionEvent>(promise_ref);
        promise->SetEventPtr(evt);

        // create the coro and put it to the ready queue
        [[maybe_unused]] EtsHandleScope scope(coroutine);
        EtsHandle<EtsPromise> promise_handle(coroutine, promise);
        auto *cm = static_cast<CoroutineManager *>(ets_vm->GetThreadManager());
        auto *coro = cm->Launch(evt, method, std::move(args));
        if (UNLIKELY(coro == nullptr)) {
            // OOM
            promise_handle.GetPtr()->SetEventPtr(nullptr);
            Runtime::GetCurrent()->GetInternalAllocator()->Delete(evt);
            this->MoveToExceptionHandler();
            return;
        }

        this->GetAccAsVReg().SetReference(promise_handle.GetPtr());
        this->GetFrame()->SetAcc(this->GetAcc());
        this->template MoveToNextInst<FORMAT, false>();
    }

    template <BytecodeInstruction::Format FORMAT, bool IS_RANGE>
    ALWAYS_INLINE void FillArgs(PandaVector<Value> &args)
    {
        if (args.empty()) {
            return;
        }

        auto cur_frame_handler = this->template GetFrameHandler<false>();
        if constexpr (IS_RANGE) {
            uint16_t start_reg = this->GetInst().template GetVReg<FORMAT, 0>();
            for (size_t i = 0; i < args.size(); ++i) {
                args[i] = Value::FromVReg(cur_frame_handler.GetVReg(start_reg + i));
            }
        } else {
            // launch.short of launch
            args[0] = Value::FromVReg(cur_frame_handler.GetVReg(this->GetInst().template GetVReg<FORMAT, 0U>()));
            if (args.size() > 1U) {
                args[1] = Value::FromVReg(cur_frame_handler.GetVReg(this->GetInst().template GetVReg<FORMAT, 1U>()));
            }
            if constexpr (FORMAT == BytecodeInstruction::Format::PREF_V4_V4_V4_V4_ID16) {
                if (args.size() > 2U) {
                    args[2] =
                        Value::FromVReg(cur_frame_handler.GetVReg(this->GetInst().template GetVReg<FORMAT, 2U>()));
                }
                if (args.size() > 3U) {
                    args[3] =
                        Value::FromVReg(cur_frame_handler.GetVReg(this->GetInst().template GetVReg<FORMAT, 3U>()));
                }
            }
        }
    }

    template <BytecodeInstruction::Format FORMAT>
    ALWAYS_INLINE ObjectHeader *GetObjHelper()
    {
        uint16_t obj_vreg = this->GetInst().template GetVReg<FORMAT, 0>();
        return this->GetFrame()->GetVReg(obj_vreg).GetReference();
    }

    template <BytecodeInstruction::Format FORMAT>
    ALWAYS_INLINE ObjectHeader *GetCallerObject()
    {
        ObjectHeader *obj = GetObjHelper<FORMAT>();

        if (UNLIKELY(obj == nullptr)) {
            RuntimeIfaceT::ThrowNullPointerException();
            this->MoveToExceptionHandler();
            return nullptr;
        }
        return obj;
    }

    ALWAYS_INLINE Method *ResolveMethod(BytecodeId id)
    {
        this->UpdateBytecodeOffset();

        auto cache = this->GetThread()->GetInterpreterCache();
        auto *res = cache->template Get<Method>(this->GetInst().GetAddress(), this->GetFrame()->GetMethod());
        if (res != nullptr) {
            return res;
        }

        this->GetFrame()->SetAcc(this->GetAcc());
        auto *method = RuntimeIfaceT::ResolveMethod(this->GetThread(), *this->GetFrame()->GetMethod(), id);
        this->GetAcc() = this->GetFrame()->GetAcc();
        if (UNLIKELY(method == nullptr)) {
            ASSERT(this->GetThread()->HasPendingException());
            return nullptr;
        }

        cache->Set(this->GetInst().GetAddress(), method, this->GetFrame()->GetMethod());
        return method;
    }
};
}  // namespace panda::ets
#endif  // PLUGINS_ETS_INTERPRETER_INTERPRETER_INL_H
