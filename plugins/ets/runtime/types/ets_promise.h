/**
 * Copyright (c) 2023 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef PANDA_RUNTIME_ETS_FFI_CLASSES_ETS_PROMISE_H_
#define PANDA_RUNTIME_ETS_FFI_CLASSES_ETS_PROMISE_H_

#include "plugins/ets/runtime/types/ets_object.h"
#include "plugins/ets/runtime/types/ets_array.h"
#include "plugins/ets/runtime/types/ets_primitives.h"
#include "runtime/include/coroutine_events.h"

namespace panda::ets {

class EtsCoroutine;

namespace test {
class EtsPromiseMembers;
}  // namespace test

class EtsPromise : public ObjectHeader {
public:
    // temp
    static constexpr EtsInt STATE_PENDING = 0;
    static constexpr EtsInt STATE_RESOLVED = 1;
    static constexpr EtsInt STATE_REJECTED = 2;

    EtsPromise() = delete;
    ~EtsPromise() = delete;

    NO_COPY_SEMANTIC(EtsPromise);
    NO_MOVE_SEMANTIC(EtsPromise);

    static EtsPromise *Create(EtsCoroutine *ets_coroutine = EtsCoroutine::GetCurrent());

    static EtsPromise *FromCoreType(ObjectHeader *promise)
    {
        return reinterpret_cast<EtsPromise *>(promise);
    }

    ObjectHeader *GetCoreType()
    {
        return reinterpret_cast<ObjectHeader *>(this);
    }

    EtsObject *AsObject()
    {
        return EtsObject::FromCoreType(this);
    }

    const EtsObject *AsObject() const
    {
        return EtsObject::FromCoreType(this);
    }

    static EtsPromise *FromEtsObject(EtsObject *promise)
    {
        return reinterpret_cast<EtsPromise *>(promise);
    }

    EtsObjectArray *GetThenQueue(EtsCoroutine *coro)
    {
        return EtsObjectArray::FromCoreType(
            ObjectAccessor::GetObject(coro, this, MEMBER_OFFSET(EtsPromise, then_queue_)));
    }

    EtsObjectArray *GetCatchQueue(EtsCoroutine *coro)
    {
        return EtsObjectArray::FromCoreType(
            ObjectAccessor::GetObject(coro, this, MEMBER_OFFSET(EtsPromise, catch_queue_)));
    }

    void ClearQueues(EtsCoroutine *coro)
    {
        ObjectAccessor::SetObject(coro, this, MEMBER_OFFSET(EtsPromise, then_queue_), nullptr);
        ObjectAccessor::SetObject(coro, this, MEMBER_OFFSET(EtsPromise, catch_queue_), nullptr);
    }

    CompletionEvent *GetEventPtr()
    {
        return reinterpret_cast<CompletionEvent *>(event_);
    }

    void SetEventPtr(CompletionEvent *e)
    {
        event_ = reinterpret_cast<EtsLong>(e);
    }

    bool IsResolved() const
    {
        return (state_ == STATE_RESOLVED);
    }

    void Resolve(EtsCoroutine *coro, EtsObject *value)
    {
        ASSERT(state_ == STATE_PENDING);
        ObjectAccessor::SetObject(coro, this, MEMBER_OFFSET(EtsPromise, value_), value->GetCoreType());
        state_ = STATE_RESOLVED;
    }

    void Reject(EtsCoroutine *coro, EtsObject *error)
    {
        ASSERT(state_ == STATE_PENDING);
        ObjectAccessor::SetObject(coro, this, MEMBER_OFFSET(EtsPromise, value_), error->GetCoreType());
        state_ = STATE_REJECTED;
    }

    EtsObject *GetValue(EtsCoroutine *coro)
    {
        return EtsObject::FromCoreType(ObjectAccessor::GetObject(coro, this, MEMBER_OFFSET(EtsPromise, value_)));
    }

    uint32_t GetState() const
    {
        return state_;
    }

    static size_t ValueOffset()
    {
        return MEMBER_OFFSET(EtsPromise, value_);
    }

private:
    ObjectPointer<EtsObject> value_;  // the completion value of the Promise
    ObjectPointer<EtsObjectArray>
        then_queue_;  // the queue of 'then' calbacks which will be called when the Promise gets resolved
    ObjectPointer<EtsObjectArray>
        catch_queue_;  // the queue of 'catch' callback which will be called when the Promise gets rejected
    uint32_t state_;   // the Promise's state
    EtsLong event_;

    friend class test::EtsPromiseMembers;
};

}  // namespace panda::ets

#endif  // PANDA_RUNTIME_ETS_FFI_CLASSES_ETS_PROMISE_H_
