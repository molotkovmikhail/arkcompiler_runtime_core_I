/*
 * Copyright (c) 2021-2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package std.core;

/**
 * @class Strores information about stacktrace and cause in case of an error.
 * Serves as a base class for all error classes.
 */
export class Error {
  private stackLines: String[];
  private cause: Object;
  private msg: String;
  private provisionStackTrace(): void {
    this.stackLines = stackTraceLines();
  }

  /**
   * Constructs a new empty error instance
   */
  constructor () {
    this.msg = "";
    this.cause = this;
    this.provisionStackTrace();
  }

  /**
   * Constructs a new error instance with provided message
   *
   * @param msg: String - message of the error
   */
  constructor(msg: String) {
    this.msg = msg;
    this.cause = this;
    this.provisionStackTrace()
  }

  /**
   * Constructs a new error instance with provided message and cause
   *
   * @param msg: String - message of the error
   *
   * @param cause: Object - cause of the error
   */
  constructor(msg: String, cause: Object) {
    this.msg = msg;
    this.cause = cause;
    this.provisionStackTrace();
  }

  /**
    * Converts this error to a string
    * Result includes error message and the stacktrace
    *
    * @returns String - result of the conversion
    */
  override toString(): String {
    let s: String = "";

    if (this.msg != "") {
      s += "Error: " + this.msg + "\n";
    }

    for (let i: int = (this.stackLines.length > 2 ? 2 : 0); i < this.stackLines.length; i++) {
      s += this.stackLines[i];
      if (i != this.stackLines.length-1) {
        s += "\n";
      }
    }

    return s;
  }

  /**
   * Returns the cause of this error
   *
   * @returns Object - the cause
   */
  public getCause(): Object {
    if (this.cause == this) {
      return null;
    } else {
      return this.cause;
    }
  }
}
