/*
 * Copyright (c) 2021-2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package std.containers;

export open class Node {
    left: Node;
    key: {{K}};
    value: {{V}};
    height: int;
    right: Node;
    constructor(left: Node, key: {{K}}, value: {{V}}, height: int, right: Node) {
        this.key = key;
        this.value = value;
        this.left = left;
        this.right = right;
        this.height = height;
    }
}

export function newEmpty(): Node {
    return null;
}

export function newLeaf(k: {{K}}, v: {{V}}): Node {
    return newNode(newEmpty(), k, v, 1, newEmpty());
}

export function newNode(left: Node, k: {{K}}, v: {{V}}, height: int, right: Node): Node {
    return new Node(left, k, v, height, right);
}

export function isEmpty(n: Node): boolean {
    return n == null;
}

export function isLeaf(n: Node): boolean {
    return n != null && n.left == null && n.right == null;
}

export function isNode(n: Node): boolean {
    return n != null && (n.left != null || n.right != null);
}

export function height(node: Node): int {
    if (isEmpty(node)) {
        return 0;
    } else if (isLeaf(node)) {
        return 1;
    } else {
        return node.height;
    }
}

function legalLeftKey(node: Node, key: {{K}}): void {
    if (isEmpty(node)) {
        return;
    }

    let leftKey: {{K}} = node.key;
    assert leftKey.compareTo(key) < 0;
}

function legalRightKey(node: Node, key: {{K}}): void {
    if (isEmpty(node)) {
        return;
    }

    let rightKey: {{K}} = node.key;
    assert rightKey.compareTo(key) > 0;
}

function inv(node: Node): void {
    if (isNode(node)) {
        let hl = height(node.left);
        let hr = height(node.right);

        inv(node.left);
        inv(node.right);

        legalLeftKey(node.left, node.key);
        legalRightKey(node.right, node.key);

        assert node.height == max(hl, hr) + 1;
        assert abs(hl - hr) <= 2;
    }
}

function updateHeight(node: Node): void {
    if (isNode(node)) {
        let newHeight = max(height(node.left), height(node.right)) + 1;
        if (newHeight != node.height) {
            node.height = newHeight;
        }
    } else if (isLeaf(node)) {
        node.height = 1;
    } else {
        assert false;
    }
}

function balanceTree(node: Node): Node {
    if (isEmpty(node) || isLeaf(node)) {
        return node;
    } else if (isNode(node)) {
        let rootNode = node;
        let hl = height(rootNode.left);
        let hr = height(rootNode.right);

        if (hl > hr + 2) {
            assert !(isEmpty(rootNode.left)) && !(isLeaf(rootNode.left));
            let leftNode = rootNode.left;
            if (height(leftNode.left) >= height(leftNode.right)) {
                rootNode.left = leftNode.right;
                leftNode.right = rootNode;

                updateHeight(rootNode);
                updateHeight(leftNode);

                return leftNode;
            } else {
                assert !(isEmpty(leftNode.right)) && !(isLeaf(leftNode.right));
                let lrNode = leftNode.right;

                leftNode.right = lrNode.left;
                rootNode.left = lrNode.right;
                lrNode.right = rootNode;
                lrNode.left = leftNode;

                updateHeight(leftNode);
                updateHeight(rootNode);
                updateHeight(lrNode);

                return lrNode;
            }
        } else if (hr > hl + 2) {
            assert !(isEmpty(rootNode.right)) && !(isLeaf(rootNode.right));
            let rightNode = rootNode.right;
            if (height(rightNode.right) >= height(rightNode.left)) {
                rootNode.right = rightNode.left;
                rightNode.left = rootNode;

                updateHeight(rootNode);
                updateHeight(rightNode);

                return rightNode;
            } else {
                assert !(isEmpty(rightNode.left)) && !(isLeaf(rightNode.left));
                let rlNode = rightNode.left;

                rightNode.left = rlNode.right;
                rootNode.right = rlNode.left;
                rlNode.left = rootNode;
                rlNode.right = rightNode;

                updateHeight(rightNode);
                updateHeight(rootNode);
                updateHeight(rlNode);

                return rlNode;
            }
        } else {
            updateHeight(node);
            return node;
        }
    } else {
        assert false;
    }
}

function setLeft(node: Node, tree: Node): void {
    let tree1 = balanceTree(tree);
    assert isNode(node);

    let n = node;
    if (n.left != tree1) {
        n.left = tree1;
    }

    updateHeight(n);
}

function setRight(node: Node, tree: Node): void {
    let tree1 = balanceTree(tree);
    assert isNode(node);

    let n = node;
    if (n.right != tree1) {
        n.right = tree1;
    }

    updateHeight(n);
}

class addResult {
    t: Node;
    res: boolean;
    constructor(t: Node, res: boolean) {
        this.t = t;
        this.res = res;
    }
}

function add(node: Node, k: {{K}}, v: {{V}}, replace: boolean): addResult {
    if (isEmpty(node)) {
        return new addResult(newLeaf(k, v), true);
    }

    if (isLeaf(node)) {
        let leaf = node;
        let c = leaf.key.compareTo(k);
        if (c == 0) {
            if (replace) {
                leaf.value = v;
                return new addResult(leaf, false);
            } else {
                return new addResult(node, false);
            }
        } else {
            if (c < 0) {
                return new addResult(newNode(node, k, v, 2, newEmpty()), true);
            } else {
                return new addResult(newNode(newEmpty(), k, v, 2, node), true);
            }
        }
    }

    if (isNode(node)) {
        let c = k.compareTo(node.key);
        if (c == 0) {
            if (replace) {
                node.value = v;
            }
            return new addResult(node, false);
        } else {
            if (c < 0) {
                let r = add(node.left, k, v, replace);
                setLeft(node, r.t);
                return new addResult(node, r.res);
            } else {
                let r = add(node.right, k, v, replace);
                setRight(node, r.t);
                return new addResult(node, r.res);
            }
        }
    }
}


export function addToTree(node: Node, k: {{K}}, v: {{V}}): Node {
    let r = add(node, k, v, true);
    if (!r.res) {
        return balanceTree(r.t)
    } else {
        return r.t
    }
}

function minElt(node: Node): Node {
    if (isEmpty(node) || isLeaf(node)) {
        return node;
    }
    if (isEmpty(node.left)) {
        return node;
    } else {
        return minElt(node.left);
    }
}

function removeMinElt(node: Node): Node {
    assert !(isEmpty(node));
    if (isLeaf(node)) {
        return null;
    }
    if (isEmpty(node.left)) {
        return node.right;
    } else if (isLeaf(node.left) && isEmpty(node.right)) {
        return newLeaf(node.key, node.value);
    } else if (isLeaf(node.left)) {
        setLeft(node, null);
        return node;
    } else {
        setLeft(node, removeMinElt(node.left));
        return node;
    }
}

export function mergeTree(t1: Node, t2: Node): Node {
    if (isEmpty(t1) && !isEmpty(t2)) {
        return t2;
    }
    if (!isEmpty(t1) && isEmpty(t2)) {
        return t1;
    }

    let tree = minElt(t2);

    assert !isEmpty(tree);

    if (isLeaf(tree)) {
        let leaf = tree;
        let t3 = balanceTree(removeMinElt(t2));
        return newNode(t1, leaf.key, leaf.value, max(height(t1), height(t3)) + 1, t3);
    } else {
        let node = tree;
        setRight(node, removeMinElt(t2));
        setLeft(node, t1);
        return node;
    }
}

class removeResult {
    t: Node;
    res: boolean;
    constructor(t: Node, res: boolean) {
        this.t = t;
        this.res = res;
    }
}

function remove(node: Node, k: {{K}}): removeResult {
    if (isEmpty(node)) {
        return new removeResult(node, false);
    } else if (isLeaf(node)) {
        let leaf = node;
        if (leaf.key.compareTo(k) == 0) {
            return new removeResult(newEmpty(), true);
        } else {
            return new removeResult(node, false);
        }
    } else {
        let c = k.compareTo(node.key);
        if (c == 0) {
            return new removeResult(mergeTree(node.left, node.right), true);
        } else if (c < 0) {
            let r = remove(node.left, k);
            setLeft(node, r.t)
            return new removeResult(node, r.res);
        } else {
            let r = remove(node.right, k);
            setRight(node, r.t)
            return new removeResult(node, r.res);
        }
    }
}

export function removeFromTree(node: Node, k: {{K}}): Node {
    let r = remove(node, k);
    return balanceTree(r.t);
}

export function lookupTree(node: Node, k: {{K}}): {{V}} {
    if (isEmpty(node)) {
        return null;
    } else if (isLeaf(node)) {
        let leaf = node;
        if (k.compareTo(leaf.key) == 0) {
            return leaf.value;
        } else {
            return null;
        }
    } else {
        let c = k.compareTo(node.key);
        if (c == 0) {
            return node.value;
        } else if (c < 0) {
            return lookupTree(node.left, k);
        } else {
            return lookupTree(node.right, k);
        }
    }
}

export function count(node: Node): int {
    if (isEmpty(node)) {
        return 0;
    } else if (isLeaf(node)) {
        return 1;
    } else {
        return 1 + count(node.left) + count(node.right);
    }
}

function treeFolder(k: {{K}}, v: {{V}}, acc: Entry[]): Entry[] {
    let entry = new Entry(k, v);
    let acc1 = new Entry[acc.length + 1];

    for (let i: int = 0; i < acc.length; i++) {
        acc1[i] = acc[i];
    }

    acc1[acc.length] = entry;

    return acc1;
}

function foldTree(node: Node, acc0: Entry[]/*, fn: (k: {{K}}, v: {{V}}, acc: Entry[]): Entry[]*/): Entry[] {
    if (isEmpty(node)) {
        return acc0;
    } else {
        let acc1 = foldTree(node.left, acc0);
        let acc2 = treeFolder(node.key, node.value, acc1);
        return foldTree(node.right, acc2);
    }
}

export function treeToArray(node: Node): Entry[] {
    return foldTree(node, new Entry[0]);
}

