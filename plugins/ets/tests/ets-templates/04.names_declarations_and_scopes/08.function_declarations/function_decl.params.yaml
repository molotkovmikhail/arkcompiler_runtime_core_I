--- # List of valid function declarations
function_decls:
    # no params, void return type
    - decl: |-
        function foo(): void {}
      call: |-
        foo();

    - decl: |-
        function foo() {}
      call: |-
        foo();

    # no params, return value type
    - decl: |-
        function foo(): int { return 0; }
      call: |-
        let i = foo();
        assert i == 0;

    # no params, return value type array
    - decl: |-
        function foo(): int[] { return new int[1]; }
      call: |-
        let arr: int[] = foo();
        assert arr instanceof int[] && arr.length == 1;

    # no params, return function
    - decl: |-
        function bar() {}
        function foo(): () => void {
            return bar;
        }
      call: |-
        let f: () => void = foo();
        assert f instanceof (() => void);

    # no params, return function array
    - decl: |-
        type BarType = () => void
        function bar() {}
        function foo(): BarType[] {
            let arr: BarType[] = new BarType[1]
            arr[0] = bar
            return arr;
        }
      call: |-
        let f: BarType[] = foo();
        assert f instanceof BarType[] && f.length == 1;


    # no params, return reference type
    - decl: |-
        function foo(): Long { return new Long(); }
      call: |-
        let f: Long = foo();
        assert f instanceof Long;

    - decl: |-
        function foo(): Object|null { return null; }
      call: |-
        let f: Object|null = foo();
        assert f == null;

    # no params, return reference type array
    - decl: |-
        function foo(): Long[] { return new Long[0]; }
      call: |-
        let f: Long[] = foo();
        assert f instanceof Long[] && f.length == 0;

    - decl: |-
        function foo(): Object|null[] {
            let arr: Object|null[] = new Object|null[0]
            return arr;
        }
      call: |-
        let f: Object|null[] = foo();
        assert f instanceof Object|null[] && f.length == 0;

    # no params, return enum
    - decl: |-
        enum Color { Red, Green, Blue }
        function foo(): Color { return Color.Red; }
      call: |-
        let e: Color  = foo();
        assert e == Color.Red;

    # no params, return enum array
    - decl: |-
        enum Color { Red, Green, Blue }
        function foo(): Color[] {
            let arr: Color[] = new Color[1]
            arr[0] = Color.Red
            return arr;
        }
      call: |-
        let arr: Color[] = foo();
        assert arr instanceof Color[] && arr.length == 1 && arr[0] == Color.Red;

    # value type params, void return
    - decl: |-
        function foo(a: long, b: double, c: char): void {}
      call: |-
        foo(42, 1.5, c'A');

    # value type array params, void return
    - decl: |-
        function foo(a: long[], b: double[], c: char[]): void {}
      call: |-
        let a: long[] = new long[0]
        let b: double[] = new double[0]
        let c: char[] = new char[0]
        foo(a, b, c);

    # function param, void return
    - decl: |-
        function bar() {}
        function foo(a: () => void): void {}
      call: |-
        foo(bar);

    # function array param, void return
    - decl: |-
        type BarType = () => void
        function bar() {}
        function foo(a: BarType[]) {}
      call: |-
        let arr: BarType[] = new BarType[1]
        arr[0] = bar
        foo(arr);

    # reference type param, void return
    - decl: |-
        function foo(a: Long) {}
      call: |-
        foo(new Long())

    - decl: |-
        function foo(a: Object|null) {}
      call: |-
        foo(null)

    # reference type array param, void return
    - decl: |-
        function foo(a: Long[]) {}
      call: |-
        foo(new Long[0])

    - decl: |-
        function foo(a: Object|null[]) {}
      call: |-
        let arr: Object|null[] = new Object|null[1]
        arr[0] = null
        foo(arr)

    # enum param, void return
    - decl: |-
        enum Color { Red, Green, Blue }
        function foo(a: Color): void {}
      call: |-
        foo(Color.Red);

    # enum array param, void return
    - decl: |-
        enum Color { Red, Green, Blue }
        function foo(a: Color[]) {}
      call: |-
        let arr: Color[] =  new Color[1]
        arr[0] = Color.Red
        foo(arr);

    # T param, T return
    - decl: |-
        function foo<T>(a: T): T {
            return a;
        }
      call: |-
        let v: Long = foo<Long>(new Long())
        assert v instanceof Long;

    - decl: |-
        function foo<T, U, V>(a: T, b: U, c: V): V {
            return c;
        }
      call: |-
        let v: Object|null = foo<Long, Boolean, Object|null>(new Long(), new Boolean(true), new Object())
        assert v instanceof Object;

    # T array param, T array return
    - decl: |-
        function foo<T>(a: T[]): T[] {
            return a;
        }
      call: |-
        let arr: Boolean[] = foo<Boolean>(new Boolean[0])
        assert arr instanceof Boolean[] && arr.length == 0;

    # mixed type params
    - decl: |-
        function foo<T>(a: byte, b: Short|null, c: long, ...d: T[]): T {
            return d[0];
        }
      call: |-
        let v: Byte = foo<Byte>(1 as byte, null, Long.MIN_VALUE, new Byte(1 as byte), new Byte(2 as byte))
        assert v instanceof Byte && v.unboxed() == 1;
