/*---
desc: >-
    Variables and other entities defined in a function are local to the function and cannot be accessed outside.
    If the name of a variable defined in the function is equal to the name of an entity in the outer scope,
    then the local definition shadows outer entity.
---*/

let q: int = 1;

function bar(): int {
    let q: int = 10   // local q shadows top-level q
    return q + q;
}

function main(): int {
    q = 1;
    if (bar() == 20 && q == 1) return 0;
    return 1;
}
