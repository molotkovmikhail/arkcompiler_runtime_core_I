/*---
desc: Static field declaration.
assert: Instance is created for every instance variable declared in that class or any of its superclasses.
---*/

class A {
  public static x: int;
}

class B extends A {
  public static y: int;

  public static getX(): int {
    return A.x;
  }

  public static setX(value: int): void {
    A.x = value;
  }
}

function main(): void {
  let i1: A = new A();
  let i2: B = new B();

  B.y = 10;
  B.setX(5);

  assert A.x == 5;
  assert i1.x == 5;

  assert B.getX() == 5;
  assert B.y == 10;
  assert i2.getX() == 5;
  assert i2.y == 10;

  B.y++;
  A.x++;

  assert A.x == 6;
  assert i1.x == 6;

  assert B.getX() == 6;
  assert B.y == 11;
  assert i2.getX() == 6;
  assert i2.y == 11;
}
