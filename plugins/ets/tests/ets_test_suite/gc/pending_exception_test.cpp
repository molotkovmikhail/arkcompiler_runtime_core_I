/*
 * Copyright (c) 2023 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <gtest/gtest.h>

#include "ets_vm.h"

namespace panda::test {
class PendingEtsExceptionTest : public testing::TestWithParam<int> {
public:
    void SetUp() override
    {
        RuntimeOptions options;
        options.SetShouldLoadBootPandaFiles(true);
        options.SetCompilerEnableJit(false);
        options.SetLoadRuntimes({"ets"});
        options.SetGcType("g1-gc");
        options.SetRunGcInPlace(false);
        options.SetGcTriggerType("debug-never");

        auto stdlib = std::getenv("PANDA_STD_LIB");
        if (stdlib == nullptr) {
            std::cerr << "PANDA_STD_LIB env variable should be set and point to etsstdlib.abc" << std::endl;
            std::abort();
        }
        options.SetBootPandaFiles({stdlib});

        bool success = Runtime::Create(options);
        ASSERT_TRUE(success) << "Cannot create Runtime";
    }

    void TearDown() override
    {
        bool success = Runtime::Destroy();
        ASSERT_TRUE(success) << "Cannot destroy Runtime";
    }
};

TEST_P(PendingEtsExceptionTest, MovingGcException)
{
#ifdef PANDA_QEMU_BUILD
    // Death test under qemu is not compatible with 'threadsafe' death test style flag
    // because in this mode after process clone() test is tried to be started from the very beginning
    // via execv() with test binary and without qemu that cause 'exec format error'
    testing::FLAGS_gtest_death_test_style = "fast";
#endif
    const std::string main_func = "ETSGLOBAL::main";
    const std::string file_name = "pending_exception_gc_test.abc";
    EXPECT_EXIT(Runtime::GetCurrent()->ExecutePandaFile(file_name.c_str(), main_func.c_str(), {}),
                testing::ExitedWithCode(1), "");
}

INSTANTIATE_TEST_SUITE_P(MovingGcExceptionTests, PendingEtsExceptionTest, ::testing::Range(0, 8));
}  // namespace panda::test
