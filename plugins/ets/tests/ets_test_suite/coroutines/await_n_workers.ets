/*
 * Copyright (c) 2023 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

// TODO(konstanting, I67QXC): find a way to check that N workers are actually running in parallel

let n_workers = 10
let flags: boolean[] = new boolean[n_workers]

/* konstanting (#I67QXC):
 * currently we have to return an object from any coroutine. This will change
 * once I add autoboxing for the primitive return values
 */
function inc(n: int): Object {
    flags[n] = true
    return new Object();
}

export function main(): int {
    for (let i = 0; i < n_workers; ++i) {
        flags[i] = false
    }

    let p: Promise<Object>[] = new Promise<Object>[n_workers]
    for (let i = 0; i < n_workers; ++i) {
        p[i] = launch inc(i);
    }

    for (let i = 0; i < n_workers; ++i) {
        p[i].awaitResolution();
    }

    let result = true
    for (let i = 0; i < n_workers; ++i) {
        result = result && flags[i]
    }    
    if (!result) {
        Console.print("One of the flags was not set! Flags:\n");
        for (let i = 0; i < n_workers; ++i) {
            Console.print("flag " + i + " : " + flags[i] + "\n");
        }        
        return 1;
    } else {
        Console.print("Everything is ok!")
    }
    return 0;
}
