/**
 * Copyright (c) 2021-2023 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the 'License');
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an 'AS IS' BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

function assertEq(a, b) {
    // console.log(`assertEq: '${a}' === '${b}'`)
    if (a !== b) {
        console.log(`assertEq failed: '${a}' === '${b}'`);
        process.exit(1);
    }
}

function runTest() {
    let etsVm = require(process.env.MODULE_PATH + '/ets_interop_js_napi.node');
    const etsOpts = {
        'panda-files': process.env.ARK_ETS_INTEROP_JS_GTEST_ABC_PATH,
        'boot-panda-files': `${process.env.ARK_ETS_STDLIB_PATH}:${process.env.ARK_ETS_INTEROP_JS_GTEST_ABC_PATH}`,
        'gc-trigger-type': 'heap-trigger',
        'load-runtimes': 'ets',
        'compiler-enable-jit': 'false',
        'enable-an': 'false',
        'run-gc-in-place': 'true',
    };
    const createRes = etsVm.createRuntime(etsOpts);
    if (!createRes) {
        console.log('Cannot create ETS runtime');
        process.exit(1);
    }

    etsVm.registerEtsFunction('Lproxy_types_test/ETSGLOBAL;', 'main')();

    testFunction(etsVm);
    testPrimitives(etsVm);
    testObjects(etsVm);
    testMethods(etsVm);
}

function testFunction(etsVm) {
    const sumDouble = etsVm.registerEtsFunction('Lproxy_types_test/ETSGLOBAL;', 'SumDouble');
    assertEq(sumDouble(3, 0.22).toFixed(3), (3.22).toFixed(3));
    const sumString = etsVm.registerEtsFunction('Lproxy_types_test/ETSGLOBAL;', 'SumString');
    assertEq(sumString("Hello from ", "Panda!!"), "Hello from Panda!!");
}

function testPrimitives(etsVm) {
    // Static fields
    const Primitives = etsVm.registerEtsClass('Lproxy_types_test/Primitives;');
    assertEq(Primitives.STATIC_BOOLEAN, true);
    assertEq(Primitives.STATIC_FLOAT.toFixed(3), (1.1).toFixed(3));
    assertEq(Primitives.STATIC_DOUBLE.toFixed(3), (2.2).toFixed(3));
    assertEq(Primitives.STATIC_INT, 3);
    assertEq(Primitives.STATIC_STRING, "Panda static string!");

    Primitives.STATIC_BOOLEAN = false;
    Primitives.STATIC_FLOAT = 7.7;
    Primitives.STATIC_DOUBLE = 8.8;
    Primitives.STATIC_INT = 9;
    Primitives.STATIC_STRING = "JS static string";
    assertEq(Primitives.STATIC_BOOLEAN, false);
    assertEq(Primitives.STATIC_FLOAT.toFixed(3), (7.7).toFixed(3));
    assertEq(Primitives.STATIC_DOUBLE.toFixed(3), (8.8).toFixed(3));
    assertEq(Primitives.STATIC_INT, 9);
    assertEq(Primitives.STATIC_STRING, "JS static string");

    // Non-static fields
    const primitives = new Primitives();
    assertEq(primitives.BOOLEAN, true);
    assertEq(primitives.FLOAT.toFixed(3), (4.4).toFixed(3));
    assertEq(primitives.DOUBLE.toFixed(3), (5.5).toFixed(3));
    assertEq(primitives.INT, 6);
    assertEq(primitives.STRING, "Panda string!!");

    primitives.BOOLEAN = false;
    primitives.FLOAT = 7.7;
    primitives.DOUBLE = 8.8;
    primitives.INT = 9;
    primitives.STRING = "JS string";
    assertEq(primitives.BOOLEAN, false);
    assertEq(primitives.FLOAT.toFixed(3), (7.7).toFixed(3));
    assertEq(primitives.DOUBLE.toFixed(3), (8.8).toFixed(3));
    assertEq(primitives.INT, 9);
    assertEq(primitives.STRING, "JS string");
}

function testObjects(etsVm) {
    const External = etsVm.registerEtsClass('Lproxy_types_test/External;');
    const o = new External();
    assertEq(o.inner.val, 322);
}

function testMethods(etsVm) {
    const Methods = etsVm.registerEtsClass('Lproxy_types_test/Methods;');

    assertEq(Methods.staticSumDouble(3, 0.22).toFixed(3), (3.22).toFixed(3));
    assertEq(Methods.staticSumString("Hello from ", "Panda!!!"), "Hello from Panda!!!");
    assertEq(Methods.staticIsTrue(true), true);

    const o = new Methods();
    assertEq(o.sumDouble(3, 0.22).toFixed(3), (3.22).toFixed(3));
    assertEq(o.sumString("Hello from ", "Panda!!!!"), "Hello from Panda!!!!");
    assertEq(o.isTrue(false), false);
}

runTest();
