/**
 * Copyright (c) 2021-2023 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package proxy_types_test;

// ================ Functions ================

function SumDouble(a: double, b: double): double {
    return a + b;
}

function SumString(a: String, b: String): String {
    return a + b;
}

// ================ Primitives ================

class Primitives {
    static STATIC_BOOLEAN: boolean = true;
    static STATIC_FLOAT: float = 1.1;
    static STATIC_DOUBLE: double = 2.2;
    static STATIC_INT: int = 3;
    static STATIC_STRING: String = "Panda static string!";

    BOOLEAN: boolean = true;
    FLOAT: float = 4.4;
    DOUBLE: double = 5.5;
    INT: int = 6;
    STRING: String = "Panda string!!";
};

// ================ Methods ================

class Methods {
    isTrue(a: boolean): boolean {
        return a;
    }

    sumDouble(a: double, b: double): double {
        return a + b;
    }

    sumString(a: String, b: String): String {
        return a + b;
    }

    static staticIsTrue(a: boolean): boolean {
        return a;
    }

    static staticSumDouble(a: double, b: double): double {
        return a + b;
    }

    static staticSumString(a: String, b: String): String {
        return a + b;
    }
};

// ================ Objects ================

class Inner {
    val: int = 322;
};

class External {
    constructor() {
        this.inner = new Inner();
    }
    inner: Inner;
};

// ================ Common part ================

function main() {
    const o0 = new Primitives();
    const o1 = new Methods();
    const o2 = new External();
}
