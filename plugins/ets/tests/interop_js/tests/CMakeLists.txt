# Copyright (c) 2021-2023 Huawei Device Co., Ltd.
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

set(INTEROP_JS_TESTS_SOURCE_DIR ${PANDA_ETS_PLUGIN_SOURCE}/tests/interop_js/tests)

set(ETS_CONFIG "${PANDA_BINARY_ROOT}/tests/ets_interop_js/arktsconfig.json")
file(WRITE ${ETS_CONFIG}
    "{\n"
    "  \"compilerOptions\": {\n"
    "    \"baseUrl\": \"${PANDA_ROOT}\",\n"
    "    \"paths\": {\n"
    "      \"std\": [\"${PANDA_ROOT}/plugins/ets/stdlib/std\"]\n"
    "    },\n"
    "    \"dynamicPaths\": {\n"
    "      \"/plugins/ets/tests/interop_js/tests/test_intrins/test_intrins.js\": {\"language\": \"js\", \"hasDecl\": false},\n"
    "      \"/plugins/ets/tests/interop_js/tests/number_subtypes/module.js\": {\"language\": \"js\", \"hasDecl\": false}\n"
    "    }\n"
    "  }\n"
    "}\n"
)

panda_ets_interop_js_gtest(ets_interop_js_test__sample
    CPP_SOURCES ${INTEROP_JS_TESTS_SOURCE_DIR}/sample/test_sample.cpp
    ETS_SOURCES ${INTEROP_JS_TESTS_SOURCE_DIR}/sample/test_sample.ets
)

panda_ets_interop_js_gtest(ets_interop_js_test__map_mirror_classes
    CPP_SOURCES ${INTEROP_JS_TESTS_SOURCE_DIR}/map_mirror_classes/test_map_mirror_classes.cpp
    ETS_SOURCES ${INTEROP_JS_TESTS_SOURCE_DIR}/map_mirror_classes/test_map_mirror_classes.ets
    LIBRARIES arkruntime
)

panda_ets_interop_js_gtest(ets_interop_js_test_bouncing_pandas
    CPP_SOURCES ${INTEROP_JS_TESTS_SOURCE_DIR}/bouncing_pandas/bouncing_pandas.cpp
    ETS_SOURCES ${PANDA_ETS_PLUGIN_SOURCE}/tests/common/bouncing_pandas/bouncing_pandas.ets
)

panda_ets_interop_js_gtest(ets_interop_js_test_intrins
    CPP_SOURCES ${INTEROP_JS_TESTS_SOURCE_DIR}/test_intrins/test_intrins.cpp
    ETS_SOURCES ${INTEROP_JS_TESTS_SOURCE_DIR}/test_intrins/test_intrins.ets
)

panda_ets_interop_js_gtest(ets_interop_js_frontend_test_intrins
    CPP_SOURCES ${INTEROP_JS_TESTS_SOURCE_DIR}/test_intrins/test_intrins.cpp
    ETS_SOURCES ${INTEROP_JS_TESTS_SOURCE_DIR}/test_intrins/frontend_test_intrins.ets
    ETS_CONFIG ${ETS_CONFIG}
)

panda_ets_interop_js_gtest(ets_interop_js_test__dynamic_object
    CPP_SOURCES ${INTEROP_JS_TESTS_SOURCE_DIR}/dynamic_object/dynamic_object.cpp
    ETS_SOURCES ${INTEROP_JS_TESTS_SOURCE_DIR}/dynamic_object/dynamic_object.ets
)

add_subdirectory(number_subtypes)

panda_ets_interop_js_gtest(ets_interop_js_class_operations
    CPP_SOURCES ${INTEROP_JS_TESTS_SOURCE_DIR}/class_operations/class_operations.cpp
    ETS_SOURCES ${INTEROP_JS_TESTS_SOURCE_DIR}/class_operations/class_operations.ets
)

panda_ets_interop_js_gtest(ets_interop_js_frontend_class_operations
    CPP_SOURCES ${INTEROP_JS_TESTS_SOURCE_DIR}/class_operations/class_operations.cpp
    ETS_SOURCES ${INTEROP_JS_TESTS_SOURCE_DIR}/class_operations/class_operations_frontend.ets
    ETS_CONFIG ${ETS_CONFIG}
)

add_subdirectory(promise)
add_subdirectory(proxy_types)
