/**
 * Copyright (c) 2021-2023 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

class Test {
    private static UNSET: int = 0;
    private static SUCCESS: int = 1;

    constructor() {
        this.result = Test.UNSET;
    }

    success(): void {
        if (this.result == Test.UNSET) {
            this.result = Test.SUCCESS;
        }
    }

    fail(): void {
        Console.println("Test failed");
        throw new Error();
    }

    check(): void {
        if (this.result == Test.SUCCESS) {
            return;
        }
        if (this.result == Test.UNSET) {
            Console.println("Test result is not set");
        }
        throw new Error();
    }

    private result: int;
}
let globalTest: Test = null;

function check(): int {
    globalTest.check();
    return 0;
}

function testPendingPromise(): int {
    globalTest = new Test();
    let p = new Promise<Object>((resolve: (value: Object) => void): void => {
        globalTest.success()
    });
    globalTest.check();
    p.then<Object>((): Object => {
        globalTest.fail();
        return null;
    });
    p.catch<Object>((error: Object): Object => {
        globalTest.fail();
        return null;
    });
    // Check callback are not called inside 'then' and 'catch'
    globalTest.check();
    return 0;
}

function testResolvedPromise(): int {
    let obj = new Object();
    let p = new Promise<Object>((resolve: (value: Object) => void): void => {
        resolve(obj);
    });
    globalTest = new Test();
    p.then<Object>((value: Object): Object => {
        if (value == obj) {
            globalTest.success();
        } else {
            globalTest.fail();
        }
        return null;
    });
    p.catch<Object>((err: Object): Object => {
        globalTest.fail();
        return null;
    });
    return 0;
}

function testRejectedPromise(): int {
    globalTest = new Test();
    let error = new Exception();
    try {
        let p = new Promise<Object>((resolve: (value: Object) => void): void throws => {
            throw error;
        });
        p.then<Object>((value: Object): Object => {
            globalTest.fail();
            return null;
        });
        p.catch<Object>((err: Object): Object => {
            if (err == error) {
                globalTest.success();
            } else {
                globalTest.fail();
            }
        });
    } catch (e: Exception) {
        globalTest.fail();
    }
    return 0;
}

class ThenBeforeResolveFixture {
    public fn: (value: Object) => void;
    public state = 0;
}

function testThenBeforeResolve(): int {
    globalTest = new Test();
    let fixture = new ThenBeforeResolveFixture();
    let p = new Promise<Object>((resolve: (value: Object) => void): void => {
        fixture.fn = resolve;
    });
    let obj = new Object();
    p.then<Object>((value: Object): Object => {
        if (value == obj) {
            if (fixture.state == 0) {
                fixture.state = 1;
            } else {
                Console.println("Wrong 'then' call order");
                globalTest.fail();
            }
        } else {
            Console.println("Then is called with wrong object");
            globalTest.fail();
        }
        return null;
    });
    p.then<Object>((value: Object): Object => {
        if (value == obj) {
            if (fixture.state == 1) {
                globalTest.success();
            } else {
                Console.println("Wrong 'then' call order");
                globalTest.fail();
            }
        } else {
            Console.println("Then is called with wrong object");
            globalTest.fail();
        }
        return null;
    });
    fixture.fn(obj);
    return 0;
}

function testPromiseEmptyThen(): int {
    globalTest = new Test();
    let p = new Promise<Object>((resolve: (value: Object) => void): void => {
        resolve(null);
    });
    let nextP = p.then<Object>((): Object => {
        globalTest.success();
    });
    return 0;
}

class PromiseChainFixture {
    public state = 0;
}

function testPromiseChain(): int {
    globalTest = new Test();
    let fixture = new PromiseChainFixture();
    let p = new Promise<Object>((resolve: (value: Object) => void): void => {
        resolve(null);
    });
    // TODO(audovichenko): Remove p1 and p2 variables
    let p1: Promise<Object> = p.then<Object>((): Object => {
        if (fixture.state == 0) {
            fixture.state = 1;
        } else {
            globalTest.fail();
        }
        return null;
    });
    let p2: Promise<Object> = p1.then<Object>((): Object => {
        if (fixture.state == 1) {
            fixture.state = 2;
        } else {
            globalTest.fail();
        }
        return null;
    });
    p2.then<Object>((): Object => {
        if (fixture.state == 2) {
            globalTest.success();
        } else {
            globalTest.fail();
        }
        return null;
    });
    return 0;
}

function testReturnPromise(): Promise<String> {
    return new Promise<String>((resolve: (value: Object) => void): void => {
        resolve("Panda");
    });
}

async function testReturnPromiseFromAsync(): Promise<String> {
    return new Promise<String>((resolve: (value: Object) => void): void => {
        resolve("Panda");
    });
}

let resolvePromiseFn: (value: Object) => void = null;

function testReturnPendingPromise(): Promise<String> {
    return new Promise<String>((resolve: (value: Object) => void): void => {
        resolvePromiseFn = resolve;
    });
}

function resolvePendingPromise(): boolean {
    if (resolvePromiseFn == null) {
        return false;
    }
    resolvePromiseFn("Panda");
    return true;
}

let unresolved1: Object = null;
let unresolved2: Object = null;

async function asyncFuncAwait(): Promise<Object> {
    let promise: Promise<Object> = new Promise<Object>((resolve: (obj: Object) => void): void => {
        resolve("resolved");
    });
    unresolved1 = await promise;
    return null;
}

let asyncLambdaAwait: () => Promise<Object> = async (): Promise<Object> => {
    let promise: Promise<Object> = new Promise<Object>((resolve: (obj: Object) => void): void => {
        resolve("resolved");
    });
    unresolved2 = await promise;
    return null;
}

function testAwaitPromise(): int {
    globalTest = new Test();
    let p1: Promise<Object> = asyncFuncAwait();
    let p2: Promise<Object> = asyncLambdaAwait();
    if (unresolved1 != "resolved" || unresolved2 != "resolved") {
        globalTest.fail();
    }
    globalTest.success();
    return 0;
}
