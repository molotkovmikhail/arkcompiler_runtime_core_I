/**
 * Copyright (c) 2021-2023 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

function runTest() {
    console.log("Running test 'returnPendingPromise'");
    let etsVm = require(process.env.MODULE_PATH + "/ets_interop_js_napi.node");
    if (!etsVm.createEtsRuntime(process.env.ARK_ETS_STDLIB_PATH, process.env.ARK_ETS_INTEROP_JS_GTEST_ABC_PATH, false, false)) {
		process.exit(1);
	}
    let res = etsVm.call("testReturnPendingPromise");
    if (typeof(res) !== "object") {
        console.log("Result is not an object");
        process.exit(1);
    }
    if (res.constructor.name !== "Promise") {
        console.log("Expect result type 'Promise' but get '" + res.constructor.name + "'");
        process.exit(1);
    }
    let testSuccess = false;
    res.then((value) => {
        if (value == "Panda") {
            testSuccess = true;
        }
    });
    queueMicrotask(() => {
        if (testSuccess) {
            console.log("Promise must not be resolved");
            process.exit(1);
        }
        if (!etsVm.call("resolvePendingPromise")) {
            console.log("Call of 'resolvePendingPromise' return false");
        } else {
            queueMicrotask(() => {
                if (!testSuccess) {
                    console.log("Promise is not resolved or value is wrong");
                    process.exit(1);
                }
            });
        }
    });
}

runTest();
