/**
 * Copyright (c) 2021-2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


function main() {
    // Init 'gtestRootPath'
    let gtestRootPath = (() => {
        let path = process.env.ETS_GTEST_ROOT_PATH;
        if (path == undefined) {
            return __dirname + "/../..";
        }
        return path;
    })();

    // Add 'gtest' object to global space.
    // This object is used by gtests as storage to save and restore variables
    gtest = {}

    // load ets_interop_js_napi to globalThis.gtest.etsVm
    gtest.etsVm = require(gtestRootPath + "/lib/module/ets_interop_js_napi");
    let penv = process.env;

    const etsVmRes = gtest.etsVm.createRuntime({
        "log-level": "info",
        "log-components": "ets_interop_js",
        "boot-panda-files": penv.ARK_ETS_STDLIB_PATH + ":" + penv.ARK_ETS_INTEROP_JS_GTEST_ABC_PATH,
        "panda-files": penv.ARK_ETS_INTEROP_JS_GTEST_ABC_PATH,
        "gc-trigger-type": "heap-trigger",
        "compiler-enable-jit": "false",
        "enable-an": "false",
        "run-gc-in-place": "true",
    });

    if (!etsVmRes) {
        console.error(`Failed to create ETS runtime`);
        return 1;
    }

    // 'gtest.require' is used by gtests to load the node modules
    gtest.require = require;
    globalThis.require = require;

    let gtestName = process.argv[2];
    if (gtestName == undefined) {
        console.error(`Usage: ${process.argv[0]} ${process.argv[1]} <test name>`);
        return 1;
    }

    // Run gtest
    console.log(`Run ets_interop_js_gtest module: ${gtestName}`)
    var ets_gtest = require(`./lib/module/${gtestName}`);
    let args = process.argv.slice(2);
    let ret = ets_gtest.main(args);
    return ret;
}

process.exit(main())
