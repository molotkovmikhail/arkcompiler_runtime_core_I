/**
 * Copyright (c) 2021-2023 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef PANDA_PLUGINS_ETS_INTEROP_JS_GTEST_H_
#define PANDA_PLUGINS_ETS_INTEROP_JS_GTEST_H_

#include <fstream>
#include <sstream>
#include <gtest/gtest.h>
#include <node_api.h>

namespace panda::ets::interop::js::testing {

class EtsInteropTest : public ::testing::Test {
public:
    static void SetUpTestSuite()
    {
        if (std::getenv("ARK_ETS_INTEROP_JS_GTEST_SOURCES") == nullptr) {
            std::cerr << "ARK_ETS_INTEROP_JS_GTEST_SOURCES not set" << std::endl;
            std::abort();
        }
    }

    void SetUp() override
    {
        interop_js_test_path_ = std::getenv("ARK_ETS_INTEROP_JS_GTEST_SOURCES");
        // This object is used to save global js names
        RunJsScript("var gtest_env = {};\n");
    }

    static void RunJsScript(const std::string &script)
    {
        DoRunJsScript(js_env_, script);
    }

    template <typename R>
    R RunJsScriptByPath(const std::string &path)
    {
        return DoRunJsScriptByPath<R>(js_env_, path);
    }

    template <typename R, typename... Args>
    static R CallEtsMethod(std::string_view fn_name, Args &&...args)
    {
        return DoCallEtsMethod<R>(fn_name, js_env_, std::forward<Args>(args)...);
    }

    static napi_env GetJsEnv()
    {
        return js_env_;
    }

    void LoadModuleAs(const std::string &module_name, const std::string &module_path)
    {
        RunJsScript("gtest_env." + module_name + " = gtest.require(\"" + interop_js_test_path_ + module_path +
                    "\");\n");
    }

private:
    static void DoRunJsScript(napi_env env, const std::string &script)
    {
        [[maybe_unused]] napi_status status;
        napi_value js_script;
        status = napi_create_string_utf8(env, script.c_str(), script.length(), &js_script);
        assert(status == napi_ok);

        napi_value js_result;
        status = napi_run_script(env, js_script, &js_result);
        if (status == napi_generic_failure) {
            napi_value fatal_exception;
            status = napi_get_and_clear_last_exception(env, &fatal_exception);
            assert(status == napi_ok);

            status = napi_fatal_exception(env, fatal_exception);
            assert(status == napi_ok);

            // Unreachable code
            std::abort();
        }
        ASSERT_EQ(status, napi_ok);
    }

    template <typename T>
    T DoRunJsScriptByPath(napi_env env, const std::string &path)
    {
        std::ifstream js_source_file(interop_js_test_path_ + path);
        assert(js_source_file.is_open());
        std::ostringstream string_stream;
        string_stream << js_source_file.rdbuf();

        DoRunJsScript(env, string_stream.str());

        // Get globalThis.gtest.ret
        napi_value js_ret_value {};
        [[maybe_unused]] napi_status status = napi_get_named_property(env, GetJsGtestObject(env), "ret", &js_ret_value);
        assert(status == napi_ok);
        return GetRetValue<T>(env, js_ret_value);
    }

    template <typename T>
    static T GetRetValue([[maybe_unused]] napi_env env, [[maybe_unused]] napi_value js_value)
    {
        if constexpr (std::is_same_v<T, double>) {
            double v;
            [[maybe_unused]] napi_status status = napi_get_value_double(env, js_value, &v);
            assert(status == napi_ok);
            return v;
        } else if constexpr (std::is_same_v<T, int32_t>) {
            int32_t v;
            [[maybe_unused]] napi_status status = napi_get_value_int32(env, js_value, &v);
            assert(status == napi_ok);
            return v;
        } else if constexpr (std::is_same_v<T, uint32_t>) {
            uint32_t v;
            [[maybe_unused]] napi_status status = napi_get_value_uint32(env, js_value, &v);
            assert(status == napi_ok);
            return v;
        } else if constexpr (std::is_same_v<T, int64_t>) {
            int64_t v;
            [[maybe_unused]] napi_status status = napi_get_value_int64(env, js_value, &v);
            assert(status == napi_ok);
            return v;
        } else if constexpr (std::is_same_v<T, bool>) {
            bool v;
            [[maybe_unused]] napi_status status = napi_get_value_bool(env, js_value, &v);
            assert(status == napi_ok);
            return v;
        } else if constexpr (std::is_same_v<T, std::string>) {
            [[maybe_unused]] napi_status status;
            size_t length;
            status = napi_get_value_string_utf8(env, js_value, nullptr, 0, &length);
            assert(status == napi_ok);
            std::string v(length, '\0');
            size_t copied;
            status = napi_get_value_string_utf8(env, js_value, v.data(), length + 1, &copied);
            assert(status == napi_ok);
            assert(length == copied);
            return v;
        } else if constexpr (std::is_same_v<T, napi_value>) {
            return js_value;
        } else if constexpr (std::is_same_v<T, void>) {
            // do nothing
        } else {
            enum { INCORRECT_TEMPLATE_TYPE = false };
            static_assert(INCORRECT_TEMPLATE_TYPE, "Incorrect template type");
        }
    }

    static napi_value MakeJsArg(napi_env env, double arg)
    {
        napi_value v;
        [[maybe_unused]] napi_status status = napi_create_double(env, arg, &v);
        assert(status == napi_ok);
        return v;
    }

    static napi_value MakeJsArg(napi_env env, int32_t arg)
    {
        napi_value v;
        [[maybe_unused]] napi_status status = napi_create_int32(env, arg, &v);
        assert(status == napi_ok);
        return v;
    }

    static napi_value MakeJsArg(napi_env env, uint32_t arg)
    {
        napi_value v;
        [[maybe_unused]] napi_status status = napi_create_uint32(env, arg, &v);
        assert(status == napi_ok);
        return v;
    }

    static napi_value MakeJsArg(napi_env env, int64_t arg)
    {
        napi_value v;
        [[maybe_unused]] napi_status status = napi_create_int64(env, arg, &v);
        assert(status == napi_ok);
        return v;
    }

    static napi_value MakeJsArg(napi_env env, std::string_view arg)
    {
        napi_value v;
        [[maybe_unused]] napi_status status = napi_create_string_utf8(env, arg.data(), arg.length(), &v);
        assert(status == napi_ok);
        return v;
    }

    static napi_value MakeJsArg([[maybe_unused]] napi_env env, napi_value arg)
    {
        return arg;
    }

    static napi_value GetJsGtestObject(napi_env env)
    {
        [[maybe_unused]] napi_status status;

        // Get globalThis
        napi_value js_global_object;
        status = napi_get_global(env, &js_global_object);
        assert(status == napi_ok);

        // Get globalThis.gtest
        napi_value js_gtest_object;
        status = napi_get_named_property(env, js_global_object, "gtest", &js_gtest_object);
        assert(status == napi_ok);

        return js_gtest_object;
    }

    template <typename R, typename... Args>
    static R DoCallEtsMethod(std::string_view fn_name, napi_env env, Args &&...args)
    {
        [[maybe_unused]] napi_status status;

        // Get globalThis.gtest
        napi_value js_gtest_object = GetJsGtestObject(env);

        // Set globalThis.gtest.functionName
        napi_value js_function_name;
        status = napi_create_string_utf8(env, fn_name.data(), fn_name.length(), &js_function_name);
        assert(status == napi_ok);
        status = napi_set_named_property(env, js_gtest_object, "functionName", js_function_name);
        assert(status == napi_ok);

        // Set globalThis.gtest.args
        std::initializer_list<napi_value> napi_args = {MakeJsArg(env, args)...};
        napi_value js_args;
        status = napi_create_array_with_length(env, napi_args.size(), &js_args);
        assert(status == napi_ok);
        uint32_t i = 0;
        for (auto arg : napi_args) {
            status = napi_set_element(env, js_args, i++, arg);
            assert(status == napi_ok);
        }
        status = napi_set_named_property(env, js_gtest_object, "args", js_args);
        assert(status == napi_ok);

        // Call ETS method via JS
        RunJsScript(R"(
            gtest.ret = gtest.etsVm.call(gtest.functionName, ...gtest.args);
        )");

        // Get globalThis.gtest.ret
        napi_value js_ret_value {};
        status = napi_get_named_property(env, js_gtest_object, "ret", &js_ret_value);
        assert(status == napi_ok);
        return GetRetValue<R>(env, js_ret_value);
    }

    friend class JsEnvAccessor;
    static napi_env js_env_;

protected:
    // NOLINTNEXTLINE(misc-non-private-member-variables-in-classes)
    std::string interop_js_test_path_;
};

}  // namespace panda::ets::interop::js::testing

#endif  // !PANDA_PLUGINS_ETS_INTEROP_JS_GTEST_H_
