
TYPES=( 
    "Int8 byte 1"
    "Int16 short 2"
    "Int32 int 4"
    "BigInt64 long 8"
)

TEMPLATES=(
    typedArray.ets.j2
)

GENPATH=../../stdlib/std/core

mkdir $GENPATH 2>/dev/null

filename=$GENPATH/TypedArrays.ets
src_abs_path=$(readlink -f $0)
jinja2 -DS=${0##*/} licence.j2 > $filename
echo "" >> $filename

for atype in "${TYPES[@]}"
do
    echo Generating $atype...
    read -a strarr <<< "$atype"

    for template in "${TEMPLATES[@]}"
    do
        jinja2 -DN=${strarr[0]} -DT=${strarr[1]} -DS=${strarr[2]} $template >> $filename
        echo "" >> $filename
    done
done

echo Done.
