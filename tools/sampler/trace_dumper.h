/*
 * Copyright (c) 2021-2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef PANDA_TOOLS_SAMPLER_TRACE_DUMPER_H
#define PANDA_TOOLS_SAMPLER_TRACE_DUMPER_H

#include <unordered_map>
#include <fstream>

#include "libpandabase/macros.h"
#include "libpandabase/utils/logger.h"
#include "libpandafile/file-inl.h"
#include "libpandafile/class_data_accessor-inl.h"

#include "runtime/tooling/sampler/sample_info.h"

namespace panda::tooling::sampler {

using ModuleMap = std::unordered_map<uintptr_t, std::unique_ptr<const panda_file::File>>;
using MethodMap = std::unordered_map<const panda_file::File *, std::unordered_map<uint32_t, std::string>>;

enum class DumpType {
    WITHOUT_THREAD_SEPARATION,  // Creates single csv file without separating threads
    THREAD_SEPARATION_BY_TID,   // Creates single csv file with separating threads by thread_id
    THREAD_SEPARATION_BY_CSV    // Creates csv file for each thread
};

class TraceDumper {
public:
    explicit TraceDumper(ModuleMap *modules_map, MethodMap *methods_map)
        : modules_map_(modules_map), methods_map_(methods_map)
    {
    }
    virtual ~TraceDumper() = default;

    virtual std::ofstream &ResolveStream(const SampleInfo &sample) = 0;

    void DumpTraces(const SampleInfo &sample, size_t count)
    {
        std::ofstream &stream = ResolveStream(sample);

        for (size_t i = sample.stack_info.managed_stack_size; i-- > 0;) {
            uintptr_t pf_id = sample.stack_info.managed_stack[i].panda_file_ptr;
            uint64_t file_id = sample.stack_info.managed_stack[i].file_id;

            std::string full_method_name;
            if (pf_id == helpers::ToUnderlying(FrameKind::BRIDGE)) {
                full_method_name = "System_Frame";
            } else {
                const panda_file::File *pf = nullptr;
                auto it = modules_map_->find(pf_id);
                if (it != modules_map_->end()) {
                    pf = it->second.get();
                }

                full_method_name = ResolveName(pf, file_id);
            }
            stream << full_method_name << "; ";
        }
        stream << count << "\n";
    }

    std::string ResolveName(const panda_file::File *pf, uint64_t file_id) const
    {
        if (pf == nullptr) {
            return std::string("__unknown_module::" + std::to_string(file_id));
        }

        auto it = methods_map_->find(pf);
        if (it != methods_map_->end()) {
            return it->second.at(file_id);
        }
        return pf->GetFilename() + "::__unknown_" + std::to_string(file_id);
    }

    NO_COPY_SEMANTIC(TraceDumper);
    NO_MOVE_SEMANTIC(TraceDumper);

private:
    ModuleMap *modules_map_ {nullptr};
    MethodMap *methods_map_ {nullptr};
};

class SingleCSVDumper final : public TraceDumper {
public:
    explicit SingleCSVDumper(const char *filename, DumpType option, ModuleMap *modules_map, MethodMap *methods_map,
                             bool build_cold_graph)
        : TraceDumper(modules_map, methods_map),
          stream_(std::ofstream(filename)),
          option_(option),
          build_cold_graph_(build_cold_graph)
    {
    }
    ~SingleCSVDumper() override = default;

    std::ofstream &ResolveStream(const SampleInfo &sample) override
    {
        if (option_ == DumpType::THREAD_SEPARATION_BY_TID) {
            stream_ << "thread_id = " << sample.thread_info.thread_id << "; ";
        }
        if (build_cold_graph_) {
            stream_ << "status = " << static_cast<uint32_t>(sample.thread_info.thread_status) << "; ";
        }
        return stream_;
    }

    NO_COPY_SEMANTIC(SingleCSVDumper);
    NO_MOVE_SEMANTIC(SingleCSVDumper);

private:
    std::ofstream stream_;
    DumpType option_;
    bool build_cold_graph_;
};

class MultipleCSVDumper final : public TraceDumper {
public:
    explicit MultipleCSVDumper(const char *filename, ModuleMap *modules_map, MethodMap *methods_map,
                               bool build_cold_graph)
        : TraceDumper(modules_map, methods_map), filename_(filename), build_cold_graph_(build_cold_graph)
    {
    }
    ~MultipleCSVDumper() override = default;

    static std::string AddThreadIdToFilename(const std::string &filename, const uint32_t thread_id)
    {
        std::string filename_with_thread_id(filename);
        std::size_t pos = filename_with_thread_id.find("csv");
        if (pos == std::string::npos) {
            LOG(FATAL, PROFILER) << "Incorrect output filename, *.csv format expected";
        }
        filename_with_thread_id.insert(pos - 1, std::to_string(thread_id));
        return filename_with_thread_id;
    }

    std::ofstream &ResolveStream(const SampleInfo &sample) override
    {
        auto it = thread_id_map_.find(sample.thread_info.thread_id);
        if (it == thread_id_map_.end()) {
            std::string filename_with_thread_id = AddThreadIdToFilename(filename_, sample.thread_info.thread_id);

            auto return_pair =
                thread_id_map_.insert({sample.thread_info.thread_id, std::ofstream(filename_with_thread_id)});
            it = return_pair.first;

            auto is_success_insert = return_pair.second;
            if (!is_success_insert) {
                LOG(FATAL, PROFILER) << "Failed while insert in unordored_map";
            }
        }
        it->second << "thread_id = " << sample.thread_info.thread_id << "; ";
        if (build_cold_graph_) {
            it->second << "status = ";
            switch (sample.thread_info.thread_status) {
                case SampleInfo::ThreadStatus::RUNNING: {
                    it->second << "active; ";
                    break;
                }
                case SampleInfo::ThreadStatus::SUSPENDED: {
                    it->second << "suspended; ";
                    break;
                }
                default: {
                    UNREACHABLE();
                }
            }
        }

        return it->second;
    }

    NO_COPY_SEMANTIC(MultipleCSVDumper);
    NO_MOVE_SEMANTIC(MultipleCSVDumper);

private:
    std::string filename_;
    std::unordered_map<size_t, std::ofstream> thread_id_map_;
    bool build_cold_graph_;
};

}  // namespace panda::tooling::sampler

#endif  // PANDA_TOOLS_SAMPLER_TRACE_DUMPER_H
