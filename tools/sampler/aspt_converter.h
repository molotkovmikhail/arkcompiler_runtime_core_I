/*
 * Copyright (c) 2021-2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef PANDA_TOOLS_SAMPLER_CONVERTER_IMPL_H
#define PANDA_TOOLS_SAMPLER_CONVERTER_IMPL_H

#include "libpandabase/os/filesystem.h"
#include "libpandabase/utils/utf.h"

#include "runtime/tooling/sampler/sample_info.h"
#include "runtime/tooling/sampler/sample_reader-inl.h"
#include "tools/sampler/trace_dumper.h"

#include <optional>

namespace panda::tooling::sampler {

struct SubstitudeModules {
    std::vector<std::string> source;
    std::vector<std::string> destination;
};

class AsptConverter {
public:
    using StackTraceMap = std::unordered_map<SampleInfo, size_t>;

    explicit AsptConverter(const char *filename, DumpType dump_type, bool build_cold_graph,
                           std::optional<SubstitudeModules> substitude_directories)
        : dump_type_(dump_type),
          reader_(filename),
          build_cold_graph_(build_cold_graph),
          substitude_directories_(std::move(substitude_directories))
    {
    }
    ~AsptConverter() = default;

    size_t CollectTracesStats()
    {
        stack_traces_.clear();

        size_t sample_counter = 0;
        SampleInfo sample;
        while (reader_.GetNextSample(&sample)) {
            ++sample_counter;

            if (dump_type_ == DumpType::WITHOUT_THREAD_SEPARATION) {
                // NOTE: zeroing thread_id to make samples indistinguishable in mode without thread separation
                sample.thread_info.thread_id = 0;
            }

            if (!build_cold_graph_) {
                // NOTE: zeroing thread_status to make samples indistinguishable
                // in mode without building cold flamegraph
                sample.thread_info.thread_status = SampleInfo::ThreadStatus::UNDECLARED;
            }

            auto it = stack_traces_.find(sample);
            if (it == stack_traces_.end()) {
                stack_traces_.insert({sample, 1});
                continue;
            }
            ++it->second;
        }
        return sample_counter;
    }

    bool CollectModules()
    {
        FileInfo m_info;
        while (reader_.GetNextModule(&m_info)) {
            std::string filepath_str = m_info.pathname;
            if (substitude_directories_.has_value()) {
                for (size_t i = 0; i < substitude_directories_->source.size(); ++i) {
                    auto pos = filepath_str.find(substitude_directories_->source[i]);
                    if (pos != std::string::npos) {
                        filepath_str.replace(pos, substitude_directories_->source[i].size(),
                                             substitude_directories_->destination[i]);
                        break;
                    }
                }
            }
            const char *filepath = filepath_str.c_str();

            if (!panda::os::IsFileExists(filepath)) {
                LOG(ERROR, PROFILER) << "Module not found, path: " << filepath;
            }
            if (modules_map_.find(m_info.ptr) == modules_map_.end()) {
                modules_map_.insert({m_info.ptr, panda_file::OpenPandaFileOrZip(filepath)});
            }
        }

        return !modules_map_.empty();
    }

    bool DumpResolvedTracesAsCSV(const char *filename)
    {
        std::unique_ptr<TraceDumper> dumper;

        switch (dump_type_) {
            case DumpType::WITHOUT_THREAD_SEPARATION:
            case DumpType::THREAD_SEPARATION_BY_TID:
                dumper = std::make_unique<SingleCSVDumper>(filename, dump_type_, &modules_map_, &methods_map_,
                                                           build_cold_graph_);
                break;
            case DumpType::THREAD_SEPARATION_BY_CSV:
                dumper = std::make_unique<MultipleCSVDumper>(filename, &modules_map_, &methods_map_, build_cold_graph_);
                break;
            default:
                UNREACHABLE();
        }
        for (auto &[sample, count] : stack_traces_) {
            ASSERT(sample.stack_info.managed_stack_size <= SampleInfo::StackInfo::MAX_STACK_DEPTH);
            dumper->DumpTraces(sample, count);
        }
        return true;
    }

    void BuildMethodsMap()
    {
        for (const auto &pf_pair : modules_map_) {
            const panda_file::File *pf = pf_pair.second.get();
            if (pf == nullptr) {
                continue;
            }
            auto classes_span = pf->GetClasses();
            for (auto id : classes_span) {
                if (pf->IsExternal(panda_file::File::EntityId(id))) {
                    continue;
                }
                panda_file::ClassDataAccessor cda(*pf, panda_file::File::EntityId(id));
                cda.EnumerateMethods([&](panda_file::MethodDataAccessor &mda) {
                    std::string method_name = utf::Mutf8AsCString(mda.GetName().data);
                    std::string class_name = utf::Mutf8AsCString(cda.GetDescriptor());
                    if (class_name[class_name.length() - 1] == ';') {
                        class_name.pop_back();
                    }
                    std::string full_name = class_name + "::";
                    full_name += method_name;
                    methods_map_[pf][mda.GetMethodId().GetOffset()] = std::move(full_name);
                });
            }
        }
    }

    NO_COPY_SEMANTIC(AsptConverter);
    NO_MOVE_SEMANTIC(AsptConverter);

private:
    DumpType dump_type_;
    SampleReader reader_;
    ModuleMap modules_map_;
    MethodMap methods_map_;
    StackTraceMap stack_traces_;
    bool build_cold_graph_;
    std::optional<SubstitudeModules> substitude_directories_;
};

}  // namespace panda::tooling::sampler

#endif  // PANDA_TOOLS_SAMPLER_CONVERTER_IMPL_H
