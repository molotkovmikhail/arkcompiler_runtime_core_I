/*
 * Copyright (c) 2021-2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "libpandabase/utils/logger.h"
#include "tools/sampler/aspt_converter.h"
#include "tools/sampler/args_parser.h"

namespace panda::tooling::sampler {

int Main(int argc, const char **argv)
{
    panda::Span<const char *> sp(argv, argc);

    ArgsParser parser;
    if (!parser.Parse(sp)) {
        parser.Help();
        return 1;
    }
    const Options &cli_options = parser.GetOptions();

    Logger::InitializeStdLogging(Logger::Level::INFO, Logger::ComponentMaskFromString("profiler"));

    std::string input_filename = cli_options.GetInput();
    std::string output_filename = cli_options.GetOutput();
    bool build_cold_graph = cli_options.IsColdGraphEnable();

    std::optional<SubstitudeModules> substitude_directories;
    if (cli_options.IsSubstitudeModuleDir()) {
        substitude_directories = {cli_options.GetSubstitudeSourceStr(), cli_options.GetSubstitudeDestinationStr()};
        if (substitude_directories->source.size() != substitude_directories->destination.size()) {
            LOG(FATAL, PROFILER) << "different number of strings in substitude option";
        }
    }

    DumpType dump_type = parser.GetDumpType();

    AsptConverter conv(input_filename.c_str(), dump_type, build_cold_graph, std::move(substitude_directories));
    if (conv.CollectTracesStats() == 0) {
        LOG(ERROR, PROFILER) << "No samples found in file";
        return 1;
    }
    if (!conv.CollectModules()) {
        LOG(ERROR, PROFILER) << "No modules found in file, names would not be resolved";
    }
    conv.BuildMethodsMap();
    conv.DumpResolvedTracesAsCSV(output_filename.c_str());
    return 0;
}

}  // namespace panda::tooling::sampler

int main(int argc, const char **argv)
{
    return panda::tooling::sampler::Main(argc, argv);
}
