# Sampler usage

## Generate flamegraph

Download `flamegraph`:

```bash
cd ${BUILD_DIR}
git clone https://github.com/brendangregg/FlameGraph.git
```

```bash
# get abc bin
${BUILD_DIR}/bin/es2panda ${BUILD_SOURCE}/plugins/ets/tests/runtime/tooling.sampler/SamplerTest.ets ${BUILD_DIR}/sampling_app.abc

# get sample dump
${BUILD_DIR}/bin/ark --load-runtimes=ets --boot-panda-files=${BUILD_DIR}/plugins/ets/etsstdlib.abc --sampling-profiler-enable --sampling-profiler-interval=200 --sampling-profiler-output-file=${BUILD_DIR}/outfile.aspt ${BUILD_DIR}/sampling_app.abc ETSGLOBAL::main

# convert sample dump to csv
${BUILD_DIR}/bin/aspt_converter --input=${BUILD_DIR}/outfile.aspt --output=${BUILD_DIR}/traceout.csv

# generate flamegrath svg
${BUILD_DIR}/FlameGraph/flamegraph.pl ${BUILD_DIR}/traceout.csv > ${BUILD_DIR}/out.svg
```

In current implementation of aspt_converter two vizual modes are supported:
```bash
# convert sample dump to single csv with separating samples by threads
${BUILD_DIR}/bin/aspt_converter --input=${BUILD_DIR}/outfile.aspt --output=${BUILD_DIR}/traceout.csv --thread-separation-by-tid

# convert sample dump many csv files with one thread per file. Name of ouput file will concatinate with thread_id
${BUILD_DIR}/bin/aspt_converter --input=${BUILD_DIR}/outfile.aspt --output=${BUILD_DIR}/traceout.csv --thread-separation-by-csv
```

## Limitations
    1. Samples can be lost but not necessarily in case if signal was sent to interpreter thread while it was in the bridge and frame was not constructed yet. (Relevant to profiling with JIT or ArkTS NAPI)
        1.1 You can track lost samples by adding options to runtime `--log-level=debug --log-components=profiler`

    2. Tests are not running with TSAN enabled because TSAN can affect user signals and rarely it causes incorrect sampler work.
