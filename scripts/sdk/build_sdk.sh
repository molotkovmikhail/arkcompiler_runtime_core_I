#!/usr/bin/env bash

set -eo pipefail

SCRIPT_DIR="$(realpath "$(dirname "${BASH_SOURCE[0]}")")"
ARK_ROOT="$SCRIPT_DIR/../.."

OHOS_SDK_NATIVE="$(realpath $1)"
SDK_BUILD_ROOT="$(realpath $2)"

usage() {
    echo "$(basename "${BASH_SOURCE[0]}") /path/to/ohos/sdk/native /path/to/panda/sdk/destination"
    exit 1
}

if [ ! -d "$OHOS_SDK_NATIVE" ]; then
    echo "Error: No such directory: $OHOS_SDK_NATIVE"
    usage
fi

if [ -z "$SDK_BUILD_ROOT" ]; then
    echo "Error: path to panda sdk destination is not provided"
    usage
fi

PANDA_SDK_PATH="$SDK_BUILD_ROOT/sdk"

# Arguments: BUILD_DIR, CMAKE_ARGUMENTS, NINJA_TARGETS
build_panda() {
    local BUILD_DIR="$1"
    local CMAKE_ARGUMENTS="$2"
    local NINJA_TARGETS="$3"

    CONCURRENCY=${NPROC_PER_JOB:=`nproc`}
    BUILD_TYPE="Release"
    COMMONS_CMAKE_ARGS="\
        -GNinja \
        -S$ARK_ROOT \
        -DCMAKE_BUILD_TYPE=$BUILD_TYPE \
        -DPANDA_ENABLE_CCACHE=OFF \
        -DPANDA_PRODUCT_BUILD=ON \
        -DPANDA_WITH_JAVA=OFF \
        -DPANDA_WITH_ACCORD=OFF \
        -DPANDA_WITH_CANGJIE=OFF"

    cmake -B"$BUILD_DIR" ${COMMONS_CMAKE_ARGS[@]} $CMAKE_ARGUMENTS
    ninja -C"$BUILD_DIR" -j${CONCURRENCY} $NINJA_TARGETS
}

# Arguments: SRC, DST, FILE_LIST, INCLUDE_PATTERN
copy_into_sdk() {
    local SRC="$1"
    local DST="$2"
    local FILE_LIST="$3"
    local INCLUDE_PATTERN="$4"
    local EXCLUDE_PATTERN='\(/tests/\|/test/\)'

    # Below construction (cd + find + cp --parents) is used to copy
    # all files listed in FILE_LIST with their relative paths
    # Example: cd /path/to/panda && cp --parents runtime/include/cframe.h /dst
    # Result: /dst/runtime/include/cframe.h
    mkdir -p "$DST"
    cd "$SRC"
    for FILE in $(cat "$FILE_LIST"); do
        for F in $(find "$FILE" -type f | grep "$INCLUDE_PATTERN" | grep -v "$EXCLUDE_PATTERN"); do
        cp --parents "$F" "$DST"
        done
    done
}

linux_tools() {
    echo "> Building linux tools..."
    local LINUX_BUILD_DIR="$SDK_BUILD_ROOT/linux_host_tools"
    local LINUX_CMAKE_ARGS=" \
        -DPANDA_CROSS_AARCH64_TOOLCHAIN_FILE=cmake/toolchain/cross-ohos-musl-aarch64.cmake \
        -DTOOLCHAIN_SYSROOT=$OHOS_SDK_NATIVE/sysroot \
        -DTOOLCHAIN_CLANG_ROOT=$OHOS_SDK_NATIVE/llvm \
        -DPANDA_WITH_ECMASCRIPT=ON"
    local LINUX_BUILD_TARGETS="ark ark_aot ark_link es2panda"
    build_panda "$LINUX_BUILD_DIR" "$LINUX_CMAKE_ARGS" "$LINUX_BUILD_TARGETS"
    copy_into_sdk "$LINUX_BUILD_DIR" "$PANDA_SDK_PATH/linux_host_tools" "$SCRIPT_DIR"/linux_host_tools.txt
}

linux_ohos_tools() {
    echo "> Building linux ohos tools..."
    local LINUX_OHOS_BUILD_DIR="$SDK_BUILD_ROOT/linux_host_ohos_tools"
    local LINUX_OHOS_CMAKE_ARGS=" \
        -DPANDA_CROSS_AARCH64_TOOLCHAIN_FILE=cmake/toolchain/cross-ohos-musl-aarch64.cmake \
        -DTOOLCHAIN_SYSROOT=$OHOS_SDK_NATIVE/sysroot \
        -DTOOLCHAIN_CLANG_ROOT=$OHOS_SDK_NATIVE/llvm \
        -DPANDA_WITH_ECMASCRIPT=ON"
    local LINUX_BUILD_TARGETS="ark_aot"
    build_panda "$LINUX_OHOS_BUILD_DIR" "$LINUX_OHOS_CMAKE_ARGS" "$LINUX_BUILD_TARGETS"
    copy_into_sdk "$LINUX_OHOS_BUILD_DIR" "$PANDA_SDK_PATH/linux_host_ohos_tools" "$SCRIPT_DIR"/linux_host_ohos_tools.txt
}

windows_tools() {
    echo "> Building windows tools..."
    local WINDOWS_BUILD_DIR="$SDK_BUILD_ROOT/windows_host_tools"
    local WINDOWS_CMAKE_ARGS="-DCMAKE_TOOLCHAIN_FILE=cmake/toolchain/cross-clang-14-x86_64-w64-mingw32-static.cmake"
    local WINDOWS_BUILD_TARGETS="es2panda ark_link"
    build_panda "$WINDOWS_BUILD_DIR" "$WINDOWS_CMAKE_ARGS" "$WINDOWS_BUILD_TARGETS"
    copy_into_sdk "$WINDOWS_BUILD_DIR" "$PANDA_SDK_PATH/windows_host_tools" "$SCRIPT_DIR"/windows_host_tools.txt
}

ohos() {
    echo "> Building runtime for OHOS ARM64..."
    local OHOS_BUILD_DIR="$SDK_BUILD_ROOT/ohos_arm64"
    local TARGET_SDK_DIR="$PANDA_SDK_PATH/ohos_arm64"
    local TARGET_CMAKE_ARGS=" \
        -DCMAKE_TOOLCHAIN_FILE=cmake/toolchain/cross-ohos-musl-aarch64.cmake \
        -DTOOLCHAIN_SYSROOT=$OHOS_SDK_NATIVE/sysroot \
        -DTOOLCHAIN_CLANG_ROOT=$OHOS_SDK_NATIVE/llvm \
        -DPANDA_ETS_INTEROP_JS=ON \
        -DPANDA_WITH_ECMASCRIPT=ON"
    local OHOS_BUILD_TARGETS="ark ark_aot arkruntime arkassembler ets_interop_js_napi"
    build_panda "$OHOS_BUILD_DIR" "$TARGET_CMAKE_ARGS" "$OHOS_BUILD_TARGETS"
    copy_into_sdk "$OHOS_BUILD_DIR" "$TARGET_SDK_DIR" "$SCRIPT_DIR"/ohos_arm64.txt

    echo "> Copying headers into SDK..."
    local HEADERS_DST="$TARGET_SDK_DIR"/include
    # Source headers
    copy_into_sdk "$ARK_ROOT" "$HEADERS_DST" "$SCRIPT_DIR"/headers.txt '\(\.h$\|\.inl$\|\.inc$\)'
    # Generated headers
    copy_into_sdk "$OHOS_BUILD_DIR" "$HEADERS_DST" "$SCRIPT_DIR"/gen_headers.txt '\(\.h$\|\.inl$\|\.inc$\)'
    # Copy compiled etsstdlib into Panda SDK
    mkdir -p "$PANDA_SDK_PATH"/ets
    cp -r "$OHOS_BUILD_DIR"/plugins/ets/etsstdlib.abc "$PANDA_SDK_PATH"/ets
}

ts_linter() {
    echo "> Building tslinter..."
    local LINTER_ROOT="$ARK_ROOT/plugins/ecmascript/es2panda/migrator/typescript"
    (cd "$LINTER_ROOT" && npm install)
    local TGZ="$(ls "$LINTER_ROOT"/bundle/panda-ts-transpiler*tgz)"
    mkdir -p "$PANDA_SDK_PATH"/tslinter
    tar -xf "$TGZ" -C "$PANDA_SDK_PATH"/tslinter
    mv "$PANDA_SDK_PATH"/tslinter/package/* "$PANDA_SDK_PATH"/tslinter/
    rm -rf "$PANDA_SDK_PATH"/tslinter/node_modules
    rm -rf "$PANDA_SDK_PATH"/tslinter/package

    # Clean up
    rm "$TGZ"
    rm -rf "$LINTER_ROOT"/build
    rm -rf "$LINTER_ROOT"/bundle
    rm -rf "$LINTER_ROOT"/dist
    rm -rf "$LINTER_ROOT"/node_modules
}

ets_std_lib() {
    echo "> Copying ets std lib into SDK..."
    mkdir -p "$PANDA_SDK_PATH"/ets
    cp -r "$ARK_ROOT"/plugins/ets/stdlib "$PANDA_SDK_PATH"/ets
}

rm -rf "$PANDA_SDK_PATH"
linux_tools
linux_ohos_tools
windows_tools
ohos
ts_linter
ets_std_lib

echo "> Packing NPM package..."
cp "$SCRIPT_DIR"/package.json "$PANDA_SDK_PATH"
cd "$PANDA_SDK_PATH"
npm pack --pack-destination "$SDK_BUILD_ROOT"
