#include "runtime/include/coroutine.h"
#include "runtime/coroutine_context.h"
#include "runtime/coroutine_manager.h"

namespace panda {

Coroutine *Coroutine::Create(Runtime *runtime, PandaVM *vm, PandaString name, CoroutineContext *context,
                             std::optional<EntrypointInfo> &&ep_info)
{
    mem::InternalAllocatorPtr allocator = runtime->GetInternalAllocator();
    auto co = allocator->New<Coroutine>(os::thread::GetCurrentThreadId(), allocator, vm,
                                        panda::panda_file::SourceLang::PANDA_ASSEMBLY, std::move(name), context,
                                        std::move(ep_info));
    co->Initialize();
    return co;
}

Coroutine::Coroutine(ThreadId id, mem::InternalAllocatorPtr allocator, PandaVM *vm,
                     panda::panda_file::SourceLang thread_lang, PandaString name, CoroutineContext *context,
                     std::optional<EntrypointInfo> &&ep_info)
    : ManagedThread(id, allocator, vm, Thread::ThreadType::THREAD_TYPE_TASK, thread_lang),
      name_(std::move(name)),
      entrypoint_(ep_info.has_value() ? ep_info->entrypoint : nullptr),
      arguments_(ep_info.has_value() ? std::move(ep_info->arguments) : PandaVector<Value> {}),
      context_(context),
      has_entrypoint_(ep_info.has_value()),
      start_suspended_(ep_info.has_value()),
      completion_event_(ep_info.has_value() ? ep_info->completion_event : nullptr)
{
    ASSERT(vm != nullptr);
    ASSERT(context != nullptr);
}

PandaString Coroutine::GetName() const
{
    return name_;
}

Coroutine::Status Coroutine::GetCoroutineStatus() const
{
    return context_->GetStatus();
}
void Coroutine::SetCoroutineStatus(Coroutine::Status new_status)
{
    context_->SetStatus(new_status);
}

void Coroutine::Destroy()
{
    context_->Destroy();
}

void Coroutine::Initialize()
{
    context_->AttachToCoroutine(this);
    InitForStackOverflowCheck(ManagedThread::STACK_OVERFLOW_RESERVED_SIZE,
                              ManagedThread::STACK_OVERFLOW_PROTECTED_SIZE);
}

bool Coroutine::RetrieveStackInfo(void *&stack_addr, size_t &stack_size, size_t &guard_size)
{
    if (HasEntrypoint()) {
        // has EP method and separate native context for its execution
        return context_->RetrieveStackInfo(stack_addr, stack_size, guard_size);
    }
    // does not have EP, executes on OS-provided context and stack
    return ManagedThread::RetrieveStackInfo(stack_addr, stack_size, guard_size);
}

void Coroutine::RequestSuspend(bool gets_blocked)
{
    context_->RequestSuspend(gets_blocked);
}

void Coroutine::RequestResume()
{
    context_->RequestResume();
}

void Coroutine::RequestUnblock()
{
    context_->RequestUnblock();
}

void Coroutine::RequestCompletion([[maybe_unused]] Value return_value)
{
    completion_event_->SetHappened();
}

std::ostream &operator<<(std::ostream &os, Coroutine::Status status)
{
    switch (status) {
        case Coroutine::Status::CREATED:
            os << "CREATED";
            break;
        case Coroutine::Status::RUNNABLE:
            os << "RUNNABLE";
            break;
        case Coroutine::Status::RUNNING:
            os << "RUNNING";
            break;
        case Coroutine::Status::BLOCKED:
            os << "BLOCKED";
            break;
        case Coroutine::Status::TERMINATING:
            os << "TERMINATING";
            break;
        case Coroutine::Status::AWAIT_LOOP:
            os << "AWAIT_LOOP";
            break;
        default:
            break;
    }
    return os;
}

}  // namespace panda
