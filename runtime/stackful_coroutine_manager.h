/**
 * Copyright (c) 2023 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#ifndef PANDA_RUNTIME_STACKFUL_COROUTINE_MANAGER_H
#define PANDA_RUNTIME_STACKFUL_COROUTINE_MANAGER_H

#include "runtime/include/mem/panda_containers.h"
#include "runtime/coroutine_manager.h"
#include "runtime/include/stackful_coroutine.h"

namespace panda {

/**
 * \brief Stackful ("fiber"-based) coroutine manager implementation.
 *
 * In this implementation coroutines are user-level threads ("fibers") with manually allocated stacks.
 *
 * For interface function descriptions see CoroutineManager class declaration.
 */
class StackfulCoroutineManager : public CoroutineManager {
public:
    NO_COPY_SEMANTIC(StackfulCoroutineManager);
    NO_MOVE_SEMANTIC(StackfulCoroutineManager);

    explicit StackfulCoroutineManager(CoroutineFactory factory) : CoroutineManager(factory) {}
    ~StackfulCoroutineManager() override = default;

    /* CoroutineManager interfaces, see CoroutineManager class for the details */
    void Initialize(CoroutineManagerConfig config) override;
    void RegisterCoroutine(Coroutine *co) override;
    bool TerminateCoroutine(Coroutine *co) override;
    Coroutine *Launch(CompletionEvent *completion_event, Method *entrypoint, PandaVector<Value> &&arguments) override;
    void Schedule() override;
    void Await(CoroutineEvent *awaitee) override;
    void UnblockWaiters(CoroutineEvent *blocker) override;

    /* ThreadManager interfaces, see ThreadManager class for the details */
    void WaitForDeregistration() override;
    void SuspendAllThreads() override;
    void ResumeAllThreads() override;
    bool IsRunningThreadExist() override;

protected:
    bool EnumerateThreadsImpl(const ThreadManager::Callback &cb, unsigned int inc_mask,
                              unsigned int xor_mask) const override;
    CoroutineContext *CreateCoroutineContext(bool coro_has_entrypoint) override;
    void DeleteCoroutineContext(CoroutineContext *ctx) override;

    size_t GetCoroutineCount() override;
    size_t GetCoroutineCountLimit() override;

    StackfulCoroutineContext *GetCurrentContext();
    bool IsJsMode();

private:
    Coroutine *LaunchImpl(CompletionEvent *completion_event, Method *entrypoint, PandaVector<Value> &&arguments);
    void ScheduleImpl(bool block_current_coro);

    /* runnables queue management */
#ifndef NDEBUG
    void PrintRunnableQueue(const PandaString &requester);
#endif
    void PushToRunnableQueue(Coroutine *co, bool push_front = false);
    Coroutine *PopFromRunnableQueue();
    bool RunnableCoroutinesExist();

    /* coroutine registry management */
    void AddToRegistry(Coroutine *co);
    void RemoveFromRegistry(Coroutine *co) REQUIRES(coro_list_lock_);

    bool RegisterWaiter(Coroutine *waiter, CoroutineEvent *awaitee);
    void FinalizeTerminatedCoros();
    void SuspendCoroutineAndScheduleNext(Coroutine *co, bool suspend_as_blocked);
    void ScheduleNextCoroutine();
    void MainCoroutineCompleted();

    /* resource management */
    uint8_t *AllocCoroutineStack();
    void FreeCoroutineStack(uint8_t *stack);

    // for thread safety with GC
    mutable os::memory::Mutex coro_list_lock_;
    // all registered coros
    PandaSet<Coroutine *> coroutines_ GUARDED_BY(coro_list_lock_);
    // runnable coros
    PandaDeque<Coroutine *> runnables_queue_;
    // blocked coros: Coroutine AWAITS CoroutineEvent
    PandaMap<CoroutineEvent *, Coroutine *> waiters_;
    // terminated coros (to be deleted)
    PandaQueue<Coroutine *> finalization_queue_;

    std::atomic_uint32_t coroutine_count_ = 0;
    size_t coroutine_count_limit_ = 0;
    size_t coro_stack_size_bytes_ = 0;
    bool js_mode_ = false;
};

}  // namespace panda

#endif /* PANDA_RUNTIME_STACKFUL_COROUTINE_MANAGER_H */