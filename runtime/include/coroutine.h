/**
 * Copyright (c) 2023 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#ifndef PANDA_RUNTIME_COROUTINE_H
#define PANDA_RUNTIME_COROUTINE_H

#include <optional>
#include "runtime/include/runtime.h"
#include "runtime/include/managed_thread.h"

namespace panda {

class CoroutineContext;
class CompletionEvent;
/**
 * \brief The base class for all coroutines. Holds the managed part of the coroutine context.
 *
 * The coroutine context is splitted into managed and native parts.
 * The managed part is store in this class and its descendants. For the native part see the
 * CoroutineContext class and its descendants.
 */
class Coroutine : public ManagedThread {
public:
    NO_COPY_SEMANTIC(Coroutine);
    NO_MOVE_SEMANTIC(Coroutine);

    /**
     * Status transitions:
     *
     * +---------+                                  +----------+
     * | CREATED | -------------------------------> |          | <-------------+
     * +---------+                                  | RUNNABLE |               |
     *                                         +--- |          | <--+          |
     *                                         |    +----------+    |          |
     *                                         |                    |          |
     *                                         |    +----------+    |     +----------+
     *                                         +--> |          | ---+     |          |
     * +------------+      +-------------+          | RUNNING  |          | BLOCKED  |
     * | AWAIT_LOOP | <--- | TERMINATING | <------- |          | -------> |          |
     * +------------+      +-------------+          +----------+          +----------+
     *
     *
     * Main coroutine gets AWAIT_LOOP status once it starts the final waiting loop. After all
     * other coroutines are completed, the main coroutine exits.
     */
    enum class Status { CREATED, RUNNABLE, RUNNING, BLOCKED, TERMINATING, AWAIT_LOOP };

    /// a helper struct that aggregates all EP related data for a coroutine
    struct EntrypointInfo {
        // NOLINTNEXTLINE(misc-non-private-member-variables-in-classes)
        CompletionEvent *completion_event;
        // NOLINTNEXTLINE(misc-non-private-member-variables-in-classes)
        Method *entrypoint;
        // NOLINTNEXTLINE(misc-non-private-member-variables-in-classes)
        PandaVector<Value> &&arguments;

        /**
         * \param completion_event an instance of CompletionEvent to be used on coroutine completion to pass the
         * return value to the point where it is needed. Also is used to unblock the coroutines that are waiting for
         * the one being created to complete.
         *
         * \param entrypoint managed method to execute in the context of coroutine.
         *
         * \param arguments the array of EP method arguments
         */
        explicit EntrypointInfo(CompletionEvent *event, Method *entry, PandaVector<Value> &&args)
            : completion_event(event), entrypoint(entry), arguments(std::move(args))
        {
            ASSERT(event != nullptr);
            ASSERT(entry != nullptr);
        }
    };

    /// The coroutine factory: creates and initializes a coroutine instance. The preferred way to create a
    /// coroutine. For details see CoroutineManager::CoroutineFactory
    static Coroutine *Create(Runtime *runtime, PandaVM *vm, PandaString name, CoroutineContext *context,
                             std::optional<EntrypointInfo> &&ep_info = std::nullopt);
    ~Coroutine() override = default;

    /// Should be called after creation in order to create native context and do other things
    virtual void Initialize();
    /// Manual destruction, applicable only to the main coro. Other ones get deleted by the coroutine manager once they
    /// finish execution of their entrypoint method.
    void Destroy();

    bool RetrieveStackInfo(void *&stack_addr, size_t &stack_size, size_t &guard_size) override;

    static bool ThreadIsCoroutine(Thread *thread)
    {
        ASSERT(thread != nullptr);
        // TODO(konstanting, #I67QXC): THREAD_TYPE_TASK -> THREAD_TYPE_COROUTINE and
        // remove the runtime/scheduler directory contents
        return thread->GetThreadType() == Thread::ThreadType::THREAD_TYPE_TASK;
    }

    static Coroutine *CastFromThread(Thread *thread)
    {
        ASSERT(thread != nullptr);
        ASSERT(ThreadIsCoroutine(thread));
        return static_cast<Coroutine *>(thread);
    }

    static Coroutine *GetCurrent()
    {
        Thread *thread = Thread::GetCurrent();
        ASSERT(thread != nullptr);
        if (ThreadIsCoroutine(thread)) {
            return CastFromThread(thread);
        }
        return nullptr;
    }

    /// Get coroutine status. It is independent from ThreadStatus.
    Status GetCoroutineStatus() const;
    /// Get coroutine name.
    PandaString GetName() const;

    /// Suspend a coroutine, so its status becomes either Status::RUNNABLE or Status::BLOCKED, depending on the suspend
    /// reason.
    virtual void RequestSuspend(bool gets_blocked);
    /// Resume the suspended coroutine, so its status becomes Status::RUNNING.
    virtual void RequestResume();
    /// Unblock the blocked coroutine, setting its status to Status::RUNNABLE
    virtual void RequestUnblock();
    /// Indicate that coroutine entrypoint execution is finished. Propagates the coroutine
    /// return value to language level objects.
    virtual void RequestCompletion(Value return_value);

    /// Get the CompletionEvent instance
    CompletionEvent *GetCompletionEvent()
    {
        return completion_event_;
    }

    /// Get coroutine's entrypoint method.
    Method *GetEntrypoint() const
    {
        return entrypoint_;
    }

    /// Get coroutine's entrypoint args if any.
    PandaVector<Value> &GetEntrypointArguments()
    {
        return arguments_;
    }

    const PandaVector<Value> &GetEntrypointArguments() const
    {
        return arguments_;
    }

    template <class T>
    T *GetContext() const
    {
        return static_cast<T *>(context_);
    }

    bool IsSuspendOnStartup()
    {
        return start_suspended_;
    }

    bool HasEntrypoint()
    {
        return has_entrypoint_;
    }

protected:
    // we would like everyone to use the factory to create a Coroutine
    explicit Coroutine(ThreadId id, mem::InternalAllocatorPtr allocator, PandaVM *vm,
                       panda::panda_file::SourceLang thread_lang, PandaString name, CoroutineContext *context,
                       std::optional<EntrypointInfo> &&ep_info);

    void SetCoroutineStatus(Status new_status);

private:
    PandaString name_;
    Method *entrypoint_ = nullptr;
    PandaVector<Value> arguments_;
    CoroutineContext *context_ = nullptr;
    bool has_entrypoint_ = false;
    bool start_suspended_ = false;
    CompletionEvent *completion_event_ = nullptr;

    // Allocator calls our protected ctor
    friend class mem::Allocator;
};

std::ostream &operator<<(std::ostream &os, Coroutine::Status status);

}  // namespace panda

#endif  // PANDA_RUNTIME_COROUTINE_H
