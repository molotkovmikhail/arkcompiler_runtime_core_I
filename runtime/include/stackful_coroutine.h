/**
 * Copyright (c) 2023 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#ifndef PANDA_RUNTIME_STACKFUL_COROUTINE_H
#define PANDA_RUNTIME_STACKFUL_COROUTINE_H

#include "runtime/fibers/fiber_context.h"
#include "runtime/coroutine_context.h"

namespace panda {

/**
 * \brief Native context of a coroutine. "Fiber"-based implementation.
 *
 * This implementation uses ucontext_t to implement native coroutine context.
 */
class StackfulCoroutineContext : public CoroutineContext {
public:
    NO_COPY_SEMANTIC(StackfulCoroutineContext);
    NO_MOVE_SEMANTIC(StackfulCoroutineContext);

    /// \param stack specifies the lowest address of the stack region to use;
    /// it should have at least \param stack_size_bytes bytes accessible. If the stack grows down on the
    /// target architecture, then the initial stack pointer of the coroutine will be set to
    /// (stack + stack_size_bytes)
    explicit StackfulCoroutineContext(uint8_t *stack, size_t stack_size_bytes);
    ~StackfulCoroutineContext() override = default;

    /// prepares the context for execution, links it to the managed context part (Coroutine instance) and registers the
    /// created coroutine in the CoroutineManager (in the RUNNABLE status)
    void AttachToCoroutine(Coroutine *co) override;
    /// Manually destroys the context. Should be called by the Coroutine instance as a part of main coroutine
    /// destruction. All other coroutines and their contexts are destroyed by the CoroutineManager once the coroutine
    /// entrypoint execution finishes
    void Destroy() override;

    bool RetrieveStackInfo(void *&stack_addr, size_t &stack_size, size_t &guard_size) override;

    /// suspends the execution context, sets its status to either Status::RUNNABLE or Status::BLOCKED, depending on the
    /// suspend reason.
    void RequestSuspend(bool gets_blocked) override;
    /// resumes the suspended context, sets status to RUNNING.
    void RequestResume() override;
    /// Unblock the coroutine and set its status to Status::RUNNABLE
    void RequestUnblock() override;

    // should be called then the main thread is done executing the program entrypoint
    void MainThreadFinished();
    /// moves the main coroutine to Status::AWAIT_LOOP
    void EnterAwaitLoop();

    /// coroutine status is a part of native context, because it might require some synchronization on access
    Coroutine::Status GetStatus() const override;

    /// transfer control to the target context
    /// NB: this method will return only after the control is transferred back to the caller context
    bool SwitchTo(StackfulCoroutineContext *target);

    /// return the lowest address of the coroutine native stack (provided in the ctx contructor)
    uint8_t *GetStackLoAddrPtr()
    {
        return stack_;
    }

    static void ThreadProcImpl(StackfulCoroutineContext *ctx);

protected:
    void SetStatus(Coroutine::Status new_status) override;

private:
    uint8_t *stack_ = nullptr;
    size_t stack_size_bytes_ = 0;
    fibers::FiberContext context_;
    Coroutine::Status status_ {Coroutine::Status::CREATED};
};

extern "C" void CoroThreadProc(void *ctx);

}  // namespace panda

#endif  // PANDA_RUNTIME_STACKFUL_COROUTINE_H
