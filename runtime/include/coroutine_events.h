/**
 * Copyright (c) 2023 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#ifndef PANDA_RUNTIME_COROUTINE_EVENTS_H
#define PANDA_RUNTIME_COROUTINE_EVENTS_H

#include "runtime/mem/refstorage/reference.h"

namespace panda {

/**
 * \brief The base class for coroutine events.
 *
 * These events are used to implement blocking and unblocking the coroutines that are waiting for something.
 */
class CoroutineEvent {
public:
    NO_COPY_SEMANTIC(CoroutineEvent);
    NO_MOVE_SEMANTIC(CoroutineEvent);

    enum class Type { GENERAL, COMPLETION, CHANNEL, IO };

    explicit CoroutineEvent(Type t) : type_(t) {}
    virtual ~CoroutineEvent() = default;

    Type GetType()
    {
        return type_;
    }

    bool Happened()
    {
        return happened_;
    }

    void SetHappened()
    {
        happened_ = true;
    }

private:
    Type type_ = Type::GENERAL;
    bool happened_ = false;
};

/**
 * \brief The coroutine completion event: happens when coroutine is done executing its bytecode.
 */
class CompletionEvent : public CoroutineEvent {
public:
    NO_COPY_SEMANTIC(CompletionEvent);
    NO_MOVE_SEMANTIC(CompletionEvent);

    /// \param promise A weak reference (from global storage) to the language-dependent promise object that will hold
    /// the coroutine return value.
    explicit CompletionEvent(mem::Reference *promise) : CoroutineEvent(Type::COMPLETION), promise_(promise) {}
    ~CompletionEvent() override = default;

    mem::Reference *GetPromise()
    {
        return promise_;
    }

private:
    mem::Reference *promise_ = nullptr;
};

}  // namespace panda

#endif /* PANDA_RUNTIME_COROUTINE_EVENTS_H */