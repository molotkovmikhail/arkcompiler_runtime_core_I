/**
 * Copyright (c) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "runtime/include/coroutine.h"
#include "runtime/include/stackful_coroutine.h"
#include "runtime/include/thread_scopes.h"
#include "libpandabase/macros.h"
#include "runtime/include/runtime.h"
#include "runtime/include/panda_vm.h"
#include "runtime/stackful_coroutine_manager.h"

namespace panda {

uint8_t *StackfulCoroutineManager::AllocCoroutineStack()
{
    Pool stack_pool = PoolManager::GetMmapMemPool()->AllocPool<OSPagesAllocPolicy::NO_POLICY>(
        coro_stack_size_bytes_, SpaceType::SPACE_TYPE_NATIVE_STACKS, AllocatorType::NATIVE_STACKS_ALLOCATOR);
    return static_cast<uint8_t *>(stack_pool.GetMem());
}

void StackfulCoroutineManager::FreeCoroutineStack(uint8_t *stack)
{
    if (stack != nullptr) {
        PoolManager::GetMmapMemPool()->FreePool(stack, coro_stack_size_bytes_);
    }
}

void StackfulCoroutineManager::Initialize(CoroutineManagerConfig config)
{
    coro_stack_size_bytes_ = Runtime::GetCurrent()->GetOptions().GetCoroutineStackSizePages() * os::mem::GetPageSize();
    if (coro_stack_size_bytes_ != AlignUp(coro_stack_size_bytes_, PANDA_POOL_ALIGNMENT_IN_BYTES)) {
        size_t alignment_pages = PANDA_POOL_ALIGNMENT_IN_BYTES / os::mem::GetPageSize();
        LOG(FATAL, COROUTINES) << "Coroutine stack size should be >= " << alignment_pages
                               << " pages and should be aligned to " << alignment_pages << "-page boundary!";
    }
    size_t coro_stack_area_size_bytes = Runtime::GetCurrent()->GetOptions().GetCoroutinesStackMemLimit();
    coroutine_count_limit_ = coro_stack_area_size_bytes / coro_stack_size_bytes_;
    js_mode_ = config.emulate_js;
}

void StackfulCoroutineManager::AddToRegistry(Coroutine *co)
{
    os::memory::LockHolder lock(coro_list_lock_);
    auto main_co = GetMainThread();
    if (main_co != nullptr) {
        // TODO(konstanting, #I67QXC): we should get this callback from GC instead of copying from the main thread
        co->SetPreWrbEntrypoint(main_co->GetPreWrbEntrypoint());
    }
    coroutines_.insert(co);
    coroutine_count_++;
}

void StackfulCoroutineManager::RemoveFromRegistry(Coroutine *co)
{
    coroutines_.erase(co);
    coroutine_count_--;
}

void StackfulCoroutineManager::RegisterCoroutine(Coroutine *co)
{
    AddToRegistry(co);
}

bool StackfulCoroutineManager::TerminateCoroutine(Coroutine *co)
{
    LOG(DEBUG, COROUTINES) << "StackfulCoroutineManager::TerminateCoroutine() started";
    co->NativeCodeEnd();
    co->UpdateStatus(ThreadStatus::TERMINATING);

    {
        os::memory::LockHolder l_list(coro_list_lock_);
        RemoveFromRegistry(co);
        co->DestroyInternalResources();
        co->UpdateStatus(ThreadStatus::FINISHED);
    }

    if (co->HasEntrypoint()) {
        // entrypointless coros should be destroyed manually
        UnblockWaiters(co->GetCompletionEvent());
        finalization_queue_.push(co);
        ScheduleNextCoroutine();
    }

    return false;
    // TODO(konstanting, #I67QXC): issue debug notifications to runtime
}

CoroutineContext *StackfulCoroutineManager::CreateCoroutineContext(bool coro_has_entrypoint)
{
    uint8_t *stack = nullptr;
    size_t stack_size_bytes = 0;
    if (coro_has_entrypoint) {
        stack = AllocCoroutineStack();
        if (stack == nullptr) {
            return nullptr;
        }
        stack_size_bytes = coro_stack_size_bytes_;
    }
    return Runtime::GetCurrent()->GetInternalAllocator()->New<StackfulCoroutineContext>(stack, stack_size_bytes);
}

void StackfulCoroutineManager::DeleteCoroutineContext(CoroutineContext *ctx)
{
    FreeCoroutineStack(static_cast<StackfulCoroutineContext *>(ctx)->GetStackLoAddrPtr());
    Runtime::GetCurrent()->GetInternalAllocator()->Delete(ctx);
}

size_t StackfulCoroutineManager::GetCoroutineCount()
{
    return coroutine_count_;
}

size_t StackfulCoroutineManager::GetCoroutineCountLimit()
{
    return coroutine_count_limit_;
}

Coroutine *StackfulCoroutineManager::Launch(CompletionEvent *completion_event, Method *entrypoint,
                                            PandaVector<Value> &&arguments)
{
    LOG(DEBUG, COROUTINES) << "StackfulCoroutineManager::Launch started";

    auto result = LaunchImpl(completion_event, entrypoint, std::move(arguments));
    if (result == nullptr) {
        ThrowOutOfMemoryError("Launch failed");
    }

    LOG(DEBUG, COROUTINES) << "StackfulCoroutineManager::Launch finished";
    return result;
}

bool StackfulCoroutineManager::RegisterWaiter(Coroutine *waiter, CoroutineEvent *awaitee)
{
    if (awaitee->Happened()) {
        return false;
    }
    LOG(DEBUG, COROUTINES) << "StackfulCoroutineManager::RegisterAsAwaitee: " << waiter->GetName() << " AWAITS";
    waiters_.insert({awaitee, waiter});
    return true;
}

void StackfulCoroutineManager::Await(CoroutineEvent *awaitee)
{
    ASSERT(awaitee != nullptr);
    LOG(DEBUG, COROUTINES) << "StackfulCoroutineManager::Await started";
    auto waiter = Coroutine::GetCurrent();
    if (!RegisterWaiter(waiter, awaitee)) {
        LOG(DEBUG, COROUTINES) << "StackfulCoroutineManager::Await finished (no await happened)";
        return;
    }
    // Once we are ready to implement the stackful N:M version, we have to consider here the case
    // when all other coros are RUNNING. So we cannot just yield, we have to implement some
    // kind of wait (e.g. on condition variable).
    ScheduleImpl(true);
    // NB: at this point the awaitee is already deleted
    LOG(DEBUG, COROUTINES) << "StackfulCoroutineManager::Await finished";
}

void StackfulCoroutineManager::UnblockWaiters(CoroutineEvent *blocker)
{
    ASSERT(blocker != nullptr);
    ASSERT(blocker->Happened());
    auto w = waiters_.find(blocker);
    if (w != waiters_.end()) {
        auto coro = w->second;
        waiters_.erase(w);
        coro->RequestUnblock();
        PushToRunnableQueue(coro);
    }
    Runtime::GetCurrent()->GetInternalAllocator()->Delete(blocker);
}

void StackfulCoroutineManager::Schedule()
{
    LOG(DEBUG, COROUTINES) << "StackfulCoroutineManager::Schedule() request from "
                           << Coroutine::GetCurrent()->GetName();
    ScheduleImpl(false);
}

bool StackfulCoroutineManager::EnumerateThreadsImpl(const ThreadManager::Callback &cb, unsigned int inc_mask,
                                                    unsigned int xor_mask) const
{
    os::memory::LockHolder lock(coro_list_lock_);
    for (auto t : coroutines_) {
        if (!ApplyCallbackToThread(cb, t, inc_mask, xor_mask)) {
            return false;
        }
    }
    return true;
}

void StackfulCoroutineManager::SuspendAllThreads()
{
    os::memory::LockHolder lock(coro_list_lock_);
    LOG(DEBUG, COROUTINES) << "StackfulCoroutineManager::SuspendAllThreads started";
    for (auto t : coroutines_) {
        t->SuspendImpl(true);
    }
    LOG(DEBUG, COROUTINES) << "StackfulCoroutineManager::SuspendAllThreads finished";
}

void StackfulCoroutineManager::ResumeAllThreads()
{
    os::memory::LockHolder lock(coro_list_lock_);
    for (auto t : coroutines_) {
        t->ResumeImpl(true);
    }
}

bool StackfulCoroutineManager::IsRunningThreadExist()
{
    UNREACHABLE();
    // TODO(konstanting): correct implementation. Which coroutine do we consider running?
    return false;
}

void StackfulCoroutineManager::WaitForDeregistration()
{
    MainCoroutineCompleted();
}

#ifndef NDEBUG
void StackfulCoroutineManager::PrintRunnableQueue(const PandaString &requester)
{
    LOG(DEBUG, COROUTINES) << "[" << requester << "] ";
    for (auto co : runnables_queue_) {
        LOG(DEBUG, COROUTINES) << co->GetName() << " <";
    }
    LOG(DEBUG, COROUTINES) << "X";
}
#endif

void StackfulCoroutineManager::PushToRunnableQueue(Coroutine *co, bool push_front)
{
    if (push_front) {
        runnables_queue_.push_front(co);
    } else {
        runnables_queue_.push_back(co);
    }
}

bool StackfulCoroutineManager::RunnableCoroutinesExist()
{
    return !runnables_queue_.empty();
}

Coroutine *StackfulCoroutineManager::PopFromRunnableQueue()
{
    auto co = runnables_queue_.front();
    runnables_queue_.pop_front();
    return co;
}

void StackfulCoroutineManager::ScheduleNextCoroutine()
{
    auto *current_ctx = GetCurrentContext();
    auto *next_ctx = PopFromRunnableQueue()->GetContext<StackfulCoroutineContext>();
    next_ctx->RequestResume();
    Coroutine::SetCurrent(next_ctx->GetCoroutine());
    current_ctx->SwitchTo(next_ctx);
}

void StackfulCoroutineManager::SuspendCoroutineAndScheduleNext(Coroutine *co, bool suspend_as_blocked)
{
    co->RequestSuspend(suspend_as_blocked);
    if (!suspend_as_blocked) {
        PushToRunnableQueue(co);
    }
    // will transfer control to another coro...
    ScheduleNextCoroutine();
    // ...this coro has been scheduled again: process finalization queue
    FinalizeTerminatedCoros();
}

void StackfulCoroutineManager::ScheduleImpl(bool block_current_coro)
{
    auto current = Coroutine::GetCurrent();
    ScopedNativeCodeThread n(current);
    if (RunnableCoroutinesExist()) {
        SuspendCoroutineAndScheduleNext(current, block_current_coro);
    }
}

void StackfulCoroutineManager::FinalizeTerminatedCoros()
{
    auto alloc = Runtime::GetCurrent()->GetInternalAllocator();
    while (!finalization_queue_.empty()) {
        auto f = finalization_queue_.front();
        finalization_queue_.pop();
        DeleteCoroutineContext(f->GetContext<CoroutineContext>());
        alloc->Delete(f);
    }
}

Coroutine *StackfulCoroutineManager::LaunchImpl(CompletionEvent *completion_event, Method *entrypoint,
                                                PandaVector<Value> &&arguments)
{
#ifndef NDEBUG
    PrintRunnableQueue("LaunchImpl begin");
#endif
    auto coro_name = entrypoint->GetFullName();
    Coroutine *co = CreateCoroutineInstance(completion_event, entrypoint, std::move(arguments), std::move(coro_name));
    if (co == nullptr) {
        LOG(DEBUG, COROUTINES) << "StackfulCoroutineManager::LaunchImpl: failed to create a coroutine!";
        return co;
    }
    PushToRunnableQueue(co, IsJsMode());
#ifndef NDEBUG
    PrintRunnableQueue("LaunchImpl end");
#endif
    return co;
}

void StackfulCoroutineManager::MainCoroutineCompleted()
{
    LOG(DEBUG, COROUTINES) << "StackfulCoroutineManager::MainCoroutineCompleted() started";
    GetCurrentContext()->MainThreadFinished();
    GetCurrentContext()->EnterAwaitLoop();
    auto main = Coroutine::GetCurrent();
    while (coroutine_count_ > 1) {  // main coro runs till VM shutdown
        LOG(DEBUG, COROUTINES) << "StackfulCoroutineManager::MainCoroutineCompleted(): await_all(): still "
                               << coroutine_count_ << " coroutines exist...";
        SuspendCoroutineAndScheduleNext(main, false);
    }
    LOG(DEBUG, COROUTINES) << "StackfulCoroutineManager::MainCoroutineCompleted(): await_all() done";
}

StackfulCoroutineContext *StackfulCoroutineManager::GetCurrentContext()
{
    auto co = Coroutine::GetCurrent();
    return co->GetContext<StackfulCoroutineContext>();
}

bool StackfulCoroutineManager::IsJsMode()
{
    return js_mode_;
}

}  // namespace panda
