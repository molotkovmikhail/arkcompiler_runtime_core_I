/**
 * Copyright (c) 2023 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <optional>
#include "runtime/coroutine_manager.h"

namespace panda {

CoroutineManager::CoroutineManager(CoroutineFactory factory) : co_factory_(factory)
{
    ASSERT(factory != nullptr);
}

Coroutine *CoroutineManager::CreateMainCoroutine(Runtime *runtime, PandaVM *vm)
{
    CoroutineContext *ctx = CreateCoroutineContext(false);
    auto main = co_factory_(runtime, vm, "_main_", ctx, std::nullopt);
    ASSERT(main != nullptr);
    ASSERT(main == ManagedThread::GetCurrent());

    main->InitBuffers();
    SetMainThread(main);
    return main;
}

void CoroutineManager::DestroyMainCoroutine()
{
    auto main = static_cast<Coroutine *>(GetMainThread());
    auto context = main->GetContext<CoroutineContext>();
    main->Destroy();
    Runtime::GetCurrent()->GetInternalAllocator()->Delete(main);
    DeleteCoroutineContext(context);
}

Coroutine *CoroutineManager::CreateEntrypointlessCoroutine(Runtime *runtime, PandaVM *vm)
{
    if (GetCoroutineCount() >= GetCoroutineCountLimit()) {
        // resource limit reached
        return nullptr;
    }
    CoroutineContext *ctx = CreateCoroutineContext(false);
    if (ctx == nullptr) {
        // do not proceed if we cannot create a context for the new coroutine
        return nullptr;
    }
    auto co = co_factory_(runtime, vm, "_coro_", ctx, std::nullopt);
    ASSERT(co != nullptr);
    ASSERT(co == ManagedThread::GetCurrent());

    co->InitBuffers();
    return co;
}

void CoroutineManager::DestroyEntrypointlessCoroutine(Coroutine *co)
{
    ASSERT(co != nullptr);
    if (!co->HasEntrypoint()) {
        auto context = co->GetContext<CoroutineContext>();
        co->Destroy();
        Runtime::GetCurrent()->GetInternalAllocator()->Delete(co);
        DeleteCoroutineContext(context);
    }
}

Coroutine *CoroutineManager::CreateCoroutineInstance(CompletionEvent *completion_event, Method *entrypoint,
                                                     PandaVector<Value> &&arguments, PandaString name)
{
    if (GetCoroutineCount() >= GetCoroutineCountLimit()) {
        // resource limit reached
        return nullptr;
    }
    CoroutineContext *ctx = CreateCoroutineContext(true);
    if (ctx == nullptr) {
        // do not proceed if we cannot create a context for the new coroutine
        return nullptr;
    }
    // assuming that the main coro is already created and Coroutine::GetCurrent is not nullptr
    ASSERT(Coroutine::GetCurrent() != nullptr);
    auto coro = co_factory_(Runtime::GetCurrent(), Coroutine::GetCurrent()->GetVM(), std::move(name), ctx,
                            Coroutine::EntrypointInfo {completion_event, entrypoint, std::move(arguments)});
    return coro;
}

}  // namespace panda
