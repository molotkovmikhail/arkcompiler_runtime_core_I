/**
 * Copyright (c) 2023 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "runtime/include/coroutine.h"
#include "runtime/include/panda_vm.h"
#include "runtime/include/thread_scopes.h"
#include "runtime/coroutine_manager.h"
#include "runtime/include/stackful_coroutine.h"

namespace panda {

/*extern "C"*/ void CoroThreadProc(void *ctx)
{
    StackfulCoroutineContext::ThreadProcImpl(reinterpret_cast<StackfulCoroutineContext *>(ctx));
}

// clang-tidy cannot detect that we are going to initialize context_ via getcontext()
// NOLINTNEXTLINE(cppcoreguidelines-pro-type-member-init)
StackfulCoroutineContext::StackfulCoroutineContext(uint8_t *stack, size_t stack_size_bytes)
    : stack_(stack), stack_size_bytes_(stack_size_bytes)
{
    fibers::GetCurrentContext(&context_);
}

void StackfulCoroutineContext::AttachToCoroutine(Coroutine *co)
{
    CoroutineContext::AttachToCoroutine(co);
    if (co->HasEntrypoint()) {
        fibers::UpdateContext(&context_, CoroThreadProc, this, stack_, stack_size_bytes_);
    }
    auto cm = static_cast<CoroutineManager *>(co->GetVM()->GetThreadManager());
    cm->RegisterCoroutine(co);
    if (co->HasEntrypoint()) {
        // TODO(konstanting, #I67QXC): IsSuspendOnStartup() support
        SetStatus(Coroutine::Status::RUNNABLE);
    } else {
        // main coroutine
        SetStatus(Coroutine::Status::RUNNING);
        Coroutine::SetCurrent(co);
        co->NativeCodeBegin();
    }
}

bool StackfulCoroutineContext::RetrieveStackInfo(void *&stack_addr, size_t &stack_size, size_t &guard_size)
{
    stack_addr = stack_;
    stack_size = stack_size_bytes_;
    guard_size = 0;
    return true;
}

Coroutine::Status StackfulCoroutineContext::GetStatus() const
{
    return status_;
}

void StackfulCoroutineContext::SetStatus(Coroutine::Status new_status)
{
#ifndef NDEBUG
    PandaString setter = (Thread::GetCurrent() == nullptr) ? "null" : Coroutine::GetCurrent()->GetName();
    LOG(DEBUG, COROUTINES) << GetCoroutine()->GetName() << ": " << status_ << " -> " << new_status << " by " << setter;
#endif
    status_ = new_status;
}

void StackfulCoroutineContext::Destroy()
{
    auto co = GetCoroutine();
    if (co->HasEntrypoint()) {
        // coroutines with an entry point should not be destroyed manually!
        UNREACHABLE();
    }
    ASSERT(co == Coroutine::GetCurrent());
    ASSERT(co->GetStatus() != ThreadStatus::FINISHED);

    co->UpdateStatus(ThreadStatus::TERMINATING);

    // TODO(konstanting, #I67QXC): do we need this (lines below)?
    // Runtime *runtime = Runtime::GetCurrent();
    // runtime->GetNotificationManager()->ThreadEndEvent(this);

    auto thread_manager = static_cast<CoroutineManager *>(co->GetVM()->GetThreadManager());
    if (thread_manager->TerminateCoroutine(co)) {
        // detach
        Coroutine::SetCurrent(nullptr);
    }
}

/*static*/ void StackfulCoroutineContext::ThreadProcImpl(StackfulCoroutineContext *ctx)
{
    auto co = ctx->GetCoroutine();
    co->NativeCodeBegin();
    ctx->SetStatus(Coroutine::Status::RUNNING);
    {
        ScopedManagedCodeThread s(co);
        PandaVector<Value> args = std::move(co->GetEntrypointArguments());
        Value result = co->GetEntrypoint()->Invoke(co, args.data());
        co->RequestCompletion(result);
    }
    ctx->SetStatus(Coroutine::Status::TERMINATING);

    auto thread_manager = static_cast<CoroutineManager *>(co->GetVM()->GetThreadManager());
    thread_manager->TerminateCoroutine(co);
}

bool StackfulCoroutineContext::SwitchTo(StackfulCoroutineContext *target)
{
    ASSERT(target != nullptr);
    fibers::SwitchContext(&context_, &target->context_);
    // maybe eventually we will check the return value of SwitchContext() and return false in case of error...
    return true;
}

void StackfulCoroutineContext::RequestSuspend(bool gets_blocked)
{
    SetStatus(gets_blocked ? Coroutine::Status::BLOCKED : Coroutine::Status::RUNNABLE);
}

void StackfulCoroutineContext::RequestResume()
{
    SetStatus(Coroutine::Status::RUNNING);
}

void StackfulCoroutineContext::RequestUnblock()
{
    SetStatus(Coroutine::Status::RUNNABLE);
}

void StackfulCoroutineContext::MainThreadFinished()
{
    SetStatus(Coroutine::Status::TERMINATING);
}

void StackfulCoroutineContext::EnterAwaitLoop()
{
    SetStatus(Coroutine::Status::AWAIT_LOOP);
}

}  // namespace panda
