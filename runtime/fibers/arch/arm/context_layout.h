/*
 * Copyright (c) 2023 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef PANDA_RUNTIME_FIBERS_ARCH_ARM_CONTEXT_LAYOUT_H
#define PANDA_RUNTIME_FIBERS_ARCH_ARM_CONTEXT_LAYOUT_H

// NOLINTBEGIN(cppcoreguidelines-macro-usage)

/**
 * Memory layout of the saved context:
 * (stub)
 */

#define FCTX_LEN_BYTES 1

// NOLINTEND(cppcoreguidelines-macro-usage)

#endif /* PANDA_RUNTIME_FIBERS_ARCH_ARM_CONTEXT_LAYOUT_H */