/**
 * Copyright (c) 2021-2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef PANDA_RUNTIME_MEM_GC_GC_WORKERS_THREAD_POOL_H
#define PANDA_RUNTIME_MEM_GC_GC_WORKERS_THREAD_POOL_H

#include "os/thread.h"
#include "runtime/include/thread.h"
#include "runtime/thread_pool.h"
#include "runtime/mem/gc/gc_workers_tasks.h"
#include "runtime/mem/gc/gc.h"

namespace panda::mem {
class GCWorkersThreadPool;

class RefInfo {
public:
    RefInfo() = default;

    RefInfo(ObjectHeader *object, uint32_t ref_offset) : object_(object), ref_offset_(ref_offset) {}

    ~RefInfo() = default;

    ObjectHeader *GetObject() const
    {
        return object_;
    }

    uint32_t GetReferenceOffset() const
    {
        return ref_offset_;
    }

    DEFAULT_COPY_SEMANTIC(RefInfo);
    DEFAULT_MOVE_SEMANTIC(RefInfo);

private:
    ObjectHeader *object_;
    uint32_t ref_offset_;
};

class GCWorkersProcessor : public ProcessorInterface<GCWorkersTask, GCWorkersThreadPool *> {
public:
    explicit GCWorkersProcessor(GCWorkersThreadPool *gc_threads_pools) : gc_threads_pools_(gc_threads_pools) {}

    ~GCWorkersProcessor() override = default;
    NO_COPY_SEMANTIC(GCWorkersProcessor);
    NO_MOVE_SEMANTIC(GCWorkersProcessor);

    bool Process(GCWorkersTask &&task) override;
    bool Init() override;
    bool Destroy() override;

private:
    GCWorkersThreadPool *gc_threads_pools_;
    void *worker_data_ {nullptr};
};

class GCWorkersQueueSimple : public TaskQueueInterface<GCWorkersTask> {
public:
    explicit GCWorkersQueueSimple(mem::InternalAllocatorPtr allocator, size_t queue_limit)
        : TaskQueueInterface<GCWorkersTask>(queue_limit), queue_(allocator->Adapter())
    {
    }

    ~GCWorkersQueueSimple() override = default;
    NO_COPY_SEMANTIC(GCWorkersQueueSimple);
    NO_MOVE_SEMANTIC(GCWorkersQueueSimple);

    GCWorkersTask GetTask() override
    {
        if (queue_.empty()) {
            LOG(DEBUG, GC) << "Empty " << queue_name_ << ", return nothing";
            return GCWorkersTask();
        }
        auto task = queue_.front();
        queue_.pop_front();
        LOG(DEBUG, GC) << "Extract a task from a " << queue_name_ << ": " << GetTaskDescription(task);
        return task;
    }

    // NOLINTNEXTLINE(google-default-arguments)
    void AddTask(GCWorkersTask &&ctx, [[maybe_unused]] size_t priority = 0) override
    {
        LOG(DEBUG, GC) << "Add task to a " << queue_name_ << ": " << GetTaskDescription(ctx);
        queue_.push_front(ctx);
    }

    void Finalize() override
    {
        // Nothing to deallocate
        LOG(DEBUG, GC) << "Clear a " << queue_name_;
        queue_.clear();
    }

protected:
    PandaString GetTaskDescription(const GCWorkersTask &ctx)
    {
        PandaOStringStream stream;
        stream << GCWorkersTaskTypesToString(ctx.GetType());
        return stream.str();
    }

    size_t GetQueueSize() override
    {
        return queue_.size();
    }

private:
    PandaList<GCWorkersTask> queue_;
    const char *queue_name_ = "simple gc workers task queue";
};

class GCWorkersCreationInterface : public WorkerCreationInterface {
public:
    explicit GCWorkersCreationInterface(PandaVM *vm) : gc_thread_(vm, Thread::ThreadType::THREAD_TYPE_GC)
    {
        ASSERT(vm != nullptr);
    }

    ~GCWorkersCreationInterface() override = default;
    NO_COPY_SEMANTIC(GCWorkersCreationInterface);
    NO_MOVE_SEMANTIC(GCWorkersCreationInterface);

    void AttachWorker(bool helper_thread) override
    {
        if (!helper_thread) {
            Thread::SetCurrent(&gc_thread_);
        }
    }
    void DetachWorker(bool helper_thread) override
    {
        if (!helper_thread) {
            Thread::SetCurrent(nullptr);
        }
    }

private:
    Thread gc_thread_;
};

class GCWorkersThreadPool {
public:
    NO_COPY_SEMANTIC(GCWorkersThreadPool);
    NO_MOVE_SEMANTIC(GCWorkersThreadPool);
    GCWorkersThreadPool(mem::InternalAllocatorPtr internal_allocator, GC *gc, size_t threads_count = 0);
    ~GCWorkersThreadPool();

    bool AddTask(GCWorkersTask &&task)
    {
        IncreaseSendedTasks();
        [[maybe_unused]] GCWorkersTaskTypes type = task.GetType();
        if (thread_pool_->TryPutTask(std::forward<GCWorkersTask &&>(task))) {
            LOG(DEBUG, GC) << "Added a new " << GCWorkersTaskTypesToString(type) << " to queue";
            return true;
        }
        DecreaseSendedTasks();
        return false;
    }

    bool AddTask(GCWorkersTaskTypes type)
    {
        return AddTask(GCWorkersTask(type));
    }

    void WaitUntilTasksEnd();

    GC *GetGC()
    {
        return gc_;
    }

    void IncreaseSolvedTasks()
    {
        solved_tasks_++;
        if (solved_tasks_ == sended_tasks_) {
            os::memory::LockHolder lock(cond_var_lock_);
            cond_var_.Signal();
        }
    }

    void IncreaseSendedTasks()
    {
        sended_tasks_++;
    }

    size_t GetThreadsCount() const
    {
        return threads_count_;
    }

    void SetAffinityForGCWorkers();

    void UnsetAffinityForGCWorkers();

private:
    static constexpr uint64_t ALL_TASK_FINISH_WAIT_TIMEOUT = 100U;

    void ResetTasks()
    {
        solved_tasks_ = 0;
        sended_tasks_ = 0;
    }

    size_t GetSolvedTasks()
    {
        return solved_tasks_;
    }

    void DecreaseSendedTasks()
    {
        sended_tasks_--;
    }

    size_t GetSendedTasks()
    {
        return sended_tasks_;
    }

    GC *gc_;
    ThreadPool<GCWorkersTask, GCWorkersProcessor, GCWorkersThreadPool *> *thread_pool_;
    GCWorkersQueueSimple *queue_;
    GCWorkersCreationInterface *worker_iface_;
    mem::InternalAllocatorPtr internal_allocator_;
    const size_t threads_count_;
    std::atomic_size_t solved_tasks_ {0};
    std::atomic_size_t sended_tasks_ {0};
    os::memory::Mutex cond_var_lock_;
    os::memory::ConditionVariable cond_var_;
};

}  // namespace panda::mem

#endif  // PANDA_RUNTIME_MEM_GC_GC_WORKERS_THREAD_POOL_H
