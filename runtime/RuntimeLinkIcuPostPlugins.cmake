# Copyright (c) 2021-2022 Huawei Device Co., Ltd.
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

if (NOT PANDA_LINK_ICU)
    return()
endif()

set(GEN_INCLUDE_DIR "${PANDA_BINARY_ROOT}/runtime/include")

# Add icu dependency and initialization if ets or ecmascript is needed
add_custom_command(OUTPUT ${GEN_INCLUDE_DIR}/init_icu_gen.cpp
    COMMAND ${PANDA_ROOT}/runtime/templates/substitute_icu_path.rb
            ${PANDA_ROOT}/runtime/templates/init_icu_gen.cpp.erb
            ${GEN_INCLUDE_DIR}/init_icu_gen.cpp ${PANDA_THIRD_PARTY_SOURCES_DIR}
    DEPENDS ${PANDA_ROOT}/runtime/templates/init_icu_gen.cpp.erb)

add_custom_target(init_icu_gen_cpp
    DEPENDS ${GEN_INCLUDE_DIR}/init_icu_gen.cpp)

add_dependencies(arkruntime_static init_icu_gen_cpp)

# Runtime uses unicode and i18n.
set(ICU_INCLUDE_DIR
    ${PANDA_THIRD_PARTY_SOURCES_DIR}/icu/icu4c/source/common
    ${PANDA_THIRD_PARTY_SOURCES_DIR}/icu/icu4c/source/i18n
    ${PANDA_THIRD_PARTY_SOURCES_DIR}/icu/icu4c/source
    ${PANDA_THIRD_PARTY_SOURCES_DIR}/icu
)

set(ICU_LIB_TYPE SHARED)
if (PANDA_TARGET_MOBILE OR PANDA_TARGET_OHOS)
    set(ICU_LIB_TYPE STATIC)
endif()

add_library(init_icu ${ICU_LIB_TYPE} ${PANDA_ROOT}/runtime/init_icu.cpp)
target_include_directories(init_icu
    PUBLIC ${GEN_INCLUDE_DIR}
    PUBLIC ${PANDA_ROOT}/runtime/
)

target_include_directories(init_icu SYSTEM PUBLIC ${ICU_INCLUDE_DIR})

add_dependencies(init_icu init_icu_gen_cpp)

if (NOT TARGET hmicuuc.z OR NOT TARGET hmicui18n.z)
    set(ICU_ROOT ${PANDA_THIRD_PARTY_SOURCES_DIR}/icu)
    set(ORIG_CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS}")
    set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -Wno-implicit-const-int-float-conversion -Wno-unknown-warning-option")
    add_subdirectory(${PANDA_THIRD_PARTY_CONFIG_DIR}/icu "${CMAKE_CURRENT_BINARY_DIR}/third_party/icu")
    set(CMAKE_CXX_FLAGS "${ORIG_CMAKE_CXX_FLAGS}")
endif()

target_include_directories(arkruntime_interpreter_impl
    SYSTEM PRIVATE ${ICU_INCLUDE_DIR}
)

target_include_directories(csa_tests_arkruntime_interpreter_impl
    SYSTEM PRIVATE ${ICU_INCLUDE_DIR}
)

if (TARGET arkruntime_test_interpreter_impl)
    target_include_directories(arkruntime_test_interpreter_impl
        SYSTEM PRIVATE ${ICU_INCLUDE_DIR}
    )
endif()

target_include_directories(asm_defines
    SYSTEM PRIVATE ${ICU_INCLUDE_DIR}
)

# Allow to link targets which is not built in this directory againt hmicuuc.z hmicui18n.z
if(POLICY CMP0079)
    cmake_policy(SET CMP0079 NEW)
endif()

target_link_libraries(arkruntime_static init_icu hmicuuc.z hmicui18n.z)

if (TARGET arkruntime_for_relayout_static)
    target_link_libraries(arkruntime_for_relayout_static init_icu hmicuuc.z hmicui18n.z)
endif()
